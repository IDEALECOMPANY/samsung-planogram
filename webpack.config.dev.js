import webpack from "webpack";
import path from "path";

const GLOBAL = {
	"process.env.NODE_ENV": JSON.stringify("development")
};

export default {
	devtool: "cheap-module-source-map",
	entry: {
		bundle: [
			"eventsource-polyfill",
			"webpack-hot-middleware/client?reload=true",
			"./src/index.js"
		],
		vendor: [
			"axios",
			"history",
			"react",
			"react-dom",
			"react-redux",
			"react-router",
			"react-router-dom",
			"react-router-redux",
			"redux",
			"redux-thunk",
			"styled-components"
		]
	},
	target: "web",
	output: {
		path: __dirname + "/dist",
		publicPath: "/",
		filename: "[name].js"
	},
	devServer: {
		contentBase: "./src",
		noInfo: false
	},
	plugins: [
		new webpack.DefinePlugin(GLOBAL),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.LoaderOptionsPlugin({
			debug: true
		})
	],
	resolve: {
		modules: ["node_modules", "src"],
		extensions: ["*", ".js", ".json"],
		alias: {
			'@components': path.join(__dirname, '../src/components'),
			'@actions': path.join(__dirname, '../src/actions'),
			'@api': path.join(__dirname, '../src/api')
		}
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				include: path.join(__dirname, "src"),
				loaders: ["babel-loader"]
			},
			{ test: /\.css$/, loaders: ["style-loader", "css-loader"] },
			{ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader" },
			{
				test: /\.(woff|woff2)$/,
				loader: "url-loader?prefix=font/&limit=5000"
			},
			{
				test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				loader:
					"url-loader?limit=10000&mimetype=application/octet-stream"
			},
			{
				test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=image/svg+xml"
			}
		]
	}
};
