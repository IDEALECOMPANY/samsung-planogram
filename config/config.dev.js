export default {
	port: 3000,
	database: {
		name: "samsung_planogram_dev",
		username: "root",
		password: "root",
		host: "localhost",
		driver: "mysql"
	},
	auth: {
		secretKey: "com.ideale.samsung-planogram.dev",
		token: "access-token"
	},
	origin: "http://localhost:3000"
};
