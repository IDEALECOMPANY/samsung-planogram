export default {
	port: 3000,
	database: {
		name: "samsung_planogram_prod",
		username: "root",
		password: "ideale@db",
		host: "localhost",
		driver: "mysql"
	},
	auth: {
		secretKey: "com.ideale.samsung-planogram.prod",
		token: "access-token"
	},
	origin: "http://samsung-planogram.idealedev.com"
};
