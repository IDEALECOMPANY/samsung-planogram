import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.MediaTypes.findAll({
			attributes: [
				"id",
				"code",
				"title",
				"active",
				"createdAt",
				"updatedAt"
			]
		})
			.then(mediaTypes => {
				res.json(convertArrayToResponse(toPlain(mediaTypes)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.MediaTypes.create({
			code: req.body.code,
			title: req.body.title,
			active: boolToNumber(req.body.active)
		})
			.then(mediaType => {
				res.json(convertObjectToReponse(toPlain(mediaType)));
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.MediaTypes.update(
			{
				code: req.body.code,
				title: req.body.title,
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(() => {
				Models.MediaTypes.findOne({
					attributes: [
						"id",
						"code",
						"title",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: id
					}
				})
					.then(mediaType => {
						res.json(convertObjectToReponse(toPlain(mediaType)));
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.MediaTypes.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
