import express from "express";
import { Models } from "../../models";
import {
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.Users.findAll({
			attributes: [
				"id",
				"email",
				"username",
				"active",
				"createdAt",
				"updatedAt"
			],
			include: [
				{
					model: Models.UserBranches,
					attributes: ["id"],
					include: [
						{
							model: Models.Branches,
							attributes: [
								"id",
								"code",
								"title",
								"retailHub",
								"customer",
								"lat",
								"lng",
								"orientation",
								"active",
								"createdAt",
								"updatedAt"
							]
						}
					]
				},
				{
					model: Models.Roles,
					attributes: ["id", "title"]
				}
			]
		})
			.then(users => {
				res.json(convertArrayToResponse(toPlain(users)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.Users.create({
			username: req.body.username,
			email: req.body.email,
			password: req.body.password,
			active: boolToNumber(req.body.active),
			roleId: null
		})
			.then(user => {
				user = toPlain(user);
				Models.UserBranches.create({
					userId: user.id,
					branchId: req.body.branchId
				})
					.then(userBranch => {
						Models.Users.findOne({
							attributes: [
								"id",
								"email",
								"username",
								"active",
								"createdAt",
								"updatedAt"
							],
							include: [
								{
									model: Models.UserBranches,
									attributes: ["id"],
									include: [
										{
											model: Models.Branches,
											attributes: [
												"id",
												"code",
												"title",
												"retailHub",
												"customer",
												"lat",
												"lng",
												"orientation",
												"active",
												"createdAt",
												"updatedAt"
											]
										}
									]
								},
								{
									model: Models.Roles,
									attributes: ["id", "title"]
								}
							],
							where: {
								id: user.id
							}
						})
							.then(users => {
								res.json(
									convertObjectToReponse(toPlain(users))
								);
							})
							.catch(error => {
								res.sendStatus(404);
							});
					})
					.catch(error => {
						console.log("Error create UserBranches: ", error);
						res.sendStatus(500);
					});
			})
			.catch(error => {
				console.log("Error create User: ", error);
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.body.id;
		Models.Users.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.body.id;
		Models.Users.update(
			{
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				active: boolToNumber(req.body.active),
				roleId: null
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(user => {
				Models.UserBranches.create({
					userId: id,
					branchId: req.body.branchId
				})
					.then(userBranch => {
						Models.Users.findOne({
							attributes: [
								"id",
								"email",
								"username",
								"active",
								"createdAt",
								"updatedAt"
							],
							include: [
								{
									model: Models.UserBranches,
									attributes: ["id"],
									include: [
										{
											model: Models.Branches,
											attributes: [
												"id",
												"code",
												"title",
												"retailHub",
												"customer",
												"lat",
												"lng",
												"orientation",
												"active",
												"createdAt",
												"updatedAt"
											]
										}
									]
								},
								{
									model: Models.Roles,
									attributes: ["id", "title"]
								}
							],
							where: {
								id: id
							}
						})
							.then(users => {
								res.json(
									convertObjectToReponse(toPlain(users))
								);
							})
							.catch(error => {
								res.sendStatus(404);
							});
					})
					.catch(error => {
						console.log("Error update UserBranches: ", error);
						res.sendStatus(500);
					});
			})
			.catch(error => {
				console.log("Error update User: ", error);
				res.sendStatus(500);
			});
	});

	return router;
};
