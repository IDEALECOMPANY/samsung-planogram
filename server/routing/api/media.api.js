import express from "express";
import { Models, Op } from "../../models";
import { saveImage } from "../../middlewares";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.Media.findAll({
			attributes: [
				"id",
				"title",
				"desc",
				"material",
				"image",
				"active",
				"createdAt",
				"updatedAt"
			],
			include: [
				{
					model: Models.MediaMediaType,
					attributes: ["id"],
					include: [
						{
							model: Models.MediaTypes,
							attributes: ["id", "code", "title"]
						}
					]
				},
				{
					model: Models.MediaMediaSize,
					attributes: ["id", "qty"],
					include: [
						{
							model: Models.MediaSizes,
							attributes: ["id", "title"]
						}
					]
				},
				{
					model: Models.MediaTableType,
					attributes: ["id"],
					include: [
						{
							model: Models.TableTypes,
							attributes: ["id", "code", "title"]
						}
					]
				}
			]
		})
			.then(media => {
				let newMedia = convertArrayToResponse(toPlain(media));
				console.log("then: ", newMedia);
				res.json(
					newMedia.map(j => ({
						id: j.id,
						title: j.title,
						desc: j.desc,
						material: j.material,
						image: getImageUploadURL(j.image),
						active: j.active,
						createdAt: j.createdAt,
						updatedAt: j.updatedAt,
						mediaTypes: j.mediaMediaTypes.map(i => ({
							...i.mediaType
						})),
						mediaSizes: j.mediaMediaSizes.map(i => ({
							...i.mediaSize,
							qty: i.qty
						})),
						tableTypes: j.mediaTableTypes.map(i => ({
							...i.tableType
						}))
					}))
				);
			})
			.catch(error => {
				console.log("Error: GET /media", error);
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		let image = "";
		if (req.body.image !== "") {
			console.log("if image");
			image = saveImage(req.body.image);
			console.log("after save image", image);
		} else {
			console.log("else image");
		}

		Models.Media.create({
			title: req.body.title,
			desc: req.body.desc,
			material: req.body.material,
			image: image,
			active: boolToNumber(req.body.active)
		})
			.then(media => {
				media = toPlain(media);
				let queries = [
					new Promise((resolve, reject) => {
						let mediaTypes = req.body.mediaTypes.map(i => ({
							mediumId: media.id,
							mediaTypeId: i
						}));
						console.log("mediaTypes", mediaTypes);
						Models.MediaMediaType.bulkCreate(mediaTypes)
							.then(() => {
								resolve();
							})
							.catch(error => {
								console.log("Error MediaMediaType:", error);
								resolve();
							});
					}),
					new Promise((resolve, reject) => {
						let mediaSizes = req.body.mediaSizes.map(i => ({
							mediumId: media.id,
							qty: i.qty,
							mediaSizeId: i.id
						}));
						console.log("mediaSizes", mediaSizes);
						Models.MediaMediaSize.bulkCreate(mediaSizes)
							.then(() => {
								resolve();
							})
							.catch(error => {
								console.log("Error MediaMediaType:", error);
								resolve();
							});
					}),
					new Promise((resolve, reject) => {
						let tableTypes = req.body.tableTypes.map(i => ({
							mediumId: media.id,
							tableTypeId: i
						}));
						Models.MediaTableType.bulkCreate(tableTypes)
							.then(() => {
								resolve();
							})
							.catch(error => {
								console.log("Error MediaTableType:", error);
								resolve();
							});
					})
				];

				Promise.all(queries)
					.then(test => {
						console.log("in promise", test);
						Models.Media.findOne({
							attributes: [
								"id",
								"title",
								"desc",
								"material",
								"image",
								"active",
								"createdAt",
								"updatedAt"
							],
							where: {
								id: media.id
							},
							include: [
								{
									model: Models.MediaMediaType,
									attributes: ["id"],
									include: [
										{
											model: Models.MediaTypes,
											attributes: ["id", "code", "title"]
										}
									]
								},
								{
									model: Models.MediaMediaSize,
									attributes: ["id", "qty"],
									include: [
										{
											model: Models.MediaSizes,
											attributes: ["id", "title"]
										}
									]
								},
								{
									model: Models.MediaTableType,
									attributes: ["id"],
									include: [
										{
											model: Models.TableTypes,
											attributes: ["id", "code", "title"]
										}
									]
								}
							]
						})
							.then(media => {
								let newMedia = convertObjectToReponse(
									toPlain(media)
								);
								res.json({
									id: newMedia.id,
									title: newMedia.title,
									desc: newMedia.desc,
									material: newMedia.material,
									image: getImageUploadURL(newMedia.image),
									active: newMedia.active,
									createdAt: newMedia.createdAt,
									updatedAt: newMedia.updatedAt,
									mediaTypes: newMedia.mediaMediaTypes.map(
										i => ({ ...i.mediaType })
									),
									mediaSizes: newMedia.mediaMediaSizes.map(
										i => ({
											...i.mediaSize,
											qty: i.qty
										})
									),
									tableTypes: newMedia.mediaTableTypes.map(
										i => ({ ...i.tableType })
									)
								});
							})
							.catch(error => {
								res.sendStatus(404);
							});
					})
					.catch(error => {
						console.log("FUCK", error);
					});
			})
			.catch(error => {
				console.log("media.create catch", error);
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		let image = "";
		let value = {
			title: req.body.title,
			desc: req.body.desc,
			material: req.body.material,
			active: boolToNumber(req.body.active)
		};
		if (req.body.image !== "") {
			image = saveImage(req.body.image);
			value = { ...value, image: image };
		}

		Models.Media.update(value, {
			where: {
				id: id
			}
		})
			.then(() => {
				let deleteQueries = [
					new Promise((resolve, reject) => {
						Models.MediaMediaType.destroy({
							where: {
								mediumId: id
							}
						})
							.then(() => {
								resolve();
							})
							.catch(error => {
								console.log(
									"Error Delete MediaMediaType:",
									error
								);
								resolve();
							});
					}),
					new Promise((resolve, reject) => {
						Models.MediaMediaSize.destroy({
							where: {
								mediumId: id
							}
						})
							.then(() => {
								resolve();
							})
							.catch(error => {
								console.log(
									"Error Delete MediaMediaSize:",
									error
								);
								resolve();
							});
					}),
					new Promise((resolve, reject) => {
						Models.MediaTableType.destroy({
							where: {
								mediumId: id
							}
						})
							.then(() => {
								resolve();
							})
							.catch(error => {
								console.log(
									"Error Delete MediaTableType:",
									error
								);
								resolve();
							});
					})
				];

				Promise.all(deleteQueries).then(() => {
					let insertQueries = [
						new Promise((resolve, reject) => {
							let mediaTypes = req.body.mediaTypes.map(i => ({
								mediumId: id,
								mediaTypeId: i
							}));
							Models.MediaMediaType.bulkCreate(mediaTypes)
								.then(() => {
									resolve();
								})
								.catch(error => {
									console.log(
										"Error Insert MediaMediaType:",
										error
									);
									resolve();
								});
						}),
						new Promise((resolve, reject) => {
							let mediaSizes = req.body.mediaSizes.map(i => ({
								mediumId: id,
								qty: i.qty,
								mediaSizeId: i.id
							}));
							Models.MediaMediaSize.bulkCreate(mediaSizes)
								.then(() => {
									resolve();
								})
								.catch(error => {
									console.log(
										"Error Insert MediaMediaSize:",
										error
									);
									resolve();
								});
						}),
						new Promise((resolve, reject) => {
							let tableTypes = req.body.tableTypes.map(i => ({
								mediumId: id,
								tableTypeId: i
							}));
							Models.MediaTableType.bulkCreate(tableTypes)
								.then(() => {
									resolve();
								})
								.catch(error => {
									console.log(
										"Error Insert MediaTableType:",
										error
									);
									resolve();
								});
						})
					];

					Promise.all(insertQueries).then(() => {
						Models.Media.findOne({
							attributes: [
								"id",
								"title",
								"desc",
								"material",
								"image",
								"active",
								"createdAt",
								"updatedAt"
							],
							where: {
								id: id
							},
							include: [
								{
									model: Models.MediaMediaType,
									attributes: ["id"],
									include: [
										{
											model: Models.MediaTypes,
											attributes: ["id", "code", "title"]
										}
									]
								},
								{
									model: Models.MediaMediaSize,
									attributes: ["id", "qty"],
									include: [
										{
											model: Models.MediaSizes,
											attributes: ["id", "title"]
										}
									]
								},
								{
									model: Models.MediaTableType,
									attributes: ["id"],
									include: [
										{
											model: Models.TableTypes,
											attributes: ["id", "code", "title"]
										}
									]
								}
							]
						})
							.then(media => {
								let newMedia = convertObjectToReponse(
									toPlain(media)
								);
								res.json({
									id: newMedia.id,
									title: newMedia.title,
									desc: newMedia.desc,
									material: newMedia.material,
									image: getImageUploadURL(newMedia.image),
									active: newMedia.active,
									createdAt: newMedia.createdAt,
									updatedAt: newMedia.updatedAt,
									mediaTypes: newMedia.mediaMediaTypes.map(
										i => ({ ...i.mediaType })
									),
									mediaSizes: newMedia.mediaMediaSizes.map(
										i => ({
											...i.mediaSize,
											qty: i.qty
										})
									),
									tableTypes: newMedia.mediaTableTypes.map(
										i => ({ ...i.tableType })
									)
								});
							})
							.catch(error => {
								console.log("404", error);
								res.sendStatus(404);
							});
					});
				});
			})
			.catch(error => {
				console.log("500", error);
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.Media.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
