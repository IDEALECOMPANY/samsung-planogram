import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.TableSizes.findAll({
			attributes: [
				"id",
				"code",
				"title",
				"capacity",
				"active",
				"createdAt",
				"updatedAt"
			],
			include: [
				{
					model: Models.TableTypes,
					attributes: ["id", "code", "title"]
				}
			]
		})
			.then(tableSizes => {
				res.json(convertArrayToResponse(toPlain(tableSizes)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.TableSizes.create({
			code: req.body.code,
			title: req.body.title,
			capacity: req.body.capacity,
			active: boolToNumber(req.body.active),
			tableTypeId: req.body.tableTypeId
		})
			.then(tableSize => {
				tableSize = toPlain(tableSize);
				Models.TableSizes.findOne({
					attributes: [
						"id",
						"code",
						"title",
						"capacity",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: tableSize.id
					},
					include: [
						{
							model: Models.TableTypes,
							attributes: ["id", "code", "title"]
						}
					]
				})
					.then(tableSize => {
						res.json(convertObjectToReponse(toPlain(tableSize)));
					})
					.catch(error => {
						res.sendStatus(500);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;

		Models.TableSizes.update(
			{
				code: req.body.code,
				title: req.body.title,
				capacity: req.body.capacity,
				active: boolToNumber(req.body.active),
				tableTypeId: req.body.tableTypeId
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(tableSize => {
				Models.TableSizes.findOne({
					attributes: [
						"id",
						"code",
						"title",
						"capacity",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: id
					},
					include: [
						{
							model: Models.TableTypes,
							attributes: ["id", "code", "title"]
						}
					]
				})
					.then(tableSize => {
						res.json(convertObjectToReponse(toPlain(tableSize)));
					})
					.catch(error => {
						res.sendStatus(500);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;

		Models.TableSizes.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
