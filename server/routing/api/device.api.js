import express from "express";
import formidable from "formidable";
import { Models } from "../../models";
import { saveFile, readExcel } from "../../middlewares";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	getUniqueString,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.Devices.findAll({
			attributes: [
				"id",
				"code",
				"model",
				"price",
				"active",
				"createdAt",
				"updatedAt"
			],
			include: [
				{
					model: Models.DeviceColors,
					attributes: ["id", "title"]
				},
				{
					model: Models.DeviceCapacities,
					attributes: ["id", "title"]
				},
				{
					model: Models.DeviceModels,
					attributes: ["id", "title"]
				},
				{
					model: Models.DeviceStatuses,
					attributes: ["id", "title"]
				},
				{
					model: Models.DeviceTypesInShop,
					attributes: ["id", "title"]
				},
				{
					model: Models.DeviceCategories,
					attributes: ["id", "title"]
				}
			]
		})
			.then(devices => {
				res.json(convertArrayToResponse(toPlain(devices)));
			})
			.catch(error => {
				console.log("GET /device", error);
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.Devices.create({
			code: req.body.barcode,
			model: req.body.modelCode,
			price: req.body.price,
			active: boolToNumber(req.body.active),
			deviceStatusId: req.body.deviceStatusId,
			deviceTypeInShopId: req.body.deviceTypeInShopId,
			deviceCategoryId: req.body.deviceCategoryId
		})
			.then(device => {
				let id = toPlain(device).id;
				let queries = [];
				queries.push(
					Models.DeviceModels.findOrCreate({
						where: {
							title: req.body.deviceModel
						},
						defaults: {
							active: 1
						}
					})
				);

				queries.push(
					Models.DeviceColors.findOrCreate({
						where: {
							title: req.body.deviceColor
						}
					})
				);

				queries.push(
					Models.DeviceCapacities.findOrCreate({
						where: {
							title: req.body.deviceCapacity
						}
					})
				);

				Promise.all(queries).then(values => {
					Promise.all([
						device.setDeviceModel(values[0][0]),
						device.setDeviceColor(values[1][0]),
						device.setDeviceCapacity(values[2][0])
					])
						.then(data => {
							Models.Devices.findOne({
								attributes: [
									"id",
									"code",
									"model",
									"price",
									"active",
									"createdAt",
									"updatedAt"
								],
								where: {
									id: id
								},
								include: [
									{
										model: Models.DeviceColors,
										attributes: ["id", "title"]
									},
									{
										model: Models.DeviceCapacities,
										attributes: ["id", "title"]
									},
									{
										model: Models.DeviceModels,
										attributes: ["id", "title"]
									},
									{
										model: Models.DeviceCategories,
										attributes: ["id", "title"]
									},
									{
										model: Models.DeviceStatuses,
										attributes: ["id", "title"]
									},
									{
										model: Models.DeviceTypesInShop,
										attributes: ["id", "title"]
									}
								]
							})
								.then(deviceGetted => {
									res.json(
										convertObjectToReponse(
											toPlain(deviceGetted)
										)
									);
								})
								.catch(error => {
									console.log("POST /device", error);
									res.sendStatus(404);
								});
						})
						.catch(error => {
							res.sendStatus(500);
						});
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/upload", (req, res) => {
		let form = new formidable.IncomingForm();
		form.parse(req, (error, fields, files) => {
			var from = files.file.path;
			var to = `tmp-device-${getUniqueString()}.xlsx`;
			saveFile(from, to)
				.then(currentPath => {
					let excelData = readExcel({ name: currentPath });
					// excelData[0].data
					res.json(excelData);
				})
				.catch(error => {
					console.log("Error branch upload: ", error);
					res.sendStatus(400);
				});
		});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.Devices.findOne({
			attributes: ["id"]
		})
			.then(device => {
				Models.Devices.update(
					{
						code: req.body.barcode,
						model: req.body.modelCode,
						price: req.body.price,
						active: boolToNumber(req.body.active),
						deviceStatusId: req.body.deviceStatusId,
						deviceTypeInShopId: req.body.deviceTypeInShopId,
						deviceCategoryId: req.body.deviceCategoryId
					},
					{
						where: {
							id: id
						}
					}
				)
					.then(() => {
						let queries = [];
						queries.push(
							Models.DeviceModels.findOrCreate({
								where: {
									title: req.body.deviceModel
								},
								defaults: {
									active: 1
								}
							})
						);

						queries.push(
							Models.DeviceColors.findOrCreate({
								where: {
									title: req.body.deviceColor
								}
							})
						);

						queries.push(
							Models.DeviceCapacities.findOrCreate({
								where: {
									title: req.body.deviceCapacity
								}
							})
						);

						Promise.all(queries).then(values => {
							Promise.all([
								device.setDeviceModel(values[0][0]),
								device.setDeviceColor(values[1][0]),
								device.setDeviceCapacity(values[2][0])
							])
								.then(data => {
									Models.Devices.findOne({
										attributes: [
											"id",
											"code",
											"model",
											"price",
											"active",
											"createdAt",
											"updatedAt"
										],
										where: {
											id: id
										},
										include: [
											{
												model: Models.DeviceColors,
												attributes: ["id", "title"]
											},
											{
												model: Models.DeviceCapacities,
												attributes: ["id", "title"]
											},
											{
												model: Models.DeviceModels,
												attributes: ["id", "title"]
											},
											{
												model: Models.DeviceCategories,
												attributes: ["id", "title"]
											},
											{
												model: Models.DeviceStatuses,
												attributes: ["id", "title"]
											},
											{
												model: Models.DeviceTypesInShop,
												attributes: ["id", "title"]
											}
										]
									})
										.then(deviceGetted => {
											res.json(
												convertObjectToReponse(
													toPlain(deviceGetted)
												)
											);
										})
										.catch(error => {
											console.log("POST /device", error);
											res.sendStatus(404);
										});
								})
								.catch(error => {
									res.sendStatus(500);
								});
						});
					})
					.catch(error => {
						res.sendStatus(500);
					});
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.Devices.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(400);
			});
	});

	return router;
};
