import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.TableTypes.findAll({
			attributes: [
				"id",
				"code",
				"title",
				"active",
				"createdAt",
				"updatedAt"
			],
			include: [
				{
					model: Models.TableTypeMediaTypes,
					attributes: ["id", "qty"],
					include: [
						{
							model: Models.MediaTypes,
							attributes: ["id", "code", "title"]
						}
					]
				}
			],
			order: [
				["title", "ASC"],
				[Models.TableTypeMediaTypes, Models.MediaTypes, "title", "ASC"]
			]
		})
			.then(tableTypes => {
				res.json(
					convertArrayToResponse(toPlain(tableTypes)).map(i => {
						return {
							id: i.id,
							code: i.code,
							title: i.title,
							active: i.active,
							createdAt: i.createdAt,
							updatedAt: i.updatedAt,
							mediaTypes: i.tableTypeMediaTypes.map(j => ({
								...j.mediaType,
								qty: j.qty
							}))
						};
					})
				);
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.TableTypes.create({
			code: req.body.code,
			title: req.body.title,
			active: boolToNumber(req.body.active)
		})
			.then(tableType => {
				tableType = toPlain(tableType);
				let records = req.body.mediaTypes.map(i => ({
					tableTypeId: tableType.id,
					mediaTypeId: parseInt(i.id),
					qty: parseInt(i.qty)
				}));

				Models.TableTypeMediaTypes.bulkCreate(records)
					.then(tableTypeMediaTypes => {
						Models.TableTypes.findOne({
							attributes: [
								"id",
								"code",
								"title",
								"active",
								"createdAt",
								"updatedAt"
							],
							where: {
								id: tableType.id
							},
							include: [
								{
									model: Models.TableTypeMediaTypes,
									attributes: ["id", "qty"],
									include: [
										{
											model: Models.MediaTypes,
											attributes: ["id", "code", "title"]
										}
									]
								}
							],
							order: [
								["title", "ASC"],
								[
									Models.TableTypeMediaTypes,
									Models.MediaTypes,
									"title",
									"ASC"
								]
							]
						})
							.then(tableType => {
								let newTableType = convertObjectToReponse(
									toPlain(tableType)
								);
								res.json({
									id: newTableType.id,
									code: newTableType.code,
									title: newTableType.title,
									active: newTableType.active,
									createdAt: newTableType.createdAt,
									updatedAt: newTableType.updatedAt,
									mediaTypes: newTableType.tableTypeMediaTypes.map(
										j => ({ ...j.mediaType, qty: j.qty })
									)
								});
							})
							.catch(error => {
								res.sendStatus(404);
							});
					})
					.catch(error => {
						res.sendStatus(500);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.TableTypes.update(
			{
				code: req.body.code,
				title: req.body.title,
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(() => {
				Models.TableTypeMediaTypes.destroy({
					where: {
						tableTypeId: id
					}
				})
					.then(() => {
						let records = req.body.mediaTypes.map(i => ({
							tableTypeId: id,
							mediaTypeId: parseInt(i.id),
							qty: parseInt(i.qty)
						}));

						Models.TableTypeMediaTypes.bulkCreate(records)
							.then(tableTypeMediaTypes => {
								Models.TableTypes.findOne({
									attributes: [
										"id",
										"code",
										"title",
										"active",
										"createdAt",
										"updatedAt"
									],
									where: {
										id: id
									},
									include: [
										{
											model: Models.TableTypeMediaTypes,
											attributes: ["id", "qty"],
											include: [
												{
													model: Models.MediaTypes,
													attributes: [
														"id",
														"code",
														"title"
													]
												}
											]
										}
									],
									order: [
										["title", "ASC"],
										[
											Models.TableTypeMediaTypes,
											Models.MediaTypes,
											"title",
											"ASC"
										]
									]
								})
									.then(tableType => {
										let newTableType = convertObjectToReponse(
											toPlain(tableType)
										);
										res.json({
											id: newTableType.id,
											code: newTableType.code,
											title: newTableType.title,
											active: newTableType.active,
											createdAt: newTableType.createdAt,
											updatedAt: newTableType.updatedAt,
											mediaTypes: newTableType.tableTypeMediaTypes.map(
												j => ({
													...j.mediaType,
													qty: j.qty
												})
											)
										});
									})
									.catch(error => {
										res.sendStatus(404);
									});
							})
							.catch(error => {
								res.sendStatus(500);
							});
					})
					.catch(error => {
						res.sendStatus(500);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.TableTypes.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				Models.TableTypeMediaTypes.destroy({
					where: {
						tableTypeId: id
					}
				})
					.then(() => {
						console.log(
							"Success: Clear TableTypeMediaTypes for id:",
							id
						);
					})
					.catch(error => {
						console.log(
							"Error: Clear TableTypeMediaTypes for id:",
							id
						);
					});

				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
