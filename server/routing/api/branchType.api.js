import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.ShopTypes.findAll({
			attributes: ["id", "title", "active", "createdAt", "updatedAt"]
		})
			.then(shopTypes => {
				res.json(convertArrayToResponse(toPlain(shopTypes)));
			})
			.catch(error => {
				console.log("err", error);
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.ShopTypes.create({
			title: req.body.title,
			active: boolToNumber(req.body.active)
		})
			.then(shopType => {
				res.json(convertObjectToReponse(toPlain(shopType)));
			})
			.catch(error => {
				console.log("Error", error);
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.ShopTypes.update(
			{
				title: req.body.title,
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(() => {
				Models.ShopTypes.findOne({
					attributes: [
						"id",
						"title",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: id
					}
				})
					.then(shopType => {
						res.json(convertObjectToReponse(toPlain(shopType)));
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.ShopTypes.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
