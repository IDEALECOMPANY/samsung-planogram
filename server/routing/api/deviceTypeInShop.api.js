import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.DeviceTypesInShop.findAll({
			attributes: ["id", "title", "active", "createdAt", "updatedAt"]
		})
			.then(deviceTypesInShops => {
				res.json(convertArrayToResponse(toPlain(deviceTypesInShops)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.DeviceTypesInShop.create({
			title: req.body.title,
			active: boolToNumber(req.body.active)
		})
			.then(deviceTypesInShop => {
				res.json(convertObjectToReponse(toPlain(deviceTypesInShop)));
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.DeviceTypesInShop.update(
			{
				title: req.body.title,
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(() => {
				Models.DeviceTypesInShop.findOne({
					attributes: [
						"id",
						"title",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: id
					}
				})
					.then(deviceTypesInShop => {
						res.json(
							convertObjectToReponse(toPlain(deviceTypesInShop))
						);
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.DeviceTypesInShop.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
