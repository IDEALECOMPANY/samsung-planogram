import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.Regions.findAll({
			attributes: ["id", "titleTh", "titleEn"],
			include: [
				{
					model: Models.Provinces,
					attributes: ["id", "code", "titleTh", "titleEn"],
					include: [
						{
							model: Models.Amphures,
							attributes: ["id", "code", "titleTh", "titleEn"],
							include: [
								{
									model: Models.Districts,
									attributes: [
										"id",
										"zipCode",
										"titleTh",
										"titleEn"
									]
								}
							]
						}
					]
				}
			]
		})
			.then(address => {
				res.json(address);
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	return router;
};
