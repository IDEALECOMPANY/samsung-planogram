import express from "express";
import { Models } from "../../models";
import {
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse,
	getPlanogramCode
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.Planogram.findAll({
			attributes: ["id", "active", "createdAt", "updatedAt"],
			include: [
				{
					model: Models.PlanogramTableSets,
					attributes: ["id"],
					include: [
						{
							model: Models.ShopTypes,
							attributes: ["id", "title"]
						},
						{
							model: Models.TableTypes,
							attributes: ["id", "code", "title"]
						},
						{
							model: Models.TableSizes,
							attributes: ["id", "code", "title", "capacity"]
						},
						{
							model: Models.PlanogramTableSetTables,
							attributes: ["id"],
							include: [
								{
									model: Models.PlanogramTableSetTableDevices,
									attributes: [
										"id",
										"index",
										"label",
										"type"
									],
									include: [
										{
											model: Models.Devices,
											attributes: [
												"id",
												"code",
												"model",
												"price"
											],
											include: [
												{
													model: Models.DeviceColors,
													attributes: ["id", "title"]
												},
												{
													model:
														Models.DeviceCapacities,
													attributes: ["id", "title"]
												},
												{
													model: Models.DeviceModels,
													attributes: ["id", "title"]
												},
												{
													model:
														Models.DeviceStatuses,
													attributes: ["id", "title"]
												},
												{
													model:
														Models.DeviceTypesInShop,
													attributes: ["id", "title"]
												},
												{
													model:
														Models.DeviceCategories,
													attributes: ["id", "title"]
												}
											]
										},
										{
											model: Models.Media,
											attributes: [
												"id",
												"title",
												"desc",
												"image"
											],
											include: [
												{
													model:
														Models.MediaMediaType,
													attributes: ["id"],
													include: [
														{
															model:
																Models.MediaTypes,
															attributes: [
																"id",
																"code",
																"title"
															]
														}
													]
												}
											]
										}
									]
								},
								{
									model: Models.PlanogramTableSetTableMedia,
									attributes: ["id", "index"],
									include: [
										{
											model: Models.Media,
											attributes: [
												"id",
												"title",
												"desc",
												"image"
											],
											include: [
												{
													model:
														Models.MediaMediaType,
													attributes: ["id"],
													include: [
														{
															model:
																Models.MediaTypes,
															attributes: [
																"id",
																"code",
																"title"
															]
														}
													]
												}
											]
										}
									]
								}
							]
						}
					]
				}
			],
			order: [
				[
					Models.PlanogramTableSets,
					Models.PlanogramTableSetTables,
					Models.PlanogramTableSetTableDevices,
					"index",
					"ASC"
				]
			]
		})
			.then(planograms => {
				res.json(
					convertArrayToResponse(toPlain(planograms)).map(i => ({
						...i,
						code: getPlanogramCode(i)
					}))
				);
			})
			.catch(error => {
				console.log("Error get planogram set", error);
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.Planogram.create({
			active: boolToNumber(req.body.active)
		})
			.then(planogram => {
				let planogramPlain = toPlain(planogram);
				let planogramSetTableQueries = req.body.tableSets.map(i => {
					return new Promise((resolveRoot, rejectRoot) => {
						Models.PlanogramTableSets.create({
							tableTypeId: i.tableTypeId,
							tableSizeId: i.tableSizeId,
							shopTypeId: i.branchTypeId,
							planogramId: planogramPlain.id
						})
							.then(planogramTableSet => {
								let planogramTableSetPlain = toPlain(
									planogramTableSet
								);

								let tablesDataQueries = i.tables.map(j => {
									return new Promise(
										(resolveTable, rejectTable) => {
											Models.PlanogramTableSetTables.create(
												{
													planogramTableSetId:
														planogramTableSetPlain.id
												}
											)
												.then(
													planogramTableSetTables => {
														let planogramTableSetTablesPlain = toPlain(
															planogramTableSetTables
														);
														let tableDeviceValues = j.devices.map(
															k => {
																let columnName =
																	k.type ===
																	"device"
																		? "deviceId"
																		: "mediumId";
																return {
																	index:
																		k.index,
																	label:
																		k.label,
																	type:
																		k.type,
																	[columnName]:
																		k.id,
																	planogramTableSetTableId:
																		planogramTableSetTablesPlain.id
																};
															}
														);

														let tableMediaValues = j.media.map(
															k => {
																return {
																	index:
																		k.index,
																	mediumId:
																		k.id,
																	planogramTableSetTableId:
																		planogramTableSetTablesPlain.id
																};
															}
														);

														let tableSetDataQueries = [
															new Promise(
																(
																	resolve,
																	reject
																) => {
																	Models.PlanogramTableSetTableDevices.bulkCreate(
																		tableDeviceValues
																	)
																		.then(
																			() => {
																				resolve();
																			}
																		)
																		.catch(
																			error => {
																				resolve();
																				console.log(
																					"Error bulk create planogram set table device",
																					error
																				);
																			}
																		);
																}
															),
															new Promise(
																(
																	resolve,
																	reject
																) => {
																	Models.PlanogramTableSetTableMedia.bulkCreate(
																		tableMediaValues
																	)
																		.then(
																			() => {
																				resolve();
																			}
																		)
																		.catch(
																			error => {
																				resolve();
																				console.log(
																					"Error bulk create planogram set table media",
																					error
																				);
																			}
																		);
																}
															)
														];

														Promise.all(
															tableSetDataQueries
														)
															.then(() => {
																resolveTable();
															})
															.catch(error => {
																console.log(
																	"Error create planogram set when all device and media",
																	error
																);
																resolveTable();
															});
													}
												)
												.catch(error => {
													console.log(
														"Error create planogram table set table",
														error
													);
													resolveTable();
												});
										}
									);
								});

								Promise.all(tablesDataQueries)
									.then(() => {
										resolveRoot();
									})
									.catch(error => {
										console.log(
											"Error create planogram table set table",
											error
										);
										resolveRoot();
									});
							})
							.catch(error => {
								console.log(
									"Error create planogram set table",
									error
								);
								resolveRoot();
							});
					});
				});

				Promise.all(planogramSetTableQueries)
					.then(() => {
						Models.Planogram.findOne({
							attributes: [
								"id",
								"active",
								"createdAt",
								"updatedAt"
							],
							where: {
								id: planogramPlain.id
							},
							include: [
								{
									model: Models.PlanogramTableSets,
									attributes: ["id"],
									include: [
										{
											model: Models.ShopTypes,
											attributes: ["id", "title"]
										},
										{
											model: Models.TableTypes,
											attributes: ["id", "code", "title"]
										},
										{
											model: Models.TableSizes,
											attributes: [
												"id",
												"code",
												"title",
												"capacity"
											]
										},
										{
											model:
												Models.PlanogramTableSetTables,
											attributes: ["id"],
											include: [
												{
													model:
														Models.PlanogramTableSetTableDevices,
													attributes: [
														"id",
														"index",
														"label",
														"type"
													],
													include: [
														{
															model:
																Models.Devices,
															attributes: [
																"id",
																"code",
																"model",
																"price"
															],
															include: [
																{
																	model:
																		Models.DeviceColors,
																	attributes: [
																		"id",
																		"title"
																	]
																},
																{
																	model:
																		Models.DeviceCapacities,
																	attributes: [
																		"id",
																		"title"
																	]
																},
																{
																	model:
																		Models.DeviceModels,
																	attributes: [
																		"id",
																		"title"
																	]
																},
																{
																	model:
																		Models.DeviceStatuses,
																	attributes: [
																		"id",
																		"title"
																	]
																},
																{
																	model:
																		Models.DeviceTypesInShop,
																	attributes: [
																		"id",
																		"title"
																	]
																},
																{
																	model:
																		Models.DeviceCategories,
																	attributes: [
																		"id",
																		"title"
																	]
																}
															]
														},
														{
															model: Models.Media,
															attributes: [
																"id",
																"title",
																"desc",
																"image"
															],
															include: [
																{
																	model:
																		Models.MediaMediaType,
																	attributes: [
																		"id"
																	],
																	include: [
																		{
																			model:
																				Models.MediaTypes,
																			attributes: [
																				"id",
																				"code",
																				"title"
																			]
																		}
																	]
																}
															]
														}
													]
												},
												{
													model:
														Models.PlanogramTableSetTableMedia,
													attributes: ["id", "index"],
													include: [
														{
															model: Models.Media,
															attributes: [
																"id",
																"title",
																"desc",
																"image"
															],
															include: [
																{
																	model:
																		Models.MediaMediaType,
																	attributes: [
																		"id"
																	],
																	include: [
																		{
																			model:
																				Models.MediaTypes,
																			attributes: [
																				"id",
																				"code",
																				"title"
																			]
																		}
																	]
																}
															]
														}
													]
												}
											]
										}
									]
								}
							],
							order: [
								[
									Models.PlanogramTableSets,
									Models.PlanogramTableSetTables,
									Models.PlanogramTableSetTableDevices,
									"index",
									"ASC"
								]
							]
						})
							.then(planogramCreated => {
								let planogramCreatedConverted = convertObjectToReponse(
									toPlain(planogramCreated)
								);
								res.json({
									...planogramCreatedConverted,
									code: getPlanogramCode(
										planogramCreatedConverted
									)
								});
							})
							.catch(error => {
								console.log(
									"Error get planogram set after create",
									error
								);
								res.sendStatus(404);
							});
					})
					.catch(error => {
						console.log(
							"Error create multiple planogram set table",
							error
						);
					});
			})
			.catch(error => {
				console.log("Error create planogram set", error);
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.Planogram.update(
			{
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(() => {
				Promise.all([
					new Promise((resolveDestroy, rejectDestroy) => {
						Models.PlanogramTableSets.destroy({
							where: {
								planogramId: id
							}
						})
							.then(() => {
								resolveDestroy();
							})
							.catch(error => {
								console.log(
									"Error destroy PlanogramTableSets: ",
									error
								);
								resolveDestroy();
							});
					})
				]).then(() => {
					let planogramSetTableQueries = req.body.tableSets.map(i => {
						return new Promise((resolveRoot, rejectRoot) => {
							Models.PlanogramTableSets.create({
								tableTypeId: i.tableTypeId,
								tableSizeId: i.tableSizeId,
								shopTypeId: i.branchTypeId,
								planogramId: id
							})
								.then(planogramTableSet => {
									let planogramTableSetPlain = toPlain(
										planogramTableSet
									);

									let tablesDataQueries = i.tables.map(j => {
										return new Promise(
											(resolveTable, rejectTable) => {
												Models.PlanogramTableSetTables.create(
													{
														planogramTableSetId:
															planogramTableSetPlain.id
													}
												)
													.then(
														planogramTableSetTables => {
															let planogramTableSetTablesPlain = toPlain(
																planogramTableSetTables
															);
															let tableDeviceValues = j.devices.map(
																k => {
																	let columnName =
																		k.type ===
																		"device"
																			? "deviceId"
																			: "mediumId";
																	return {
																		index:
																			k.index,
																		label:
																			k.label,
																		type:
																			k.type,
																		[columnName]:
																			k.id,
																		planogramTableSetTableId:
																			planogramTableSetTablesPlain.id
																	};
																}
															);

															let tableMediaValues = j.media.map(
																k => {
																	return {
																		index:
																			k.index,
																		mediumId:
																			k.id,
																		planogramTableSetTableId:
																			planogramTableSetTablesPlain.id
																	};
																}
															);

															let tableSetDataQueries = [
																new Promise(
																	(
																		resolve,
																		reject
																	) => {
																		Models.PlanogramTableSetTableDevices.bulkCreate(
																			tableDeviceValues
																		)
																			.then(
																				() => {
																					resolve();
																				}
																			)
																			.catch(
																				error => {
																					resolve();
																					console.log(
																						"Error bulk create planogram set table device",
																						error
																					);
																				}
																			);
																	}
																),
																new Promise(
																	(
																		resolve,
																		reject
																	) => {
																		Models.PlanogramTableSetTableMedia.bulkCreate(
																			tableMediaValues
																		)
																			.then(
																				() => {
																					resolve();
																				}
																			)
																			.catch(
																				error => {
																					resolve();
																					console.log(
																						"Error bulk create planogram set table media",
																						error
																					);
																				}
																			);
																	}
																)
															];

															Promise.all(
																tableSetDataQueries
															)
																.then(() => {
																	resolveTable();
																})
																.catch(
																	error => {
																		console.log(
																			"Error create planogram set when all device and media",
																			error
																		);
																		resolveTable();
																	}
																);
														}
													)
													.catch(error => {
														console.log(
															"Error create planogram table set table",
															error
														);
														resolveTable();
													});
											}
										);
									});

									Promise.all(tablesDataQueries)
										.then(() => {
											resolveRoot();
										})
										.catch(error => {
											console.log(
												"Error create planogram table set table",
												error
											);
											resolveRoot();
										});
								})
								.catch(error => {
									console.log(
										"Error create planogram set table",
										error
									);
									resolveRoot();
								});
						});
					});

					Promise.all(planogramSetTableQueries)
						.then(() => {
							Models.Planogram.findOne({
								attributes: [
									"id",
									"active",
									"createdAt",
									"updatedAt"
								],
								where: {
									id: id
								},
								include: [
									{
										model: Models.PlanogramTableSets,
										attributes: ["id"],
										include: [
											{
												model: Models.ShopTypes,
												attributes: ["id", "title"]
											},
											{
												model: Models.TableTypes,
												attributes: [
													"id",
													"code",
													"title"
												]
											},
											{
												model: Models.TableSizes,
												attributes: [
													"id",
													"code",
													"title",
													"capacity"
												]
											},
											{
												model:
													Models.PlanogramTableSetTables,
												attributes: ["id"],
												include: [
													{
														model:
															Models.PlanogramTableSetTableDevices,
														attributes: [
															"id",
															"index",
															"label",
															"type"
														],
														include: [
															{
																model:
																	Models.Devices,
																attributes: [
																	"id",
																	"code",
																	"model",
																	"price"
																],
																include: [
																	{
																		model:
																			Models.DeviceColors,
																		attributes: [
																			"id",
																			"title"
																		]
																	},
																	{
																		model:
																			Models.DeviceCapacities,
																		attributes: [
																			"id",
																			"title"
																		]
																	},
																	{
																		model:
																			Models.DeviceModels,
																		attributes: [
																			"id",
																			"title"
																		]
																	},
																	{
																		model:
																			Models.DeviceStatuses,
																		attributes: [
																			"id",
																			"title"
																		]
																	},
																	{
																		model:
																			Models.DeviceTypesInShop,
																		attributes: [
																			"id",
																			"title"
																		]
																	},
																	{
																		model:
																			Models.DeviceCategories,
																		attributes: [
																			"id",
																			"title"
																		]
																	}
																]
															},
															{
																model:
																	Models.Media,
																attributes: [
																	"id",
																	"title",
																	"desc",
																	"image"
																],
																include: [
																	{
																		model:
																			Models.MediaMediaType,
																		attributes: [
																			"id"
																		],
																		include: [
																			{
																				model:
																					Models.MediaTypes,
																				attributes: [
																					"id",
																					"code",
																					"title"
																				]
																			}
																		]
																	}
																]
															}
														]
													},
													{
														model:
															Models.PlanogramTableSetTableMedia,
														attributes: [
															"id",
															"index"
														],
														include: [
															{
																model:
																	Models.Media,
																attributes: [
																	"id",
																	"title",
																	"desc",
																	"image"
																],
																include: [
																	{
																		model:
																			Models.MediaMediaType,
																		attributes: [
																			"id"
																		],
																		include: [
																			{
																				model:
																					Models.MediaTypes,
																				attributes: [
																					"id",
																					"code",
																					"title"
																				]
																			}
																		]
																	}
																]
															}
														]
													}
												]
											}
										]
									}
								],
								order: [
									[
										Models.PlanogramTableSets,
										Models.PlanogramTableSetTables,
										Models.PlanogramTableSetTableDevices,
										"index",
										"ASC"
									]
								]
							})
								.then(planogramCreated => {
									let planogramCreatedConverted = convertObjectToReponse(
										toPlain(planogramCreated)
									);
									res.json({
										...planogramCreatedConverted,
										code: getPlanogramCode(
											planogramCreatedConverted
										)
									});
								})
								.catch(error => {
									console.log(
										"Error get planogram set after create",
										error
									);
									res.sendStatus(404);
								});
						})
						.catch(error => {
							console.log(
								"Error create multiple planogram set table",
								error
							);
						});
				});
			})
			.catch(error => {
				console.log("Error create planogram set", error);
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.Planogram.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
