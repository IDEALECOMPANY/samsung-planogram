import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.Roles.findAll({
			attributes: ["id", "title", "active", "createdAt", "updatedAt"]
		})
			.then(roles => {
				res.json(convertArrayToResponse(toPlain(roles)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.Roles.create({
			title: req.body.title,
			active: boolToNumber(req.body.active)
		})
			.then(role => {
				res.json(convertObjectToReponse(toPlain(role)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.Roles.update(
			{
				title: req.body.title,
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(role => {
				Models.Roles.findOne({
					attributes: [
						"id",
						"title",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: id
					}
				})
					.then(role => {
						res.json(convertArrayToResponse(toPlain(role)));
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.Roles.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
