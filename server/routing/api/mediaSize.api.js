import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.MediaSizes.findAll({
			attributes: ["id", "title", "active", "createdAt", "updatedAt"]
		})
			.then(mediaSizes => {
				res.json(convertArrayToResponse(toPlain(mediaSizes)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.MediaSizes.create({
			title: req.body.title,
			active: boolToNumber(req.body.active)
		})
			.then(mediaSize => {
				res.json(convertObjectToReponse(toPlain(mediaSize)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.MediaSizes.update(
			{
				title: req.body.title,
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(mediaSize => {
				Models.MediaSizes.findOne({
					attributes: [
						"id",
						"title",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: id
					}
				})
					.then(mediaSize => {
						res.json(convertObjectToReponse(toPlain(mediaSize)));
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.MediaSizes.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
