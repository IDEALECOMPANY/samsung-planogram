import express from "express";
import { Models, sequelize } from "../../models";
import { generateToken, verifyToken } from "../../middlewares";

import Address from "./address.api";
import Branch from "./branch.api";
import BranchType from "./branchType.api";
import Device from "./device.api";
import DeviceCapacity from "./deviceCapacity.api";
import DeviceCategory from "./deviceCategory.api";
import DeviceColor from "./deviceColor.api";
import DeviceModel from "./deviceModel.api";
import DeviceStatus from "./deviceStatus.api";
import DeviceTypeInShop from "./deviceTypeInShop.api";
import Media from "./media.api";
import MediaType from "./mediaType.api";
import TableSize from "./tableSize.api";
import TableType from "./tableType.api";
import Planogram from "./planogram.api";
import MediaSize from "./mediaSize.api";
import User from "./user.api";
import Role from "./role.api";
import Accessory from "./accessory.api";

export default () => {
	const router = express.Router();

	router.post("/login", (req, res) => {
		const user = {
			userId: 1,
			email: "admin@samsung-planogram"
		};

		generateToken({
			user
		})
			.then(token => {
				res.json({
					token,
					user
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.get("/user", verifyToken, (req, res) => {
		res.json(res.locals.authInfo.user);
	});
	router.use("/users", verifyToken, User());

	router.use("/role", verifyToken, Role());

	router.use("/address", verifyToken, Address());
	router.use("/branch/type", verifyToken, BranchType());
	router.use("/branch", verifyToken, Branch());

	router.use("/device/color", verifyToken, DeviceColor());
	router.use("/device/capacity", verifyToken, DeviceCapacity());
	router.use("/device/model", verifyToken, DeviceModel());
	router.use("/device/status", verifyToken, DeviceStatus());
	router.use("/device/type", verifyToken, DeviceTypeInShop());
	router.use("/device/category", verifyToken, DeviceCategory());
	router.use("/device", verifyToken, Device());

	router.use("/media/size", verifyToken, MediaSize());
	router.use("/media/type", verifyToken, MediaType());
	router.use("/media", verifyToken, Media());

	router.use("/table/size", verifyToken, TableSize());
	router.use("/table/type", verifyToken, TableType());

	router.use("/planogram", verifyToken, Planogram());

	router.use("/accessory", verifyToken, Accessory());

	return router;
};
