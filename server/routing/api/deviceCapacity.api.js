import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.DeviceCapacities.findAll({
			attributes: ["id", "title", "createdAt", "updatedAt"]
		})
			.then(deviceCapacities => {
				res.json(deviceCapacities);
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.DeviceCapacities.create({
			title: req.body.title
		})
			.then(deviceCapacity => {
				res.json(deviceCapacity);
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
