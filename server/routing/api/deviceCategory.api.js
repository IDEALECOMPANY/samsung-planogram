import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.DeviceCategories.findAll({
			attributes: ["id", "title", "active", "createdAt", "updatedAt"]
		})
			.then(deviceCategories => {
				res.json(convertArrayToResponse(toPlain(deviceCategories)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.DeviceCategories.create({
			title: req.body.title,
			active: boolToNumber(req.body.active)
		})
			.then(deviceCategory => {
				res.json(convertObjectToReponse(toPlain(deviceCategory)));
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.DeviceCategories.update(
			{
				title: req.body.title,
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(() => {
				Models.DeviceCategories.findOne({
					attributes: [
						"id",
						"title",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: id
					}
				})
					.then(deviceCategory => {
						res.json(
							convertObjectToReponse(toPlain(deviceCategory))
						);
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.DeviceCategories.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
