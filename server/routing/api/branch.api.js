import express from "express";
import formidable from "formidable";
import { Models, Op } from "../../models";
import { saveImage, saveFile, readExcel } from "../../middlewares";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	stringToBool,
	getUniqueString,
	convertArrayToResponse,
	convertObjectToReponse,
	getPlanogramCode,
	upsert
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.Branches.findAll({
			attributes: [
				"id",
				"code",
				"title",
				"retailHub",
				"customer",
				"lat",
				"lng",
				"orientation",
				"active",
				"createdAt",
				"updatedAt",
				"accessoryId"
			],
			include: [
				{
					model: Models.ShopTypes,
					attributes: ["id", "title"]
				},
				{
					model: Models.FloorPlans,
					attributes: ["id", "title"]
				},
				{
					model: Models.Districts,
					attributes: ["id", "zipCode", "titleTh", "titleEn"],
					include: [
						{
							model: Models.Amphures,
							attributes: ["id", "code", "titleTh", "titleEn"],
							include: [
								{
									model: Models.Provinces,
									attributes: [
										"id",
										"code",
										"titleTh",
										"titleEn"
									],
									include: [
										{
											model: Models.Regions,
											attributes: [
												"id",
												"titleTh",
												"titleEn"
											]
										}
									]
								}
							]
						}
					]
				},
				{
					model: Models.Planogram,
					attributes: ["id", "active", "createdAt", "updatedAt"],
					include: [
						{
							model: Models.PlanogramTableSets,
							attributes: ["id"],
							include: [
								{
									model: Models.ShopTypes,
									attributes: ["id", "title"]
								},
								{
									model: Models.TableTypes,
									attributes: ["id", "code", "title"]
								},
								{
									model: Models.TableSizes,
									attributes: [
										"id",
										"code",
										"title",
										"capacity"
									]
								},
								{
									model: Models.PlanogramTableSetTables,
									attributes: ["id"]
								}
							]
						}
					]
				},
				{
					model: Models.BranchMedia,
					attributes: ["id", "mediumId", "mediaTypeId"]
				}
			]
		})
			.then(branches => {
				res.json(
					convertArrayToResponse(
						toPlain(branches).map(i => {
							return {
								...i,
								floorPlans: i.floorPlans.map(j => ({
									...j,
									image: getImageUploadURL(j.title)
								})),
								planogram: i.planogram
									? {
											...i.planogram,
											code: getPlanogramCode(i.planogram)
									  }
									: null
							};
						})
					)
				);
			})
			.catch(error => {
				console.log("err", error);
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.Branches.create({
			code: req.body.code,
			title: req.body.title,
			retailHub: req.body.retailHub,
			customer: req.body.customer,
			lat: req.body.lat,
			lng: req.body.lng,
			orientation: req.body.orientation,
			active: boolToNumber(req.body.active),
			shopTypeId: req.body.shopTypeId,
			districtId: req.body.districtId
		})
			.then(branch => {
				let id = toPlain(branch).id;
				Models.Branches.findOne({
					attributes: [
						"id",
						"code",
						"title",
						"retailHub",
						"customer",
						"lat",
						"lng",
						"orientation",
						"active",
						"createdAt",
						"updatedAt",
						"accessoryId"
					],
					where: {
						id: id
					},
					include: [
						{
							model: Models.ShopTypes,
							attributes: ["id", "title"]
						},
						{
							model: Models.FloorPlans,
							attributes: ["id", "title"]
						},
						{
							model: Models.Districts,
							attributes: ["id", "zipCode", "titleTh", "titleEn"],
							include: [
								{
									model: Models.Amphures,
									attributes: [
										"id",
										"code",
										"titleTh",
										"titleEn"
									],
									include: [
										{
											model: Models.Provinces,
											attributes: [
												"id",
												"code",
												"titleTh",
												"titleEn"
											],
											include: [
												{
													model: Models.Regions,
													attributes: [
														"id",
														"titleTh",
														"titleEn"
													]
												}
											]
										}
									]
								}
							]
						},
						{
							model: Models.Planogram,
							attributes: [
								"id",
								"active",
								"createdAt",
								"updatedAt"
							],
							include: [
								{
									model: Models.PlanogramTableSets,
									attributes: ["id"],
									include: [
										{
											model: Models.ShopTypes,
											attributes: ["id", "title"]
										},
										{
											model: Models.TableTypes,
											attributes: ["id", "code", "title"]
										},
										{
											model: Models.TableSizes,
											attributes: [
												"id",
												"code",
												"title",
												"capacity"
											]
										},
										{
											model:
												Models.PlanogramTableSetTables,
											attributes: ["id"]
										}
									]
								}
							]
						},
						{
							model: Models.BranchMedia,
							attributes: ["id", "mediumId", "mediaTypeId"]
						}
					]
				})
					.then(branch => {
						branch = toPlain(branch);
						branch.floorPlans = branch.floorPlans.map(i => ({
							...i,
							image: getImageUploadURL(i.title)
						}));
						branch.planogram = branch.planogram
							? {
									...branch.planogram,
									code: getPlanogramCode(branch.planogram)
							  }
							: null;

						res.json(convertObjectToReponse(branch));
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				console.log("err", error);
				res.sendStatus(500);
			});
	});

	router.post("/upload", (req, res) => {
		console.log("upload coming");
		let form = new formidable.IncomingForm();
		form.parse(req, (error, fields, files) => {
			var from = files.file.path;
			var to = `tmp-branch-${getUniqueString()}.xlsx`;
			console.log("upload parsed");
			saveFile(from, to)
				.then(currentPath => {
					console.log("upload save file then");
					let excelData = readExcel({ name: currentPath });
					let branches = excelData.filter(
						i => i.name.toLocaleLowerCase() === "branch"
					);
					let branchDelete = [];
					let branchQueries = [];

					if (branches.length > 0) {
						console.log(
							"upload save file then branch > 0",
							branches
						);
						branchQueries = branches[0].data
							.shift()
							.map((data, row) => {
								branchDelete.push(data[1]);
								Promise.all([
									new Promise((resolve, reject) => {
										console.log(
											"upload select district coming"
										);
										Models.Districts.findOne({
											where: {
												firstName: {
													[Op.iLike]: `%${`${
														data[18]
													}`.toLocaleLowerCase()}%`
												}
											}
										})
											.then(district => {
												district = toPlain(district);
												console.log(
													"upload select district then",
													district
												);
												resolve(district.id);
											})
											.catch(error => {
												console.log(
													"Error cannot find district name: ",
													data[18]
												);
												resolve(null);
											});
									}),
									new Promise((resolve, reject) => {
										console.log(
											"upload select shop type coming"
										);
										Models.ShopTypes.findOrCreate({
											where: {
												title: {
													[Op.iLike]: `%${`${
														data[38]
													}`.toLocaleLowerCase()}%`
												}
											},
											defaults: {
												active: 1
											}
										})
											.then(shopType => {
												shopType = toPlain(shopType);
												console.log(
													"upload select shop type then",
													shopType
												);
												resolve(shopType.id);
											})
											.catch(error => {
												console.log(
													"Error in promise all of shop type: ",
													error
												);
												resolve(null);
											});
									})
								])
									.then(values => {
										console.log(
											"upload Promise all district and shopType then",
											values
										);
										return upsert(
											Models.Branches,
											{
												code: data[1],
												title: data[2],
												retailHub: data[25],
												customer: data[26],
												active: boolToNumber(
													stringToBool(data[23])
												),
												districtId: values[0],
												shopTypeId: values[1]
											},
											{
												code: data[1]
											}
										);
									})
									.catch(error => {
										console.log(
											"Error in promise all of district and shop type: ",
											error
										);
										return 0;
									});
							});

						Promise.all(branchQueries)
							.then(() => {
								console.log("upload final");
								res.json({
									status: true
								});
							})
							.catch(error => {
								console.log("Error upload branch: ", error);
								res.sendStatus(500);
							});
					} else {
						console.log("upload save file then branch <= 0");
						res.sendStatus(400);
					}
				})
				.catch(error => {
					console.log("Error branch upload: ", error);
					res.sendStatus(400);
				});
		});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.Branches.update(
			{
				code: req.body.code,
				title: req.body.title,
				retailHub: req.body.retailHub,
				customer: req.body.customer,
				lat: req.body.lat,
				lng: req.body.lng,
				orientation: req.body.orientation,
				active: boolToNumber(req.body.active),
				shopTypeId: req.body.shopTypeId,
				districtId: req.body.districtId
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(() => {
				Models.Branches.findOne({
					attributes: [
						"id",
						"code",
						"title",
						"retailHub",
						"customer",
						"lat",
						"lng",
						"orientation",
						"active",
						"createdAt",
						"updatedAt",
						"accessoryId"
					],
					where: {
						id: id
					},
					include: [
						{
							model: Models.ShopTypes,
							attributes: ["id", "title"]
						},
						{
							model: Models.FloorPlans,
							attributes: ["id", "title"]
						},
						{
							model: Models.Districts,
							attributes: ["id", "zipCode", "titleTh", "titleEn"],
							include: [
								{
									model: Models.Amphures,
									attributes: [
										"id",
										"code",
										"titleTh",
										"titleEn"
									],
									include: [
										{
											model: Models.Provinces,
											attributes: [
												"id",
												"code",
												"titleTh",
												"titleEn"
											],
											include: [
												{
													model: Models.Regions,
													attributes: [
														"id",
														"titleTh",
														"titleEn"
													]
												}
											]
										}
									]
								}
							]
						},
						{
							model: Models.Planogram,
							attributes: [
								"id",
								"active",
								"createdAt",
								"updatedAt"
							],
							include: [
								{
									model: Models.PlanogramTableSets,
									attributes: ["id"],
									include: [
										{
											model: Models.ShopTypes,
											attributes: ["id", "title"]
										},
										{
											model: Models.TableTypes,
											attributes: ["id", "code", "title"]
										},
										{
											model: Models.TableSizes,
											attributes: [
												"id",
												"code",
												"title",
												"capacity"
											]
										},
										{
											model:
												Models.PlanogramTableSetTables,
											attributes: ["id"]
										}
									]
								}
							]
						},
						{
							model: Models.BranchMedia,
							attributes: ["id", "mediumId", "mediaTypeId"]
						}
					]
				})
					.then(branch => {
						branch = toPlain(branch);
						branch.floorPlans = branch.floorPlans.map(i => ({
							...i,
							image: getImageUploadURL(i.title)
						}));
						branch.planogram = branch.planogram
							? {
									...branch.planogram,
									code: getPlanogramCode(branch.planogram)
							  }
							: null;
						res.json(convertObjectToReponse(branch));
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				console.log("err", error);
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.Branches.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id/floorPlan", (req, res) => {
		let id = req.params.id;
		let images = req.body.floorPlans;
		let oldImageIds = images.filter(i => i.id !== null).map(i => i.id);
		Models.FloorPlans.destroy({
			where: {
				branchId: id,
				id: { [Op.notIn]: oldImageIds }
			}
		})
			.then(() => {
				console.log("foorPlan destroy");
				let queries = images.filter(i => i.id === null).map(i => {
					let title = saveImage(i.image);
					return {
						title: title,
						branchId: id
					};
				});

				Models.FloorPlans.bulkCreate(queries)
					.then(() => {
						Models.FloorPlans.findAll({
							attributes: ["id", "title"],
							where: {
								branchId: id
							}
						})
							.then(floorPlans => {
								res.json(
									toPlain(floorPlans).map(i => ({
										...i,
										image: getImageUploadURL(i.title)
									}))
								);
							})
							.catch(error => {
								console.log("foorPlan get", error);
								res.sendStatus(404);
							});
					})
					.catch(error => {
						console.log("foorPlan bulkcreate", error);
						res.sendStatus(500);
					});
			})
			.catch(error => {
				console.log("foorPlan destroy", error);
				res.sendStatus(500);
			});
	});

	router.post("/:id/planogram", (req, res) => {
		let id = req.body.id;
		Promise.all([
			new Promise((resolve, reject) => {
				Models.Branches.update({
					planogramId: req.body.planogramId,
					accessoryId: req.body.accessoryId
				}, {
					where: {
						id: id
					}
				}).then(() => {
					resolve();
				}).catch(error => {
					console.log("Error set branch planogram and accessory : ", error);
					resolve();
				});
			}),
			new Promise((resolve, reject) => {
				Promise.all([
					new Promise((resol, rejec) => {
						Models.BranchMedia.destroy({
							where: {
								branchId: id
							}
						}).then(() => {
							resol();
						}).catch(error => {
							console.log("Error set branch delete media: ", error);
							resol();
						});
					})
				]).then(() => {
					Models.BranchMedia.bulkCreate(req.body.media.map(i => ({
						mediumId: i.model,
						mediaTypeId: i.type,
						branchId: id
					}))).then(() => {
						resolve();
					}).catch(error => {
						console.log("Error set branch bulk create media: ", error);
						resolve();
					});
				});
			})
		]).then(() => {
			res.json({
				status: true
			});
		}).catch(error => {
			console.log("Error set branch all detail: ", error);
			res.sendStatus(500);
		});
	});

	return router;
};
