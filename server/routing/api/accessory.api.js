import express from "express";
import { Models } from "../../models";
import { saveImage } from "../../middlewares";
import {
	toPlain,
	boolToNumber,
	getImageUploadURL,
	convertArrayToResponse,
	convertObjectToReponse,
	getAccessoryCode
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.Accessory.findAll({
			attributes: ["id", "image", "active", "createdAt", "updatedAt"],
			include: [
				{
					model: Models.ShopTypes,
					attributes: ["id", "title"]
				}, {
					model: Models.AccessoryTableSets,
					attributes: ["id"],
					include: [
						{
							model: Models.AccessoryTableSetDevices,
							attributes: ["id", "index", "label", "type"],
							include: [
								{
									model: Models.Devices,
									attributes: ["id", "code", "model", "price"],
									include: [
										{
											model: Models.DeviceColors,
											attributes: ["id", "title"]
										}, {
											model:
												Models.DeviceCapacities,
											attributes: ["id", "title"]
										}, {
											model: Models.DeviceModels,
											attributes: ["id", "title"]
										}, {
											model:
												Models.DeviceStatuses,
											attributes: ["id", "title"]
										}, {
											model:
												Models.DeviceTypesInShop,
											attributes: ["id", "title"]
										}, {
											model:
												Models.DeviceCategories,
											attributes: ["id", "title"]
										}
									]
								}, {
									model: Models.Media,
									attributes: ["id", "title", "desc", "image"],
									include: [
										{
											model: Models.MediaMediaType,
											attributes: ["id"],
											include: [
												{
													model: Models.MediaTypes,
													attributes: ["id", "code", "title"]
												}
											]
										}
									]
								}
							]
						}
					]
				}
			],
			order: [
				[
					Models.AccessoryTableSets,
					Models.AccessoryTableSetDevices,
					"id",
					"ASC"
				]
			]
		}).then(accessories => {
			res.json(convertArrayToResponse(toPlain(accessories)).map(i => ({
				...i,
				image: getImageUploadURL(i.image),
				code: getAccessoryCode(i)
			})));
		}).catch(error => {
			console.log("Error get planogram set", error);
			res.sendStatus(404);
		});
	});

	router.post("/", (req, res) => {
		let image = "";
		if (req.body.image !== "") {
			image = saveImage(req.body.image);
		}

		Models.Accessory.create({
			image: image,
			shopTypeId: req.body.shopTypeId,
			active: boolToNumber(req.body.active)
		}).then(accessory => {
			let accessoryPlain = toPlain(accessory);
			let accessoryTableSetQueries = req.body.tables.map(i => {
				return new Promise((resolveRoot, rejectRoot) => {
					Models.AccessoryTableSets.create({
						accessoryId: accessoryPlain.id
					}).then(accessoryTableSet => {
						let accessoryTableSetPlain = toPlain(accessoryTableSet);

						let tableDeviceValues = i.map(k => {
							let columnName = k.type === "device" ? "deviceId" : "mediumId";
							return {
								label: k.label,
								type: k.type,
								[columnName]: k.itemId,
								accessoryTableSetId: accessoryTableSetPlain.id
							};
						});

						let tableSetDataQueries = [
							new Promise((resolve, reject) => {
								Models.AccessoryTableSetDevices.bulkCreate(tableDeviceValues).then(() => {
									resolve();
								}).catch(error => {
									resolve();
									console.log("Error bulk create accessory set table device", error);
								});
							})
						];

						Promise.all(tableSetDataQueries).then(() => {
							resolveRoot();
						}).catch(error => {
							console.log("Error create accessory set when all device and media", error);
							resolveRoot();
						});
					}).catch(error => {
						console.log("Error create accessory set table", error);
						resolveRoot();
					});
				});
			});

			Promise.all(accessoryTableSetQueries).then(() => {
				Models.Accessory.findOne({
					attributes: ["id", "image", "active", "createdAt", "updatedAt"],
					where: {
						id: accessoryPlain.id
					},
					include: [
						{
							model: Models.ShopTypes,
							attributes: ["id", "title"]
						}, {
							model: Models.AccessoryTableSets,
							attributes: ["id"],
							include: [
								{
									model: Models.AccessoryTableSetDevices,
									attributes: ["id", "index", "label", "type"],
									include: [
										{
											model: Models.Devices,
											attributes: ["id", "code", "model", "price"],
											include: [
												{
													model: Models.DeviceColors,
													attributes: ["id", "title"]
												}, {
													model:
														Models.DeviceCapacities,
													attributes: ["id", "title"]
												}, {
													model: Models.DeviceModels,
													attributes: ["id", "title"]
												}, {
													model:
														Models.DeviceStatuses,
													attributes: ["id", "title"]
												}, {
													model:
														Models.DeviceTypesInShop,
													attributes: ["id", "title"]
												}, {
													model:
														Models.DeviceCategories,
													attributes: ["id", "title"]
												}
											]
										}, {
											model: Models.Media,
											attributes: ["id", "title", "desc", "image"],
											include: [
												{
													model: Models.MediaMediaType,
													attributes: ["id"],
													include: [
														{
															model: Models.MediaTypes,
															attributes: ["id", "code", "title"]
														}
													]
												}
											]
										}
									]
								}
							]
						}
					],
					order: [
						[
							Models.AccessoryTableSets,
							Models.AccessoryTableSetDevices,
							"id",
							"ASC"
						]
					]
				}).then(accessoryCreated => {
					let accessoryCreatedConverted = convertObjectToReponse(toPlain(accessoryCreated));
					res.json({
						...accessoryCreatedConverted,
						image: getImageUploadURL(accessoryCreatedConverted.image),
						code: getAccessoryCode(accessoryCreatedConverted)
					});
				}).catch(error => {
					console.log("Error get planogram set after create", error);
					res.sendStatus(404);
				});
			}).catch(error => {
				console.log("Error create multiple planogram set table", error);
			});
		}).catch(error => {
			console.log("Error create planogram set", error);
			res.sendStatus(500);
		});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		let params = {
			shopTypeId: req.body.shopTypeId,
			active: boolToNumber(req.body.active)
		};

		if (req.body.image !== "") {
			params.image = saveImage(req.body.image);
		}

		Models.Accessory.update(params, {
			where: {
				id: id
			}
		}).then(() => {
			Promise.all([
				new Promise((resolveDestroy, rejectDestroy) => {
					Models.AccessoryTableSets.destroy({
						where: {
							accessoryId: id
						}
					}).then(() => {
						resolveDestroy();
					}).catch(error => {
						console.log("Error destroy PlanogramTableSets: ", error);
						resolveDestroy();
					});
				})
			]).then(() => {
				let accessoryTableSetQueries = req.body.tables.map(i => {
					return new Promise((resolveRoot, rejectRoot) => {
						Models.AccessoryTableSets.create({
							accessoryId: id
						}).then(accessoryTableSet => {
							let accessoryTableSetPlain = toPlain(accessoryTableSet);
	
							let tableDeviceValues = i.map(k => {
								let columnName = k.type === "device" ? "deviceId" : "mediumId";
								return {
									label: k.label,
									type: k.type,
									[columnName]: k.itemId,
									accessoryTableSetId: accessoryTableSetPlain.id
								};
							});
	
							let tableSetDataQueries = [
								new Promise((resolve, reject) => {
									Models.AccessoryTableSetDevices.bulkCreate(tableDeviceValues).then(() => {
										resolve();
									}).catch(error => {
										resolve();
										console.log("Error bulk create accessory set table device", error);
									});
								})
							];
	
							Promise.all(tableSetDataQueries).then(() => {
								resolveRoot();
							}).catch(error => {
								console.log("Error create accessory set when all device and media", error);
								resolveRoot();
							});
						}).catch(error => {
							console.log("Error create accessory set table", error);
							resolveRoot();
						});
					});
				});
	
				Promise.all(accessoryTableSetQueries).then(() => {
					Models.Accessory.findOne({
						attributes: ["id", "image", "active", "createdAt", "updatedAt"],
						where: {
							id: id
						},
						include: [
							{
								model: Models.ShopTypes,
								attributes: ["id", "title"]
							}, {
								model: Models.AccessoryTableSets,
								attributes: ["id"],
								include: [
									{
										model: Models.AccessoryTableSetDevices,
										attributes: ["id", "index", "label", "type"],
										include: [
											{
												model: Models.Devices,
												attributes: ["id", "code", "model", "price"],
												include: [
													{
														model: Models.DeviceColors,
														attributes: ["id", "title"]
													}, {
														model:
															Models.DeviceCapacities,
														attributes: ["id", "title"]
													}, {
														model: Models.DeviceModels,
														attributes: ["id", "title"]
													}, {
														model:
															Models.DeviceStatuses,
														attributes: ["id", "title"]
													}, {
														model:
															Models.DeviceTypesInShop,
														attributes: ["id", "title"]
													}, {
														model:
															Models.DeviceCategories,
														attributes: ["id", "title"]
													}
												]
											}, {
												model: Models.Media,
												attributes: ["id", "title", "desc", "image"],
												include: [
													{
														model: Models.MediaMediaType,
														attributes: ["id"],
														include: [
															{
																model: Models.MediaTypes,
																attributes: ["id", "code", "title"]
															}
														]
													}
												]
											}
										]
									}
								]
							}
						],
						order: [
							[
								Models.AccessoryTableSets,
								Models.AccessoryTableSetDevices,
								"id",
								"ASC"
							]
						]
					}).then(accessoryCreated => {
						let accessoryCreatedConverted = convertObjectToReponse(toPlain(accessoryCreated));
						res.json({
							...accessoryCreatedConverted,
							image: getImageUploadURL(accessoryCreatedConverted.image),
							code: getAccessoryCode(accessoryCreatedConverted)
						});
					}).catch(error => {
						console.log("Error get accessory set after create", error);
						res.sendStatus(404);
					});
				}).catch(error => {
					console.log("Error create multiple accessory set table", error);
				});
			}).catch(error => {
				console.log("Error delete accessory table", error);
			});
		}).catch(error => {
			console.log("Error create planogram set", error);
			res.sendStatus(500);
		});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.Accessory.destroy({
			where: {
				id: id
			}
		}).then(() => {
			res.json({
				status: true
			});
		}).catch(error => {
			res.sendStatus(500);
		});
	});

	return router;
};
