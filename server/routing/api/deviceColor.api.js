import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.DeviceColors.findAll({
			attributes: ["id", "title", "createdAt", "updatedAt"]
		})
			.then(deviceColors => {
				res.json(deviceColors);
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.DeviceColors.create({
			title: req.body.title
		})
			.then(deviceColor => {
				res.json(deviceColor);
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
