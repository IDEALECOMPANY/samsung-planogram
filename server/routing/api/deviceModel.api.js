import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.DeviceModels.findAll({
			attributes: ["id", "title", "createdAt", "updatedAt"]
		})
			.then(deviceModels => {
				res.json(deviceModels);
			})
			.catch(error => {
				console.log(error);
				res.sendStatus(404);
			});
	});

	return router;
};
