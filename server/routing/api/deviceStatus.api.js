import express from "express";
import { Models } from "../../models";
import {
	getImageUploadURL,
	toPlain,
	boolToNumber,
	convertArrayToResponse,
	convertObjectToReponse
} from "../../utils/commons";

export default () => {
	const router = express.Router();

	router.get("/", (req, res) => {
		Models.DeviceStatuses.findAll({
			attributes: ["id", "title", "active", "createdAt", "updatedAt"]
		})
			.then(deviceStatuses => {
				res.json(convertArrayToResponse(toPlain(deviceStatuses)));
			})
			.catch(error => {
				res.sendStatus(404);
			});
	});

	router.post("/", (req, res) => {
		Models.DeviceStatuses.create({
			title: req.body.title,
			active: boolToNumber(req.body.active)
		})
			.then(deviceStatus => {
				res.json(convertObjectToReponse(toPlain(deviceStatus)));
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id", (req, res) => {
		let id = req.params.id;
		Models.DeviceStatuses.update(
			{
				title: req.body.title,
				active: boolToNumber(req.body.active)
			},
			{
				where: {
					id: id
				}
			}
		)
			.then(() => {
				Models.DeviceStatuses.findOne({
					attributes: [
						"id",
						"title",
						"active",
						"createdAt",
						"updatedAt"
					],
					where: {
						id: id
					}
				})
					.then(deviceStatus => {
						res.json(convertObjectToReponse(toPlain(deviceStatus)));
					})
					.catch(error => {
						res.sendStatus(404);
					});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	router.post("/:id/delete", (req, res) => {
		let id = req.params.id;
		Models.DeviceStatuses.destroy({
			where: {
				id: id
			}
		})
			.then(() => {
				res.json({
					status: true
				});
			})
			.catch(error => {
				res.sendStatus(500);
			});
	});

	return router;
};
