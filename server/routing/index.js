import apiRoutes from "./api";
import serviceRoutes from "./service";

export { apiRoutes, serviceRoutes };
