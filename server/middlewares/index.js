import jwt from "jsonwebtoken";
import fs from "fs";
import xlsx from "node-xlsx";
import nodemailer from "nodemailer";
import config from "../../config";
import { getUniqueString, toImage } from "../utils/commons";

export const generateToken = payload => {
	return new Promise((resolve, reject) => {
		jwt.sign(payload, config.auth.secretKey, (error, token) => {
			if (error) {
				reject(error);
			} else {
				resolve(token);
			}
		});
	});
};

export const verifyToken = (req, res, next) => {
	const bearerHeader = req.headers.authorization;

	if (bearerHeader) {
		const bearer = bearerHeader.split(" ");
		const bearerToken = bearer[1];
		req.token = bearerToken;
		jwt.verify(bearerToken, config.auth.secretKey, (error, authInfo) => {
			if (error) {
				res.sendStatus(403);
			} else {
				res.locals.authInfo = authInfo;
				next();
			}
		});
	} else {
		res.sendStatus(403);
	}
};

export const sendMail = ({
	from = '"Toomtam" <songkrod@toomtam.io>',
	to = [],
	subject = "",
	text,
	html
}) => {
	let transporter = nodemailer.createTransport({
		// host: "",
		// port: 587,
		// secure: false,
		service: "gmail",
		auth: {
			user: "songkrod.th@gmail.com",
			pass: "Mujiyd_3d2"
		}
	});

	let mailOptions = {
		from: from,
		to: to.join(", "),
		subject: subject
	};

	mailOptions = text ? { ...mailOptions, text } : mailOptions;
	mailOptions = html ? { ...mailOptions, html } : mailOptions;

	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			console.log("Sand Mail Error: ", error);
		} else {
			console.log("Sand Mail Success: ", info);
		}
	});
};

export const saveImage = image => {
	let imageName = getUniqueString();
	fs.writeFile(`./uploads/images/${imageName}.jpg`, toImage(image));
	return imageName;
};

export const saveFile = (from, to) => {
	let destinationPath = `./uploads/files/${to}`;
	return new Promise((resolve, reject) => {
		fs.rename(from, destinationPath, error => {
			if (error) reject(error);
			resolve(destinationPath);
		});
	});
};

export const readExcel = ({ name = "" }) => {
	let dataObject = xlsx.parse(`${name}`);
	return dataObject;
};
