module.exports = function(sequelize, DataTypes) {
	return sequelize.define("planogramTableSet", {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		}
	});
};
