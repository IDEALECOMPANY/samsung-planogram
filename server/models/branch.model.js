module.exports = function(sequelize, DataTypes) {
	return sequelize.define("branch", {
		code: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		title: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		retailHub: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		customer: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		lat: {
			type: DataTypes.STRING,
			defaultValue: "0"
		},
		lng: {
			type: DataTypes.STRING,
			defaultValue: "0"
		},
		orientation: {
			type: DataTypes.INTEGER,
			defaultValue: 1
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
