module.exports = function(sequelize, DataTypes) {
	return sequelize.define("mediaMediaSize", {
		qty: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
