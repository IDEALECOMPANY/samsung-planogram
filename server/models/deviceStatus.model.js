module.exports = function(sequelize, DataTypes) {
	return sequelize.define("deviceStatus", {
		title: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
