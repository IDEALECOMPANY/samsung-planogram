module.exports = function(sequelize, DataTypes) {
	return sequelize.define("planogramTableSetTable", {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		}
	});
};
