import Sequelize from "sequelize";
import config from "../../config";

const sequelize = new Sequelize(
	config.database.name,
	config.database.username,
	config.database.password,
	{
		host: config.database.host,
		dialect: config.database.driver,
		define: {
			paranoid: true
		}
	}
);

const Op = sequelize.Op;

const Models = {
	Roles: sequelize.import("./role.model.js"),
	Users: sequelize.import("./user.model.js"),
	UserBranches: sequelize.import("./userBranch.model.js"),

	Regions: sequelize.import("./region.model.js"),
	Provinces: sequelize.import("./province.model.js"),
	Amphures: sequelize.import("./amphur.model.js"),
	Districts: sequelize.import("./district.model.js"),

	ShopTypes: sequelize.import("./shopType.model.js"),
	TableTypes: sequelize.import("./tableType.model.js"),
	TableSizes: sequelize.import("./tableSize.model.js"),

	DeviceCategories: sequelize.import("./deviceCategory.model.js"),
	DeviceStatuses: sequelize.import("./deviceStatus.model.js"),
	DeviceTypesInShop: sequelize.import("./deviceTypeInShop.model.js"),
	DeviceModels: sequelize.import("./deviceModel.model.js"),
	DeviceCapacities: sequelize.import("./deviceCapacity.model.js"),
	DeviceColors: sequelize.import("./deviceColor.model.js"),
	Devices: sequelize.import("./device.model.js"),

	Branches: sequelize.import("./branch.model.js"),
	FloorPlans: sequelize.import("./floorPlan.model.js"),
	BranchMedia: sequelize.import("./branchMedia.model.js"),

	MediaTypes: sequelize.import("./mediaType.model.js"),
	MediaSizes: sequelize.import("./mediaSize.model.js"),
	Media: sequelize.import("./media.model.js"),
	MediaMediaType: sequelize.import("./mediaMediaType.model.js"),
	MediaMediaSize: sequelize.import("./mediaMediaSize.model.js"),
	MediaTableType: sequelize.import("./mediaTableType.model.js"),

	TableTypeMediaTypes: sequelize.import("./tableTypeMediaType.model.js"),

	Planogram: sequelize.import("./planogram.model.js"),
	PlanogramTableSets: sequelize.import("./planogramTableSet.model.js"),
	PlanogramTableSetTables: sequelize.import("./planogramTableSetTable.model.js"),
	PlanogramTableSetTableDevices: sequelize.import("./planogramTableSetTableDevice.model.js"),
	PlanogramTableSetTableMedia: sequelize.import("./planogramTableSetTableMedia.model.js"),

	Accessory: sequelize.import("./accessory.model.js"),
	AccessoryTableSets: sequelize.import("./accessoryTableSet.model.js"),
	AccessoryTableSetDevices: sequelize.import("./accessoryTableSetDevice.model.js")
};

/** User */
Models.Roles.hasMany(Models.Users);
Models.Users.belongsTo(Models.Roles);

Models.Users.hasMany(Models.UserBranches);
Models.Branches.hasMany(Models.UserBranches);
Models.UserBranches.belongsTo(Models.Users);
Models.UserBranches.belongsTo(Models.Branches);

/** Location */
Models.Regions.hasMany(Models.Provinces);
Models.Provinces.belongsTo(Models.Regions);

Models.Provinces.hasMany(Models.Amphures);
Models.Amphures.belongsTo(Models.Provinces);

Models.Amphures.hasMany(Models.Districts);
Models.Districts.belongsTo(Models.Amphures);

/** Table */
Models.TableTypes.hasMany(Models.TableSizes);
Models.TableSizes.belongsTo(Models.TableTypes);

Models.TableTypes.hasMany(Models.TableTypeMediaTypes);
Models.TableTypeMediaTypes.belongsTo(Models.TableTypes);

Models.MediaTypes.hasMany(Models.TableTypeMediaTypes);
Models.TableTypeMediaTypes.belongsTo(Models.MediaTypes);

/** Branch */
Models.Districts.hasOne(Models.Branches);
Models.Branches.belongsTo(Models.Districts);

Models.ShopTypes.hasOne(Models.Branches);
Models.Branches.belongsTo(Models.ShopTypes);

Models.Branches.hasMany(Models.FloorPlans);
Models.FloorPlans.belongsTo(Models.Branches);

Models.Planogram.hasOne(Models.Branches);
Models.Branches.belongsTo(Models.Planogram);

Models.Accessory.hasOne(Models.Branches);
Models.Branches.belongsTo(Models.Accessory);

Models.Branches.hasMany(Models.BranchMedia);
Models.Media.hasMany(Models.BranchMedia);
Models.MediaTypes.hasMany(Models.BranchMedia);
Models.BranchMedia.belongsTo(Models.Branches);
Models.BranchMedia.belongsTo(Models.Media);
Models.BranchMedia.belongsTo(Models.MediaTypes);

/** Media */
// Models.MediaTypes.hasMany(Models.Media);
// Models.Media.belongsTo(Models.MediaTypes);

// Models.TableTypes.hasMany(Models.Media);
// Models.Media.belongsTo(Models.TableTypes);

Models.Media.hasMany(Models.MediaTableType);
Models.TableTypes.hasMany(Models.MediaTableType);
Models.MediaTableType.belongsTo(Models.Media);
Models.MediaTableType.belongsTo(Models.TableTypes);

Models.Media.hasMany(Models.MediaMediaType);
Models.MediaTypes.hasMany(Models.MediaMediaType);
Models.MediaMediaType.belongsTo(Models.Media);
Models.MediaMediaType.belongsTo(Models.MediaTypes);

Models.Media.hasMany(Models.MediaMediaSize);
Models.MediaSizes.hasMany(Models.MediaMediaSize);
Models.MediaMediaSize.belongsTo(Models.Media);
Models.MediaMediaSize.belongsTo(Models.MediaSizes);

/** Device */
Models.DeviceCategories.hasMany(Models.Devices);
Models.Devices.belongsTo(Models.DeviceCategories);

Models.DeviceModels.hasMany(Models.Devices);
Models.Devices.belongsTo(Models.DeviceModels);

Models.DeviceCapacities.hasMany(Models.Devices);
Models.Devices.belongsTo(Models.DeviceCapacities);

Models.DeviceColors.hasMany(Models.Devices);
Models.Devices.belongsTo(Models.DeviceColors);

Models.DeviceStatuses.hasMany(Models.Devices);
Models.Devices.belongsTo(Models.DeviceStatuses);

Models.DeviceTypesInShop.hasMany(Models.Devices);
Models.Devices.belongsTo(Models.DeviceTypesInShop);

/** Planogram */
Models.Planogram.hasMany(Models.PlanogramTableSets);
Models.PlanogramTableSets.belongsTo(Models.Planogram);

Models.ShopTypes.hasMany(Models.PlanogramTableSets);
Models.TableTypes.hasMany(Models.PlanogramTableSets);
Models.TableSizes.hasMany(Models.PlanogramTableSets);
Models.PlanogramTableSets.belongsTo(Models.ShopTypes);
Models.PlanogramTableSets.belongsTo(Models.TableSizes);
Models.PlanogramTableSets.belongsTo(Models.TableTypes);

Models.PlanogramTableSets.hasMany(Models.PlanogramTableSetTables);
Models.PlanogramTableSetTables.belongsTo(Models.PlanogramTableSets);

Models.PlanogramTableSetTables.hasMany(Models.PlanogramTableSetTableDevices);
Models.PlanogramTableSetTableDevices.belongsTo(Models.PlanogramTableSetTables);
Models.PlanogramTableSetTables.hasMany(Models.PlanogramTableSetTableMedia);
Models.PlanogramTableSetTableMedia.belongsTo(Models.PlanogramTableSetTables);

Models.Devices.hasMany(Models.PlanogramTableSetTableDevices);
Models.Media.hasMany(Models.PlanogramTableSetTableDevices);
Models.PlanogramTableSetTableDevices.belongsTo(Models.Devices);
Models.PlanogramTableSetTableDevices.belongsTo(Models.Media);

Models.Media.hasMany(Models.PlanogramTableSetTableMedia);
Models.PlanogramTableSetTableMedia.belongsTo(Models.Media);

/** Accessory */
Models.Accessory.hasMany(Models.AccessoryTableSets);
Models.AccessoryTableSets.belongsTo(Models.Accessory);

Models.ShopTypes.hasMany(Models.Accessory);
Models.Accessory.belongsTo(Models.ShopTypes);

Models.AccessoryTableSets.hasMany(Models.AccessoryTableSetDevices);
Models.AccessoryTableSetDevices.belongsTo(Models.AccessoryTableSets);

Models.Devices.hasMany(Models.AccessoryTableSetDevices);
Models.Media.hasMany(Models.AccessoryTableSetDevices);
Models.AccessoryTableSetDevices.belongsTo(Models.Devices);
Models.AccessoryTableSetDevices.belongsTo(Models.Media);

export { sequelize, Sequelize, Op, Models };
