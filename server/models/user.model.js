module.exports = function(sequelize, DataTypes) {
	return sequelize.define("user", {
		username: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		password: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		email: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
