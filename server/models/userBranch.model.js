module.exports = function(sequelize, DataTypes) {
	return sequelize.define("userBranch", {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		}
	});
};
