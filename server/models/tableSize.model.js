module.exports = function(sequelize, DataTypes) {
	return sequelize.define("tableSize", {
		code: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		title: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		capacity: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
