module.exports = function(sequelize, DataTypes) {
	return sequelize.define("province", {
		code: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		titleTh: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		titleEn: {
			type: DataTypes.STRING,
			defaultValue: ""
		}
	});
};
