module.exports = function(sequelize, DataTypes) {
	return sequelize.define("deviceCategory", {
		title: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
