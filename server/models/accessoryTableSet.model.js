module.exports = function(sequelize, DataTypes) {
	return sequelize.define("accessoryTableSet", {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		}
	});
};
