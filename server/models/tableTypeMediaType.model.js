module.exports = function(sequelize, DataTypes) {
	return sequelize.define("tableTypeMediaType", {
		qty: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
