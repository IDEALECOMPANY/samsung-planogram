module.exports = function(sequelize, DataTypes) {
	return sequelize.define("media", {
		image: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		title: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		desc: {
			type: DataTypes.TEXT,
			defaultValue: ""
		},
		material: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
