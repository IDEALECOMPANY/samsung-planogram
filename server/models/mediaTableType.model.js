module.exports = function(sequelize, DataTypes) {
	return sequelize.define("mediaTableType", {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		}
	});
};
