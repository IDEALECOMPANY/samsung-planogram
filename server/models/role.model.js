module.exports = function(sequelize, DataTypes) {
	return sequelize.define("role", {
		title: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
