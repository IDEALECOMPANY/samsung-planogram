module.exports = function(sequelize, DataTypes) {
	return sequelize.define("tableType", {
		code: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		title: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
