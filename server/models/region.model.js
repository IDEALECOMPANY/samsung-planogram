module.exports = function(sequelize, DataTypes) {
	return sequelize.define("region", {
		titleTh: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		titleEn: {
			type: DataTypes.STRING,
			defaultValue: ""
		}
	});
};
