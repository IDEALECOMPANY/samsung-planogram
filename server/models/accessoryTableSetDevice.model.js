module.exports = function(sequelize, DataTypes) {
	return sequelize.define("accessoryTableSetDevice", {
		label: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		type: {
			type: DataTypes.STRING,
			defaultValue: ""
		}
	});
};
