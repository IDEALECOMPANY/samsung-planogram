module.exports = function(sequelize, DataTypes) {
	return sequelize.define("planogram", {
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
