module.exports = function(sequelize, DataTypes) {
	return sequelize.define("device", {
		code: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		model: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		price: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
