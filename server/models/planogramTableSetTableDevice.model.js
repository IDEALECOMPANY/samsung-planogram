module.exports = function(sequelize, DataTypes) {
	return sequelize.define("planogramTableSetTableDevice", {
		index: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		},
		label: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		type: {
			type: DataTypes.STRING,
			defaultValue: ""
		}
	});
};
