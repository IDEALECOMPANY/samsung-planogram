module.exports = function(sequelize, DataTypes) {
	return sequelize.define("accessory", {
		image: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		active: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
