import express from "express";
import path from "path";
import open from "open";
import cors from "cors";
import bodyParser from "body-parser";
import compression from "compression";
import config from "../config";
import { sequelize } from "./models";
import { apiRoutes, serviceRoutes } from "./routing";

/* elint-disable no-console */

const port = config.port;
const app = express();

app.use(
	bodyParser.urlencoded({
		limit: "50mb",
		extended: true
	})
);
app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());

app.use(compression());
app.use(express.static("dist"));
app.use("/uploads", express.static("uploads"));
app.use("/api", apiRoutes());
app.use("/service", serviceRoutes());

app.get("*", (req, res) => {
	res.sendFile(path.join(__dirname, "../dist/index.html"));
});

sequelize
	.sync()
	.then(() => {
		console.log("Database connected.");
	})
	.catch(error => {
		console.log("Error: Can't connect to database.", error);
	});

app.listen(port, error => {
	if (error) {
		console.log("Server start fail: ", error);
	} else {
		console.log(
			`Run on production build: http://localhost:${port}`
		);
	}
});