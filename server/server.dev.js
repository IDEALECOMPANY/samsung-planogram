import express from "express";
import webpack from "webpack";
import path from "path";
import open from "open";
import cors from "cors";
import bodyParser from "body-parser";
import webpackConfig from "../webpack.config.dev";
import config from "../config";
import { sequelize } from "./models";
import { apiRoutes, serviceRoutes } from "./routing";

/* eslint-disable no-console */

const port = config.port;
const app = express();

app.use(
	bodyParser.urlencoded({
		limit: "50mb",
		extended: true
	})
);
app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());

const compiler = webpack(webpackConfig);

app.use(
	require("webpack-dev-middleware")(compiler, {
		publicPath: webpackConfig.output.publicPath
	})
);

app.use(require("webpack-hot-middleware")(compiler));

app.use(express.static("public"));
app.use("/uploads", express.static("uploads"));
app.use("/api", apiRoutes());
app.use("/service", serviceRoutes());

app.get("*", (req, res) => {
	res.sendFile(path.join(__dirname, "../public/index.html"));
});

sequelize
	.sync({ force: false })
	.then(() => {
		console.log("Database connected.");
	})
	.catch(error => {
		console.log("Error: Can't connect to database.", error);
	});

app.listen(port, error => {
	if (error) {
		console.log("Server start fail: ", error);
	} else {
		console.log(
			`Run on development build: http://localhost:${port}`
		);
	}
});