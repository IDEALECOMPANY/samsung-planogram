import config from "../../config";

export const getFullURL = () => config.origin;
export const getImageUploadURL = image => `${getFullURL()}/uploads/images/${image}.jpg`;
export const toPlain = obj => JSON.parse(JSON.stringify(obj));
export const stringToBool = status => status !== "Inactive";
export const boolToNumber = status => (status ? 1 : 0);
export const numberToBool = status => status === 1;

export const convertArrayToResponse = arr =>
	arr.map(i => convertObjectToReponse(i));

export const convertObjectToReponse = obj => ({
	...obj,
	active: numberToBool(obj.active)
});

export const toImage = dataString => {
	let matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);

	if (matches.length !== 3) {
		return new Error("Invalid input string");
	}

	return new Buffer(matches[2], "base64");
};

export const randomString = (length = 10) => {
	let text = "";
	let possible =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
};

export const timestamp = () => {
	return Math.floor(new Date() / 1000);
};

export const getUniqueString = (separate = "_", length = 10) => {
	return `${timestamp()}${separate}${randomString(length)}`;
};

export const upsert = (model, values = {}, condition = {}) => {
	return model
		.findOne({
			where: condition
		})
		.then(data => {
			if (data) {
				return data.update(values, { where: condition });
			} else {
				return model.create(values);
			}
		})
		.catch(error => {
			console.log("Error upsert ", error);
			return model.create(values);
		});
};

export const getPlanogramCode = planogram => `${planogram.planogramTableSets[0].shopType.title}-${planogram.planogramTableSets[0].planogramTableSetTables.length}${planogram.planogramTableSets[0].tableType.code}-${planogram.planogramTableSets[0].tableSize.code}`;

export const getAccessoryCode = accessory => {
	let device = 0, media = 0;
	accessory.accessoryTableSets.map(i => {
		i.accessoryTableSetDevices.map(j => j.type === "device" ? device++ : media++);
	});

	return `${accessory.shopType.title.toUpperCase().split(" ").join("_")}-${accessory.accessoryTableSets.length}-${device}ACC-${media}MD`;
};