import fs from "fs";
import cheerio from "cheerio";
import colors from "colors";
import { ncp } from "ncp";

/* eslint-disable no-console */

ncp("public", "dist", error => {
	if (error) {
		return console.log(error.red);
	}

	console.log("clone /public to /dist".green);

	fs.readFile("public/index.html", "utf8", (error, markup) => {
		if (error) {
			return console.log(error.red);
		}

		const $ = cheerio.load(markup);
		$("head").prepend("<link rel='stylesheet' href='styles.css'>");

		fs.writeFile("dist/index.html", $.html(), "utf8", error => {
			if (error) {
				return console.log(error.red);
			}

			console.log("index.html written to /dist".green);
		});
	});
});
