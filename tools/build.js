import webpack from "webpack";
import webpackConfig from "../webpack.config.prod";
import color from "colors";

/* eslint-disable no-console */

process.env.NODE_ENV = "production";

console.log(
	"Generating minified bundle for production via Webpack. This will take a moment..."
		.blue
);

webpack(webpackConfig).run((error, stats) => {
	if (error) {
		console.log(error.bold.red);
		return 1;
	}

	const jsonStats = stats.toJson();

	if (jsonStats.hasErrors) {
		return jsonStats.errors.map(i => console.log(i.red));
	}

	if (jsonStats.hasWarnings) {
		console.log("Webpack generated the following warnings: ".bold.yellow);
		jsonStats.warnings.map(i => console.log(i.yellow));
	}

	console.log(`Webpack stats: ${stats}`);

	console.log(
		"Your app has been compiled in production mode and written to /dist. It's ready to roll!"
			.green
	);
	return 0;
});
