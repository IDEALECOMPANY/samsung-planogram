import loadable from "loadable-components";

export const LoginPage = loadable(() => import("./login"));
export const HomePage = loadable(() => import("./home"));

export const BranchPage = loadable(() => import("./branch"));
export const AddBranchPage = loadable(() => import("./branch/add-branch"));
export const EditBranchPage = loadable(() => import("./branch/edit-branch"));
export const BranchDetailPage = loadable(() => import("./branch/branch-detail"));
export const ShopTypePage = loadable(() => import("./branch/shop-type"));
export const AddShopTypePage = loadable(() => import("./branch/add-shop-type"));
export const ShopTypeDetailPage = loadable(() => import("./branch/shop-type-detail"));

export const DevicePage = loadable(() => import("./device"));
export const DeviceAddPage = loadable(() => import("./device/add-device"));
export const DeviceDetailPage = loadable(() => import("./device/device-detail"));

export const DeviceCategoryPage = loadable(() => import("./device/device-category"));
export const DeviceCategoryAddPage = loadable(() => import("./device/add-device-catagory"));
export const DeviceCategoryDetailPage = loadable(() => import("./device/device-category-detail"));

export const DeviceStatusPage = loadable(() => import("./device/device-status"));
export const DeviceStatusAddPage = loadable(() => import("./device/add-device-status"));
export const DeviceStatusDetailPage = loadable(() => import("./device/device-status-detail"));

export const DeviceTypeShopPage = loadable(() => import("./device/device-type-in-shop"));
export const DeviceTypeShopAddPage = loadable(() => import("./device/add-device-type-in-shop"));
export const DeviceTypeShopDetailPage = loadable(() => import("./device/device-type-in-shop-detail"));

export const EditProfilePage = loadable(() => import("./edit-profile"));
export const LogoutPage = loadable(() => import("./logout"));

export const PlanogramSetPage = loadable(() => import("./planogram-set"));
export const addPlanogramSetPage = loadable(() => import("./planogram-set/add-planogram-set"));
export const PlanogramSetDetailPage = loadable(() => import("./planogram-set/planogram-set-detail"));

export const TablePage = loadable(() => import("./table/table-type"));
export const AddTablePage = loadable(() => import("./table/add-table-type"));
export const TableTypeDetail = loadable(() => import("./table/table-type-detail"));
export const SizePage = loadable(() => import("./table/tableSize"));
export const AddSizePage = loadable(() => import("./table/add-size"));
export const SizeDetailPage = loadable(() => import("./table/size-detail"));

export const MediaPage = loadable(() => import("./media"));
export const AddMediaPage = loadable(() => import("./media/add-media"));
export const MediaDetailPage = loadable(() => import("./media/media-detail"));
export const PlanogramDetailPage = loadable(() => import("./planogram-set/planogram-set-detail"));
export const MediaTypePage = loadable(() => import("./media/media-type"));
export const addMediaTypePage = loadable(() => import("./media/add-media-type"));
export const MediaTypeDetailPage = loadable(() => import("./media/media-type-detail"));
export const MediaSizePage = loadable(() => import("./media/media-size"));

export const AddMediaSizePage = loadable(() => import("./media/add-media-size"));
export const MediaSizeDetailPage = loadable(() => import("./media/media-size-detail"));

export const UserPage = loadable(() => import("./user"));
export const addUserPage = loadable(() => import("./user/add-user"));
export const UserDetailPage = loadable(() => import("./user/user-detail"));

export const AccessoriesSetPage = loadable(() => import("./accessories-set"));
export const AddAccessoriesSetPage = loadable(() => import("./accessories-set/add-accessories-set"));
export const AccessoriesSetDetailPage = loadable(() => import("./accessories-set/accessories-set-detail"));
