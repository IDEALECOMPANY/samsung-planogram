import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";
import Titlepage from "../../commons/titlepage";
import Panel from "../../commons/panel";
import LastestLogin from "../../commons/lastestLogin";
import Nav from "../../commons/nav";
import Hr from "../../commons/hr";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

const PanelData1 = {
	columns: ["Item Name", "Publish Date"],
	rows: [
		{
			"Item Name": " Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		},
		{
			"Item Name": "Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		},
		{
			"Item Name": "Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		},
		{
			"Item Name": "Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		},
		{
			"Item Name": "Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		}
	]
};

const LoginData = {
	columns: ["user", "Date", "time"],
	rows: [
		{
			user: "Super Admin",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			user: "admin",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			user: "admin",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			user: "admin",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			user: "admin",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		}
	]
};

const BranchData = {
	columns: ["branch", "Date", "time"],
	rows: [
		{
			branch: "Paragon",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			branch: "Paragon",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			branch: "Paragon",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			branch: "Paragon",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			branch: "Paragon",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		}
	]
};

const ImportData = {
	columns: ["name", "Date", "time"],
	rows: [
		{
			name: "Branch",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "Device",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "Planogram Set",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "Media",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "Table",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		}
	]
};

const BrowserData = {
	columns: ["name", "Date", "time"],
	rows: [
		{
			name: "Google Chrome",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "Google Chrome",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "Google Chrome",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "Google Chrome",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "Google Chrome",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		}
	]
};

const IpData = {
	columns: ["name", "Date", "time"],
	rows: [
		{
			name: "216.58.216.164",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "216.58.216.164",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "216.58.216.164",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "216.58.216.164",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		},
		{
			name: "216.58.216.164",
			Date: "29 - 10 - 2017",
			time: "11:00:23"
		}
	]
};

const PanelData = {
	columns: ["Item Name", "Publish Date"],
	rows: [
		{
			"Item Name": " Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		},
		{
			"Item Name": "Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		},
		{
			"Item Name": "Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		},
		{
			"Item Name": "Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		},
		{
			"Item Name": "Galaxy Note8",
			"Publish Date": "29 - 10 - 2017"
		}
	]
};

export class Page extends Component {
	static defaultProps = {
		posts: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่"
		};
	}

	filterPosts(posts) {
		return posts.filter(i => i.title.indexOf(this.state.searchTxt) !== -1 || i.msg.indexOf(this.state.searchTxt) !== -1);
	}

	customTableSearch(onClick) {
		return <span>Search:</span>;
	}

	editTable(cell, row) {
		return (
			<a href="device-detail">
				<i className="fa fa-pencil" aria-hidden="true" />
			</a>
		);
	}

	deleteTable(cell, row) {
		return (
			<a href="#">
				<i className="fa fa-trash" aria-hidden="true" />
			</a>
		);
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="home" title="Home" line />
						</div>
						<div className="col-md-6 text-right">
							<LastestLogin date="2017-10-25" time="14:42:38" />
						</div>
					</div>
					<Hr />
					<div className="table-box_">
						<div className="text-right" />
						<div className="row">
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="New Device" PanelData={PanelData1} />
							</div>
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="Login ล่าสุด" PanelData={LoginData} />
							</div>
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="Publish report ล่าสุด" PanelData={BranchData} />
							</div>
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="สาขาใหม่" PanelData={PanelData} />
							</div>
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="สาขาที่ถูกลบ" PanelData={PanelData} />
							</div>
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="Import ล่าสุด" PanelData={ImportData} />
							</div>
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="Export ล่าสุด" PanelData={PanelData} />
							</div>
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="Browser ที่เข้าใช้ล่าสุด" PanelData={BrowserData} />
							</div>
							<div className="col-6 col-sm-6 col-lg-4">
								<Panel size="sm" title="IP ที่เข้าใช้ล่าสุด" PanelData={IpData} />
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		// posts: postsFormatter(state.posts)
	};
};

const mapDispatchToProps = dispatch => {
	return {
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
