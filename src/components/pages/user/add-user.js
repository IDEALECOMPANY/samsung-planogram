import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

import { usersActions } from "../../../actions";
import { usersFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Hr from "../../commons/hr";
import FormUser from "../../commons/formUser";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		branchs: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			username: "",
			email: "",
			password: "",
			branchId: "",
			active: "1",
			branchs: this.props.branchs
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.branchs === null) {
			this.onCancel();
		}
	}

	filterPosts(posts) {
		return posts.filter(
			i =>
				i.title.indexOf(this.state.searchTxt) !== -1 ||
				i.msg.indexOf(this.state.searchTxt) !== -1
		);
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (
			this.state.username !== "" &&
			this.state.email !== "" &&
			this.state.branchId !== ""
		) {
			this.props.usersActions
				.addUser({
					username: this.state.username,
					email: this.state.email,
					branchId: this.state.branchId,
					active: `${this.state.active}` === "1"
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/user");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="media" title="New User" line />
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<div className="row">
							<FormUser
								view
								data={this.state}
								onChange={this.onChange}
							/>
							<div className="col-md-12 text-center bt">
								<Button
									onClick={this.onCancel}
									className="pull-left "
									btnType="cancel"
									children="Cancle"
								/>
								<Button
									onClick={this.onAccept}
									className="pull-left "
									btnType="default"
									children="Submit"
								/>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		users: state.users,
		branchs: state.branchs
	};
};

const mapDispatchToProps = dispatch => {
	return {
		usersActions: bindActionCreators(usersActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
