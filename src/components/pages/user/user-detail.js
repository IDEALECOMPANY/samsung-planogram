import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

import { usersActions } from "../../../actions";
import { usersFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Hr from "../../commons/hr";
import FormUser from "../../commons/formUser";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		user: null,
		users: null,
		branchs: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			id: "",
			username: "",
			email: "",
			password: "",
			branchId: "",
			active: "",
			branchs: this.props.branchs
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.users === null || this.props.branchs === null) {
			this.onCancel();
		}
		if (this.props.user !== null) {
			this.checkProps(this.props.user);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.user !== nextProps.user) {
			this.checkProps(nextProps.user);
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/user");
		} else {
			this.setState(prevState => ({
				...prevState,
				id: props.id,
				username: props.username,
				email: props.email,
				branchId:
					props.userBranches.length > 0
						? props.userBranches[0].branch.id
						: "",
				active: props.active ? "1" : "2"
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	validateEmail(email) {
		let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	onAccept() {
		if (
			this.state.username !== "" &&
			this.state.email !== "" &&
			this.state.branchId !== ""
		) {
			if (this.validateEmail(this.state.email)) {
				this.props.usersActions
					.editUser({
						id: this.state.id,
						username: this.state.username,
						email: this.state.email,
						branchId: this.state.branchId,
						active: `${this.state.active}` === "1",
						createdAt: this.props.user.createdAt,
						updatedAt: new Date()
					})
					.then(() => {
						this.onCancel();
					})
					.catch(error => {
						// show error alert
					});
			} else {
				alert("กรุณาใส่อีเมลให้ถูกต้อง");
			}
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/user");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="media" title="User" line />
						</div>

						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />

					<div className="table-box">
						<div className="row">
							<FormUser
								view
								data={this.state}
								onChange={this.onChange}
							/>

							<div className="col-md-12 text-center bt">
								<Button
									onClick={this.onCancel}
									className="pull-left "
									btnType="cancel"
									children="Cancle"
								/>
								<Button
									onClick={this.onAccept}
									className="pull-left "
									btnType="default"
									children="Submit"
								/>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let users = state.users.filter(i => `${i.id}` === `${id}`);
	let user = users.length > 0 ? users[0] : { error: true };

	return {
		user: user,
		users: state.users,
		branchs: state.branchs
	};
};

const mapDispatchToProps = dispatch => {
	return {
		usersActions: bindActionCreators(usersActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
