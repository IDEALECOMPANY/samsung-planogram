import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import { usersActions, branchActions } from "../../../actions";
import { usersFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import UserList from "../../commons/userList";
import MySearchField from "../../commons/MySearchField";

const Div = styled.div`
	width: 100%;
	position: relative;
	.btn {
		&.btn-toggle {
			line-height: 1;
			padding-top: 42px;
			background-color: transparent;
			color: #7281d6;

			&:focus {
				outline: none;
				-webkit-box-shadow: none;
			}
		}
	}
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 200px;
		margin-top: 5px;
		position: absolute;
		right: 15px;
		margin-top: 15px;
		z-index: 99;
		transition: all 0.2s ease-out;

		li {
			display: list-item;
			list-style: none;
			padding: 15px 20px;
			border-bottom: 1px solid #e5e5e5;
			font-size: 14px;
			text-align: left;
			cursor: pointer;
			color: ${props => props.theme.color.black};

			.d-text {
				margin-left: 15px;
			}

			&:hover {
				color: ${props => props.theme.color.primary};
			}

			.icon {
				width: 20px;
			}
			li:last-child {
				border-bottom: 1px solid transparent;
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		users: [],
		branchs: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false
		};

		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.importBoxOpen = this.importBoxOpen.bind(this);
	}

	componentDidMount() {
		if (this.props.users.length === 0) {
			this.props.usersActions.getUsers();
		}
		if (this.props.branchs.length === 0) {
			this.props.branchActions.getBranchs();
		}
	}

	importBoxOpen() {
		this.setState(prevState => ({
			...prevState,
			importBox: !prevState.importBox
		}));
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.usersActions.deleteUser({id: id}).then(() => {
				resolve();
			}).catch(error => {
				// show error alert
				reject();
			});
		});
	}

	render() {
		const options = {
			sizePerPageList: [
				{
					text: "6",
					value: 6
				}, {
					text: "20",
					value: 20
				}, {
					text: "All",
					value: this.props.users.length
				}
			],
			sizePerPage: 6,
			prePage: "Previous", // Previous page button text
			nextPage: "Next", // Next page button text
			firstPage: "First", // First page button text
			lastPage: "Last", // Last page button text
			searchField: props => <MySearchField text="search" {...props} />
		};

		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="media" title="User" line />
						</div>
						<div className="col-md-6 text-right">
							<button
								onMouseOver={this.toggle}
								className="btn btn-toggle"
							>
								<img src="assets/images/icon-dropdown.png" />
							</button>
							{this.state.showDropdown && (
								<ul className="box">
									<li
										onClick={() =>
											this.nextPath("/user/add")
										}
									>
										<img
											className="icon"
											src="assets/images/icon-add.png"
										/>
										<span className="d-text">
											Add User
										</span>
									</li>
									<li onClick={this.importBoxOpen}>
										<img
											className="icon"
											src="assets/images/import.png"
										/>
										<span className="d-text">
											Import User
										</span>
									</li>
									<li>
										<img
											className="icon"
											src="assets/images/icon-export.png"
										/>
										<span className="d-text">
											Export User
										</span>
									</li>
								</ul>
							)}
						</div>
					</div>
					<Hr />
					<div className="table-box">
						{this.state.importBox && (
							<ImportBox
								title="Import User"
								sheetname="user"
								onChangeImport={this.onChangeImport}
								onCancelImport={this.onCancelImport}
								onAcceptImport={this.onAcceptImport}
							/>
						)}
						<UserList onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		users: state.users,
		branchs: state.branchs
	};
};

const mapDispatchToProps = dispatch => {
	return {
		usersActions: bindActionCreators(usersActions, dispatch),
		branchActions: bindActionCreators(branchActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
