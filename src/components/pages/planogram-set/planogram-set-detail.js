import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import {
	tableTypeActions,
	tableSizeActions,
	branchTypeActions,
	deviceActions,
	deviceCategoryActions,
	mediaActions,
	mediaTypeActions,
	planogramActions
} from "../../../actions";
import { planogramsFormatter, getUniqueString } from "../../../selectors";

import Titlepage from "../../commons/titlepage";
import Hr from "../../commons/hr";
import FormPlanogramSet from "../../commons/formPlanogramSet";

const Div = styled.div`
	width: 100%;
	position: relative;
	.modal {
		display: block !important;
		.modal-header {
			padding: 10px 15px;
		}
		.modal-footer {
			border-top: 0;
		}

		.modal-body {
			h6 {
				background-color: #f5f5f5;
				font-size: 16px;
				padding: 10px 15px;
				border-bottom: 1px solid #ccc;
			}

			.body-inner {
				float: left;
				width: 100%;
				padding: 15px;
				border: 1px solid #ccc;
				padding: 0;
				border-radius: 7px;
			}
		}
	}
	.modal_ {
		position: fixed !important;
		width: 100%;
		height: 100%;
		z-index: 99999;
		background-color: rgba(0, 0, 0, 0.5);
		top: 0;
		left: 0;
	}

	.modal-dialog {
		max-width: 800px;
	}

	button {
		height: 33px;
		border: 1px solid ${props => props.theme.color.primary};
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 30px;
		border-radius: 5px;
		color: ${props => props.theme.color.primary};

		&.btn {
			height: 33px;
			cursor: pointer;
			font-size: ${props => props.theme.font.small};
			padding: 0px 20px;
			border-radius: 5px;
			color: ${props => props.theme.color.primary};
			display: inline-block;

			&.btn-primary {
				background-color: ${props => props.theme.bg.primary};
				border: 1px solid ${props => props.theme.border.primary};
				color: #fff;
			}

			&.btn-default {
				background-color: ${props => props.theme.bg.default};
				border: 1px solid ${props => props.theme.border.primary};
			}

			&.btn-cancel {
				background-color: ${props => props.theme.bg.gray};
				border: 1px solid ${props => props.theme.border.gray};
				color: ${props => props.theme.color.gray};
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		planograms: [],
		tableTypes: [],
		tableSizes: [],
		branchTypes: [],
		devices: [],
		deviceCategories: [],
		medias: [],
		mediaTypes: [],
		options_device: [
			{
				label: "media",
				options: [
					// { label: "Yellow", value: "yellow" },
					// { label: "Red", value: "red" },
					// { label: "Blue", value: "blue" }
				]
			}, {
				label: "device",
				options: [
					// { label: "Orange", value: "orange" },
					// { label: "Green", value: "green" }
				]
			}
		],
		options_model: [
			{
				label: "media",
				options: [
					// { label: "Yellow", value: "yellow" },
					// { label: "Red", value: "red" },
					// { label: "Blue", value: "blue" }
				]
			}, {
				label: "device",
				options: [
					// { label: "Orange", value: "orange" },
					// { label: "Green", value: "green" }
				]
			}
		],
		options_model_media: [
			// { value: "one", label: "One" },
			// { value: "two", label: "Two" }
		],
		row: ["1", "2", "3", "4", "5", "6"],
		planogram: null
	};

	constructor(props, context) {
		super(props, context);
		this.state = {
			showManagePlanogramSet: false,
			showArrangeTableModal: false,
			planogramSetArrange: {},
			options_device: this.props.options_device,
			options_model: this.props.options_model,
			options_model_media: this.props.options_model_media,
			devices: this.props.devices,
			medias: this.props.medias,
			row: this.props.row,
			planogramSet: {
				id: "",
				active: "1",
				tableSets: [
					{
						id: `tableSet-${getUniqueString()}`,
						qty: "",
						tableTypeId: "",
						tableSizeId: "",
						branchTypeId: "",
						capacity: "",
						tables: []
					}
				]
			},
			tableTypes: this.props.tableTypes,
			tableSizes: this.props.tableSizes,
			branchTypes: this.props.branchTypes
		};

		this.openManage = this.openManage.bind(this);
		this.openArrangeTable = this.openArrangeTable.bind(this);
		this.closeArrangeTable = this.closeArrangeTable.bind(this);
		this.addTableSet = this.addTableSet.bind(this);
		this.deleteTableSet = this.deleteTableSet.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onChangeSelect = this.onChangeSelect.bind(this);
		this.onChangeSelectDevice = this.onChangeSelectDevice.bind(this);
		this.onAccept = this.onAccept.bind(this);
	}

	componentDidMount() {
		if (this.props.tableTypes.length === 0) {
			this.props.tableTypeActions.getTableTypes();
		}
		if (this.props.tableSizes.length === 0) {
			this.props.tableSizeActions.getTableSizes();
		}
		if (this.props.branchTypes.length === 0) {
			this.props.branchTypeActions.getBranchTypes();
		}
		if (this.props.branchTypes.length === 0) {
			this.props.branchTypeActions.getBranchTypes();
		}
		if (this.props.devices.length === 0) {
			this.props.deviceActions.getDevices();
		}
		if (this.props.deviceCategories.length === 0) {
			this.props.deviceCategoryActions.getDeviceCategories();
		}
		if (this.props.medias.length === 0) {
			this.props.mediaActions.getMedias();
		}
		if (this.props.mediaTypes.length === 0) {
			this.props.mediaTypeActions.getMediaTypes();
		}

		if (this.props.planogram !== null) {
			this.checkProps(this.props.planogram);
		} else {
			this.onCancel();
		}
	}
	componentWillReceiveProps(nextProps) {
		if (this.props.planogram !== nextProps.planogram) {
			this.checkProps(nextProps.planogram);
		}
	}

	openManage() {
		this.setState({ showManagePlanogramSet: true });
	}

	openArrangeTable(planogramSet) {
		this.setState({
			showArrangeTableModal: true,
			planogramSetArrange: planogramSet
		});
	}
	closeArrangeTable() {
		this.setState({ showArrangeTableModal: false });
	}
	addTableSet() {
		let uuid = getUniqueString();
		this.setState(prevState => ({
			...prevState,
			planogramSet: {
				...prevState.planogramSet,
				tableSets: [
					...prevState.planogramSet.tableSets,
					{
						id: `tableSet-${uuid}`,
						qty: "",
						tableTypeId: "",
						tableSizeId: "",
						branchTypeId: "",
						capacity: "",
						tables: []
					}
				]
			}
		}));
	}
	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;
		let zone = `${event.zone}`.split("-");
		if (zone[0] === "tableSet") {
			this.setState(prevState => ({
				...prevState,
				planogramSet: {
					...prevState.planogramSet,
					tableSets: this.state.planogramSet.tableSets.map((i, k) => {
						if (`${event.zone}` === `${i.id}`) {
							if (name === "tableTypeId") {
								let media = [];
								let mediaTypes = this.state.tableTypes.find(t => `${t.id}` === `${value}`).mediaTypes;
								mediaTypes.forEach((element, key) => {
									for (let m = 0; m < element.qty; m++) {
										media.push({
											name: `media-${getUniqueString()}`,
											index: key + m + 1,
											label: `${element.code}${m + 1}`,
											id: null,
											value: "",
											mediaType: {
												id: element.id,
												title: element.title
											}
										});
									}
								});
								return {
									...i,
									[name]: value,
									tables: i.tables.map(t => {
										return {
											devices: t.devices,
											media: media
										};
									})
								};
							}
							if (name === "tableSizeId") {
								let capacity = this.state.tableSizes.find(ts => `${ts.id}` === `${value}`).capacity;
								if (`${capacity}` !== `${i.capacity}`) {
									let devices = [];
									let el = ["E1", "E2", "L1", "L2"];
									for (let d = 0; d < capacity; d++) {
										devices.push({
											name: "",
											index: d + 1,
											label: d + 1,
											type: "",
											typeId: "",
											id: null,
											value: "",
											price: ""
										});
									}
									el.forEach(function(element, key) {
										devices.push({
											name: "",
											index: devices.length,
											label: element,
											type: "",
											typeId: "",
											id: null,
											value: "",
											price: ""
										});
									});
									return {
										...i,
										[name]: value,
										capacity: capacity,
										tables: i.tables.map(t => {
											return {
												devices: devices.map(d => {
													return {
														...d,
														name: `devices-${getUniqueString()}`
													};
												}),
												media: t.media
											};
										})
									};
								}
							}
							if (name === "qty") {
								if (value > i.tables.length && value !== "") {
									let diff = value - i.tables.length;
									let max = i.tables.length;
									let tables = i.tables;
									// devices
									let devices = [];
									let el = ["E1", "E2", "L1", "L2"];
									for (let d = 0; d < i.capacity; d++) {
										devices.push({
											name: "",
											index: d + 1,
											label: d + 1,
											type: "",
											typeId: "",
											id: null,
											value: "",
											price: ""
										});
									}
									el.forEach(function(element, key) {
										devices.push({
											name: "",
											index: devices.length,
											label: element,
											type: "",
											typeId: "",
											id: null,
											value: "",
											price: ""
										});
									});
									// media
									let media = [];
									let mediaTypes = i.tableTypeId === "" ? [] : this.state.tableTypes.find(t => `${t.id}` === `${i.tableTypeId}`).mediaTypes;
									mediaTypes.forEach((element, key) => {
										for (let m = 0; m < element.qty; m++) {
											media.push({
												name: "",
												index: key + m + 1,
												label: `${element.code}${m + 1}`,
												id: null,
												value: "",
												mediaType: {
													id: element.id,
													title: element.title
												}
											});
										}
									});
									for (let t = 0; t < diff; t++) {
										tables.push({
											devices: devices.map(d => {
												return {
													...d,
													name: `devices-${getUniqueString()}`
												};
											}),
											media: media.map(m => {
												return {
													...m,
													name: `media-${getUniqueString()}`
												};
											})
										});
									}
									return {
										...i,
										[name]: value,
										tables: tables
									};
								} else if (
									value < i.tables.length &&
									value !== ""
								) {
									return {
										...i,
										[name]: value,
										tables: i.tables.slice(0, value)
									};
								} else {
									return {
										...i,
										[name]: value
									};
								}
							} else {
								return {
									...i,
									[name]: value
								};
							}
						} else {
							return i;
						}
					})
				}
			}), () => {
			});
		} else {
			this.setState(prevState => ({
				...prevState,
				planogramSet: {
					...prevState.planogramSet,
					[name]: value
				}
			}), () => {
			});
		}
	}
	onChangeSelect(name, event) {
		let key = name;
		let zone = `${name}`.split("-");
		this.setState(prevState => ({
			...prevState,
			planogramSet: {
				...prevState.planogramSet,
				tableSets: this.state.planogramSet.tableSets.map(ts => {
					return {
						...ts,
						tables: ts.tables.map(t => {
							if (zone[0] === "media") {
								return {
									devices: t.devices,
									media: t.media.map(m => {
										if (`${m.name}` === `${key}`) {
											return {
												...m,
												id: event.value,
												value: event.label
											};
										} else {
											return {
												...m
											};
										}
									})
								};
							} else if (zone[0] === "devices") {
								return {
									devices: t.devices.map(d => {
										if (`${d.name}` === `${key}`) {
											return {
												...d,
												id: event.value,
												value: event.label,
												price: event.price
											};
										} else {
											return {
												...d
											};
										}
									}),
									media: t.media
								};
							} else {
								return {
									...t
								};
							}
						})
					};
				})
			}
		}), () => {
		});
	}
	onChangeSelectDevice(name, event) {
		let key = name;
		let zone = `${name}`.split("-");
		let value = `${event.value}`.split("-");
		this.setState(prevState => ({
			...prevState,
			planogramSet: {
				...prevState.planogramSet,
				tableSets: this.state.planogramSet.tableSets.map(ts => {
					return {
						...ts,
						tables: ts.tables.map(t => {
							if (zone[0] === "devices") {
								return {
									devices: t.devices.map(d => {
										if (`${d.name}` === `${key}`) {
											return {
												...d,
												type: value[0],
												typeId: value[1],
												id: "",
												value: "",
												price: ""
											};
										} else {
											return {
												...d
											};
										}
									}),
									media: t.media
								};
							} else {
								return {...t};
							}
						})
					};
				})
			}
		}), () => {
		});
	}

	deleteTableSet(id) {
		this.setState(prevState => ({
			...prevState,
			planogramSet: {
				...prevState.planogramSet,
				tableSets: this.state.planogramSet.tableSets.filter(i => `${i.id}` !== `${id}`)
			}
		}));
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/planogram-set");
		} else {
			this.setState(prevState => ({
				...prevState,
				planogramSet: {
					id: props.id,
					active: props.active ? "1" : "2",
					tableSets: props.planogramTableSets.map(ts => {
						let mediaTypes = [];
						let media = [];
						if (ts.tableType !== null && this.state.tableTypes.length > 0)
							mediaTypes = this.state.tableTypes.find(t => `${t.id}` === `${ts.tableType.id}`).mediaTypes;
						mediaTypes.forEach((element, key) => {
							for (let m = 0; m < element.qty; m++) {
								media.push({
									name: `media-${getUniqueString()}`,
									index: key + m + 1,
									label: `${element.code}${m + 1}`,
									id: null,
									value: "",
									mediaType: {
										id: element.id,
										title: element.title
									}
								});
							}
						});
						return {
							id: `tableSet-${getUniqueString()}`,
							qty: ts.planogramTableSetTables.length,
							tableTypeId: ts.tableType ? ts.tableType.id : "",
							tableSizeId: ts.tableSize ? ts.tableSize.id : "",
							branchTypeId: ts.shopType ? ts.shopType.id : "",
							capacity: ts.tableSize ? ts.tableSize.capacity : "",
							tables: ts.planogramTableSetTables.map(t => {
								return {
									devices: t.planogramTableSetTableDevices.map(d => {
										let type, typeId, id, value, price;
										if (d.device === null) {
											type = "media";
											typeId = d.medium ? d.medium.mediaMediaTypes[0].mediaType.id : "";
											id = d.medium ? d.medium.id : null;
											value = d.medium ? d.medium.desc : "";
											price = "";
										} else {
											type = "device";
											typeId = d.device ? d.device.deviceCategory.id : "";
											id = d.device ? d.device.id : null;
											value = d.device ? (d.device.deviceModel ? d.device.deviceModel.title : "") : "";
											price = d.device ? d.device.price : "";
										}
										return {
											name: `devices-${getUniqueString()}`,
											index: d.index,
											label: d.label,
											type: type,
											typeId: typeId,
											id: id,
											value: value,
											price: price
										};
									}),
									media: media.map((m, k) => {
										return {
											...m,
											id: t.planogramTableSetTableMedia[k] ? (t.planogramTableSetTableMedia[k].medium === null ? null : t.planogramTableSetTableMedia[k].medium.id) : m.id,
											value: t.planogramTableSetTableMedia[k] ? (t.planogramTableSetTableMedia[k].medium === null ? "" : t.planogramTableSetTableMedia[k].medium.desc) : m.value
										};
									})
								};
							})
						};
					})
				}
			}), () => {
			});
		}
	}
	onCancel() {
		this.props.history.push("/planogram-set");
	}
	onAccept() {
		this.props.planogramActions
			.editPlanogram({
				id: this.state.planogramSet.id,
				active: this.state.planogramSet.active,
				tableSets: this.state.planogramSet.tableSets
			})
			.then(() => {
				alert("ระบบได้ทำการอัพเดตเรียบร้อย");
				// this.onCancel();
			})
			.catch(error => {
				// show error alert
			});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="planogram"
								title="Planogram Set"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-20 bt" />
					</div>
					<Hr />
					<FormPlanogramSet
						onChange={this.onChange}
						openManage={this.openManage}
						openArrangeTable={this.openArrangeTable}
						closeArrangeTable={this.closeArrangeTable}
						addTableSet={this.addTableSet}
						deleteTableSet={this.deleteTableSet}
						onChangeSelect={this.onChangeSelect}
						onChangeSelectDevice={this.onChangeSelectDevice}
						data={this.state}
						onAccept={this.onAccept}
					/>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let planograms = state.planograms.filter(i => `${i.id}` === `${id}`);
	let planogram = planograms.length > 0 ? planograms[0] : { error: true };
	return {
		planogram: planogram,
		tableTypes: state.tableTypes,
		tableSizes: state.tableSizes,
		branchTypes: state.branchTypes,
		devices: state.devices,
		deviceCategories: state.deviceCategories,
		medias: state.medias,
		mediaTypes: state.mediaTypes,
		options_model_media: state.medias.map(i => {
			return {
				value: i.id,
				label: i.title,
				mediaTypes: i.mediaTypes,
				tableTypes: i.tableTypes
			};
		}),
		options_device: [
			{
				label: "media",
				options: state.mediaTypes.map(i => {
					return {
						value: `media-${i.id}`,
						label: i.title
					};
				})
			},
			{
				label: "device",
				options: state.deviceCategories.map(i => {
					return {
						value: `device-${i.id}`,
						label: i.title
					};
				})
			}
		],
		options_model: [
			{
				label: "media",
				options: state.medias.map(i => {
					return {
						value: i.id,
						label: i.title,
						typeInfo: i.mediaTypes,
						type: "media"
					};
				})
			},
			{
				label: "device",
				options: state.devices.map(i => {
					let name = i.deviceModel ? i.deviceModel.title : "";
					let color = i.deviceColor ? i.deviceColor.title : "";
					let gb = i.deviceCapacity ? i.deviceCapacity.title : "";
					return {
						value: i.id,
						label: `${name} ${color} ${gb}`.trim(),
						typeInfo: i.deviceCategory,
						type: "devices",
						price: i.price
					};
				})
			}
		]
	};
};

const mapDispatchToProps = dispatch => {
	return {
		tableTypeActions: bindActionCreators(tableTypeActions, dispatch),
		tableSizeActions: bindActionCreators(tableSizeActions, dispatch),
		branchTypeActions: bindActionCreators(branchTypeActions, dispatch),
		deviceActions: bindActionCreators(deviceActions, dispatch),
		deviceCategoryActions: bindActionCreators(deviceCategoryActions, dispatch),
		mediaActions: bindActionCreators(mediaActions, dispatch),
		mediaTypeActions: bindActionCreators(mediaTypeActions, dispatch),
		planogramActions: bindActionCreators(planogramActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
