import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import { planogramActions } from "../../../actions";
import { planogramsFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import PlanogramSetList from "../../commons/planogramSetList";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		planograms: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false
		};
		this.importBoxOpen = this.importBoxOpen.bind(this);
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	componentDidMount() {
		if (this.props.planograms.length === 0) {
			this.props.planogramActions.getPlanograms();
		}
	}

	importBoxOpen() {
		this.setState(prevState => ({
			...prevState,
			importBox: !prevState.importBox
		}));
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.planogramActions.deletePlanogram({
				id: id
			}).then(() => {
				resolve();
			}).catch(error => {
				// show error alert
				reject();
			});
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="planogram" title="Planogram  Set" line />
						</div>
						<div className="col-md-6 text-right mt-20 bt">
							<Button btnType="default" iconLeft="icon-export">Export Planogram Set</Button>
							<Button onClick={() => this.nextPath("planogram-set/add")} btnType="default" iconLeft="icon-add">Add Planogram Set</Button>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<PlanogramSetList onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		planograms: state.planograms
	};
};

const mapDispatchToProps = dispatch => {
	return {
		planogramActions: bindActionCreators(planogramActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
