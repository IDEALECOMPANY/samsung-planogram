import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { deviceStatusActions } from "../../../actions";
import { postsFormatter } from "../../../selectors";
import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormDeviceStatus from "../../commons/formDeviceStatus";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		deviceStatuses: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			title: "",
			active: 1
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.deviceStatuses === null) {
			this.onCancel();
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (this.state.title !== "") {
			this.props.deviceStatusActions.addDeviceStatus({
				title: this.state.title,
				active: `${this.state.active}` === "1"
			}).then(() => {
				this.onCancel();
			}).catch(error => {
				// show error alert
			});
		} else {
			// error alert
		}
	}

	onCancel() {
		this.props.history.push("/device/status");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="device" title="Real Set" line />
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<FormDeviceStatus view data={this.state} onChange={this.onChange} />
						<div className="col-md-12 text-center bt">
							<Button onClick={this.onCancel} className="pull-left" btnType="cancel" children="Cancle" />
							<Button onClick={this.onAccept} className="pull-left" btnType="default" children="Save" />
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		deviceStatuses: state.deviceStatuses
	};
};

const mapDispatchToProps = dispatch => {
	return {
		deviceStatusActions: bindActionCreators(deviceStatusActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
