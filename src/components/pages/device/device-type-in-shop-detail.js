import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { deviceTypeShopActions } from "../../../actions";
import { postsFormatter } from "../../../selectors";
import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormDeviceTypeShop from "../../commons/formDeviceTypeShop";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		deviceTypeShop: null,
		deviceTypeShops: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			id: "",
			title: "",
			active: ""
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.deviceTypeShops === null) {
			this.onCancel();
		}
		if (this.props.deviceTypeShop !== null) {
			this.checkProps(this.props.deviceTypeShop);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.deviceTypeShop !== nextProps.deviceTypeShop) {
			this.checkProps(nextProps.deviceTypeShop);
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/device/type");
		} else {
			this.setState(prevState => ({
				...prevState,
				id: props.id,
				title: props.title,
				active: props.active ? "1" : "2"
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (this.state.title !== "") {
			this.props.deviceTypeShopActions
				.editDeviceTypeShop({
					id: this.state.id,
					title: this.state.title,
					active: `${this.state.active}` === "1",
					createdAt: this.props.deviceTypeShop.createdAt,
					updatedAt: new Date()
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			// error alert
		}
	}

	onCancel() {
		this.props.history.push("/device/type");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="device"
								title="Flag Ship - 1"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<FormDeviceTypeShop
							view
							data={this.state}
							onChange={this.onChange}
						/>
						<div className="col-md-12 text-center bt">
							<Button
								onClick={this.onCancel}
								className="pull-left "
								btnType="cancel"
								children="Cancle"
							/>
							<Button
								onClick={this.onAccept}
								className="pull-left "
								btnType="default"
								children="Save"
							/>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let deviceTypeShops = state.deviceTypeShops.filter(
		i => `${i.id}` === `${id}`
	);

	let deviceTypeShop =
		deviceTypeShops.length > 0 ? deviceTypeShops[0] : { error: true };

	return {
		deviceTypeShop: deviceTypeShop,
		deviceTypeShops: state.deviceTypeShops
	};
};

const mapDispatchToProps = dispatch => {
	return {
		deviceTypeShopActions: bindActionCreators(
			deviceTypeShopActions,
			dispatch
		)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
