import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { deviceTypeShopActions } from "../../../actions";
import { postsFormatter } from "../../../selectors";
import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormDeviceTypeShop from "../../commons/formDeviceTypeShop";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		deviceTypeShops: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			title: "",
			active: 1
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.deviceTypeShops === null) {
			this.onCancel();
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (this.state.title !== "") {
			this.props.deviceTypeShopActions.addDeviceTypeShop({
				title: this.state.title,
				active: `${this.state.active}` === "1"
			}).then(() => {
				this.onCancel();
			}).catch(error => {
				// show error alert
			});
		} else {
			// error alert
		}
	}

	onCancel() {
		this.props.history.push("/device/type");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="device" title="New Device Type Shop" line />
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<FormDeviceTypeShop view data={this.state} onChange={this.onChange} />
						<div className="col-md-12 text-center bt">
							<Button onClick={this.onCancel} className="pull-left" btnType="cancel" children="Cancle" />
							<Button onClick={this.onAccept} className="pull-left" btnType="default" children="Save" />
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		deviceTypeShops: state.deviceTypeShops
	};
};

const mapDispatchToProps = dispatch => {
	return {
		deviceTypeShopActions: bindActionCreators(deviceTypeShopActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
