import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import {
	deviceActions,
	deviceCategoryActions,
	deviceTypeShopActions,
	deviceStatusActions,
	deviceColorActions,
	deviceModelActions,
	fileActions
} from "../../../actions";
import { devicesFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import DeviceList from "../../commons/deviceList";

const Div = styled.div`
	width: 100%;
	position: relative;

	.btn {
		&.btn-toggle {
			line-height: 1;
			background-color: transparent;
			color: #7281d6;
			padding-right: 0px;
			margin-top: 20px;
			text-align: right;
			width: 100px;
			height: 40px;
			position: relative;
			&:focus {
				outline: none;
				-webkit-box-shadow: none;
			}
			img {
				position: absolute;
				right: 0;
				z-index: 99;
			}
			~ .box {
				display: block;
			}
		}
	}
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 250px;
		margin-top: 5px;
		position: absolute;
		right: 15px;
		margin-top: 0px;
		z-index: 99;
		-webkit-transition: all 0.2s ease-out;
		transition: all 0.2s ease-out;
		&:hover {
			display: block;
		}

		li {
			display: list-item;
			list-style: none;
			padding: 15px 20px;
			border-bottom: 1px solid #e5e5e5;
			font-size: 14px;
			text-align: left;
			cursor: pointer;
			color: ${props => props.theme.color.black};

			.d-text {
				margin-left: 15px;
			}

			&:hover {
				color: ${props => props.theme.color.primary};
			}

			.icon {
				width: 20px;
			}
			li:last-child {
				border-bottom: 1px solid transparent;
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		devices: [],
		deviceCategories: [],
		deviceTypeShops: [],
		deviceStatuses: [],
		deviceColors: [],
		deviceModels: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false,
			file: null
		};
		this.importBoxOpen = this.importBoxOpen.bind(this);
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onCancelImport = this.onCancelImport.bind(this);
		this.onAcceptImport = this.onAcceptImport.bind(this);
		this.onChangeImport = this.onChangeImport.bind(this);
		this.fileUpload = this.fileUpload.bind(this);
	}

	componentDidMount() {
		if (this.props.devices.length === 0) {
			this.props.deviceActions.getDevices();
		}
		if (this.props.deviceCategories.length === 0) {
			this.props.deviceCategoryActions.getDeviceCategories();
		}
		if (this.props.deviceStatuses.length === 0) {
			this.props.deviceStatusActions.getDeviceStatuses();
		}
		if (this.props.deviceTypeShops.length === 0) {
			this.props.deviceTypeShopActions.getDeviceTypeShops();
		}
		this.props.deviceColorActions.getDeviceColors();
		this.props.deviceModelActions.getDeviceModels();
	}

	onCancelImport() {
		this.setState({
			importBox: false
		});
	}

	onAcceptImport(event) {
		event.preventDefault();
		this.fileUpload(this.state.file)
			.then(res => {
				this.props.deviceActions.getDevices();
				this.onCancelImport();
			})
			.catch(error => {
				alert("Import file error.");
			});
	}

	fileUpload(file) {
		const formData = new FormData();
		formData.append("file", file);
		return this.props.fileActions.uploadFile(formData, "device");
	}

	onChangeImport(event) {
		this.setState({
			file: event.target.files[0]
		});
	}

	importBoxOpen() {
		this.setState(prevState => ({
			...prevState,
			importBox: !prevState.importBox
		}));
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.deviceActions
				.deleteDevice({
					id: id
				})
				.then(() => {
					resolve();
				})
				.catch(error => {
					// show error alert
					reject();
				});
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="device" title="Device" line />
						</div>
						<div className="col-md-6 text-right">
							<button
								onMouseOver={this.toggle}
								className="btn btn-toggle"
							>
								<img src="assets/images/icon-dropdown.png" />
							</button>
							{this.state.showDropdown && (
								<ul className="box">
									<li onClick={() => this.nextPath("/device/add")}>
										<img className="icon" src="assets/images/icon-add.png" />
										<span className="d-text">Add Device</span>
									</li>
									<li onClick={this.importBoxOpen}>
										<img className="icon" src="assets/images/import.png" />
										<span className="d-text">Import Device</span>
									</li>
									<li>
										<img className="icon" src="assets/images/icon-export.png" />
										<span className="d-text">Export Device</span>
									</li>
								</ul>
							)}
						</div>
					</div>
					<Hr />
					<div className="table-box">
						{this.state.importBox && (
							<ImportBox
								title="Import Device"
								sheetname="device"
								onChangeImport={this.onChangeImport}
								onCancelImport={this.onCancelImport}
								onAcceptImport={this.onAcceptImport}
							/>
						)}
						<DeviceList onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		devices: state.devices,
		deviceCategories: state.deviceCategories,
		deviceTypeShops: state.deviceTypeShops,
		deviceStatuses: state.deviceStatuses,
		deviceColors: state.deviceColors,
		deviceModels: state.deviceModels
	};
};

const mapDispatchToProps = dispatch => {
	return {
		deviceActions: bindActionCreators(deviceActions, dispatch),
		deviceCategoryActions: bindActionCreators(deviceCategoryActions, dispatch),
		deviceTypeShopActions: bindActionCreators(deviceTypeShopActions, dispatch),
		deviceStatusActions: bindActionCreators(deviceStatusActions, dispatch),
		deviceColorActions: bindActionCreators(deviceColorActions, dispatch),
		deviceModelActions: bindActionCreators(deviceModelActions, dispatch),
		fileActions: bindActionCreators(fileActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
