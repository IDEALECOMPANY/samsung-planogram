import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

import { deviceActions } from "../../../actions";
import { devicesFormatter } from "../../../selectors";

import Textfield from "../../commons/textfield";
import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import SelectOption from "../../commons/selectOption";
import Textarea from "../../commons/textarea";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Panel from "../../commons/panel";
import Topbar from "../../commons/topBar";
import Nav from "../../commons/nav";
import Hr from "../../commons/hr";
import FormDevice from "../../commons/formDevice";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		deviceCategories: [],
		deviceColors: [],
		deviceStatuses: [],
		deviceTypeShops: [],
		deviceCapacities: [],
		deviceModels: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			barcode: "",
			modelCode: "",
			price: "",
			active: "1",
			deviceModel: "",
			deviceColor: "",
			deviceCapacity: "",
			deviceStatusId: "",
			deviceTypeInShopId: "",
			deviceCategoryId: "",
			deviceCategories: this.props.deviceCategories,
			deviceColors: this.props.deviceColors,
			deviceStatuses: this.props.deviceStatuses,
			deviceTypeShops: this.props.deviceTypeShops,
			deviceCapacities: this.props.deviceCapacities,
			deviceModels: this.props.deviceModels,
			mode: "add"
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
		this.onSelectColor = this.onSelectColor.bind(this);
		this.onSelectCapacity = this.onSelectCapacity.bind(this);
		this.onSelectModel = this.onSelectModel.bind(this);
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onSelectColor(text) {
		this.setState({
			deviceColor: text
		});
	}

	onSelectCapacity(text) {
		this.setState({
			deviceCapacity: text
		});
	}

	onSelectModel(text) {
		this.setState({
			deviceModel: text
		});
	}

	onAccept() {
		if (
			this.state.deviceModel !== "" ||
			this.state.deviceStatusId !== "" ||
			this.state.deviceTypeInShopId !== "" ||
			this.props.deviceCategoryId !== ""
		) {
			this.props.deviceActions
				.addDevice({
					barcode: this.state.barcode,
					modelCode: this.state.modelCode,
					price: this.state.price,
					active: `${this.state.active}` === "1",
					deviceModel: this.state.deviceModel,
					deviceColor: this.state.deviceColor,
					deviceCapacity: this.state.deviceCapacity,
					deviceStatusId: this.state.deviceStatusId,
					deviceTypeInShopId: this.state.deviceTypeInShopId,
					deviceCategoryId: this.state.deviceCategoryId
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			// error alert
		}
	}

	onCancel() {
		this.props.history.push("/device");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="device"
								title="New Device"
								line={true}
							/>
						</div>
						<div className="col-md-6 text-right mt-15">
							<Button
								btnType="default"
								children="Export pdf"
							/>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<h3 className="title-page color-primary">
							Device Info
						</h3>
						<FormDevice
							view
							data={this.state}
							onSelectColor={this.onSelectColor}
							onSelectCapacity={this.onSelectCapacity}
							onSelectModel={this.onSelectModel}
							onChange={this.onChange}
						/>
						<div className="col-md-12 text-center bt">
							<Button
								onClick={this.onCancel}
								className="pull-left "
								btnType="cancel"
								children="Cancel"
							/>
							<Button
								onClick={this.onAccept}
								className="pull-left "
								btnType="default"
								children="Save"
							/>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		deviceCategories: state.deviceCategories,
		deviceColors: state.deviceColors,
		deviceStatuses: state.deviceStatuses,
		deviceTypeShops: state.deviceTypeShops,
		deviceCapacities: state.deviceCapacities,
		deviceModels: state.deviceModels
	};
};

const mapDispatchToProps = dispatch => {
	return {
		deviceActions: bindActionCreators(deviceActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
