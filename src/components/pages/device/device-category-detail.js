import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { deviceCategoryActions } from "../../../actions";
import { postsFormatter } from "../../../selectors";
import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormDeviceCategory from "../../commons/formDeviceCategory";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		deviceCategory: null,
		deviceCategories: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			id: "",
			title: "",
			active: ""
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.deviceCategories === null) {
			this.onCancel();
		}
		if (this.props.deviceCategory !== null) {
			this.checkProps(this.props.deviceCategory);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.deviceCategory !== nextProps.deviceCategory) {
			this.checkProps(nextProps.deviceCategory);
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/device/category");
		} else {
			this.setState(prevState => ({
				...prevState,
				id: props.id,
				title: props.title,
				active: props.active ? "1" : "2"
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (this.state.title !== "") {
			this.props.deviceCategoryActions
				.editDeviceCategory({
					id: this.state.id,
					title: this.state.title,
					active: `${this.state.active}` === "1",
					createdAt: this.props.deviceCategory.createdAt,
					updatedAt: new Date()
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			// error alert
		}
	}

	onCancel() {
		this.props.history.push("/device/category");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="device" title="Mobile" line />
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<FormDeviceCategory
							view
							data={this.state}
							onChange={this.onChange}
						/>
						<div className="col-md-12 text-center bt">
							<Button
								onClick={this.onCancel}
								className="pull-left "
								btnType="cancel"
								children="Cancle"
							/>
							<Button
								className="pull-left "
								btnType="default"
								children="Save"
								onClick={this.onAccept}
							/>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let deviceCategories = state.deviceCategories.filter(
		i => `${i.id}` === `${id}`
	);

	let deviceCategory =
		deviceCategories.length > 0 ? deviceCategories[0] : { error: true };

	return {
		deviceCategory: deviceCategory,
		deviceCategories: state.deviceCategories
	};
};

const mapDispatchToProps = dispatch => {
	return {
		deviceCategoryActions: bindActionCreators(
			deviceCategoryActions,
			dispatch
		)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
