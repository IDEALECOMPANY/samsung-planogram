import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";

import { deviceActions } from "../../../actions";
import { devicesFormatter } from "../../../selectors";

import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormDevice from "../../commons/formDevice";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		device: "",
		deviceCategories: [],
		deviceColors: [],
		deviceStatuses: [],
		deviceTypeShops: [],
		deviceCapacities: [],
		deviceModels: [],
		deviceTitle: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			id: "",
			barcode: "",
			modelCode: "",
			price: "",
			active: "",
			deviceModel: "",
			deviceColor: "",
			deviceCapacity: "",
			deviceStatusId: "",
			deviceTypeInShopId: "",
			deviceCategoryId: "",
			deviceCategories: this.props.deviceCategories,
			deviceColors: this.props.deviceColors,
			deviceStatuses: this.props.deviceStatuses,
			deviceTypeShops: this.props.deviceTypeShops,
			deviceCapacities: this.props.deviceCapacities,
			deviceModels: this.props.deviceModels,
			mode: "edit"
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
		this.onSelectColor = this.onSelectColor.bind(this);
		this.onSelectCapacity = this.onSelectCapacity.bind(this);
		this.onSelectModel = this.onSelectModel.bind(this);
	}

	componentDidMount() {
		if (this.props.deviceCategories.length === 0 || this.props.deviceStatuses.length === 0 || this.props.deviceTypeShops.length === 0) {
			this.onCancel();
		}

		if (this.props.device !== null) {
			this.checkProps(this.props.device);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.device !== nextProps.device) {
			this.checkProps(nextProps.device);
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/device");
		} else {
			this.setState(prevState => ({
				...prevState,
				id: props.id,
				barcode: props.barcode,
				modelCode: props.modelCode,
				price: props.price,
				active: props.active ? "1" : "2",
				deviceModel: props.deviceModel ? props.deviceModel.title : "",
				deviceColor: props.deviceColor ? props.deviceColor.title : "",
				deviceCapacity: props.deviceCapacity ? props.deviceCapacity.title : "",
				deviceStatusId: props.deviceStatus ? props.deviceStatus.id : "",
				deviceTypeInShopId: props.deviceTypeInShop ? props.deviceTypeInShop.id : "",
				deviceCategoryId: props.deviceCategory ? props.deviceCategory.id : "",
				deviceCategories: this.props.deviceCategories,
				deviceColors: this.props.deviceColors,
				deviceStatuses: this.props.deviceStatuses,
				deviceTypeShops: this.props.deviceTypeShops,
				deviceCapacities: this.props.deviceCapacities,
				deviceModels: this.props.deviceModels
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onSelectColor(text) {
		this.setState({
			deviceColor: text
		});
	}

	onSelectCapacity(text) {
		this.setState({
			deviceCapacity: text
		});
	}

	onSelectModel(text) {
		this.setState({
			deviceModel: text
		});
	}

	onAccept() {
		if (this.state.deviceModel !== "" || this.state.deviceStatusId !== "" || this.state.deviceTypeInShopId !== "" || this.props.deviceCategoryId !== "") {
			this.props.deviceActions.editDevice({
				id: this.state.id,
				barcode: this.state.barcode,
				modelCode: this.state.modelCode,
				price: this.state.price,
				active: `${this.state.active}` === "1",
				deviceModel: this.state.deviceModel,
				deviceColor: this.state.deviceColor,
				deviceCapacity: this.state.deviceCapacity,
				deviceStatusId: this.state.deviceStatusId,
				deviceTypeInShopId: this.state.deviceTypeInShopId,
				deviceCategoryId: this.state.deviceCategoryId,
				createdAt: this.props.device.createdAt,
				updatedAt: new Date()
			}).then(() => {
				this.onCancel();
			}).catch(error => {
				// show error alert
			});
		} else {
			// error alert
		}
	}

	onCancel() {
		this.props.history.push("/device");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="device" title={this.props.deviceTitle} line />
						</div>
						<div className="col-md-6 text-right mt-15">
							<Button btnType="default" children="Export pdf" />
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<h3 className="title-page color-primary">Device Info</h3>
						<FormDevice view data={this.state} onSelectColor={this.onSelectColor} onSelectCapacity={this.onSelectCapacity} onSelectModel={this.onSelectModel} onChange={this.onChange} />
						<div className="col-md-12 text-center bt">
							<Button onClick={this.onCancel} className="pull-left" btnType="cancel" children="Cancle" />
							<Button onClick={this.onAccept} className="pull-left" btnType="default" children="Save" />
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let devices = state.devices.filter(i => `${i.id}` === `${id}`);

	let device = devices.length > 0 ? devices[0] : { error: true };
	let deviceTitle = devices.length > 0 ? `${devices[0].deviceModel.title} ${devices[0].deviceColor.title} ${devices[0].deviceCapacity.title}` : "";
	return {
		device: device,
		deviceCategories: state.deviceCategories,
		deviceColors: state.deviceColors,
		deviceStatuses: state.deviceStatuses,
		deviceTypeShops: state.deviceTypeShops,
		deviceCapacities: state.deviceCapacities,
		deviceTitle: deviceTitle,
		deviceModels: state.deviceModels
	};
};

const mapDispatchToProps = dispatch => {
	return {
		deviceActions: bindActionCreators(deviceActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
