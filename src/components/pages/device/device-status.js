import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";
import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import DeviceStatusList from "../../commons/deviceStatusList";

import { deviceStatusActions } from "../../../actions";

const Div = styled.div`
	width: 100%;
	position: relative;

	.btn {
		&.btn-toggle {
			line-height: 1;
			padding-top: 42px;
			background-color: transparent;
			color: #7281d6;

			&:focus {
				outline: none;
				-webkit-box-shadow: none;
			}
		}
	}
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 200px;
		margin-top: 5px;
		position: absolute;
		right: 15px;
		margin-top: 15px;
		z-index: 99;
		transition: all 0.2s ease-out;

		li {
			display: list-item;
			list-style: none;
			padding: 15px 20px;
			border-bottom: 1px solid #e5e5e5;
			font-size: 14px;
			text-align: left;
			cursor: pointer;
			color: ${props => props.theme.color.black};

			.d-text {
				margin-left: 15px;
			}

			&:hover {
				color: ${props => props.theme.color.primary};
			}

			.icon {
				width: 20px;
			}
			li:last-child {
				border-bottom: 1px solid transparent;
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = { deviceStatuses: [] };

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false
		};

		this.importBoxOpen = this.importBoxOpen.bind(this);
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	componentDidMount() {
		// if (this.props.deviceStatuses.length === 0) {
		this.props.deviceStatusActions.getDeviceStatuses();
		// }
	}

	importBoxOpen() {
		this.setState(prevState => ({
			...prevState,
			importBox: !prevState.importBox
		}));
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.deviceStatusActions.deleteDeviceStatus({
				id: id
			}).then(() => {
				resolve();
			}).catch(error => {
				// show error alert
				reject();
			});
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="device" title="Device" line />
						</div>
						<div className="col-md-6 text-right mt-20 bt">
							<Button btnType="default" iconLeft="icon-export">Export Device Status</Button>
							<Button onClick={() => this.nextPath("/device/status/add")} btnType="default" iconLeft="icon-add">Add Device Status</Button>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						{this.state.importBox && (
							<ImportBox title="Import Device" />
						)}
						<DeviceStatusList mode="edit" onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return { deviceStatuses: state.deviceStatuses };
};

const mapDispatchToProps = dispatch => {
	return {
		deviceStatusActions: bindActionCreators(deviceStatusActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
