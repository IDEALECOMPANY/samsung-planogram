import React, { Component } from "react";

import styled from "styled-components";

import Header from "../../commons/topBar";
import Menus from "../../commons/nav";

const Div = styled.div`
	width: 100%;
	position: relative;
	padding: 30px 20px 20px 20px;
	box-sizing: border-box;
	transition: all 0.2s ease-out;
	margin-top: 30px;
	font-size: 14px;

	.section {
		width: 100%;
		position: relative;

		&.section-body {
			padding: 30px;
		}

		&.section-menu {
			top: 0px;
			left: -200px;
			width: 200px;
			height: 100%;
			position: fixed;
			padding-top: 80px;
			overflow: auto;
			box-sizing: border-box;
			transition: all 0.2s ease-out;
			background-color: #f7f7f7;
		}

		&.section-header {
			top: 0px;
			left: 0px;
			width: 100%;
			height: 80px;
			position: fixed;
			z-index: 100;
		}
	}

	&.is-show {
		padding-left: 210px;

		.section {
			&.section-menu {
				left: 0px;
			}
		}
	}
`;

class Comp extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			isShowMenu: true
		};

		this.toggleMenu = this.toggleMenu.bind(this);
	}

	toggleMenu() {
		this.setState(prevState => ({
			...prevState,
			isShowMenu: !prevState.isShowMenu
		}));
	}

	render() {
		return (
			<Div className={`${this.state.isShowMenu ? "is-show" : ""}`}>
				<div className="section section-body">
					{this.props.children}
				</div>
				<div className="section section-menu">
					<Menus />
				</div>
				<div className="section section-header">
					<Header toggleMenu={this.toggleMenu} />
				</div>
			</Div>
		);
	}
}

export default Comp;
