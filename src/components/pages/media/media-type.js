import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import { mediaTypeActions } from "../../../actions";
import { mediaTypesFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import MediaTypeList from "../../commons/mediaTypeList";

const Div = styled.div`
	position: relative;
	width: 100%;
`;

export class Page extends Component {
	static defaultProps = {
		mediaTypes: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false
		};
		this.importBoxOpen = this.importBoxOpen.bind(this);
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	componentDidMount() {
		if (this.props.mediaTypes.length === 0) {
			this.props.mediaTypeActions.getMediaTypes();
		}
	}

	importBoxOpen() {
		this.setState(prevState => ({
			...prevState,
			importBox: !prevState.importBox
		}));
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.mediaTypeActions
				.deleteMediaType({
					id: id
				})
				.then(() => {
					resolve();
				})
				.catch(error => {
					// show error alert
					reject();
				});
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="media"
								title="Media Type"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-20 bt">
							<Button
								btnType="default"
								iconLeft="icon-export"
							>
								Export Media Type{" "}
							</Button>
							<Button
								onClick={() =>
									this.nextPath("/media/type/add")
								}
								btnType="default"
								iconLeft="icon-add"
							>
								Add Media Type
							</Button>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<MediaTypeList onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		mediaTypes: state.mediaTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		mediaTypeActions: bindActionCreators(mediaTypeActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
