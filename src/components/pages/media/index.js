import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";
import {
	mediaActions,
	tableTypeActions,
	mediaTypeActions,
	mediaSizeActions
} from "../../../actions";
import { mediasFormatter, getUniqueString } from "../../../selectors";
import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import MediaList from "../../commons/mediaList";
import MySearchField from "../../commons/MySearchField";

const Div = styled.div`
	position: relative;
	width: 100%;
`;

export class Page extends Component {
	static defaultProps = {
		medias: [],
		tableTypes: [],
		mediaTypes: [],
		mediaSizes: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false
		};
		this.importBoxOpen = this.importBoxOpen.bind(this);
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	componentDidMount() {
		if (this.props.medias.length === 0) {
			this.props.mediaActions.getMedias();
		}
		if (this.props.tableTypes.length === 0) {
			this.props.tableTypeActions.getTableTypes();
		}
		if (this.props.mediaTypes.length === 0) {
			this.props.mediaTypeActions.getMediaTypes();
		}
		if (this.props.mediaSizes.length === 0) {
			this.props.mediaSizeActions.getMediaSizes();
		}
	}

	importBoxOpen() {
		this.setState(prevState => ({
			...prevState,
			importBox: !prevState.importBox
		}));
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.mediaActions.deleteMedia({
				id: id
			}).then(() => {
				resolve();
			}).catch(error => {
				// show error alert
				reject();
			});
		});
	}

	render() {
		const options = {
			sizePerPageList: [
				{
					text: "6",
					value: 6
				}, {
					text: "20",
					value: 20
				}, {
					text: "All",
					value: this.props.medias.length
				}
			],
			sizePerPage: 6,
			prePage: "Previous", // Previous page button text
			nextPage: "Next", // Next page button text
			firstPage: "First", // First page button text
			lastPage: "Last", // Last page button text
			searchField: props => <MySearchField text="search" {...props} />
		};

		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="media" title="Media" line />
						</div>
						<div className="col-md-6 text-right mt-20 bt">
							<Button btnType="default" iconLeft="icon-export">Export Media </Button>
							<Button btnType="default" iconLeft="icon-add" onClick={() => this.nextPath("/media/add")}>Add Media </Button>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<MediaList onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		medias: state.medias,
		tableTypes: state.tableTypes,
		mediaTypes: state.mediaTypes,
		mediaSizes: state.mediaSizes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		mediaActions: bindActionCreators(mediaActions, dispatch),
		tableTypeActions: bindActionCreators(tableTypeActions, dispatch),
		mediaTypeActions: bindActionCreators(mediaTypeActions, dispatch),
		mediaSizeActions: bindActionCreators(mediaSizeActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
