import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

import { mediaSizeActions } from "../../../actions";
import { mediaSizesFormatter } from "../../../selectors";

import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import FormMediaSize from "../../commons/formMediaSize";

const Div = styled.div`
	position: relative;
	width: 100%;
`;

export class Page extends Component {
	static defaultProps = {
		mediaSize: [],
		mediaSizes: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			title: "",
			active: "1"
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.mediaSizes === null) {
			this.onCancel();
		}
	}

	filterPosts(posts) {
		return posts.filter(i => i.title.indexOf(this.state.searchTxt) !== -1 || i.msg.indexOf(this.state.searchTxt) !== -1);
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (this.state.title !== "") {
			this.props.mediaSizeActions.addMediaSize({
				title: this.state.title,
				active: `${this.state.active}` === "1"
			}).then(() => {
				this.onCancel();
			}).catch(error => {
				// show error alert
			});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/media/size");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="media" title="New Media Size" line />
						</div>
						<div className="col-md-6 text-right mt-15">
							{/* <Button btnType="default" children="Export pdf" /> */}
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<div className="row">
							<FormMediaSize view data={this.state} onChange={this.onChange} />
							<div className="col-md-12 text-center bt">
								<Button onClick={this.onCancel} className="pull-left" btnType="cancel" children="Cancle" />
								<Button onClick={this.onAccept} className="pull-left" btnType="default" children="Submit" />
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		mediaSizes: state.mediaSizes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		mediaSizeActions: bindActionCreators(mediaSizeActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
