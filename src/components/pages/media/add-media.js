import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { mediaActions } from "../../../actions";
import { mediasFormatter, getUniqueString } from "../../../selectors";
import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import Titlepage from "../../commons/titlepage";
import Hr from "../../commons/hr";
import FormMedia from "../../commons/formMedia";

const Div = styled.div`
	position: relative;
	width: 100%;
`;

export class Page extends Component {
	static defaultProps = {
		mediaTypes: null,
		tableTypes: null,
		mediaSizes: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			search: "",
			image: "",
			title: "",
			desc: "",
			material: "",
			tableTypes: [],
			mediaTypes: [],
			mediaSizes: [],
			active: "1",
			tableTypesOptions: this.props.tableTypes,
			mediaTypesOptions: this.props.mediaTypes,
			mediaSizesOptions: this.props.mediaSizes
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
		this.onChangeImage = this.onChangeImage.bind(this);
		this.onAddMediaType = this.onAddMediaType.bind(this);
		this.onDeleteMediaType = this.onDeleteMediaType.bind(this);
		this.onAddTableType = this.onAddTableType.bind(this);
		this.onDeleteTableType = this.onDeleteTableType.bind(this);
		this.onAddMediaSize = this.onAddMediaSize.bind(this);
		this.onDeleteMediaSize = this.onDeleteMediaSize.bind(this);
		this.searchMediaSize = this.searchMediaSize.bind(this);
	}

	componentDidMount() {
		if (this.props.mediaTypes === null) {
			this.onCancel();
		} else {
			this.setState(prevState => ({
				...prevState,
				mediaTypesOptions: this.props.mediaTypes.map(i => ({
					id: i.id,
					title: i.title
				}))
			}));
		}

		if (this.props.tableTypes === null) {
			this.onCancel();
		} else {
			this.setState(prevState => ({
				...prevState,
				tableTypesOptions: this.props.tableTypes.map(i => ({
					id: i.id,
					title: i.title
				}))
			}));
		}

		if (this.props.mediaSizes === null) {
			this.onCancel();
		} else {
			this.setState(prevState => ({
				...prevState,
				mediaSizesOptions: this.props.mediaSizes.map(i => ({
					id: i.id,
					title: i.title
				}))
			}));
		}
	}

	filterPosts(posts) {
		return posts.filter(
			i =>
				i.title.indexOf(this.state.searchTxt) !== -1 ||
				i.msg.indexOf(this.state.searchTxt) !== -1
		);
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;
		let zone = event.zone;

		if (zone === "mediaTypes") {
			this.setState({
				mediaTypes: this.state.mediaTypes.map((i, k) => {
					if (i.name === name) {
						return {
							...i,
							selectedOption: value
						};
					} else {
						return i;
					}
				})
			});
		} else if (zone === "tableTypes") {
			this.setState({
				tableTypes: this.state.tableTypes.map((i, k) => {
					if (i.name === name) {
						return {
							...i,
							selectedOption: value
						};
					} else {
						return i;
					}
				})
			});
		} else if (zone === "mediaSizes") {
			this.setState({
				mediaSizes: this.state.mediaSizes.map((i, k) => {
					if (i.name === name) {
						return {
							...i,
							selectedOption: value,
							label: this.state.mediaSizesOptions.find(
								i => `${i.id}` === `${value}`
							).title
						};
					} else {
						return i;
					}
				})
			});
		} else if (zone === "qty") {
			this.setState({
				mediaSizes: this.state.mediaSizes.map((i, k) => {
					if (i.nameQty === name) {
						return {
							...i,
							qty: value
						};
					} else {
						return i;
					}
				})
			});
		} else {
			this.setState(prevState => ({ ...prevState, [name]: value }));
		}
	}

	onChangeImage(event) {
		let reader = new FileReader();
		let file = event.target.files[0];
		let name = event.target.name;
		reader.onloadend = () => {
			this.setState(prevState => ({
				...prevState,
				[name]: reader.result
			}));
		};
		reader.readAsDataURL(file);
	}

	onAccept() {
		if (this.state.title !== "") {
			this.props.mediaActions
				.addMedia({
					image: this.state.image,
					title: this.state.title,
					desc: this.state.desc,
					material: this.state.material,
					tableTypes: this.state.tableTypes.map(
						i => i.selectedOption
					),
					mediaTypes: this.state.mediaTypes.map(
						i => i.selectedOption
					),
					mediaSizes: this.state.mediaSizes.map(i => {
						return {
							id: i.selectedOption,
							qty: i.qty
						};
					}),
					active: `${this.state.active}` === "1"
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/media");
	}

	onAddMediaType() {
		let uuid = getUniqueString();
		let selectBox = {
			id: uuid,
			name: `mediaTypes-${uuid}`,
			selectedOption: "",
			value: "",
			placeholder: "Media Type"
		};
		this.setState(prevState => ({
			...prevState,
			mediaTypes: [...prevState.mediaTypes, selectBox]
		}));
	}

	onDeleteMediaType(uuid) {
		this.setState({
			mediaTypes: this.state.mediaTypes.filter(
				i => `${i.id}` !== `${uuid}`
			)
		});
	}

	onAddTableType() {
		let uuid = getUniqueString();
		let selectBox = {
			id: uuid,
			name: `tableTypes-${uuid}`,
			selectedOption: "",
			value: "",
			placeholder: "Media Location (Table Type)"
		};
		this.setState(prevState => ({
			...prevState,
			tableTypes: [...prevState.tableTypes, selectBox]
		}));
	}

	onDeleteTableType(uuid) {
		this.setState({
			tableTypes: this.state.tableTypes.filter(
				i => `${i.id}` !== `${uuid}`
			)
		});
	}

	onAddMediaSize() {
		let uuid = getUniqueString();
		let selectBox = {
			id: uuid,
			name: `mediaSizes-${uuid}`,
			qty: "",
			nameQty: `qty-${uuid}`,
			selectedOption: "",
			label: "",
			value: "",
			placeholder: "Media Size"
		};
		this.setState(prevState => ({
			...prevState,
			mediaSizes: [...prevState.mediaSizes, selectBox]
		}));
	}

	onDeleteMediaSize(uuid) {
		this.setState({
			mediaSizes: this.state.mediaSizes.filter(
				i => `${i.id}` !== `${uuid}`
			)
		});
	}

	searchMediaSize(event) {
		let value = event.target.value;
		this.setState({ search: value });
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="media"
								title="New Media"
								line={true}
							/>
						</div>
						<div className="col-md-6 text-right mt-15">
							{/* <Button btnType="default" children="Export pdf" /> */}
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<div className="">
							<FormMedia
								view
								data={this.state}
								onChange={this.onChange}
								onChangeImage={this.onChangeImage}
								mediaTypesOptions={
									this.state.mediaTypesOptions
								}
								onAddMediaType={this.onAddMediaType}
								onDeleteMediaType={this.onDeleteMediaType}
								tableTypesOptions={
									this.state.tableTypesOptions
								}
								onAddTableType={this.onAddTableType}
								onDeleteTableType={this.onDeleteTableType}
								mediaSizesOptions={
									this.state.mediaSizesOptions
								}
								onAddMediaSize={this.onAddMediaSize}
								onDeleteMediaSize={this.onDeleteMediaSize}
								searchMediaSize={this.searchMediaSize}
							/>
							<div className="col-md-12 text-center bt">
								<br />
								<Button
									onClick={this.onCancel}
									className="pull-left "
									btnType="cancel"
									children="Cancle"
								/>
								<Button
									onClick={this.onAccept}
									className="pull-left "
									btnType="default"
									children="Submit"
								/>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		mediaTypes: state.mediaTypes,
		tableTypes: state.tableTypes,
		mediaSizes: state.mediaSizes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		mediaActions: bindActionCreators(mediaActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
