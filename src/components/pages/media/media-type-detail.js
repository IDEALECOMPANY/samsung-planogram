import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { mediaTypeActions } from "../../../actions";
import { mediaTypesFormatter } from "../../../selectors";
import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import FormMediaSet from "../../commons/formMediaSet";

const Div = styled.div`
	position: relative;
	width: 100%;
`;

export class Page extends Component {
	static defaultProps = {
		mediaType: null,
		mediaTypes: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			id: "",
			code: "",
			title: "",
			active: ""
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.mediaTypes === null) {
			this.onCancel();
		}
		if (this.props.mediaType !== null) {
			this.checkProps(this.props.mediaType);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.mediaType !== nextProps.mediaType) {
			this.checkProps(nextProps.mediaType);
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/media/type");
		} else {
			this.setState(prevState => ({
				...prevState,
				id: props.id,
				code: props.code,
				title: props.title,
				active: props.active ? "1" : "2"
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (this.state.title !== "") {
			this.props.mediaTypeActions
				.editMediaType({
					id: this.state.id,
					code: this.state.code,
					title: this.state.title,
					active: `${this.state.active}` === "1",
					createdAt: this.props.mediaType.createdAt,
					updatedAt: new Date()
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/media/type");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="media"
								title="New Media Type"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-15">
							{/* <Button btnType="default" children="Export pdf" /> */}
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<div className="row">
							<FormMediaSet
								view
								data={this.state}
								onChange={this.onChange}
							/>
							<div className="col-md-12 text-center bt">
								<Button
									onClick={this.onCancel}
									className="pull-left "
									btnType="cancel"
									children="Cancle"
								/>
								<Button
									onClick={this.onAccept}
									className="pull-left "
									btnType="default"
									children="Submit"
								/>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let mediaTypes = state.mediaTypes.filter(i => `${i.id}` === `${id}`);

	let mediaType = mediaTypes.length > 0 ? mediaTypes[0] : { error: true };

	return {
		mediaType: mediaType,
		mediaTypes: state.mediaTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		mediaTypeActions: bindActionCreators(mediaTypeActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
