import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import { mediaSizeActions } from "../../../actions";
import { mediaSizesFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import MediaSizeList from "../../commons/mediaSizeList";

const Div = styled.div`
	width: 100%;
	position: relative;
	.btn {
		&.btn-toggle {
			line-height: 1;
			padding-top: 42px;
			background-color: transparent;
			color: #7281d6;

			&:focus {
				outline: none;
				-webkit-box-shadow: none;
			}
		}
	}
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 200px;
		margin-top: 5px;
		position: absolute;
		right: 15px;
		margin-top: 15px;
		z-index: 99;
		transition: all 0.2s ease-out;

		li {
			display: list-item;
			list-style: none;
			padding: 15px 20px;
			border-bottom: 1px solid #e5e5e5;
			font-size: 14px;
			text-align: left;
			cursor: pointer;
			color: ${props => props.theme.color.black};

			.d-text {
				margin-left: 15px;
			}

			&:hover {
				color: ${props => props.theme.color.primary};
			}

			.icon {
				width: 20px;
			}
			li:last-child {
				border-bottom: 1px solid transparent;
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		mediaSizes: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false
		};
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	componentDidMount() {
		if (this.props.mediaSizes.length === 0) {
			this.props.mediaSizeActions.getMediaSizes();
		}
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.mediaSizeActions
				.deleteMediaSize({
					id: id
				})
				.then(() => {
					resolve();
				})
				.catch(error => {
					// show error alert
					reject();
				});
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="media"
								title="Media Size"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-20 bt">
							<Button
								btnType="default"
								iconLeft="icon-export"
							>
								Export Media Size
							</Button>
							<Button
								onClick={() =>
									this.nextPath("/media/size/add")
								}
								btnType="default"
								iconLeft="icon-add"
							>
								Add Media Size
							</Button>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<div className="text-right">
							<LastestLogin
								date="2017-10-25"
								time="14:42:38"
							/>
						</div>
						<MediaSizeList onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return { mediaSizes: state.mediaSizes };
};

const mapDispatchToProps = dispatch => {
	return {
		mediaSizeActions: bindActionCreators(mediaSizeActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
