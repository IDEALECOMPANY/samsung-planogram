import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import Textfield from "../../commons/textfield";
import { userActions } from "../../../actions";

const Div = styled.div`
	width: 100%;
	position: relative;

	.topbar {
		box-shadow: 1px 1px 6px 2px #d8d8d8;
		position: fixed;
		top: 0;
		width: 100%;
		background-color: #fff;
		z-index: 999;

		.top-logo {
			padding: 15px 30px;
			padding-left: 30px;
			width: 240px;
			float: left;
		}

		.toggle-menu {
			padding: 15px;
			cursor: pointer;

			img {
				margin-right: 5px;
				margin-top: -3px;
			}

			a {
				color: ${props => props.theme.color.primary};
				font-family: ${props => props.theme.text.bold};
			}

			i {
				margin-right: 10px;
			}
		}
	}

	.login-wrapper {
		position: relative;
		widows: 100%;
		height: 100vh;
		.box {
			    width: 500px;
				height: auto;
				position: absolute;
				top: 50%;
				left: 50%;
				-webkit-transform: translate(-50%,-50%);
				-ms-transform: translate(-50%,-50%);
				transform: translate(-50%,-50%);
				padding: 50px;
				background-color: #f7f7f7;

			h3{
				font-size: 22px;
			}
		}
	}

	button {
		height: 33px;
		border: 1px solid ${props => props.theme.color.primary};
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 30px;
		border-radius: 5px;
		color: ${props => props.theme.color.primary};

		&.btn {
			height: 33px;
			cursor: pointer;
			font-size: ${props => props.theme.font.small};
			padding: 0px 20px;
			border-radius: 5px;
			color: ${props => props.theme.color.primary};
			display: inline-block;
			line-height: 2;

			&.btn-primary {
				background-color: ${props => props.theme.bg.primary};
				border: 1px solid ${props => props.theme.border.primary};
				color: #fff;
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		user: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			user: "",
			pass: ""
		};

		this.login = this.login.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	componentDidMount() {
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.user !== null) {
			this.props.history.push("/");
		}
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	login() {
		if (this.state.user !== null && this.state.pass !== null) {
			this.props.userActions.login(this.state.user, this.state.pass);
		}
	}

	render() {
		return (
			<Div>
				<div className="login-wrapper">
					<div className=" topbar">
						<div className="top-logo">
							<img src="/assets/images/logo-samsung.png" />
						</div>
					</div>
					<div className="box">
						<label>
							<h3>Login</h3>{" "}
						</label>
						<div className="form-group">
							<Textfield
								type="text"
								placeholder="Email"
								name="user"
								value={this.state.user}
								onChange={this.onChange}
							/>
						</div>
						<div className="form-group">
							<Textfield
								type="password"
								placeholder="Password"
								name="pass"
								value={this.state.pass}
								onChange={this.onChange}
							/>
						</div>
						<button className="btn btn-primary" type="button" onClick={this.login}>Login</button>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		user: state.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		userActions: bindActionCreators(userActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
