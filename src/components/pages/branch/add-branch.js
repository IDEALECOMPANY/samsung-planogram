import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";

import { branchActions } from "../../../actions";
import { branchsFormatter } from "../../../selectors";

import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Textfield from "../../commons/textfield";
import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import SelectOption from "../../commons/selectOption";
import Textarea from "../../commons/textarea";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Panel from "../../commons/panel";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormBranch from "../../commons/formBranch";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		branchs: [],
		branchTypes: null,
		address: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			code: "",
			title: "",
			retailHub: "",
			customer: "",
			lat: "",
			lng: "",
			orientation: "1",
			active: "1",
			shopTypeId: "",
			regionId: null,
			provinceId: null,
			amphurId: null,
			districtId: null,
			branchTypes: this.props.branchTypes,
			address: this.props.address,
			provinces: [],
			amphurs: [],
			districts: []
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.branchTypes === null) {
			this.onCancel();
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.address !== nextProps.address) {
			this.setState({
				address: nextProps.address
			});
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;
		if (name == "regionId") {
			this.setState({
				provinceId: null,
				amphurId: null,
				districtId: null,
				provinces: this.state.address.find(i => `${i.id}` === `${value}`).provinces
			});
		} else if (name == "provinceId") {
			this.setState({
				amphurId: null,
				districtId: null,
				amphurs: this.state.provinces.find(i => `${i.id}` === `${value}`).amphurs
			});
		} else if (name == "amphurId") {
			this.setState({
				districtId: null,
				districts: this.state.amphurs.find(i => `${i.id}` === `${value}`).districts
			});
		}
		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (
			this.state.code !== "" ||
			this.state.title !== "" ||
			this.state.shopTypeId !== ""
		) {
			this.props.branchActions.addBranch({
				code: this.state.code,
				title: this.state.title,
				retailHub: this.state.retailHub,
				customer: this.state.customer,
				lat: this.state.lat,
				lng: this.state.lng,
				orientation: `${this.state.orientation}` === "1",
				active: `${this.state.active}` === "1",
				shopTypeId: this.state.shopTypeId,
				districtId: this.state.districtId
			}).then(() => {
				this.onCancel();
			}).catch(error => {
				// show error alert
			});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/branch");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="branch" title="New Branch" line/>
						</div>
						<div className="col-md-6 text-right mt-15">
							<ul className="list-inline mt-20">
								<li className="list-inline-item">
									<Button btnType="default">Export pdf</Button>
								</li>
							</ul>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<div className="row">
							<FormBranch view data={this.state} onChange={this.onChange}/>
							<div className="col-md-12 text-center bt">
								<Button className="pull-left" btnType="cancel" onClick={this.onCancel} children="Cancel"/>
								<Button className="pull-left" btnType="default" onClick={this.onAccept} children="Submit" />
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let provinces = [];
	return {
		branchs: state.branchs,
		branchTypes: state.branchTypes,
		address: state.address
	};
};

const mapDispatchToProps = dispatch => {
	return {
		branchActions: bindActionCreators(branchActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
