import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import {
	branchActions,
	branchTypeActions,
	fileActions
} from "../../../actions";
import { branchsFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import BranchList from "../../commons/branchList";
import MySearchField from "../../commons/MySearchField";

const Div = styled.div`
	position: relative;
	width: 100%;

	.btn {
		&.btn-toggle {
			line-height: 1;
			background-color: transparent;
			color: #7281d6;
			&:focus {
				outline: none;
				-webkit-box-shadow: none;
			}
		}
	}
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 200px;
		margin-top: 5px;
		position: absolute;
		right: 15px;
		margin-top: 15px;
		z-index: 99;
		transition: all 0.2s ease-out;

		li {
			display: list-item;
			list-style: none;
			padding: 15px 20px;
			border-bottom: 1px solid #e5e5e5;
			font-size: 14px;
			text-align: left;
			cursor: pointer;
			color: ${props => props.theme.color.black};

			.d-text {
				margin-left: 15px;
			}

			&:hover {
				color: ${props => props.theme.color.primary};
			}

			.icon {
				width: 20px;
			}
			li:last-child {
				border-bottom: 1px solid transparent;
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		branchs: [],
		branchTypes: [],
		address: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false,
			file: null
		};
		this.importBoxOpen = this.importBoxOpen.bind(this);
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onCancelImport = this.onCancelImport.bind(this);
		this.onAcceptImport = this.onAcceptImport.bind(this);
		this.onChangeImport = this.onChangeImport.bind(this);
		this.fileUpload = this.fileUpload.bind(this);
	}

	componentDidMount() {
		// if (this.props.branchs.length === 0) {
		this.props.branchActions.getBranchs();
		// }
		if (this.props.branchTypes.length === 0) {
			this.props.branchTypeActions.getBranchTypes();
		}
	}

	onCancelImport() {
		this.setState({
			importBox: false
		});
	}

	onAcceptImport(event) {
		event.preventDefault();
		this.fileUpload(this.state.file)
			.then(res => {
				this.props.branchTypeActions.getBranchTypes();
				this.onCancelImport();
			})
			.catch(error => {
				alert("Import file error.");
			});
	}

	fileUpload(file) {
		const formData = new FormData();
		formData.append("file", file);
		return this.props.fileActions.uploadFile(formData, "branch");
	}

	onChangeImport(event) {
		this.setState({
			file: event.target.files[0]
		});
	}

	importBoxOpen() {
		this.setState(prevState => ({
			...prevState,
			importBox: true
		}));
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.branchActions.deleteBranch({
				id: id
			}).then(() => {
				resolve();
			}).catch(error => {
				// show error alert
				reject();
			});
		});
	}

	render() {
		const options = {
			sizePerPageList: [
				{ text: "6", value: 6 },
				{ text: "20", value: 20 },
				{ text: "All", value: this.props.branchs.length }
			],
			sizePerPage: 6,
			prePage: "Previous",
			nextPage: "Next",
			firstPage: "First",
			lastPage: "Last",
			searchField: props => <MySearchField text="search" {...props} />
		}; // Previous page button text // Next page button text // First page button text // Last page button text

		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="branch" title="Branch" line />
						</div>
						<div className="col-md-6 text-right">
							<ul className="list-inline mt-20">
								<li className="list-inline-item">
									<button onMouseOver={this.toggle} className="btn btn-toggle">
										<img src="assets/images/icon-dropdown.png" />
									</button>
									{this.state.showDropdown && (
										<ul className="box">
											<li onClick={() => this.nextPath("/branch/add")}>
												<img className="icon" src="assets/images/icon-add.png" />
												<span className="d-text">Add Branches</span>
											</li>
											<li onClick={this.importBoxOpen}>
												<img className="icon" src="assets/images/import.png" />
												<span className="d-text">Import Branches</span>
											</li>
											<li>
												<img className="icon" src="assets/images/icon-export.png" />
												<span className="d-text">Export Branches</span>
											</li>
										</ul>
									)}
								</li>
								<li className="list-inline-item">
									<Button btnType="default">Publish</Button>
								</li>
							</ul>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						{this.state.importBox && (
							<ImportBox title="Import Branch" sheetname="branch" onChangeImport={this.onChangeImport} onCancelImport={this.onCancelImport} onAcceptImport={this.onAcceptImport} />
						)}
						<BranchList mode="edit" onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		branchs: state.branchs,
		branchTypes: state.branchTypes,
		address: state.address
	};
};

const mapDispatchToProps = dispatch => {
	return {
		branchActions: bindActionCreators(branchActions, dispatch),
		branchTypeActions: bindActionCreators(branchTypeActions, dispatch),
		fileActions: bindActionCreators(fileActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
