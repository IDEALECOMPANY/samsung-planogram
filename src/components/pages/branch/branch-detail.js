import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import {
	branchActions,
	planogramActions,
	deviceCategoryActions,
	deviceActions,
	mediaActions,
	mediaTypeActions,
	tableTypeActions,
	tableSizeActions,
	accessoryActions
} from "../../../actions";
import { branchsFormatter, getUniqueString } from "../../../selectors";
import { uuidUtils, arrayUtils } from "../../../utils";

import Button from "../../commons/button";
import Textfield from "../../commons/textfield";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import SelectOption from "../../commons/selectOption";
import Titlepage from "../../commons/titlepage";
import Panel from "../../commons/panel";
import Topbar from "../../commons/topBar";
import Nav from "../../commons/nav";
import Hr from "../../commons/hr";
import TableSet from "../../commons/tableSet";
import PlanogramSet from "../../commons/planogramSet";
import Select from "react-select";
import "react-select/dist/react-select.css";
import FormBranch from "../../commons/formBranch";
import Image from "../../commons/image";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import MediaSet from "../../commons/mediaSet";
import AccessoriesSet from "../../commons/accessoriesSet";

const Div = styled.div`
	h2 {
		font-size: 20px;
	}
	.info {
		padding-left: 71px;
		float: left;
		margin-top: 0;
		font-size: 14px;

		h6 {
			font-size: 16px;
			font-family: ${props => props.theme.text.bold};
		}

		a {
			color: ${props => props.theme.color.primary};
		}
	}

	.box {
		.title-bg {
			background-color: #000;
			padding: 12px 30px;
			color: #fff;
			font-family: ${props => props.theme.text.bold};
		}

		.box-inner {
			padding: 30px 0 30px 0px;

			.img-sm {
				width: 29.33%;
				float: left;
				margin: 0px;
				cursor: pointer;
				padding: 3px;
				border: 1px solid #fff;
				position: relative;
				margin: 0px 2% 2%;
				&.active {
					border: 1px solid #aaa;
				}
				img {
					margin: 0px;
					width: 100%;
					position: relative;
				}
			}
			.img-lg {
				padding: 3px;
				border: 1px solid #aaa;
				img {
					width: 100%;
				}
			}
			.btn-delete {
				border: 0px;
				background: #000;
				border-radius: 50%;
				width: 22px;
				height: 22px;
				min-width: initial;
				padding: 0px;
				position: absolute;
				right: -7px;
				top: -5px;
				z-index: 1;
				border: 1px solid #000;
				outline: none !important;
				&:after {
					content: "";
					position: absolute;
					border: 1px solid #fff;
					width: 12px;
					left: 4px;
					top: 9px;
					transform: rotate(45deg);
				}
				&:before {
					content: "";
					position: absolute;
					border: 1px solid #fff;
					width: 12px;
					left: 4px;
					top: 9px;
					transform: rotate(-45deg);
				}
			}
		}
	}

	.default-p {
		margin: 80px 0;
		font-size: 25px;
		color: #e6e6e6;
		font-family: ${props => props.theme.text.bold};
	}

	.modal_ {
		position: fixed;
		width: 100%;
		height: 100%;
		z-index: 99999;
		background-color: rgba(0, 0, 0, 0.5);
		top: 0;
		left: 0;
	}

	.modal {
		display: block !important;
		.modal-dialog {
			max-width: 800px;

			.modal-header {
				padding: 10px 15px;

				h5 {
					font-size: 18px;
					font-family: ${props => props.theme.text.bold};
				}
			}
			.modal-footer {
				border-top: 0;
			}

			.modal-body {
				h6 {
					background-color: #f5f5f5;
					font-size: 16px;
					padding: 10px 15px;
					border-bottom: 1px solid #ccc;
				}

				.body-inner {
					float: left;
					width: 100%;
					padding: 15px;
					border: 1px solid #ccc;
					padding: 0;
					border-radius: 7px;
					height: 500px;
					overflow: auto;
				}

				.p-30 {
					padding: 30px;
				}
			}
		}
	}

	button {
		height: 33px;
		border: 1px solid ${props => props.theme.color.primary};
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 30px;
		border-radius: 5px;
		&.btn {
			height: 33px;
			cursor: pointer;
			font-size: ${props => props.theme.font.small};
			padding: 0px 20px;
			border-radius: 5px;
			color: ${props => props.theme.color.primary};
			display: inline-block;

			&.btn-primary {
				background-color: ${props => props.theme.bg.primary};
				border: 1px solid ${props => props.theme.border.primary};
				color: #fff;
			}

			&.btn-default {
				background-color: ${props => props.theme.bg.default};
				border: 1px solid ${props => props.theme.border.primary};
			}

			&.btn-cancel {
				background-color: ${props => props.theme.bg.gray};
				border: 1px solid ${props => props.theme.border.gray};
				color: ${props => props.theme.color.gray};
			}
		}
	}

	.set .list-inline {
		li {
			display: inline-block;
			margin-right: 20px;
			font-size: 12px;
		}
		li:last-child {
			margin-right: 0px;
		}
	}

	.set-detail {
		margin: 30px 0;
	}

	.btnUpload {
		margin-left: 5px;
		position: relative;
		input {
			top: 0px;
			left: 0px;
			width: 100%;
			height: 100%;
			position: absolute;
			opacity: 0;
			z-index: 1;
			cursor: pointer;
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		floorPlanImages: [
			// { id: 1, image: "/assets/images/plan1.png" },
			// { id: 2, image: "/assets/images/plan2.png" },
			// { id: 3, image: "/assets/images/plan3.png" }
		],
		branch: null,
		branchs: null,
		branchTypes: [],
		address: [],
		planograms: [],
		devices: [],
		deviceCategories: [],
		medias: [],
		mediaTypes: [],
		tableTypes: [],
		tableSizes: [],
		options_device: [
			{
				label: "media",
				options: []
			},
			{
				label: "device",
				options: []
			}
		],
		options_model: [
			{
				label: "media",
				options: []
			},
			{
				label: "device",
				options: []
			}
		],
		options_model_media: [],
		row: ["1", "2", "3", "4", "5", "6"],
		mediaList: [],
		accessories: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			manageSet: false,
			selectedplanogram_set: "",
			modal_upload_floorplan: false,
			edit_branch: false,
			floorPlanImages: this.props.floorPlanImages,
			activeFloorPlan: this.props.floorPlanImages.length > 0 ? this.props.floorPlanImages[0] : null,
			id: "",
			code: "",
			title: "",
			retailHub: "",
			customer: "",
			lat: "",
			lng: "",
			orientation: "",
			active: "",
			shopTypeId: "",
			regionId: null,
			provinceId: null,
			amphurId: null,
			districtId: null,
			branchTypes: this.props.branchTypes,
			address: this.props.address,
			provinces: [],
			amphurs: [],
			districts: [],
			planograms: [...this.props.planograms],
			planogram: null,
			planogramSet: null,
			options_device: this.props.options_device,
			options_model: this.props.options_model,
			options_model_media: this.props.options_model_media,
			devices: this.props.devices,
			medias: this.props.medias,
			row: this.props.row,
			mediaTab: false,
			accessoriesTab: null,
			selectedOptionAccessories: null,
			mediaList: []
		};

		this.openManage = this.openManage.bind(this);
		this.openModal_upload_floorplan = this.openModal_upload_floorplan.bind(this);
		this.closeModal_upload_floorplan = this.closeModal_upload_floorplan.bind(this);

		this.modal_edit_branch = this.modal_edit_branch.bind(this);
		this.closeModal_edit_branch = this.closeModal_edit_branch.bind(this);
		this.changeActiveFloorPlan = this.changeActiveFloorPlan.bind(this);
		this.onCancel = this.onCancel.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onChangeUpload = this.onChangeUpload.bind(this);
		this.deleteFloorPlan = this.deleteFloorPlan.bind(this);
		this.onAcceptFloorPlan = this.onAcceptFloorPlan.bind(this);
		this.handle_planogram_set = this.handle_planogram_set.bind(this);
		this.onChangeSelect = this.onChangeSelect.bind(this);
		this.onChangeSelectDevice = this.onChangeSelectDevice.bind(this);
		this.onAcceptPlanogramSet = this.onAcceptPlanogramSet.bind(this);

		this.onAddMedia = this.onAddMedia.bind(this);
		this.onAddAccessories = this.onAddAccessories.bind(this);
		this.onAddMediaList = this.onAddMediaList.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.checkProps = this.checkProps.bind(this);
		this.onMediaChange = this.onMediaChange.bind(this);
	}

	componentDidMount() {
		if (this.props.branchs === null) {
			this.onCancel();
		}
		if (this.props.branch !== null) {
			this.checkProps(this.props.branch);
		}
		if (this.props.planograms.length === 0) {
			this.props.planogramActions.getPlanograms();
		}
		if (this.props.devices.length === 0) {
			this.props.deviceActions.getDevices();
		}
		if (this.props.deviceCategories.length === 0) {
			this.props.deviceCategoryActions.getDeviceCategories();
		}
		if (this.props.medias.length === 0) {
			this.props.mediaActions.getMedias();
		}
		if (this.props.mediaTypes.length === 0) {
			this.props.mediaTypeActions.getMediaTypes();
		}
		if (this.props.tableTypes.length === 0) {
			this.props.tableTypeActions.getTableTypes();
		}
		if (this.props.tableSizes.length === 0) {
			this.props.tableSizeActions.getTableSizes();
		}

		if (this.props.accessories.length === 0) {
			this.props.accessoryActions.getAccessories();
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.branch !== nextProps.branch) {
			this.checkProps(nextProps.branch);
		}

		if (this.props.planograms !== nextProps.planograms) {
			this.setState(prevState => ({...prevState, planogram: nextProps.planogram}));
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/branch");
		} else {
			let district = props.district ? props.district : null, districts = [];
			let amphur = null, amphurs = [];
			let province = null, provinces = [];
			let region = null, regions = [];
			if (district !== null) {
				amphur = district.amphur;
				if (amphur !== null) {
					province = amphur.province;
					if (province !== null) {
						region = province.region;
						regions = this.state.address.find(i => `${i.id}` === `${region.id}`);
						provinces = regions.provinces;
						amphurs = provinces.find(i => `${i.id}` === `${province.id}`).amphurs;
						districts = amphurs.find(i => `${i.id}` === `${amphur.id}`).districts;
					}
				}
			}

			let accessory = this.props.accessories.filter(i => `${i.id}` === `${props.accessoryId}`);

			this.setState(prevState => ({
				...prevState,
				id: props.id,
				code: props.code,
				title: props.title,
				retailHub: props.retailHub,
				customer: props.customer,
				lat: props.lat,
				lng: props.lng,
				orientation: props.orientation ? "1" : "2",
				active: props.active ? "1" : "2",
				shopTypeId: props.shopType ? props.shopType.id : "",
				shopTypeTitle: props.shopType ? props.shopType.title : "",
				regionId: region ? region.id : null,
				provinceId: province ? province.id : null,
				amphurId: amphur ? amphur.id : null,
				districtId: district ? district.id : null,
				provinces: provinces,
				amphurs: amphurs,
				districts: districts,
				mediaList: props.branchMedia.map(i => this.createMedia(i.mediaTypeId, i.mediumId)),
				selectedOptionAccessories: this.reformatAccesory(accessory[0])
			}), () => {
				if (props.planogram !== null) {
					this.handle_planogram_set({
						value: props.planogram.id
					});
				}
			});
		}
	}

	handle_planogram_set(event) {
		let id = event.value;
		let planogram = this.props.planograms.filter(
			i => `${i.id}` === `${id}`
		);
		if (planogram.length > 0) {
			this.setState(prevState => ({
				...prevState,
				planogram: {
					id: planogram[0].id,
					active: planogram[0].active ? "1" : "2",
					tableSets: planogram[0].planogramTableSets.map(ts => {
						let mediaTypes = [];
						let media = [];
						if (ts.tableType !== null && this.props.tableTypes.length > 0)
							mediaTypes = this.props.tableTypes.find(t => `${t.id}` === `${ts.tableType.id}`).mediaTypes;

						mediaTypes.forEach((element, key) => {
							for (let m = 0; m < element.qty; m++) {
								media.push({
									name: `media-${getUniqueString()}`,
									index: key + m + 1,
									label: `${element.code}${m + 1}`,
									id: null,
									value: "",
									mediaType: {
										id: element.id,
										title: element.title
									}
								});
							}
						});
						return {
							id: `tableSet-${getUniqueString()}`,
							qty: ts.planogramTableSetTables.length,
							tableTypeId: ts.tableType ? ts.tableType.id : "",
							tableSizeId: ts.tableSize ? ts.tableSize.id : "",
							branchTypeId: ts.shopType ? ts.shopType.id : "",
							capacity: ts.tableSize ? ts.tableSize.capacity : "",
							tables: ts.planogramTableSetTables.map(t => {
								return {
									devices: t.planogramTableSetTableDevices.map(d => {
										let type, typeId, id, value, price;
										if (d.device === null) {
											type = "media";
											typeId = d.medium ? d.medium.mediaMediaTypes[0].mediaType.id : "";
											id = d.medium ? d.medium.id : null;
											value = d.medium ? d.medium.desc : "";
											price = "";
										} else {
											type = "device";
											typeId = d.device ? d.device.deviceCategory.id : "";
											id = d.device ? d.device.id : null;
											value = d.device ? (d.device.deviceModel ? d.device.deviceModel.title : "") : "";
											price = d.device ? d.device.price : "";
										}
										return {
											name: `devices-${getUniqueString()}`,
											index: d.index,
											label: d.label,
											type: type,
											typeId: typeId,
											id: id,
											value: value,
											price: price
										};
									}),
									media: media.map((m, k) => {
										return {
											...m,
											id: t.planogramTableSetTableMedia[k] ? t.planogramTableSetTableMedia[k].medium === null ? null : t.planogramTableSetTableMedia[k].medium.id : m.id,
											value: t.planogramTableSetTableMedia[k] ? t.planogramTableSetTableMedia[k].medium === null ? "" : t.planogramTableSetTableMedia[k].medium.desc : m.value
										};
									})
								};
							})
						};
					})
				}
			}));
		}
	}

	openManage() {
		this.setState(prevState => ({ ...prevState, manageSet: true }));
	}

	openModal_upload_floorplan() {
		this.setState(prevState => ({ ...prevState, modal_upload_floorplan: true }));
	}

	closeModal_upload_floorplan() {
		this.setState(prevState => ({ ...prevState, modal_upload_floorplan: false }));
	}

	modal_edit_branch() {
		this.setState(prevState => ({ ...prevState, edit_branch: true }));
	}

	closeModal_edit_branch() {
		this.setState(prevState => ({ ...prevState, edit_branch: false }));
	}

	changeActiveFloorPlan(image) {
		this.setState(prevState => ({
			...prevState,
			activeFloorPlan: { ...image }
		}));
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;
		if (name == "regionId") {
			this.setState(prevState => ({
				...prevState,
				provinceId: null,
				amphurId: null,
				districtId: null,
				provinces: this.state.address.find(i => `${i.id}` === `${value}`).provinces
			}));
		} else if (name == "provinceId") {
			this.setState(prevState => ({
				...prevState,
				amphurId: null,
				districtId: null,
				amphurs: this.state.provinces.find(i => `${i.id}` === `${value}`).amphurs
			}));
		} else if (name == "amphurId") {
			this.setState(prevState => ({
				...prevState,
				districtId: null,
				districts: this.state.amphurs.find(i => `${i.id}` === `${value}`).districts
			}));
		}
		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (
			this.state.code !== "" ||
			this.state.title !== "" ||
			this.state.shopTypeId !== ""
		) {
			this.props.branchActions.editBranch({
				id: this.state.id,
				code: this.state.code,
				title: this.state.title,
				retailHub: this.state.retailHub,
				customer: this.state.customer,
				lat: this.state.lat,
				lng: this.state.lng,
				orientation: `${this.state.orientation}` === "1",
				active: `${this.state.active}` === "1",
				shopTypeId: this.state.shopTypeId,
				districtId: this.state.districtId
			})
			.then(() => {
				this.closeModal_edit_branch();
			})
			.catch(error => {
				// show error alert
			});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onAcceptPlanogramSet() {
		if (this.state.planogram !== null) {
			this.props.branchActions.editPlanogram({
				id: this.state.id,
				planogramId: this.state.planogram.id,
				accessoryId: this.state.selectedOptionAccessories.id,
				media: this.state.mediaList.map(i => ({
					model: i.model,
					type: i.type
				}))
			}).then(() => {
				alert("ระบบได้ทำการอัพเดตเรียบร้อย");
			}).catch(error => {
				// show error alert
			});
		} else {
			alert("not choose planogram set");
		}
	}

	onChangeUpload(event) {
		let reader = new FileReader();
		let file = event.target.files[0];
		let uuid = getUniqueString();
		reader.onloadend = () => {
			let value = {
				id: `edit-${uuid}`,
				image: reader.result
			};
			this.state.floorPlanImages.push(value);
			this.changeActiveFloorPlan(value);
		};
		reader.readAsDataURL(file);
	}

	deleteFloorPlan(id) {
		this.setState({
				floorPlanImages: this.state.floorPlanImages.filter(i => `${id}` !== `${i.id}`)
			}, () => {
				if (`${this.state.activeFloorPlan.id}` === `${id}`) {
					this.setState(prevState => ({
						...prevState,
						activeFloorPlan: this.state.floorPlanImages.length > 0 ? this.state.floorPlanImages[0] : null
					}));
				}
			}
		);
	}

	onAcceptFloorPlan() {
		const floorPlan = this.state.floorPlanImages.map(i => {
			let str = `${i.id}`;
			return {
				id: str.indexOf("edit") > -1 ? null : i.id,
				image: i.image
			};
		});

		this.props.branchActions.editFloorPlan({
			id: this.state.id,
			floorPlans: floorPlan
		}).then(() => {
			alert("ระบบได้ทำการอัพเดตเรียบร้อย");
		}).catch(error => {
			// show error alert
		});
	}

	onChangeSelect(name, event) {
		let key = name;
		let zone = `${name}`.split("-");
		this.setState(prevState => ({
			...prevState,
			planograms: {
				...prevState.planogramSet,
				tableSets: this.state.planogramSet.tableSets.map(ts => {
					return {
						...ts,
						tables: ts.tables.map(t => {
							if (zone[0] === "media") {
								return {
									devices: t.devices,
									media: t.media.map(m => {
										if (`${m.name}` === `${key}`) {
											return {
												...m,
												id: event.value,
												value: event.label
											};
										} else {
											return {...m};
										}
									})
								};
							} else if (zone[0] === "devices") {
								return {
									devices: t.devices.map(d => {
										if (`${d.name}` === `${key}`) {
											return {
												...d,
												id: event.value,
												value: event.label,
												price: event.price
											};
										} else {
											return {...d};
										}
									}),
									media: t.media
								};
							} else {
								return {...t};
							}
						})
					};
				})
			}
		}), () => {
		});
	}

	onChangeSelectDevice(name, event) {
		let key = name;
		let zone = `${name}`.split("-");
		let value = `${event.value}`.split("-");
		this.setState(prevState => ({
			...prevState,
			planograms: {
				...prevState.planogramSet,
				tableSets: this.state.planogramSet.tableSets.map(ts => {
					return {
						...ts,
						tables: ts.tables.map(t => {
							if (zone[0] === "devices") {
								return {
									devices: t.devices.map(d => {
										if (`${d.name}` === `${key}`) {
											return {
												...d,
												type: value[0],
												typeId: value[1],
												id: "",
												value: "",
												price: ""
											};
										} else {
											return { ...d };
										}
									}),
									media: t.media
								};
							} else {
								return {...t};
							}
						})
					};
				})
			}
		}), () => {
		});
	}

	onCancel() {
		this.props.history.push("branch");
	}

	onAddMedia() {
		this.setState(prevState => ({ ...prevState, mediaTab: true }));
	}

	onAddAccessories() {
		this.setState(prevState => ({ ...prevState, accessoriesTab: true }));
	}

	handleChange(selectedOptionAccessories) {
		let accessory = this.props.accessories.filter(i => `${i.id}` === `${selectedOptionAccessories.value}`);
		this.setState(prevState => ({
			...prevState,
			selectedOptionAccessories: this.reformatAccesory(accessory[0])
		}));
	}

	reformatAccesory(accessory) {
		if (accessory !== undefined) {
			return {
				...accessory,
				devices: accessory.accessoryTableSets.length > 0 ? accessory.accessoryTableSets.map(i => {
					let devices = i.accessoryTableSetDevices.map((j, k) => {
						let price, typeId, id;
						
						if (j.type === "device") {
							price = j.device.price;
							typeId = `device-${j.device.deviceCategory.id}`;
							id = `device-${j.device.id}`;
						} else {
							price = "";
							let tmp = this.props.mediaTypes.filter(p => p.code === j.label);
							typeId = `media-${tmp.length > 0 ? tmp[0].id : ""}`;
							id = `media-${j.medium.id}`;
						}
	
						let options = {
							positions: [...this.props.options_position],
							devices: j.label === "Number" ? [...this.props.options_device] : this.props.options_device.filter(k => k.label === "media").map(k => {
								return {
									...k,
									options: k.options.filter(p => p.code === j.label)
								};
							}),
							models: this.props.options_model.filter(k => k.label === j.type).map(k => {
								return {
									...k,
									options: j.label === "Number" ? k.options.filter(p => p.typeInfo.indexOf(typeId) !== -1) : k.options.filter(p => p.typeInfo.indexOf(j.label) !== -1)
								};
							})
						};
	
						return this.createDevice(k+1, j.label, j.label, id, j.type, price, typeId, options);
					});
	
					return this.createTable(devices);
				}) : []
			};
		} else {
			return {
				id: "",
				image: "",
				devices: []
			};
		}
	}

	createTable(devices=false) {
		return {
			id: uuidUtils.create(),
			devices: devices ? devices : arrayUtils.generateArray(this.props.defaultDeviceLength).map((i, k) => this.createDevice(k + 1)),
		};
	}

	createDevice(index, label="Number", value="Number", id=uuidUtils.create(), type="", price="", typeId="", options=false) {
		return {
			id: id,
			index: index,
			label: label,
			value: value,
			type: type,
			price: price,
			typeId: typeId,
			options: options ? options : {
				positions: [...this.props.options_position],
				devices: [...this.props.options_device],
				models: [],
			}
		};
	}

	onAddMediaList() {
		this.setState(prevState => ({
			...prevState,
			mediaList: [...prevState.mediaList, this.createMedia()]
		}), () => {
			console.log("After onAddMediaList", this.state.mediaList);
		});
	}

	createMedia(type="", model="") {
		return {
			id: uuidUtils.create(),
			type: type,
			model: model,
			options: {
				devices: this.props.mediaTypes.map(i => {
					return {
						value: `${i.id}`,
						label: i.title,
						code: i.code
					};
				}),
				models: this.props.medias.map(i => {
					return {
						value: `${i.id}`,
						label: i.title,
						typeInfo: i.mediaTypes.map(j => j.code),
					};
				})
			}
		};
	}

	onMediaChange(data) {
		this.setState(prevState => ({
			...prevState,
			mediaList: prevState.mediaList.map(i => i.id !== data.id ? i : {...i, ...data})
		}));
	}

	render() {
		let title = `${this.state.code} | ${this.state.title}`;
		const { selectedOptionAccessories } = this.state;
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-10">
							<Titlepage icon="branch" title={title} line />
							<div className="info">
								<h6 className="color-primary">Information</h6>
								<ul className="inline">
									<li>Retail : {this.state.retailHub}</li>
									<li>Customer: {this.state.customer}</li>
									<li>Shop Type: {this.state.shopTypeTitle}</li>
									<li><a onClick={this.modal_edit_branch} href="#">Edit</a></li>
								</ul>
							</div>
						</div>
						<div className="col-md-2 text-right mt-20 bt">
							<ul className="list-inline mt-20">
								<li className="list-inline-item">
									<Button btntype="default">Export pdf</Button>
								</li>
							</ul>
						</div>
					</div>
					<Hr />
					<div className="box">
						<div className="box-inner">
							<div className="row">
								<div className="col-md-6 ">
									<h3 className="title-page color-primary mb-0">
										Floor Plan
									</h3>
								</div>
								<div className="col-md-6 text-right">
									<button className="btn btn-primary" onClick={this.onAcceptFloorPlan}>
										Save
									</button>
									<button className="btn btnUpload">
										+ Upload Floor Plan{" "}
										<input type="file" accept="image/*" onChange={this.onChangeUpload} />
									</button>
								</div>
							</div>
							<hr />
							{this.state.activeFloorPlan && this.state.floorPlanImages.length > 0 ? (
								<div className="row">
									<div className="col-md-6 ">
										<div className="img-lg">
											<Image src={this.state.activeFloorPlan.image} />
										</div>
									</div>
									<div className="col-md-6 ">
										{this.state.floorPlanImages.map((i, k) => (
											<div className={this.state.activeFloorPlan.id === i.id ? "img-sm active" : "img-sm"} key={uuidUtils.create()}>
												<button className="btn-delete" onClick={event => this.deleteFloorPlan(i.id)} />
												<img src={i.image} onClick={event => this.changeActiveFloorPlan(i)} />
											</div>
										))}
									</div>
								</div>
							) : (
								<div>
									<div className="default-p text-center">
										<div>
											<img src="/assets/images/uploadfloor.png" />
										</div>
										Upload Floor Plan
									</div>
								</div>
							)}
							<div className="tabs-info">
								<Tabs>
									<TabList>
										<Tab>Planogram Set</Tab>
										<Tab>Media</Tab>
										<Tab>Accessories</Tab>
									</TabList>
									<TabPanel>
										<div className="row mt-30">
											<div className="col-md-9 " />
											<div className="col-md-3 text-right">
												<Select
													name="field-Planogram"
													placeholder="Select Planogram Set"
													value={this.state.planogram === null ? "" : this.state.planogram.id}
													onChange={this.handle_planogram_set}
													options={this.state.planograms.map(i => ({value: i.id, label: i.code }))}
													className="select-planogram"
												/>
											</div>
										</div>
										<div className="row mb-30 ">
											<div className="col-md-12">
												{this.state.planogram !== null ? (
													<Tabs>
														<TabList>
															{this.state.planogram.tableSets.map((i, k) => {
																return (
																	<Tab key={uuidUtils.create()}>Table Set {k + 1}</Tab>
																);
															})}
														</TabList>
														{this.state.planogram.tableSets.map((i, k) => {
															return (
																<div key={uuidUtils.create()}>
																	<TabPanel>
																		<div className="list row">
																			<div className="col-md-12">
																				<div className="row set-detail">
																					<div className="col-md-6">
																						<div className="title">SES_6M_Smart Table_M_6</div>
																						<div className="set">
																							<ul className="list-inline">
																								<li>Table: 6M</li>
																								<li>Table Type: Smart Table</li>
																								<li>Sub Type: Size M, Capacity 6</li>
																							</ul>
																						</div>
																					</div>
																					<div className="col-md-6 text-right set">
																						<ul className="list-inline">
																							<li>E: End Cap (หัวโต๊ะ)</li>
																							<li>L: LED Panel (ป้ายไฟ)</li>
																							<li>P: Promotion Stand (โปรโมชั่น)</li>
																						</ul>
																					</div>
																				</div>
																			</div>
																			{i.tables.map((t, k2) => {
																				let row = t.devices;
																				let el = t.devices;
																				row = row.slice(0, row.length - 4);
																				el = el.slice(row.length,el.length);
																				let text = this.props.tableSizes.find(ts => `${ts.id}` === `${i.tableSizeId}`).title;
																				return (
																					<div className="col-12 col-md-6 col-lg-6 col-xl-6" key={uuidUtils.create()}>
																						<PlanogramSet
																							title={`${text}${k2 +1}`}
																							tableQTY={`${text}${k2 +1}`}
																							options_device={this.state.options_device}
																							options_model={this.state.options_model}
																							options_model_media={this.state.options_model_media}
																							row={row}
																							el={el}
																							tables={t}
																							devices={this.state.devices}
																							medias={this.state.medias}
																							onChangeSelect={this.onChangeSelect}
																							onChangeSelectDevice={this.onChangeSelectDevice}
																							edit
																						/>
																					</div>
																				);
																			})}
																		</div>
																	</TabPanel>
																</div>
															);
														})
													}
													</Tabs>
												) : (
													<div className="default-p  text-center">
														<div>
															<img src="/assets/images/planogramset.png" />
														</div>
														Select Planogram Set
													</div>
												)}
											</div>
										</div>
									</TabPanel>
									{/* END Planogram Set */}
									<TabPanel>
										<div className="row mt-30">
											<div className="col-md-9 " />
											<div className="col-md-3 text-right">
												<Button
													onClick={this.onAddMediaList}
													btnType="default"
													iconLeft="icon-add"
												>
													Add Media
												</Button>
											</div>
										</div>
										<div className="row mt-4">
											{this.state.mediaList.map((i, k) => (
												<div key={uuidUtils.create()} className="col-12 col-sm-12 col-md-6">
													{" "}
													<MediaSet data={i} title={`Media ${k + 1}`} onChange={this.onMediaChange} />
												</div>
											))}
										</div>
									</TabPanel>
									{/* END Media */}
									<TabPanel>
										<div className="row mt-30">
											<div className="col-md-9 " />
											<div className="col-md-3 text-right">
												<Select
													name="field-Planogram"
													placeholder="Select Accessories Set"
													value={this.state.selectedOptionAccessories ? this.state.selectedOptionAccessories.id : ""}
													onChange={this.handleChange}
													options={this.props.accessories.map(i => ({label: i.code, value: i.id}))}
													className="select-planogram"
												/>
											</div>
										</div>
										{this.state.selectedOptionAccessories !== null ? (
											<div className="row device mt-4">
												<div className="col-md-4 offset-md-4 mb-4">
													<img className="img-fluid" src={this.state.selectedOptionAccessories.image}/>
												</div>
												{this.state.selectedOptionAccessories.devices.map(i => (
													<div className="col-12 col-sm-12 col-md-6" key={uuidUtils.create()}>
														<AccessoriesSet
															title="Box 1"
															data={i}
															disabled={true}
														/>
													</div>
												))}
											</div>
										) : (
											<div className="default-p text-center">
												<div>
													<img src="/assets/images/icon-accessories-lg.png" />
												</div>
												Select Accesories Set
											</div>
										)}
									</TabPanel>
									{/* END Accessories Set */}
								</Tabs>
							</div>
						</div>
						{this.state.modal_upload_floorplan && (
							<div className="modal  modal_">
								<div className="modal-dialog" role="document">
									<div className="modal-content">
										<div className="modal-header">
											<h5 className="modal-title">
												Branch : {this.state.title}
											</h5>
										</div>
										<div className="modal-body">
											<div className="body-inner">
												<h6>Upload Floor Plan</h6>
												<div className="p-30 bt">
													<Textfield type="file" title="Table Image" placeholder="Table Image" />
													<button onClick={this.closeModal_upload_floorplan} link="add-planogram-set" className="btn  btn-cancel">Cancel</button>
													<button onClick={this.closeModal_upload_floorplan} link="add-planogram-set" btntype="btn  btn-default">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						)}
						{this.state.edit_branch && (
							<div className="modal modal_ ">
								<div className="modal-dialog" role="document">
									<div className="modal-content">
										<div className="modal-header">
											<h5 className="modal-title">Branch : {this.state.title}</h5>
										</div>
										<div className="modal-body">
											<div className="body-inner">
												<h6>Edit Branch</h6>
												<div className=" p-30 bt">
													<FormBranch view data={this.state} onChange={this.onChange} />
													<button onClick={this.closeModal_edit_branch} link="add-planogram-set" className="btn  btn-cancel">Cancel</button>
													<button onClick={this.onAccept} link="add-planogram-set" btntype="btn  btn-default">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						)}
						<div className="col-md-12 text-center bt">
							<Button
								onClick={this.onCancel}
								className="pull-left "
								btnType="cancel"
								children="Cancle"
							/>
							<Button
								onClick={this.onAcceptPlanogramSet}
								className="pull-left "
								btnType="default"
								children="Save"
							/>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let branchs = state.branchs.filter(i => `${i.id}` === `${id}`);
	let branch = branchs.length > 0 ? branchs[0] : { error: true };

	return {
		accessories: state.accessories,
		branch: branch,
		branchs: state.branchs,
		address: state.address,
		branchTypes: state.branchTypes,
		floorPlanImages: branch.floorPlans,
		planograms: state.planograms,
		devices: state.devices,
		deviceCategories: state.deviceCategories,
		medias: state.medias,
		mediaTypes: state.mediaTypes,
		tableTypes: state.tableTypes,
		tableSizes: state.tableSizes,
		options_model_media: state.medias.map(i => {
			return {
				value: i.id,
				label: i.title,
				mediaTypes: i.mediaTypes,
				tableTypes: i.tableTypes
			};
		}),
		options_device: [
			{
				label: "media",
				options: state.mediaTypes.map(i => {
					return {
						value: `media-${i.id}`,
						label: i.title,
						code: i.code
					};
				})
			}, {
				label: "device",
				options: state.deviceCategories.map(i => {
					return {
						value: `device-${i.id}`,
						label: i.title,
						code: `device-${i.id}`
					};
				})
			}
		],
		options_model: [
			{
				label: "media",
				options: state.medias.map(i => {
					return {
						value: `media-${i.id}`,
						label: i.title,
						typeInfo: i.mediaTypes.map(j => j.code),
						type: "media"
					};
				})
			}, {
				label: "device",
				options: state.devices.map(i => {
					let name = i.deviceModel ? i.deviceModel.title : "";
					let color = i.deviceColor ? i.deviceColor.title : "";
					let gb = i.deviceCapacity ? i.deviceCapacity.title : "";
					return {
						value: `device-${i.id}`,
						label: `${name} ${color} ${gb}`.trim(),
						typeInfo: [`device-${i.deviceCategory.id}`],
						type: "devices",
						price: i.price
					};
				})
			}
		],
		options_position: [{ label: "Number", value: "Number" }, ...state.mediaTypes.map(i => ({ label: i.code, value: i.code, data: {...i} }))]
	};
};

const mapDispatchToProps = dispatch => {
	return {
		branchActions: bindActionCreators(branchActions, dispatch),
		planogramActions: bindActionCreators(planogramActions, dispatch),
		deviceActions: bindActionCreators(deviceActions, dispatch),
		deviceCategoryActions: bindActionCreators(deviceCategoryActions, dispatch),
		mediaActions: bindActionCreators(mediaActions, dispatch),
		mediaTypeActions: bindActionCreators(mediaTypeActions, dispatch),
		tableTypeActions: bindActionCreators(tableTypeActions, dispatch),
		tableSizeActions: bindActionCreators(tableSizeActions, dispatch),
		accessoryActions: bindActionCreators(accessoryActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
