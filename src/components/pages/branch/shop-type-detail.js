import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

import { branchTypeActions } from "../../../actions";
import { branchTypesFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormShopType from "../../commons/formShopType";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		branchType: null,
		branchTypes: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			id: "",
			title: "",
			active: ""
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.branchTypes === null) {
			this.onCancel();
		}
		if (this.props.branchType !== null) {
			this.checkProps(this.props.branchType);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.branchType !== nextProps.branchType) {
			this.checkProps(nextProps.branchType);
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/branch/type");
		} else {
			this.setState(prevState => ({
				...prevState,
				id: props.id,
				title: props.title,
				active: props.active ? "1" : "2"
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (this.state.title !== "") {
			this.props.branchTypeActions
				.editBranchType({
					id: this.state.id,
					title: this.state.title,
					active: `${this.state.active}` === "1",
					createdAt: this.props.branchType.createdAt,
					updatedAt: new Date()
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/branch/type");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="branch"
								title="Shop type"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-15">
							<Button
								btnType="default"
								children="Export pdf"
							/>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						<h3 className="title-page color-primary">
							Shop Type Info
						</h3>
						<FormShopType
							view
							data={this.state}
							onChange={this.onChange}
						/>
						<div className="col-md-12 text-center bt">
							<Button
								onClick={this.onCancel}
								className="pull-left "
								btnType="cancel"
								children="Cancle"
							/>
							<Button
								onClick={this.onAccept}
								className="pull-left "
								btnType="default"
								children="Save"
							/>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let branchTypes = state.branchTypes.filter(i => `${i.id}` === `${id}`);

	let branchType = branchTypes.length > 0 ? branchTypes[0] : { error: true };

	return {
		branchType: branchType,
		branchTypes: state.branchTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		branchTypeActions: bindActionCreators(branchTypeActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
