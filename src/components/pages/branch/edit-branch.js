import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

import Textfield from "../../commons/textfield";
import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import SelectOption from "../../commons/selectOption";
import Textarea from "../../commons/textarea";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Panel from "../../commons/panel";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		posts: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่"
		};
	}

	render() {
		return (
			<Div>
				<div className="row">
					<div className="col-md-10">
						<div className="row">
							<div className="col-md-9">
								<Titlepage
									icon="branch"
									title="C030140747 | SAMSUNG EXPERIENCE SHOP (POWERFULL CENTRAL WESTGATE BANGYAI)"
									line
								/>
							</div>
							<div className="col-md-3 text-right mt-15">
								<ul className="list-inline mt-20">
									<li className="list-inline-item">
										<Button btnType="defauld">
											Export pdf
										</Button>
									</li>
								</ul>
							</div>
						</div>
						<Hr />
						<div className="table-box">
							<div className="row">
								<div className=" col-md-6 ">
									<Textfield
										type="text"
										title="Site Code"
										placeholder="Site Code"
									/>
									<Textfield
										type="text"
										title="Site Name"
										placeholder="Site Name"
									/>
									<Textfield
										type="text"
										title="Retail"
										placeholder="Retail"
									/>
									<Textfield
										type="text"
										title="Customer"
										placeholder="Customer"
									/>
									<SelectOption
										title="Select Type"
										name=""
										options={[
											"Select Type",
											"Select Type",
											"Select Type"
										]}
										placeholder="Select Type"
									/>
									<CheckboxRadioInput
										title="Status"
										type="radio"
										name="status"
										options={["Active", "Inactive"]}
									/>
								</div>
								<div className="col-md-12 text-center bt">
									<Button
										className="pull-left "
										btnType="cancel"
										LinkURL="branch"
										children="Cancle"
									/>
									<Button
										className="pull-left "
										btnType="default"
										children="Submit"
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		// posts: postsFormatter(state.posts)
	};
};

const mapDispatchToProps = dispatch => {
	return {
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
