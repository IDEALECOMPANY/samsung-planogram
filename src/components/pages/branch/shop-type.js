import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import { branchTypeActions } from "../../../actions";
import { branchTypesFormatter } from "../../../selectors";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import ShopTypeList from "../../commons/shopTypeList";

const Div = styled.div`
	position: relative;
	width: 100%;

	.btn {
		&.btn-toggle {
			line-height: 1;
			background-color: transparent;
			color: #7281d6;
			&:focus {
				outline: none;
				-webkit-box-shadow: none;
			}
		}
	}
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 200px;
		margin-top: 5px;
		position: absolute;
		right: 15px;
		margin-top: 15px;
		z-index: 99;
		transition: all 0.2s ease-out;

		li {
			display: list-item;
			list-style: none;
			padding: 15px 20px;
			border-bottom: 1px solid #e5e5e5;
			font-size: 14px;
			text-align: left;
			cursor: pointer;
			color: ${props => props.theme.color.black};

			.d-text {
				margin-left: 15px;
			}

			&:hover {
				color: ${props => props.theme.color.primary};
			}

			.icon {
				width: 20px;
			}
			li:last-child {
				border-bottom: 1px solid transparent;
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		branchTypes: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false
		};
		this.importBoxOpen = this.importBoxOpen.bind(this);
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	componentDidMount() {
		if (this.props.branchTypes.length === 0) {
			this.props.branchTypeActions.getBranchTypes();
		}
	}

	importBoxOpen() {
		this.setState(prevState => ({
			...prevState,
			importBox: !prevState.importBox
		}));
	}

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.branchTypeActions
				.deleteBranchType({
					id: id
				})
				.then(() => {
					resolve();
				})
				.catch(error => {
					// show error alert
					reject();
				});
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="branch"
								title="Shop Type"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-20 bt">
							<Button
								btnType="default"
								iconLeft="icon-export"
							>
								Export Shop Type
							</Button>
							<Button
								onClick={() =>
									this.nextPath("/branch/type/add")
								}
								btnType="default"
								iconLeft="icon-add"
							>
								Add Shop Type
							</Button>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						{this.state.importBox && (
							<ImportBox nav="branch" title="Import Branch" />
						)}
						<ShopTypeList onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		branchTypes: state.branchTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		branchTypeActions: bindActionCreators(branchTypeActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
