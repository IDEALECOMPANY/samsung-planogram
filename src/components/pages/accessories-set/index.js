import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import {
	branchTypeActions,
	deviceActions,
	deviceCategoryActions,
	mediaActions,
	mediaTypeActions,
	accessoryActions
} from "../../../actions";

import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import ImportBox from "../../commons/importBox";
import AccessoriesList from "../../commons/accessoriesList";

const Div = styled.div`
	width: 100%;
	position: relative;
	margin-top: 30px;

	.btn {
		&.btn-toggle {
			line-height: 1;
			padding-top: 42px;
			background-color: transparent;
			color: #7281d6;

			&:focus {
				outline: none;
				-webkit-box-shadow: none;
			}
		}
	}
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 200px;
		margin-top: 5px;
		position: absolute;
		right: 15px;
		margin-top: 15px;
		z-index: 99;
		transition: all 0.2s ease-out;

		li {
			display: list-item;
			list-style: none;
			padding: 15px 20px;
			border-bottom: 1px solid #e5e5e5;
			font-size: 14px;
			text-align: left;
			cursor: pointer;
			color: ${props => props.theme.color.black};

			.d-text {
				margin-left: 15px;
			}

			&:hover {
				color: ${props => props.theme.color.primary};
			}

			.icon {
				width: 20px;
			}
			li:last-child {
				border-bottom: 1px solid transparent;
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		branchTypes: [],
		devices: [],
		deviceCategories: [],
		medias: [],
		mediaTypes: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			showDropdown: false,
			importBox: false
		};
		this.importBoxOpen = this.importBoxOpen.bind(this);
		this.toggle = this.toggle.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	componentDidMount() {
		if (this.props.branchTypes.length === 0) {
			this.props.branchTypeActions.getBranchTypes();
		}
		
		if (this.props.devices.length === 0) {
			this.props.deviceActions.getDevices();
		}
		
		if (this.props.deviceCategories.length === 0) {
			this.props.deviceCategoryActions.getDeviceCategories();
		}
		
		if (this.props.medias.length === 0) {
			this.props.mediaActions.getMedias();
		}
		
		if (this.props.mediaTypes.length === 0) {
			this.props.mediaTypeActions.getMediaTypes();
		}
	}

	importBoxOpen = () => {
		this.setState(prevState => ({
			...prevState,
			importBox: !prevState.importBox
		}));
	};

	toggle(e) {
		e.preventDefault();
		this.setState({
			showDropdown: !this.state.showDropdown
		});
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onDelete(id) {
		return new Promise((resolve, reject) => {
			this.props.accessoryActions.deleteAccessory({
				id: id
			}).then(() => {
				resolve();
			}).catch(error => {
				// show error alert
				reject();
			});
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-5">
							<Titlepage icon="accessories" title="Accessories Set" line/>
						</div>
						<div className="col-md-7 text-right mt-20 bt">
							<Button btnType="default" iconLeft="icon-export">Export Accessories Set</Button>
							<Button onClick={() => this.nextPath("/accessories-set/add")} btnType="default" iconLeft="icon-add">Add Accessories Set</Button>
						</div>
					</div>
					<Hr />
					<div className="table-box">
						{this.state.importBox && (
							<ImportBox title="Import Device" />
						)}
						<AccessoriesList onDelete={this.onDelete} />
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		branchTypes: state.branchTypes,
		devices: state.devices,
		deviceCategories: state.deviceCategories,
		medias: state.medias,
		mediaTypes: state.mediaTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		branchTypeActions: bindActionCreators(branchTypeActions, dispatch),
		deviceActions: bindActionCreators(deviceActions, dispatch),
		deviceCategoryActions: bindActionCreators(deviceCategoryActions, dispatch),
		mediaActions: bindActionCreators(mediaActions, dispatch),
		mediaTypeActions: bindActionCreators(mediaTypeActions, dispatch),
		accessoryActions: bindActionCreators(accessoryActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
