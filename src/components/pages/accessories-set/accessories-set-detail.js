import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import { accessoryActions } from "../../../actions";
import { postsFormatter, getUniqueString } from "../../../selectors";
import { uuidUtils, arrayUtils } from "../../../utils";

import Titlepage from "../../commons/titlepage";
import Hr from "../../commons/hr";
import FormAccessories from "../../commons/FormAccessories";

const Div = styled.div`
	width: 100%;
	position: relative;
	.modal {
		display: block !important;
		.modal-header {
			padding: 10px 15px;
		}
		.modal-footer {
			border-top: 0;
		}

		.modal-body {
			h6 {
				background-color: #f5f5f5;
				font-size: 16px;
				padding: 10px 15px;
				border-bottom: 1px solid #ccc;
			}

			.body-inner {
				float: left;
				width: 100%;
				padding: 15px;
				border: 1px solid #ccc;
				padding: 0;
				border-radius: 7px;
			}
		}
	}
	.modal_ {
		position: fixed !important;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background-color: rgba(0, 0, 0, 0.5);
		top: 0;
		left: 0;
	}

	.modal-dialog {
		max-width: 800px;
	}

	button {
		height: 33px;
		border: 1px solid ${props => props.theme.color.primary};
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 30px;
		border-radius: 5px;
		color: ${props => props.theme.color.primary};

		&.btn {
			height: 33px;
			cursor: pointer;
			font-size: ${props => props.theme.font.small};
			padding: 0px 20px;
			border-radius: 5px;
			color: ${props => props.theme.color.primary};
			display: inline-block;

			&.btn-primary {
				background-color: ${props => props.theme.bg.primary};
				border: 1px solid ${props => props.theme.border.primary};
				color: #fff;
			}

			&.btn-default {
				background-color: ${props => props.theme.bg.default};
				border: 1px solid ${props => props.theme.border.primary};
			}

			&.btn-cancel {
				background-color: ${props => props.theme.bg.gray};
				border: 1px solid ${props => props.theme.border.gray};
				color: ${props => props.theme.color.gray};
			}
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		accessory: null
	};

	constructor(props, context) {
		super(props, context);
		this.state = {
			image: "",
			imagePreview: "",
			qty: "1",
			shopType: "",
			active: "1",
			accessories: [],
			showManagePlanogramSet: false,
			showArrangeTableModal: false,
			planogramSetArrange: {},
			options_device: this.props.options_device,
			options_model: this.props.options_model,
			branchTypes: this.props.branchTypes
		};

		this.openManage = this.openManage.bind(this);
		this.onChangeImage = this.onChangeImage.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onAddcapacity = this.onAddcapacity.bind(this);
		this.onSelectChange = this.onSelectChange.bind(this);
		this.onDeleteBox = this.onDeleteBox.bind(this);
		
		this.manageQty = this.manageQty.bind(this);
		this.createTable = this.createTable.bind(this);
		this.createDevice = this.createDevice.bind(this);
		this.scrollToBottom = this.scrollToBottom.bind(this);
		this.propToState = this.propToState.bind(this);
	}

	componentDidMount() {
		if (this.props.accessory === null) {
			this.onCancel();
		} else {
			this.propToState(this.props.accessory);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.accessory !== nextProps.accessory) {
			this.propToState(nextProps.accessory);
		}
	}

	propToState(props) {
		this.setState(prevState => ({
			...prevState,
			shopType: props.shopType.title,
			active: props.active ? "1" : "2",
			imagePreview: props.image,
			qty: props.accessoryTableSets.length,
			accessories: props.accessoryTableSets.map(i => {
				let devices = i.accessoryTableSetDevices.map((j, k) => {
					let price, typeId, id;
					
					if (j.type === "device") {
						price = j.device.price;
						typeId = `device-${j.device.deviceCategory.id}`;
						id = `device-${j.device.id}`;
					} else {
						price = "";
						let tmp = this.props.mediaTypes.filter(p => p.code === j.label);
						typeId = `media-${tmp.length > 0 ? tmp[0].id : ""}`;
						id = `media-${j.medium.id}`;
					}

					let options = {
						positions: [...this.props.options_position],
						devices: j.label === "Number" ? [...this.props.options_device] : this.props.options_device.filter(k => k.label === "media").map(k => {
							return {
								...k,
								options: k.options.filter(p => p.code === j.label)
							};
						}),
						models: this.props.options_model.filter(k => k.label === j.type).map(k => {
							return {
								...k,
								options: j.label === "Number" ? k.options.filter(p => p.typeInfo.indexOf(typeId) !== -1) : k.options.filter(p => p.typeInfo.indexOf(j.label) !== -1)
							};
						})
					};
					console.log("propToState", this.props.options_model, options);
					return this.createDevice(k+1, j.label, j.label, id, j.type, price, typeId, options);
				});

				return this.createTable(devices);
			})
		}));
	}

	onSelectChange(tableId, elementId, event) {
		let name = event.selectName;
		let value = event;
		let tables = this.state.accessories.filter(i => i.id === tableId);

		if (tables.length > 0) {
			if (name === "position") {
				this.setState(prevState => ({
					...prevState,
					accessories: prevState.accessories.map(i => i.id !== tableId ? i
						: {
							...i,
							devices: i.devices.map(j => j.id !== elementId ? j
								: {
									...j,
									label: value.label,
									value: value.value,
									price: "",
									typeId: "",
									id: "",
									options: {
										...j.options,
										devices: value.value === "Number" ? [...this.props.options_device]
											: this.props.options_device.filter(k => k.label === "media").map(k => {
												return {
													...k,
													options: k.options.filter(p => p.code === value.value)
												};
											}),
										models: []
									}
								}
							)
						}
					)
				}));
			} else if (name === "device") {
				let type = value.value.split("-")[0];
				this.setState(prevState => ({
					...prevState,
					accessories: prevState.accessories.map(i => i.id !== tableId ? i : {
						...i,
						devices: i.devices.map(j => j.id !== elementId ? j : {
							...j,
							typeId: value.value,
							id: "",
							price: "",
							type: type,
							options: {
								...j.options,
								models: this.props.options_model.filter(k => k.label === type).map(k => {
									return {
										...k,
										options: k.options.filter(p => p.typeInfo.indexOf(value.code) !== -1)
									};
								})
							}
						})
					})
				}));
			} else if (name === "model") {
				this.setState(prevState => ({
					...prevState,
					accessories: prevState.accessories.map(i => i.id !== tableId ? i : {
						...i,
						devices: i.devices.map(j => j.id !== elementId ? j : {
							...j,
							id: value.value,
							price: value.price,
							options: {
								...j.options
							}
						})
					})
				}));
			}
		}
	}

	onDeleteBox(tableId, itemId) {
		let tables = this.state.accessories.filter(i => i.id === tableId);

		if (tables.length > 0) {
			this.setState(prevState => ({
				...prevState,
				accessories: prevState.accessories.map(i => i.id !== tableId ? i : {
					...i,
					devices: i.devices.filter(j => `${j.id}` !== `${itemId}`)
				})
			}));
		}
	}

	onAddcapacity(tableId, ref) {
		let tables = this.state.accessories.filter(i => i.id === tableId);

		if (tables.length > 0) {
			this.setState(prevState => ({
				...prevState,
				accessories: prevState.accessories.map(i => i.id !== tableId ? i : {
					...i,
					devices: [...i.devices, this.createDevice(i.devices.length + 1)] 
				})
			}), () => {
				console.log("After add device: ", this.state.accessories);
				this.scrollToBottom(ref);
			});
		}
	}

	openManage() {
		this.setState(prevState => ({ ...prevState, showManagePlanogramSet: true }));
		// this.manageQty(this.state.qty, () => {
			
		// });
	}

	openArrangeTable(planogramSet) {
		this.setState(prevState => ({
			...prevState,
			showArrangeTableModal: true,
			planogramSetArrange: planogramSet
		}));
	}

	closeArrangeTable() {
		this.setState(prevState => ({ ...prevState, showArrangeTableModal: false }));
	}

	onChangeImage(event) {
		let reader = new FileReader();
		let file = event.target.files[0];
		let name = event.target.name;
		reader.onloadend = () => {
			this.setState(prevState => ({
				...prevState,
				[name]: reader.result
			}));
		};
		reader.readAsDataURL(file);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;
		let zone = event.zone;
		if (zone === "qty") {
			this.manageQty(value);
		} else {
			this.setState(prevState => ({ ...prevState, [name]: value }));
		}
	}

	manageQty(value, callback) {
		let tmpVal = parseInt(value);
		let validValue = tmpVal;

		if (tmpVal) {
			if (tmpVal < 0) {
				validValue = value = 0;
			}
		} else {
			validValue = 0;
		}

		this.setState(prevState => ({
			...prevState,
			qty: value,
			accessories: arrayUtils.generateArray(validValue).map((i, k) => k < prevState.accessories.length
				? prevState.accessories[k] : this.createTable()
			)
		}), () => {
			if (callback) callback();
		});
	}

	createTable(devices=false) {
		return {
			id: uuidUtils.create(),
			devices: devices ? devices : arrayUtils.generateArray(this.props.defaultDeviceLength).map((i, k) => this.createDevice(k + 1)),
		};
	}

	createDevice(index, label="Number", value="Number", id=uuidUtils.create(), type="", price="", typeId="", options=false) {
		return {
			id: id,
			index: index,
			label: label,
			value: value,
			type: type,
			price: price,
			typeId: typeId,
			options: options ? options : {
				positions: [...this.props.options_position],
				devices: [...this.props.options_device],
				models: [],
			}
		};
	}

	scrollToBottom(ref) {
		if (ref) {
			ref.scrollTop = ref.scrollHeight;
		}
	}

	onCancel() {
		this.props.history.push("/accessories-set");
	}

	onAccept() {
		let params = {
			id: this.props.accessory.id,
			image: this.state.image,
			shopTypeId: this.state.shopType,
			active: this.state.active,
			tables: this.state.accessories.map(i => {
				return i.devices.map(j => ({
					itemId: j.id.split("-")[1],
					type: j.type,
					label: j.label
				}));
			})
		};

		this.props.accessoryActions.editAccessory(params).then(res => {
			console.log("Edit Accessory Success:", res);
		}).catch(error => {
			console.log("Add Accessory Error:", error);
		})
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage icon="accessories" title="New Accessories Set" line />
						</div>
						<div className="col-md-6 text-right mt-20 bt" />
					</div>
					<Hr />
					<FormAccessories
						data={this.state}
						onChange={this.onChange}
						openManage={this.openManage}
						openArrangeTable={this.openArrangeTable}
						closeArrangeTable={this.closeArrangeTable}
						onChangeImage={this.onChangeImage}
						onAccept={this.onAccept}
						onSelectChange={this.onSelectChange}
						onDeleteBox={this.onDeleteBox}
						onAddcapacity={this.onAddcapacity}
					/>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let accessories = state.accessories.filter(i => `${i.id}` === `${id}`);
	let accessory = accessories.length > 0 ? accessories[0] : { error: true };

	console.log("mapStateToProps", accessory);

	return {
		accessory: accessory,
		branchTypes: state.branchTypes,
		devices: state.devices,
		deviceCategories: state.deviceCategories,
		medias: state.medias,
		mediaTypes: state.mediaTypes,
		options_device: [
			{
				label: "media",
				options: state.mediaTypes.map(i => {
					return {
						value: `media-${i.id}`,
						label: i.title,
						code: i.code
					};
				})
			}, {
				label: "device",
				options: state.deviceCategories.map(i => {
					return {
						value: `device-${i.id}`,
						label: i.title,
						code: `device-${i.id}`
					};
				})
			}
		],
		options_model: [
			{
				label: "media",
				options: state.medias.map(i => {
					return {
						value: `media-${i.id}`,
						label: i.title,
						typeInfo: i.mediaTypes.map(j => j.code),
						type: "media"
					};
				})
			}, {
				label: "device",
				options: state.devices.map(i => {
					let name = i.deviceModel ? i.deviceModel.title : "";
					let color = i.deviceColor ? i.deviceColor.title : "";
					let gb = i.deviceCapacity ? i.deviceCapacity.title : "";
					return {
						value: `device-${i.id}`,
						label: `${name} ${color} ${gb}`.trim(),
						typeInfo: [`device-${i.deviceCategory.id}`],
						type: "devices",
						price: i.price
					};
				})
			}
		],
		options_position: [{ label: "Number", value: "Number" }, ...state.mediaTypes.map(i => ({ label: i.code, value: i.code, data: {...i} }))]
	};
};

const mapDispatchToProps = dispatch => {
	return {
		accessoryActions: bindActionCreators(accessoryActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
