import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { tableTypeActions } from "../../../actions";
import { tableTypesFormatter, getUniqueString } from "../../../selectors";
import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import Titlepage from "../../commons/titlepage";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormTableType from "../../commons/formTableType";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		tableType: null,
		mediaTypes: null,
		tableTypes: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			id: "",
			code: "",
			title: "",
			active: "",
			mediaTypes: [],
			mediaTypesOptions: []
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
		this.onAddMediaType = this.onAddMediaType.bind(this);
		this.onDeleteMediaType = this.onDeleteMediaType.bind(this);
	}

	componentDidMount() {
		if (this.props.mediaTypes === null || this.props.tableTypes === null) {
			this.onCancel();
		} else {
			if (this.props.tableType !== null) {
				this.checkProps(this.props.tableType);
			}
			this.setState(prevState => ({
				...prevState,
				mediaTypesOptions: this.props.mediaTypes.map(i => ({
					id: i.id,
					title: i.title
				}))
			}));
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.tableType !== nextProps.tableType) {
			this.checkProps(nextProps.tableType);
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/table/type");
		} else {
			this.setState(prevState => ({
				...prevState,
				id: props.id,
				code: props.code,
				title: props.title,
				active: props.active ? "1" : "2",
				mediaTypes: props.mediaTypes.map(i => {
					let uuid = getUniqueString();
					return {
						id: uuid,
						name: `mediaTypes-${uuid}`,
						selectedOption: `${i.id}`,
						qty: `${i.qty}`,
						nameQty: `qty-${uuid}`,
						value: "",
						placeholder: "E: End Cap หัวโต๊ะ"
					};
				}),
				mediaTypesOptions: props.mediaTypesOptions
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;
		let zone = event.zone;

		if (zone === "mediaTypes") {
			this.setState({
				mediaTypes: this.state.mediaTypes.map((i, k) => {
					if (i.name === name) {
						return {
							...i,
							selectedOption: value
						};
					} else {
						return i;
					}
				})
			});
		} else if (zone === "qty") {
			this.setState({
				mediaTypes: this.state.mediaTypes.map((i, k) => {
					if (i.nameQty === name) {
						return {
							...i,
							qty: value
						};
					} else {
						return i;
					}
				})
			});
		} else {
			this.setState(prevState => ({ ...prevState, [name]: value }));
		}
	}

	onAccept() {
		if (this.state.title !== "" || this.state.code !== "") {
			this.props.tableTypeActions
				.editTableType({
					id: this.state.id,
					code: this.state.code,
					title: this.state.title,
					active: `${this.state.active}` === "1",
					createdAt: this.props.tableType.createdAt,
					updatedAt: new Date(),
					mediaTypes: this.state.mediaTypes.map(i => {
						return {
							id: parseInt(i.selectedOption),
							qty: parseInt(i.qty)
						};
					})
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/table/type");
	}

	onAddMediaType() {
		let uuid = getUniqueString();
		let selectBox = {
			id: uuid,
			name: `mediaTypes-${uuid}`,
			qty: "",
			nameQty: `qty-${uuid}`,
			selectedOption: "",
			value: "",
			placeholder: "E: End Cap หัวโต๊ะ"
		};
		this.setState(prevState => ({
			...prevState,
			mediaTypes: [...prevState.mediaTypes, selectBox]
		}));
	}

	onDeleteMediaType(uuid) {
		this.setState({
			mediaTypes: this.state.mediaTypes.filter(
				i => `${i.id}` !== `${uuid}`
			)
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="table"
								title="Table Type"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<div className="row">
							<FormTableType
								view
								data={this.state}
								onChange={this.onChange}
								mediaTypesOptions={
									this.state.mediaTypesOptions
								}
								onAddMediaType={this.onAddMediaType}
								onDeleteMediaType={this.onDeleteMediaType}
							/>
							<div className="col-md-12 text-center bt">
								<Button
									onClick={this.onCancel}
									className="pull-left "
									btnType="cancel"
									children="Cancle"
								/>
								<Button
									onClick={this.onAccept}
									className="pull-left "
									btnType="default"
									children="Save"
								/>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let tableTypes = state.tableTypes.filter(i => `${i.id}` === `${id}`);

	let tableType = tableTypes.length > 0 ? tableTypes[0] : { error: true };

	return {
		tableType: tableType,
		mediaTypes: state.mediaTypes,
		tableTypes: state.tableTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		tableTypeActions: bindActionCreators(tableTypeActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
