import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";

import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Button from "../../commons/button";
import Titlepage from "../../commons/titlepage";
import Hr from "../../commons/hr";
import FormTableSize from "../../commons/formTableSize";

import { tableSizeActions } from "../../../actions";
import { tableSizesFormatter } from "../../../selectors";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		tableTypes: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			code: "",
			title: "",
			capacity: "",
			tableTypeId: "",
			active: "1",
			tableTypes: this.props.tableTypes
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.state.tableTypes === null) {
			this.onCancel();
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (
			this.state.title !== "" &&
			this.state.code !== "" &&
			this.state.capacity !== "" &&
			this.state.tableTypeId !== ""
		) {
			this.props.tableSizeActions
				.addTableSize({
					code: this.state.code,
					title: this.state.title,
					capacity: this.state.capacity,
					tableTypeId: this.state.tableTypeId,
					active: `${this.state.active}` === "1"
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/table/size");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="table"
								title="New Table Size"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<div className="row">
							<FormTableSize
								view
								data={this.state}
								onChange={this.onChange}
							/>
							<div className="col-md-12 text-center bt">
								<Button
									onClick={this.onCancel}
									className="pull-left "
									btnType="cancel"
									children="Cancle"
								/>
								<Button
									onClick={this.onAccept}
									className="pull-left "
									btnType="default"
									children="Submit"
								/>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		tableTypes: state.tableTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		tableSizeActions: bindActionCreators(tableSizeActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
