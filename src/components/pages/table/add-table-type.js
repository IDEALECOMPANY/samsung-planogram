import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { tableTypeActions } from "../../../actions";
import { tableTypesFormatter, getUniqueString } from "../../../selectors";
import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import Titlepage from "../../commons/titlepage";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormTableType from "../../commons/formTableType";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		mediaTypes: null,
		tableTypes: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			code: "",
			title: "",
			active: 1,
			mediaTypes: [],
			mediaTypesOptions: []
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
		this.onAddMediaType = this.onAddMediaType.bind(this);
		this.onDeleteMediaType = this.onDeleteMediaType.bind(this);
	}

	componentDidMount() {
		if (this.props.mediaTypes === null || this.props.tableTypes === null) {
			this.onCancel();
		} else {
			this.setState(prevState => ({
				...prevState,
				mediaTypesOptions: this.props.mediaTypes.map(i => ({
					id: i.id,
					title: i.title
				}))
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;
		let zone = event.zone;

		if (zone === "mediaTypes") {
			this.setState({
				mediaTypes: this.state.mediaTypes.map((i, k) => {
					if (i.name === name) {
						return {
							...i,
							selectedOption: value
						};
					} else {
						return i;
					}
				})
			});
		} else if (zone === "qty") {
			this.setState({
				mediaTypes: this.state.mediaTypes.map((i, k) => {
					if (i.nameQty === name) {
						return {
							...i,
							qty: value
						};
					} else {
						return i;
					}
				})
			});
		} else {
			this.setState(prevState => ({ ...prevState, [name]: value }));
		}
	}

	onAccept() {
		this.props.tableTypeActions
			.addTableType({
				code: this.state.code,
				title: this.state.title,
				active: `${this.state.active}` === "1",
				mediaTypes: this.state.mediaTypes.map(i => {
					return {
						id: i.selectedOption,
						qty: i.qty
					};
				})
			})
			.then(() => {
				this.onCancel();
			})
			.catch(error => {
				// show error alert
			});
	}

	onCancel() {
		this.props.history.push("/table/type");
	}

	onAddMediaType() {
		let uuid = getUniqueString();
		let selectBox = {
			id: uuid,
			name: `mediaTypes-${uuid}`,
			qty: "",
			nameQty: `qty-${uuid}`,
			selectedOption: "",
			value: "",
			placeholder: "E: End Cap หัวโต๊ะ"
		};
		this.setState(prevState => ({
			...prevState,
			mediaTypes: [...prevState.mediaTypes, selectBox]
		}));
	}

	onDeleteMediaType(uuid) {
		this.setState({
			mediaTypes: this.state.mediaTypes.filter(
				i => `${i.id}` !== `${uuid}`
			)
		});
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="table"
								title="New Table Type"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<div className="row">
							<FormTableType
								view
								data={this.state}
								onChange={this.onChange}
								mediaTypesOptions={
									this.state.mediaTypesOptions
								}
								onAddMediaType={this.onAddMediaType}
								onDeleteMediaType={this.onDeleteMediaType}
							/>
							<div className="col-md-12 text-center bt">
								<Button
									onClick={this.onCancel}
									className="pull-left "
									btnType="cancel"
									children="Cancle"
								/>
								<Button
									onClick={this.onAccept}
									className="pull-left "
									btnType="default"
									children="Submit"
								/>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		mediaTypes: state.mediaTypes,
		tableTypes: state.tableTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		tableTypeActions: bindActionCreators(tableTypeActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
