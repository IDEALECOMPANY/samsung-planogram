import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import { tableSizeActions } from "../../../actions";
import { tableSizesFormatter } from "../../../selectors";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Textfield from "../../commons/textfield";
import Button from "../../commons/button";
import MySearchField from "../../commons/MySearchField";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import SelectOption from "../../commons/selectOption";
import Textarea from "../../commons/textarea";
import Titlepage from "../../commons/titlepage";
import Dropdown from "../../commons/dropdown";
import Panel from "../../commons/panel";
import LastestLogin from "../../commons/lastestLogin";
import Hr from "../../commons/hr";
import FormTableSize from "../../commons/formTableSize";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

export class Page extends Component {
	static defaultProps = {
		tableSize: [],
		tableTypes: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			id: "",
			code: "",
			title: "",
			capacity: "",
			tableTypeId: "",
			active: "1",
			tableTypes: this.props.tableTypes
		};

		this.onChange = this.onChange.bind(this);
		this.onAccept = this.onAccept.bind(this);
		this.onCancel = this.onCancel.bind(this);
	}

	componentDidMount() {
		if (this.props.tableTypes === null) {
			this.onCancel();
		}
		if (this.props.tableSize !== null) {
			this.checkProps(this.props.tableSize);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.tableSize !== nextProps.tableSize) {
			this.checkProps(nextProps.tableSize);
		}
	}

	checkProps(props) {
		if (props.error) {
			this.props.history.push("/table/size");
		} else {
			this.setState(prevState => ({
				...prevState,
				id: props.id,
				code: props.code,
				title: props.title,
				capacity: props.capacity,
				tableTypeId: props.tableType === null ? "" : props.tableType.id,
				active: props.active ? "1" : "2"
			}));
		}
	}

	nextPath(path) {
		this.props.history.push(path);
	}

	onChange(event) {
		let name = event.target.name;
		let value = event.target.value;

		this.setState(prevState => ({ ...prevState, [name]: value }));
	}

	onAccept() {
		if (
			this.state.title !== "" &&
			this.state.code !== "" &&
			this.state.capacity !== "" &&
			this.state.tableTypeId !== ""
		) {
			this.props.tableSizeActions
				.editTableSize({
					id: this.state.id,
					code: this.state.code,
					title: this.state.title,
					capacity: this.state.capacity,
					active: `${this.state.active}` === "1",
					createdAt: this.props.tableSize.createdAt,
					updatedAt: new Date(),
					tableTypeId: this.state.tableTypeId
				})
				.then(() => {
					this.onCancel();
				})
				.catch(error => {
					// show error alert
				});
		} else {
			alert("กรุณาใส่ข้อมูลให้ครบ");
		}
	}

	onCancel() {
		this.props.history.push("/table/size");
	}

	render() {
		return (
			<Div>
				<div className="content">
					<div className="row">
						<div className="col-md-6">
							<Titlepage
								icon="table"
								title="New Table Size"
								line
							/>
						</div>
						<div className="col-md-6 text-right mt-15" />
					</div>
					<Hr />
					<div className="table-box">
						<div className="row">
							<FormTableSize
								view
								data={this.state}
								onChange={this.onChange}
							/>
							<div className="col-md-12 text-center bt">
								<Button
									onClick={this.onCancel}
									className="pull-left "
									btnType="cancel"
									children="Cancle"
								/>
								<Button
									onClick={this.onAccept}
									className="pull-left "
									btnType="default"
									children="Save"
								/>
							</div>
						</div>
					</div>
				</div>
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params.id;
	let tableSizes = state.tableSizes.filter(i => `${i.id}` === `${id}`);

	let tableSize = tableSizes.length > 0 ? tableSizes[0] : { error: true };

	return {
		tableSize: tableSize,
		tableTypes: state.tableTypes
	};
};

const mapDispatchToProps = dispatch => {
	return {
		tableSizeActions: bindActionCreators(tableSizeActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
