import React, { Component } from "react";
import styled from "styled-components";

import Textfield from "../../commons/textfield";
import Button from "../../commons/button";

const Div = styled.div`
	width: 100%;
	position: relative;
	transition: all 0.2s ease-out;

	button {
		margin-right: 5px;
	}

	.red {
		color: red;
	}
`;

const Comp = ({
	title = "",
	sheetname = "",
	onCancelImport,
	onAcceptImport,
	onChangeImport
}) => {
	return (
		<Div>
			<form className="box-dash mb-40  ">
				<div className="row form-group">
					<div className="col-md-2 text-right">
						{title && <h4 className="title">{title}</h4>}
					</div>
				</div>
				<div className="row form-group">
					<div className="col-md-2 text-right">*Remark</div>
					<div className="col-md-10 text-left">
						<span className="red">
							ก่อนอัพโหลดไฟล์ กรุณาตรวจสอบ sheetname ว่าเป็น{" "}
							{sheetname} หรือไม่?
						</span>
					</div>
				</div>
				<div className="row">
					<div className="col-md-2 text-right">Choose your file</div>
					<div className="col-md-7 text-left">
						<Textfield type="file" onChange={onChangeImport} />
					</div>
				</div>
				<div className="row">
					<div className="col-md-2 text-right" />
					<div className="col-md-7 text-left">
						<Button
							btnType="default"
							children="Submit"
							onClick={onAcceptImport}
						/>
						<Button
							btnType="cancel"
							children="Cancel"
							onClick={onCancelImport}
						/>
					</div>
				</div>
			</form>
		</Div>
	);
};

export default Comp;
