import React from "react";
import styled from "styled-components";

import Modal from "../modalAlert";

const Div = styled.div`
	width: 50px;
	height: 50px;
	position: relative;

	.loader {
		width: 100%;
		height: 100%;
		border-radius: 50%;
		border: 5px solid #f3f3f3;
		border-top: 5px solid #3498db;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}

	/* Safari */
	@-webkit-keyframes spin {
		0% {
			-webkit-transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
		}
	}

	@keyframes spin {
		0% {
			transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
		}
	}
`;

const Comp = () => {
	return (
		<Modal>
			<Div>
				<div className="loader" />
			</Div>
		</Modal>
	);
};

export default Comp;
