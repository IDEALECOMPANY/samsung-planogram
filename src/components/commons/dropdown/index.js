import React, { Component } from "react";
import styled from "styled-components";

const Div = styled.div`
	position: relative;
	transition: all 0.2s ease-out;

	.btn {
		font-size: 50px;
		line-height: 1;
		margin-top: -30px;
		background-color: transparent;
		color: #7281d6;

		&:focus {
			outline: none;
			-webkit-box-shadow: none;
		}
	}

	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 200px;
		margin-top: 5px;
		position: absolute;
		right: 0;
		margin-top: 15px;
		z-index: 99;
		transition: all 0.2s ease-out;

		ul {
			display: block;
			padding-left: 0;

			li {
				display: list-item;
				list-style: none;
				padding: 15px 20px;
				border-bottom: 1px solid #e5e5e5;
				font-size: 14px;
				text-align: left;

				a {
					text-decoration: none;
					color: ${props => props.theme.color.black};
					cursor: pointer;

					.d-text {
						margin-left: 15px;
					}

					&:hover {
						color: ${props => props.theme.color.primary};
					}

					.icon {
						width: 20px;
					}
				}
				li:last-child {
					border-bottom: 1px solid transparent;
				}
			}
		}
	}
`;

class Comp extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			showDropdown: false
		};

		this.toggle = this.toggle.bind(this);
	}

	toggle(e) {
		e.preventDefault();
		this.setState({ showDropdown: !this.state.showDropdown });
	}

	render() {
		return (
			<Div>
				<button onClick={this.toggle} className="btn">
					{this.props.button}
				</button>

				{this.state.showDropdown && (
					<div className="box">
						<ul>
							{this.props.dropdownList.map((i, k) => {
								return (
									<li key={k}>
										<span>
											{(() => {
												switch (i.icon) {
													case "add":
														return (
															<div
																onClick={event => {
																	if (
																		this
																			.props
																			.onClick
																	)
																		this.props.onClick(
																			event
																		);
																}}
															>
																<img
																	className="icon"
																	src="assets/images/icon-add.png"
																/>
																<span className="d-text">
																	{i.text}
																</span>
															</div>
														);
													case "export":
														return (
															<div>
																<img
																	className="icon"
																	src="assets/images/icon-export.png"
																/>
																<span className="d-text">
																	{i.text}
																</span>
															</div>
														);
													case "import":
														return (
															<div>
																<img
																	className="icon"
																	src="assets/images/import.png"
																/>
																<span className="d-text">
																	{i.text}
																</span>
															</div>
														);
													default:
														return "";
												}
											})()}
										</span>
									</li>
								);
							})}
						</ul>
					</div>
				)}
			</Div>
		);
	}
}

export default Comp;
