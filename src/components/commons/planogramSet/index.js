import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../textfield";
import SelectOption from "../selectOption";
import DragSortableList from "react-drag-sortable";
import Listsort from "../listSort";
import Listsort1 from "../listSort1";
import LayoutSizeS from "../layoutSizeS";
import LayoutVerticle from "../layoutVerticle";

const Div = styled.div`
	padding: 15px;
	box-shadow: 1px 1px 10px #ccc;
	margin-bottom: 30px;
	float: left;
	width: 100%;

	.top {
		a {
			color: ${props => props.theme.color.primary} !important;
			cursor: pointer;
		}
		h3 {
			font-size: 25px;
			color: ${props => props.theme.color.primary};
			font-family: ${props => props.theme.text.bold};
		}
	}

	h4 {
		font-size: 16px;
		color: ${props => props.theme.color.primary};
		font-family: ${props => props.theme.text.bold};
		margin: 25px 0;
	}
	.flip {
		border: 0;
		margin-top: -4px;
		&:focus {
			border: 0;
			outline: none;
		}
	}
`;

class Comp extends Component {
	static defaultProps = {
		row: [],
		edit: ""
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			selectedOption: "",
			flipButton: 0,
			row1: [],
			row2: [],
			edit: this.props.edit
		};

		this.getRow1 = this.getRow1.bind(this);
		this.getRow2 = this.getRow2.bind(this);
		this.editOption = this.editOption.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.flipClick = this.flipClick.bind(this);
	}

	componentDidMount() {
		if (this.props.row.length > 0) {
			let half = this.props.row.length / 2;
			let row1 = this.props.row.slice(0, half);
			let row2 = this.props.row.slice(half);
			this.setState(prevState => ({
				...prevState,
				row1: row1,
				row2: row2
			}));
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.row.length !== nextProps.row.length) {
			let half = nextProps.row.length / 2;
			let row1 = nextProps.row.slice(0, half);
			let row2 = nextProps.row.slice(half);
			this.setState(prevState => ({
				...prevState,
				row1: row1,
				row2: row2
			}));
		}
	}

	getSlice() {
		if (this.props.row.length > 0) {
			let half = this.props.row.length / 2;
			let row1 = this.props.row.slice(0, half);
			let row2 = this.props.row.slice(half);
			let res = [row1, row2];
			return res;
		}
	}

	handleChange(selectedOption) {
		this.setState(prevState => ({...prevState, selectedOption }));
	}

	flipClick() {
		this.setState(prevState => ({
			...prevState,
			flipButton: prevState.flipButton + 1 > 3 ? 0 : prevState.flipButton + 1
		}));
	}

	getRow1() {
		if (this.state.flipButton === 0) {
			return [...this.state.row1];
		} else if (this.state.flipButton === 1) {
			return [...this.state.row2];
		} else if (this.state.flipButton === 2) {
			return [...this.state.row2.reverse()];
		} else if (this.state.flipButton === 3) {
			return [...this.state.row1.reverse()];
		}
	}

	getRow2() {
		if (this.state.flipButton === 0) {
			return [...this.state.row2];
		} else if (this.state.flipButton === 1) {
			return [...this.state.row1];
		} else if (this.state.flipButton === 2) {
			return [...this.state.row1.reverse()];
		} else if (this.state.flipButton === 3) {
			return [...this.state.row2.reverse()];
		}
	}

	editOption() {
		this.setState(prevState => ({...prevState, edit: false }));
	}

	render() {
		let listGridDevice = [
			{
				content: (
					<table>
						<tbody>
							<tr width="100%">
								<td width="12%">Position</td>
								<td width="20%">Device</td>
								<td width="20%">Model</td>
								<td width="20%">Price</td>
							</tr>
						</tbody>
					</table>
				),
				classes: ["head no-drag"]
			}
		];
		this.props.tables.devices.forEach((element, index) => {
			listGridDevice.push({
				content: (
					<Listsort
						id={element.label}
						selectAble
						data={element}
						options_model={this.props.options_model}
						options_device={this.props.options_device}
						onChangeSelect={this.props.onChangeSelect}
						onChangeSelectDevice={this.props.onChangeSelectDevice}
						edit={this.state.edit ? true : false}
					/>
				),
				classes: ["body no-drag"]
			});
		});
		let listGridPromotion = [
			{
				content: (
					<table>
						<tbody>
							<tr width="100%">
								<td width="12%">Position</td>
								<td width="50%">Media</td>
							</tr>
						</tbody>
					</table>
				),
				classes: ["head no-drag"]
			}
		];
		this.props.tables.media.forEach((element, index) => {
			listGridPromotion.push({
				content: (
					<Listsort1
						id={element.label}
						selectAble
						data={element}
						options_model_media={this.props.options_model_media}
						onChangeSelect={this.props.onChangeSelect}
						edit={this.state.edit ? true : false}
					/>
				),
				classes: ["no-drag body"]
			});
		});

		let onSort = function(sortedList) {
		};

		let dataRow = this.props.tables.devices;

		let row2 = dataRow.slice(Math.ceil(dataRow.length / 2));

		// value select
		const { selectedOption } = this.state;
		const value = selectedOption && selectedOption.value;

		return (
			<Div>
				<div className="row top">
					<div className="col-md-4">
						{this.props.title && <h3>{this.props.title}</h3>}
					</div>
					<div className="col-md-4 text-center">
						{this.props.edit && (
							<button className="flip">
								<img
									onClick={this.flipClick}
									src="/assets/images/flip-left-to-right.png"
								/>
							</button>
						)}
					</div>
					<div className="col-md-4 text-right">
						{this.props.edit && (
							<a onClick={this.editOption}>Edit </a>
						)}
					</div>
				</div>
				<div>
					<LayoutVerticle
						row={this.getSlice()}
						el={this.props.el}
						media={this.props.tables.media}
						devices={this.props.devices}
						medias={this.props.medias}
					/>
				</div>

				<div className="device">
					<div className="row">
						<div className="col-md-12">
							<h4>Device</h4>
						</div>
					</div>

					<DragSortableList
						items={listGridDevice}
						onSort={onSort}
						type="vertical"
						moveTransitionDuration={0.3}
					/>

					<div className="row float-left">
						<div className="col-md-12">
							<h4>Media</h4>
						</div>
					</div>

					<DragSortableList
						items={listGridPromotion}
						onSort={onSort}
						type="vertical"
						moveTransitionDuration={0.3}
					/>
				</div>
			</Div>
		);
	}
}

export default Comp;
