import React, { Component } from "react";
import styled from "styled-components";

const Div = styled.div`
	width: 100%;
	position: relative;
	background-color: #fff;

	.topbar {
		box-shadow: 1px 1px 6px 2px #d8d8d8;
		position: fixed;
		top: 0;
		width: 100%;
		background-color: #fff;
		z-index: 999;

		.top-logo {
			padding: 15px 30px;
			padding-left: 30px;
			width: 240px;
			float: left;
		}

		.toggle-menu {
			padding: 15px;
			cursor: pointer;

			img {
				margin-right: 5px;
				margin-top: -3px;
			}

			a {
				color: ${props => props.theme.color.primary};
				font-family: ${props => props.theme.text.bold};
			}

			i {
				margin-right: 10px;
			}
		}
	}
`;

class Comp extends Component {
	static defaultProps = {
		src: "",
		errorSrc: "",
		toggleMenu: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			visible: false
		};
	}

	render() {
		return (
			<Div>
				<div className=" topbar">
					<div className="top-logo">
						<img src="/assets/images/logo-samsung.png" />
					</div>
					<div className="toggle-menu">
						<a
							onClick={event => {
								if (this.props.toggleMenu)
									this.props.toggleMenu();
							}}
						>
							<img src="/assets/images/icon-toogle-menu.png" />{" "}
							Menu
						</a>
					</div>
				</div>
			</Div>
		);
	}
}

export default Comp;
