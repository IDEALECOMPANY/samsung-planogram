import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../../commons/textfield";
import SelectOption from "../../commons/selectOption";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import Button from "../../commons/button";
import ListMediaSize from "../../commons/listMediaSize";

const Div = styled.div`
	width: 100%;
	position: relative;
	.box {
		width: 100%;
		position: relative;
		img {
			position: absolute;
			right: -20px;
			top: 8px;
			cursor: pointer;
			&:hover {
				opacity: 0.8;
			}
		}
	}
	.box-media-size {
		margin: 0px -5px;
		position: relative;
		.in {
			width: 50%;
			padding: 0px 5px;
			display: inline-block;
		}
	}
	.label-head {
		font-size: 16px;
		margin-top: 0;
		margin-bottom: 5px;
		font-family: Roboto Bold;
	}

	.mediaSize-result {
		margin-top: 30px;
		padding: 15px;
		border: 1px solid #ced4da;
		min-height: 200px;
		max-height: 350px;
		overflow: auto;

		ul {
			margin-top: 15px;
		}
	}
`;

const Comp = ({
	data = {},
	onChange,
	onChangeImage,
	mediaTypesOptions = [],
	onAddMediaType,
	onDeleteMediaType,
	tableTypesOptions = [],
	onAddTableType,
	onDeleteTableType,
	mediaSizesOptions = [],
	onAddMediaSize,
	onDeleteMediaSize,
	searchMediaSize
}) => {
	return (
		<Div>
			<div className="row">
				<div className=" col-md-6 ">
					<Textfield type="file" title="Model Image" name="image" onChange={onChangeImage} />
					<Textfield type="text" title="Name" placeholder="samsungad" name="title" value={data.title} onChange={onChange} />
					<Textfield type="text" title="Detail" placeholder="ad wall" name="desc" value={data.desc} onChange={onChange} />
					<Textfield type="text" title="Media Material" placeholder="Media Material" name="material" value={data.material} onChange={onChange} />
					<label className="label-head">Media Location (Table Type)</label>
					{data.tableTypes.map((i, k) => (
						<div className="box" key={k}>
							<SelectOption key={k} {...i} onChange={onChange} options={tableTypesOptions} zone="tableTypes" />
							<img src="/assets/images/cabbage.png" onClick={event => onDeleteTableType(i.id)} />
						</div>
					))}
					<div>
						<Button onClick={onAddTableType} iconLeft="icon-add" btnType="default" children="Add Table Type" />
					</div>
					<br />
					<label className="label-head">Media Type</label>
					{data.mediaTypes.map((i, k) => (
						<div className="box" key={k}>
							<SelectOption key={k} {...i} onChange={onChange} options={mediaTypesOptions} zone="mediaTypes" />
							<img src="/assets/images/cabbage.png" onClick={event => onDeleteMediaType(i.id)} />
						</div>
					))}
					<div>
						<Button onClick={onAddMediaType} iconLeft="icon-add" btnType="default" children="Add Media Type" />
					</div>
					<br />
					<CheckboxRadioInput
						title="Status"
						type="radio"
						name="active"
						value={data.active}
						onChange={onChange}
						options={[
							{
								id: 1,
								title: "Active"
							}, {
								id: 2,
								title: "Inactive"
							}
						]}
					/>
				</div>
				<div className="col-md-6">
					<div className="row">
						<div className="col-md-12">
							<label className="label-head">Media Size</label>
						</div>
						<div className="col-md-12">
							{data.mediaSizes.map((i, k) => (
								<div className="box-media-size" key={k}>
									<div className="in">
										<SelectOption
											key={k}
											{...i}
											onChange={onChange}
											options={mediaSizesOptions}
											zone="mediaSizes"
										/>
									</div>
									<div className="in">
										<Textfield
											type="number"
											placeholder="pieces"
											name={i.nameQty}
											value={i.qty}
											onChange={onChange}
											zone="qty"
										/>
									</div>
								</div>
							))}
							<Button
								onClick={onAddMediaSize}
								iconLeft="icon-add"
								btnType="default"
								children="Add Media Size"
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<div className="mediaSize-result">
								<Textfield
									type="text"
									placeholder="Search:"
									value={data.search}
									onChange={searchMediaSize}
								/>
								<ul className="list-inline">
									{data.search.trim() === "" ? data.mediaSizes.map((i, k) => {
										return (
											<ListMediaSize
												data={i}
												onDeleteMediaSize={onDeleteMediaSize}
												key={k}
											/>
										);
									}) : data.mediaSizes.map((i, k) => {
										if (i.label.toLowerCase().search(data.search.toLowerCase().trim()) > -1) {
											return (
												<ListMediaSize data={i} onDeleteMediaSize={onDeleteMediaSize} key={k} />
											);
										}
									})}
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Div>
	);
};

export default Comp;
