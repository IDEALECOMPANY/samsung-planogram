import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { accessoryActions } from "../../../actions";
import { deviceCatagoriesFormatter, accessoriesFormatter } from "../../../selectors";
import MySearchField from "../../commons/MySearchField";

import ModalConfiemDelete from "../../commons/modalDelete";
import ModalConfiemDeleted from "../../commons/modalMessage";
import { UPLOAD_FILE_ERROR } from "../../../actions/actionTypes";
import ModalAlert from "../../commons/modalAlert";

const Div = styled.div`
	position: relative;
	width: 100%;
`;

export class Page extends Component {
	static defaultProps = {
		accessories: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			importBox: false,
			openDelete: false,
			openDeleted: false,
			deleteId: 0,
			openBranchList: false,
			openDeviceList: false
		};

		this.genEditTable = this.genEditTable.bind(this);
		this.genDeleteTable = this.genDeleteTable.bind(this);
		this.toEditPage = this.toEditPage.bind(this);
		this.openConfirmDelete = this.openConfirmDelete.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.viewBranchList = this.viewBranchList.bind(this);
		this.modalBranchList = this.modalBranchList.bind(this);
		this.viewDeviceList = this.viewDeviceList.bind(this);
		this.modalBranchList = this.modalBranchList.bind(this);
	}

	componentDidMount() {
		if (this.props.accessories.length === 0) {
			this.props.accessoryActions.getAccessories();
		}
	}

	filterPosts(posts) {
		return posts.filter(i => i.title.indexOf(this.state.searchTxt) !== -1 || i.msg.indexOf(this.state.searchTxt) !== -1);
	}

	customTableSearch(onClick) {
		return <span>Search:</span>;
	}

	genEditTable(cell, row) {
		return (
			<a
				onClick={() => {
					this.toEditPage(row.id);
				}}
			>
				<img src="/assets/images/edit.png" />
			</a>
		);
	}

	genDeleteTable(cell, row) {
		return (
			<a
				onClick={() => {
					this.openConfirmDelete(row.id);
				}}
			>
				<img src="/assets/images/cabbage.png" />
			</a>
		);
	}

	toEditPage(id) {
		this.props.history.push(`/accessories-set/detail/${id}`);
	}

	openConfirmDelete(id) {
		this.setState({ openDelete: true, deleteId: id });
	}

	deleteItem(id) {
		if (this.props.onDelete) {
			this.props
				.onDelete(this.state.deleteId)
				.then(() => {
					this.setState({
						openDelete: false,
						openDeleted: true
					});
				})
				.catch(error => {
					//
				});
		}
	}

	modalBranchList() {
		this.setState({ openBranchList: true });
	}
	modalDeviceList() {
		this.setState({ openDeviceList: true });
	}

	viewBranchList(cell, row) {
		return (
			<div cell={cell} row={row}>
				{cell}{" "}
				<a onClick={() => this.modalBranchList()}>
					<img src="assets/images/icon-search.png" />
				</a>
			</div>
		);
	}

	viewDeviceList(cell, row) {
		return (
			<div cell={cell} row={row}>
				{cell}{" "}
				<a onClick={() => this.modalDeviceList()}>
					<img src="assets/images/icon-search.png" />
				</a>
			</div>
		);
	}

	closeModal() {
		this.setState({
			openDelete: false,
			openDeleted: false,
			openTableList: false,
			openBranchList: false,
			openDeviceList: false
		});
	}

	render() {
		const options = {
			sizePerPageList: [
				{ text: "6", value: 6 },
				{ text: "20", value: 20 },
				{ text: "All", value: this.props.accessories.length }
			],
			sizePerPage: 6,
			prePage: "Previous",
			nextPage: "Next",
			firstPage: "First",
			lastPage: "Last",
			searchField: props => <MySearchField text="search" {...props} />
		}; // Previous page button text // Next page button text // First page button text // Last page button text

		return (
			<Div>
				<BootstrapTable striped data={this.props.accessories} pagination options={options} search>
					<TableHeaderColumn width="80" dataSort dataField="id" isKey>No</TableHeaderColumn>
					<TableHeaderColumn width="180" dataSort dataField="Accessories_Code">Accessories Code</TableHeaderColumn>
					<TableHeaderColumn width="150" dataSort dataField="Shop_Type">Shop Type</TableHeaderColumn>
					<TableHeaderColumn width="150" dataSort dataField="Box_QTY">Box QTY</TableHeaderColumn>
					<TableHeaderColumn width="150" dataSort dataField="Position">Position</TableHeaderColumn>
					<TableHeaderColumn width="150" dataSort dataField="Wall_Image">Wall Image</TableHeaderColumn>
					<TableHeaderColumn width="150" dataSort dataField="Branch_List" dataFormat={this.viewBranchList}>Branch List</TableHeaderColumn>
					<TableHeaderColumn width="150" dataSort dataField="Device_List" dataFormat={this.viewDeviceList}>Device List</TableHeaderColumn>
					<TableHeaderColumn width="150" dataSort dataField="Status">Status</TableHeaderColumn>
					<TableHeaderColumn width="160" dataSort dataField="Updated_On">Updated On</TableHeaderColumn>
					<TableHeaderColumn width="160" dataSort dataField="Published_On">Published On</TableHeaderColumn>
					<TableHeaderColumn width="80" dataField="edit" dataFormat={this.genEditTable}>Edit</TableHeaderColumn>
					<TableHeaderColumn width="80" dataField="delete" dataFormat={this.genDeleteTable}>Delete</TableHeaderColumn>
				</BootstrapTable>
				{this.state.openDelete && (
					<ModalConfiemDelete onAccept={this.deleteItem} onCancel={this.closeModal} />
				)}
				{this.state.openDeleted && (
					<ModalConfiemDeleted onAccept={this.closeModal} />
				)}
				{this.state.openBranchList && (
					<ModalAlert>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">Branch List</h5>
								<button onClick={this.closeModal} className="btn btn-default pull-right">Export PDF</button>
							</div>
							<div className="modal-body">
								<div className="body-inner">
									<div className="table-responsive">
										<table className="table table-striped">
											<thead>
												<tr>
													<th>ID</th>
													<th>Site Code</th>
													<th>Site Name</th>
													<th>State</th>
													<th>Shop Type</th>
													<th>Planogram Set</th>
													<th>Customer</th>
													<th>Updated On</th>
												</tr>
											</thead>
											<tbody>
												{this.props.accessories.map((i, k) => {
													return (
														<tr key={k}>
															{Object.keys(i).map((j, k2) => {
																return (
																	<td key={k2}>{i[j]}</td>
																);
															})}
														</tr>
													);
												})}
											</tbody>
										</table>
									</div>
									<div className=" p-30 bt">
										<button onClick={this.closeModal} className="btn btn-cancel">Close</button>
									</div>
								</div>
							</div>
						</div>
					</ModalAlert>
				)}
				{this.state.openDeviceList && (
					<ModalAlert>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">Device List</h5>
								<button onClick={this.closeModal} className="btn btn-default pull-right">Export PDF</button>
							</div>
							<div className="modal-body">
								<div className="body-inner">
									<div className="table-responsive">
										<table className="table table-striped">
											<thead>
												<tr>
													<th>ID</th>
													<th>Device Category</th>
													<th>Model Name</th>
													<th>Device Status</th>
													<th>Color</th>
													<th>Price</th>
												</tr>
											</thead>
											<tbody>
												{this.props.accessories.map((i, k) => {
														return (
															<tr key={k}>
																{Object.keys(i).map((j, k2) => {
																	return (
																		<td key={k2}>{i[j]}</td>
																	);
																})}
															</tr>
														);
													}
												)}
											</tbody>
										</table>
									</div>
									<div className=" p-30 bt">
										<button onClick={this.closeModal} className="btn btn-cancel">Close</button>
									</div>
								</div>
							</div>
						</div>
					</ModalAlert>
				)}
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		accessories: accessoriesFormatter(state.accessories)
	};
};

const mapDispatchToProps = dispatch => {
	return {
		accessoryActions: bindActionCreators(accessoryActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Page));
