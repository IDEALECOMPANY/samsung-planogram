import React from "react";
import styled from "styled-components";
import Switch, { Case, Default } from "react-switch-case";

const Div = styled.div`
	position: relative;
	width: 100%;
	margin: 15px 0;
	.hr {
		height: 1px;
		background-color: ${props => props.theme.color.gray1};
		box-shadow: 1px 1px 1px ${props => props.theme.color.gray1};
	}
`;

const Comp = ({ icon, title }) => {
	return (
		<Div>
			<div className="hr" />
		</Div>
	);
};

export default Comp;
