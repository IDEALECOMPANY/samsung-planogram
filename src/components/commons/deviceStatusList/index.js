import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { deviceStatusesFormatter } from "../../../selectors";
import MySearchField from "../../commons/MySearchField";

import ModalConfiemDelete from "../../commons/modalDelete";
import ModalConfiemDeleted from "../../commons/modalMessage";

const Div = styled.div`
	position: relative;
	width: 100%;
`;
export class Page extends Component {
	static defaultProps = {
		deviceStatuses: [],
		onDelete: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			importBox: false,
			openDelete: false,
			openDeleted: false
		};

		this.genEditTable = this.genEditTable.bind(this);
		this.genDeleteTable = this.genDeleteTable.bind(this);
		this.toEditPage = this.toEditPage.bind(this);
		this.openConfirmDelete = this.openConfirmDelete.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	filterPosts(posts) {
		return posts.filter(
			i =>
				i.title.indexOf(this.state.searchTxt) !== -1 ||
				i.msg.indexOf(this.state.searchTxt) !== -1
		);
	}

	customTableSearch(onClick) {
		return <span>Search:</span>;
	}

	genEditTable(cell, row) {
		let id = row.raw.id;
		return (
			<a
				onClick={() => {
					this.toEditPage(id);
				}}
			>
				<img src="/assets/images/edit.png" />
			</a>
		);
	}

	genDeleteTable(cell, row) {
		return (
			<a
				onClick={() => {
					this.openConfirmDelete(row.raw.id);
				}}
			>
				<img src="/assets/images/cabbage.png" />
			</a>
		);
	}

	toEditPage(id) {
		this.props.history.push(`/device/status/${id}`);
	}

	openConfirmDelete(id) {
		this.setState({ openDelete: true, deleteId: id });
	}

	deleteItem(id) {
		if (this.props.onDelete) {
			this.props
				.onDelete(this.state.deleteId)
				.then(() => {
					this.setState({
						openDelete: false,
						openDeleted: true
					});
				})
				.catch(error => {
					//
				});
		}
	}

	closeModal() {
		this.setState({
			openDelete: false,
			openDeleted: false
		});
	}

	render() {
		const options = {
			sizePerPageList: [
				{ text: "6", value: 6 },
				{ text: "20", value: 20 },
				{ text: "All", value: this.props.deviceStatuses.length }
			],
			sizePerPage: 6,
			prePage: "Previous",
			nextPage: "Next",
			firstPage: "First",
			lastPage: "Last",
			searchField: props => <MySearchField text="search" {...props} />
		}; // Previous page button text // Next page button text // First page button text // Last page button text

		return (
			<Div>
				<BootstrapTable
					striped
					data={this.props.deviceStatuses}
					pagination
					options={options}
					search
				>
					<TableHeaderColumn width="80" dataSort dataField="No" isKey>
						No
					</TableHeaderColumn>
					<TableHeaderColumn dataSort dataField="Device_status">
						Device Status
					</TableHeaderColumn>

					<TableHeaderColumn dataSort dataField="Status">
						Status
					</TableHeaderColumn>
					<TableHeaderColumn dataSort dataField="Updated_On">
						Updated On
					</TableHeaderColumn>
					<TableHeaderColumn dataSort dataField="Published_On">
						Published On
					</TableHeaderColumn>
					<TableHeaderColumn
						width="80"
						dataField="edit"
						dataFormat={this.genEditTable}
					>
						Edit
					</TableHeaderColumn>
					<TableHeaderColumn
						width="80"
						dataField="delete"
						dataFormat={this.genDeleteTable}
					>
						Delete
					</TableHeaderColumn>
				</BootstrapTable>
				{this.state.openDelete && (
					<ModalConfiemDelete
						onAccept={this.deleteItem}
						onCancel={this.closeModal}
					/>
				)}

				{this.state.openDeleted && (
					<ModalConfiemDeleted onAccept={this.closeModal} />
				)}
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		deviceStatuses: deviceStatusesFormatter(state.deviceStatuses)
	};
};

export default withRouter(connect(mapStateToProps)(Page));
