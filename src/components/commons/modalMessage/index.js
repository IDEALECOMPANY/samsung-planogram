import React, { Component } from "react";
import styled from "styled-components";
import Switch, { Case, Default } from "react-switch-case";
import Alert from "../alert";
import Button from "../button";
import ModalAlert from "../../commons/modalAlert";

const Div = styled.div`
	.model-alert {
		min-width: 400px;
	}

	h2 {
		font-size: 30px;
		color: ${props => props.theme.color.gray};
	}

	p {
		font-size: 16px;
		color: ${props => props.theme.color.gray};
	}

	.btn {
		height: 35px;
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 15px;
		border-radius: 8px;
		color: ${props => props.theme.color.primary};
		display: inline-block;
		margin-right: 5px;
		&.btn-primary {
			background-color: ${props => props.theme.bg.primary};
			border: 1px solid ${props => props.theme.border.primary};
			color: #fff;
		}

		&.btn-default {
			background-color: ${props => props.theme.bg.default};
			border: 1px solid ${props => props.theme.border.primary};
		}

		&.btn-cancel {
			background-color: ${props => props.theme.bg.gray};
			border: 1px solid ${props => props.theme.border.gray};
			color: ${props => props.theme.color.gray};
		}
	}
`;

class Comp extends Component {
	render() {
		return (
			<ModalAlert>
				<Div>
					<div>
						<div>
							<img src="/assets/images/check.png" />
						</div>
						<h2>Deleted</h2>

						<p>Your imaginary file has been deleted</p>

						<button
							onClick={() => {
								if (this.props.onAccept) this.props.onAccept();
							}}
							className="btn btn-primary"
						>
							Ok
						</button>
					</div>
				</Div>
			</ModalAlert>
		);
	}
}

export default Comp;
