import React from "react";
import styled from "styled-components";
import Textfield from "../textfield";
import SelectOption from "../selectOption";

const Div = styled.div`
	position: relative;
	width: 100%;
	margin-bottom: 30px;
`;

const Comp = ({
	data = {},
	icon,
	title,
	type,
	qty,
	size,
	shop,
	onChange,
	deleteTableSet,
	tableTypes,
	tableSizes,
	branchTypes
}) => {
	return (
		<Div>
			<div className="row">
				<div className="col-md-6">
					<h3 className="title-page color-primary">{title}</h3>
				</div>
				<div className="col-md-6 text-right">
					{title !== "Table Set 1" && (
						<img src="/assets/images/cabbage.png" onClick={event => deleteTableSet(data.id)} />
					)}
				</div>
			</div>
			<div className="row">
				<div className="col-md-6">
					<SelectOption
						title="Table Type"
						name="tableTypeId"
						options={tableTypes.map(i => i)}
						placeholder="Choose Table Type"
						selectedOption={data.tableTypeId}
						onChange={onChange}
						zone={data.id}
					/>
				</div>
				<div className="col-md-6">
					<Textfield
						type="number"
						title="Table QTY"
						name="qty"
						placeholder="Table QTY"
						value={data.qty}
						onChange={onChange}
						zone={data.id}
					/>
				</div>
			</div>
			<div className="row">
				<div className="col-md-6">
					<SelectOption
						title="Size"
						name="tableSizeId"
						options={data.tableTypeId === "" ? [] : tableSizes.filter(i => `${i.tableType.id}` === `${data.tableTypeId}`)}
						placeholder="Choose Size"
						selectedOption={data.tableSizeId}
						onChange={onChange}
						zone={data.id}
					/>
				</div>
				<div className="col-md-6">
					<SelectOption
						title="Shop Type"
						name="branchTypeId"
						options={branchTypes.map(i => i)}
						placeholder="Choose Shop Type"
						selectedOption={data.branchTypeId}
						onChange={onChange}
						zone={data.id}
					/>
				</div>
			</div>
		</Div>
	);
};

export default Comp;
