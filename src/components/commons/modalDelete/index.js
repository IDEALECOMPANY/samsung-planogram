import React, { Component } from "react";
import styled from "styled-components";
import Switch, { Case, Default } from "react-switch-case";
import Alert from "../alert";
import Button from "../button";
import ModalAlert from "../../commons/modalAlert";

const Div = styled.div`
	h2 {
		font-size: 30px;
		color: ${props => props.theme.color.gray};
	}

	p {
		font-size: 16px;
		color: ${props => props.theme.color.gray};
	}

	.btn {
		height: 35px;
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 15px;
		border-radius: 8px;
		color: ${props => props.theme.color.primary};
		display: inline-block;
		margin-right: 5px;
		&.btn-primary {
			background-color: ${props => props.theme.bg.primary};
			border: 1px solid ${props => props.theme.border.primary};
			color: #fff;
		}

		&.btn-default {
			background-color: ${props => props.theme.bg.default};
			border: 1px solid ${props => props.theme.border.primary};
		}

		&.btn-cancel {
			background-color: ${props => props.theme.bg.gray};
			border: 1px solid ${props => props.theme.border.gray};
			color: ${props => props.theme.color.gray};
		}
	}
`;

class Comp extends Component {
	static defaultProps = {
		onAccept: null,
		onCancel: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			modalIsOpen: false,
			modalDeleted: false,
			openDeleted: false
		};

		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.confirmDelete = this.confirmDelete.bind(this);
	}

	openModal = () => {
		this.setState({ modalIsOpen: true });
	};

	closeModal() {
		this.setState({ modalIsOpen: false, modalDeleted: false });
	}

	confirmDelete = () => {
		return this.setState({
			openDelete: false,
			openDeleted: true
		});
	};

	render() {
		return (
			<ModalAlert>
				<Div>
					<div>
						<img src="/assets/images/exclamation-mark.png" />
					</div>

					<h2>Are you sure?</h2>

					<p>You will not be able to recover this imaginary file!</p>

					<button
						onClick={() => {
							if (this.props.onCancel) this.props.onCancel();
						}}
						className="btn btn-cancel"
					>
						Cancel
					</button>
					<button
						onClick={() => {
							if (this.props.onAccept) this.props.onAccept();
						}}
						className="btn btn-primary"
					>
						Yes, delete it!
					</button>
				</Div>
			</ModalAlert>
		);
	}
}

export default Comp;
