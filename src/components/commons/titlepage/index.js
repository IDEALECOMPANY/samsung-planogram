import React from "react";
import styled from "styled-components";
import Switch, { Case, Default } from "react-switch-case";

const Div = styled.div`
	ul {
		display: block;
		padding-left: 0;
		margin-top: 15px;
		li {
			display: inline-block;
			vertical-align: middle;

			img {
				margin-right: 15px;
			}
		}
	}

	h2 {
		font-size: 25px;
		margin: 0px;

		font-family: ${props => props.theme.text.bold};
	}
`;

const Comp = ({ icon, title }) => {
	return (
		<Div>
			<ul>
				<li>
					<Switch condition={icon}>
						<Case value="home">
							<img src="/assets/images/home.png" />
						</Case>
						<Case value="branch">
							<img src="/assets/images/branch.png" />
						</Case>
						<Case value="device">
							<img src="/assets/images/device.png" />
						</Case>
						<Case value="table">
							<img src="/assets/images/table.png" />
						</Case>
						<Case value="planogram">
							<img src="/assets/images/planogram-set.png" />
						</Case>
						<Case value="media">
							<img src="/assets/images/media.png" />
						</Case>
						<Case value="accessories">
							<img src="/assets/images/accessories.png" />
						</Case>
					</Switch>
				</li>
				<li>{title && <h2>{title}</h2>}</li>
			</ul>
		</Div>
	);
};

export default Comp;
