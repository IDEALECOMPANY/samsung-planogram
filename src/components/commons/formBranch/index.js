import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../../commons/textfield";
import SelectOption from "../../commons/selectOption";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import Button from "../../commons/button";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

const Comp = ({ data = {}, onChange }) => {
	return (
		<Div>
			<div className="col-md-6">
				<Textfield
					type="text"
					title="Site Code"
					placeholder="Site Code"
					name="code"
					value={data.code}
					onChange={onChange}
				/>
				<Textfield
					type="text"
					title="Site Name"
					placeholder="Site Name"
					name="title"
					value={data.title}
					onChange={onChange}
				/>
				<Textfield
					type="text"
					title="Retail"
					placeholder="Retail"
					name="retailHub"
					value={data.retailHub}
					onChange={onChange}
				/>
				<Textfield
					type="text"
					title="Customer"
					placeholder="Customer"
					name="customer"
					value={data.customer}
					onChange={onChange}
				/>
				<SelectOption
					title="Region"
					name="regionId"
					options={data.address.map(i => {
						return {
							id: i.id,
							title: i.titleTh
						};
					})}
					selectedOption={data.regionId}
					placeholder="Region"
					onChange={onChange}
				/>
				<SelectOption
					title="Province"
					name="provinceId"
					options={data.provinces.map(i => {
						return {
							id: i.id,
							title: i.titleTh
						};
					})}
					selectedOption={data.provinceId}
					placeholder="Province"
					onChange={onChange}
				/>
				<SelectOption
					title="District"
					name="amphurId"
					options={data.amphurs.map(i => {
						return {
							id: i.id,
							title: i.titleTh
						};
					})}
					selectedOption={data.amphurId}
					placeholder="Select District"
					onChange={onChange}
				/>
				<SelectOption
					title="Sub District"
					name="districtId"
					options={data.districts.map(i => {
						return {
							id: i.id,
							title: i.titleTh
						};
					})}
					selectedOption={data.districtId}
					placeholder="Select Sub District"
					onChange={onChange}
				/>
				<div className="row">
					<div className="col-md-6">
						<Textfield
							type="text"
							title="Latitude"
							placeholder="Latitude"
							name="lat"
							value={data.lat}
							onChange={onChange}
						/>
					</div>

					<div className="col-md-6">
						<Textfield
							type="text"
							title="Longitude"
							placeholder="Longitude"
							name="lng"
							value={data.lng}
							onChange={onChange}
						/>
					</div>
				</div>

				<SelectOption
					title="Select Type"
					name="shopTypeId"
					selectedOption={data.shopTypeId}
					options={data.branchTypes.map(i => i)}
					placeholder="Select Type"
					onChange={onChange}
				/>

				<CheckboxRadioInput
					title="Orientation"
					type="radio"
					name="orientation"
					value={data.orientation}
					onChange={onChange}
					options={[
						{
							id: 1,
							title: "Portrait"
						},
						{
							id: 2,
							title: "Landscape"
						}
					]}
				/>

				<CheckboxRadioInput
					title="Status"
					type="radio"
					name="active"
					value={data.active}
					onChange={onChange}
					options={[
						{
							id: 1,
							title: "Active"
						},
						{
							id: 2,
							title: "Inactive"
						}
					]}
				/>
			</div>
		</Div>
	);
};

export default Comp;
