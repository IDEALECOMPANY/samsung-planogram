import React, { Component } from "react";
import styled from "styled-components";

const Div = styled.div`
	width: 100%;
	position: relative;

	.plan {
		min-width: 240px;
		min-width: 240px;
		width: 240px;
		margin: auto;
		font-family: ${props => props.theme.text.bold};
		font-size: 22px;
		.p-list {
			display: table-cell;
			width: 24%;
			text-align: center;
		}

		.p-border {
			border: 1px solid #ccc;
		}

		.plan-full {
			width: 100%;
			padding: 10px 30px;
		}

		.active {
			color: #b3b3b3;
		}

		.table_ {
			width: 100%;

			> ul {
				list-style: none;
				padding-left: 0;
				width: 100%;
				display: table;

				> li {
					display: table-cell;
					text-align: center;
					list-style: none;
					height: 100%;
					vertical-align: middle;
					border-right: 1px solid #ccc;

					&.border {
						li {
							border-bottom: 1px solid #ccc;
							border-right: 0;
							border-right: 0;

							&:last-child {
								border-bottom: 0px solid #ccc;
							}
						}
					}

					> ul {
						list-style: none;
						padding-left: 0;
						height: 100%;
						> li {
							list-style: none;
							display: list-item;
							height: 100%;
							vertical-align: middle;
							padding: 19px;
						}
					}
				}

				&li:last-child {
					li {
						border-left: 0;
					}
				}
			}

			.list {
				padding: 10px 30px;
				vertical-align: middle;
				list-style: none;
				border: 1px solid #ccc;
				border-top: 0;
				border-right: 0;
				border-left: 0;
			}
		}
	}
`;

class Comp extends Component {
	static defaultProps = {
		src: "",
		errorSrc: ""
	};
	
	constructor(props, context) {
		super(props, context);

		this.state = {
			src: ""
		};
	}

	componentDidMount() {
		if (this.props.src !== this.state.src) {
			this.setState(prevState => ({ ...prevState, src: this.props.src }));
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.src !== nextProps.src) {
			if (this.state.src !== nextProps.src) {
				this.setState(prevState => ({
					...prevState,
					src: this.props.src
				}));
			}
		}
	}

	onError() {
		this.setState(prevState => ({
			...prevState,
			src: this.props.errorSrc
		}));
	}

	render() {
		return (
			<Div>
				<div className="plan">
					<div className=" plan-full p-border text-center active">
						E1
					</div>
					<div className="table_">
						<ul className="list-inline">
							<li className="border">
								<ul>
									<li className="active">1</li>
									<li>2</li>
									<li>3</li>
								</ul>
							</li>
							<li>
								<ul>
									<li>L1</li>
									<li className="active">P1</li>
								</ul>
							</li>
							<li>
								<ul>
									<li>L2</li>
									<li>P2</li>
								</ul>
							</li>
							<li className="border">
								<ul>
									<li>5</li>
									<li>6</li>
									<li>7</li>
								</ul>
							</li>
						</ul>
					</div>
					<div className=" plan-full p-border text-center">E2</div>
				</div>
			</Div>
		);
	}
}

export default Comp;
