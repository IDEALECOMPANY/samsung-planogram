import React from "react";
import styled from "styled-components";
import Switch, { Case, Default } from "react-switch-case";

const Div = styled.div`
	position: absolute;
	right: 0;
	margin-top: 15px;
	z-index: 99;
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 0 10px;
		width: 200px;
		margin-top: 5px;

		ul {
			display: block;
			padding-left: 0;

			li {
				display: list-item;
				list-style: none;
				padding: 15px 20px;
				border-bottom: 1px solid #e5e5e5;
				font-size: 14px;
				text-align: left;

				a {
					text-decoration: none;
					color: ${props => props.theme.color.black};

					.d-text {
						margin-left: 15px;
					}

					&:hover {
						color: ${props => props.theme.color.primary};
					}

					.icon {
						width: 20px;
					}
				}
				li:last-child {
					border-bottom: 1px solid transparent;
				}
			}
		}
	}
`;

const Comp = ({ dropdownList, onClick }) => {
	return (
		<Div>
			<div className="box">
				<ul>
					{dropdownList.map((i, k) => {
						return (
							<li key={k}>
								<a href={i.link}>
									<span>
										{(() => {
											switch (i.icon) {
												case "add":
													return (
														<img
															className="icon"
															src="assets/images/icon-add.png"
														/>
													);
												case "export":
													return (
														<img
															className="icon"
															src="assets/images/icon-export.png"
														/>
													);
												case "import":
													return (
														<img
															className="icon"
															src="assets/images/import.png"
														/>
													);
												default:
													return "";
											}
										})()}
									</span>
									<span className="d-text">{i.text}</span>
								</a>
							</li>
						);
					})}
				</ul>
			</div>
		</Div>
	);
};

export default Comp;
