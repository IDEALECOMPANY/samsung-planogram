import React, { Component } from "react";
import styled from "styled-components";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

class Comp extends Component {

	static defaultProps = {
		filterText: "",
		onUserInput: () => {}
	}

	constructor(props, context) {
		super(props, context);
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange() {
		this.props.onUserInput(this.refs.filterTextInput.value);
	}

	render() {
		return (
			<div>
				<input
					type="text"
					placeholder="Search..."
					value={this.props.filterText}
					ref="filterTextInput"
					onChange={this.handleChange}
				/>
			</div>
		);
	}
}

export default Comp;
