import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import styled from "styled-components";
import { userActions, openMenuActions } from "../../../actions";
import Topbar from "../../commons/topBar";

const Div = styled.div`
	width: 100%;
	position: relative;
	font-family: ${props => props.theme.text.normal};
	color: ${props => props.theme.color.black};
	padding-left: -200px;
	float: left;
	min-height: 600px;
	overflow: auto;

	.l-login {
		font-size: 10px;
	}
	.nav-inactive {
		width: 0%;
	}
	.top-sidemenu {
		border-bottom: 1px solid ${props => props.theme.color.gray};

		li {
			margin: 15px 0;
		}
	}
	.nav-bar {
		li {
			margin: 10px 0;
			cursor: pointer;

			a.active {
				font-family: ${props => props.theme.text.bold};
				color: ${props => props.theme.color.primary};
				width: 100%;
				display: block;
			}
		}
	}

	.nav-sidebar {
		padding: 0 15px;

		a {
			font-size: 14px;
			color: ${props => props.theme.color.black};
			width: 160px;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
			display: block;
		}
		ul {
			li {
				margin: 15px 0;
				a {
					&:hover,
					&:focus {
						color: #1227a0;
						text-decoration: none;
						font-family: ${props => props.theme.text.bold};
					}
				}
			}
		}
		.submenu {
			padding-left: 15px;
			li {
				list-style: none;
			}
		}

		.nav-bar {
			li {
				&.close-submenu {
					background-image: url(/assets/images/down.png);
					background-repeat: no-repeat;
					background-position: 100% 10px;
					.submenu {
						display: none;
					}
				}

				&.open-submenu {
					> div {
						font-family: ${props => props.theme.text.bold};
						color: ${props => props.theme.color.primary};
					}
					background-image: url(/assets/images/up.png);
					background-repeat: no-repeat;
					background-position: 100% 10px;
					.submenu {
						display: block;

						.active {
							background-image: url(/assets/images/dot.png);
							background-repeat: no-repeat;
							background-position: 100% 7px;
						}
					}
				}
			}
		}

		a img:parent {
			background: none;
		}
	}
`;

class Comp extends Component {
	static defaultProps = {
		menuOpen: ""
	};
	constructor(props, context) {
		super(props, context);

		this.state = {
			navActive: true
		};

		this.openSubMenu = this.openSubMenu.bind(this);

		this.logout = this.logout.bind(this);
	}

	logout() {
		this.props.userActions.logout();
	}

	openSubMenu(menu) {
		this.props.openMenuActions.changeOpenMenu(menu);
	}

	render() {
		return (
			<Div>
				{this.state.navActive && (
					<div className="nav-sidebar">
						<ul className="list-inline top-sidemenu">
							<li>
								<NavLink to="/">
									{this.props.user && this.props.user.email}
								</NavLink>

								<p className="l-login">
									Lastest login: 2017-10-25 14:42:38
								</p>
							</li>
							<li>
								<NavLink to="/edit-profile">
									Edit Profile
								</NavLink>
							</li>
							<li>
								<a onClick={this.logout}>Logout</a>
							</li>
						</ul>

						<div className="">
							<ul className="list-inline nav-bar">
								<li>
									<NavLink exact to="/">
										Home
									</NavLink>
								</li>
								<li
									className={
										this.props.menuOpen === "branch"
											? " open-submenu"
											: " close-submenu"
									}
								>
									<div
										onClick={event =>
											this.openSubMenu("branch")
										}
									>
										Branch
									</div>
									<ul className="submenu">
										<li>
											<NavLink exact to="/branch">
												Branch
											</NavLink>
										</li>
										<li>
											<NavLink exact to="/branch/type">
												Shop Type
											</NavLink>
										</li>
									</ul>
								</li>

								<li
									className={
										this.props.menuOpen === "device"
											? " open-submenu"
											: " close-submenu"
									}
								>
									<div
										onClick={event =>
											this.openSubMenu("device")
										}
									>
										Device
									</div>
									<ul className="submenu">
										<li>
											<NavLink exact to="/device">
												Device
											</NavLink>
										</li>
										<li>
											<NavLink
												exact
												to="/device/category"
											>
												Device Category
											</NavLink>{" "}
										</li>
										<li>
											<NavLink exact to="/device/status">
												Device Status
											</NavLink>
										</li>
										<li>
											<NavLink exact to="/device/type">
												Device Type In Shop
											</NavLink>
										</li>
									</ul>
								</li>

								<li
									className={
										this.props.menuOpen === "table"
											? " open-submenu"
											: " close-submenu"
									}
								>
									<div
										onClick={event =>
											this.openSubMenu("table")
										}
									>
										Table
									</div>
									<ul className="submenu">
										<li>
											<NavLink exact to="/table/type">
												Table Type
											</NavLink>{" "}
										</li>
										<li>
											<NavLink exact to="/table/size">
												Table Size
											</NavLink>
										</li>
									</ul>
								</li>

								<li>
									<NavLink exact to="/planogram-set">
										Planogram Set
									</NavLink>
								</li>

								<li>
									<NavLink exact to="/accessories-set">
										Wall Accessories Set
									</NavLink>
								</li>

								<li
									className={
										this.props.menuOpen === "media"
											? " open-submenu"
											: " close-submenu"
									}
								>
									<div
										onClick={event =>
											this.openSubMenu("media")
										}
									>
										Media
									</div>
									<ul className="submenu">
										<li>
											<NavLink exact to="/media">
												Media
											</NavLink>
										</li>
										<li>
											<NavLink exact to="/media/type">
												Media Type
											</NavLink>
										</li>
										<li>
											<NavLink exact to="/media/size">
												Media Size
											</NavLink>
										</li>
									</ul>
								</li>

								<li>
									<NavLink exact to="/user">
										User
									</NavLink>
								</li>
							</ul>
						</div>
					</div>
				)}
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		user: state.user,
		menuOpen: state.openMenu
	};
};

const mapDispatchToProps = dispatch => {
	return {
		userActions: bindActionCreators(userActions, dispatch),
		openMenuActions: bindActionCreators(openMenuActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Comp);
