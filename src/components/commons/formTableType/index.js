import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../../commons/textfield";
import SelectOption from "../../commons/selectOption";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import Button from "../../commons/button";

const Div = styled.div`
	width: 100%;
	position: relative;
	.box {
		margin: 0px -5px;
		position: relative;
		img {
			position: absolute;
			right: -20px;
			top: 8px;
			cursor: pointer;
			&:hover {
				opacity: 0.8;
			}
		}
		.in {
			width: 50%;
			padding: 0px 5px;
			display: inline-block;
		}
	}
`;

const Comp = ({
	data = {},
	onChange,
	onAddMediaType,
	onDeleteMediaType,
	mediaTypesOptions = []
}) => {
	return (
		<Div>
			<div className=" col-md-6 ">
				<Textfield
					type="text"
					title="Table Type"
					placeholder="Wall Acrylic"
					name="title"
					value={data.title}
					onChange={onChange}
				/>
				<Textfield
					type="text"
					title="Code"
					placeholder="WA"
					name="code"
					value={data.code}
					onChange={onChange}
				/>

				{data.mediaTypes.map((i, k) => (
					<div className="box" key={k}>
						<div className="in">
							<SelectOption
								key={k}
								{...i}
								onChange={onChange}
								options={mediaTypesOptions}
								zone="mediaTypes"
							/>
						</div>
						<div className="in">
							<Textfield
								type="number"
								placeholder="QTY"
								name={i.nameQty}
								value={i.qty}
								onChange={onChange}
								zone="qty"
							/>
						</div>
						<img
							src="/assets/images/cabbage.png"
							onClick={event => onDeleteMediaType(i.id)}
						/>
					</div>
				))}

				<div className="text-center">
					<Button
						onClick={onAddMediaType}
						iconLeft="icon-add"
						btnType="default"
						children="Add Media Type"
					/>
				</div>

				<CheckboxRadioInput
					title="Status"
					type="radio"
					name="active"
					value={data.active}
					onChange={onChange}
					options={[
						{
							id: 1,
							title: "Active"
						},
						{
							id: 2,
							title: "Inactive"
						}
					]}
				/>
			</div>
		</Div>
	);
};

export default Comp;
