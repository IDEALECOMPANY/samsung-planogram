import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../../commons/textfield";
import SelectOption from "../../commons/selectOption";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import Button from "../../commons/button";
import { Typeahead } from "react-bootstrap-typeahead";
import Label from "../../commons/label";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

const Comp = ({
	data = {},
	onChange,
	onSelectColor,
	onSelectCapacity,
	onSelectModel
}) => {
	return (
		<Div>
			{(data.mode === "edit" && data.id !== "") || data.mode === "add" ? (
				<div className="row">
					<div className=" col-md-6 ">
						<SelectOption
							title="Device Category"
							placeholder="Device Category"
							name="deviceCategoryId"
							selectedOption={data.deviceCategoryId}
							options={data.deviceCategories.map(i => i)}
							onChange={onChange}
						/>
						<Label title="Model Name">
							<Typeahead
								caseSensitive
								labelKey="title"
								placeholder="Model Name"
								defaultInputValue={data.deviceModel}
								options={data.deviceModels.map(i => i)}
								onInputChange={onSelectModel}
							/>
						</Label>
						<SelectOption
							title="Device Status"
							placeholder="Device Status"
							name="deviceStatusId"
							selectedOption={data.deviceStatusId}
							options={data.deviceStatuses.map(i => i)}
							onChange={onChange}
						/>

						<SelectOption
							title="Device Type In Shop"
							placeholder="Device Type In Shop"
							name="deviceTypeInShopId"
							selectedOption={data.deviceTypeInShopId}
							options={data.deviceTypeShops.map(i => i)}
							onChange={onChange}
						/>
						<Label title="Color">
							<Typeahead
								labelKey="title"
								placeholder="Select Color"
								defaultInputValue={data.deviceColor}
								options={data.deviceColors.map(i => i)}
								onInputChange={onSelectColor}
							/>
						</Label>

						<Label title="Capacity">
							<Typeahead
								labelKey="title"
								defaultInputValue={data.deviceCapacity}
								options={data.deviceCapacities.map(i => i)}
								placeholder="Select Capacity"
								onInputChange={onSelectCapacity}
							/>
						</Label>

						<Textfield
							type="text"
							title="Price"
							placeholder="Price"
							name="price"
							value={data.price}
							onChange={onChange}
						/>

						<Textfield
							type="text"
							title="Barcode"
							placeholder="Barcode"
							name="barcode"
							value={data.barcode}
							onChange={onChange}
						/>

						<Textfield
							type="text"
							title="Model Code"
							placeholder="Model Code"
							name="modelCode"
							value={data.modelCode}
							onChange={onChange}
						/>

						<CheckboxRadioInput
							title="Status"
							type="radio"
							name="active"
							value={data.active}
							onChange={onChange}
							options={[
								{
									id: 1,
									title: "Active"
								},
								{
									id: 2,
									title: "Inactive"
								}
							]}
						/>
					</div>
				</div>
			) : (
				""
			)}
		</Div>
	);
};

export default Comp;
