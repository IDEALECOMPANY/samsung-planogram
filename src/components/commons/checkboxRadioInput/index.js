import React from "react";
import styled from "styled-components";

import Label from "../label";

const Div = styled.div`
	width: 100%;
	position: relative;

	.list {
		margin-right: 20px;
		position: relative;

		.text {
			margin-left: 35px;
		}

		input {
			position: absolute;
			opacity: 0;
			cursor: pointer;
		}

		.checkmark {
			position: absolute;
			top: 0;
			left: 0;
			height: 20px;
			width: 20px;
			background-color: transparent;
			border-radius: 50%;
			border: 1px solid #b3b3b3;

			&:after {
				content: "";
				position: absolute;
				display: none;
			}

			&:after {
				top: 2px;
				left: 2px;
				width: 14px;
				height: 14px;
				border-radius: 50%;
				background: #1228a0;
			}
		}

		&:hover input ~ .checkmark {
			background-color: #ccc;
		}

		input:checked ~ .checkmark {
			background-color: transparent;
		}

		input:checked ~ .checkmark:after {
			display: block;
		}
	}
`;

const Comp = ({ title, type, name, options, value, onChange, error }) => {
	return (
		<Label title={title}>
			<Div>
				{options.map((i, k) => {
					return (
						<label className="list" key={k}>
							<input
								className="form-checkbox"
								name={name}
								onChange={event => {
									if (onChange) onChange(event);
								}}
								value={i.id}
								type={type}
								checked={`${value}` === `${i.id}`}
							/>{" "}
							<span className="checkmark" />
							<span className="text">{i.title}</span>
						</label>
					);
				})}

				{error && <span>{error}</span>}
			</Div>
		</Label>
	);
};

export default Comp;
