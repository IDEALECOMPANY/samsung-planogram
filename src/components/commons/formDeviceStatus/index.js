import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../../commons/textfield";
import SelectOption from "../../commons/selectOption";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import Button from "../../commons/button";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

const Comp = ({ data = {}, onChange }) => {
	return (
		<Div>
			<div className="row">
				<div className=" col-md-6 ">
					<Textfield
						type="text"
						title="Device Status"
						placeholder="Device Status"
						name="title"
						value={data.title}
						onChange={onChange}
					/>

					<CheckboxRadioInput
						title="Status"
						type="radio"
						name="active"
						value={data.active}
						onChange={onChange}
						options={[
							{
								id: 1,
								title: "Active"
							},
							{
								id: 2,
								title: "Inactive"
							}
						]}
					/>
				</div>
			</div>
		</Div>
	);
};

export default Comp;
