import React, { Component } from "react";
import styled from "styled-components";

const Div = styled.div`
	max-width: 130px;
	width: 130px;
	position: relative;
	display: inline-block;
	margin-right: 10px;

	.list-size {
		background-color: ${props => props.theme.color.primary};
		color: #fff;
		margin: 5px 0;
		padding: 7px 10px;
		font-size: 11px;
		border-radius: 3px;
		position: relative;
		min-height: 30px;
		display: inline-block;
		float: left;
		padding-right: 30px;
		width: 100%;

		.list-close {
			position: absolute;
			right: 2px;
			top: 2px;
		}
	}

	@media (max-width: 1200px) {
		width: 100px;
		.list-size {
			font-size: 9px;

			.list-close {
				img {
					width: 14px;
					margin-top: 4px;
				}
			}
		}
	}
`;

const Comp = ({ data, onDeleteMediaSize }) => {
	return (
		<Div>
			<li>
				<div className="list-size">
					<span>{data.label}</span>{" "}
					{data.qty !== "" ? <span>| {data.qty} pcs</span> : ""}
					<div
						className="list-close"
						onClick={event => onDeleteMediaSize(data.id)}
					>
						<img src="/assets/images/icon-close.png" />
					</div>
				</div>
			</li>
		</Div>
	);
};

export default Comp;
