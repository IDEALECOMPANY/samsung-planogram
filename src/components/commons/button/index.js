import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Switch, { Case, Default } from "react-switch-case";

const Div = styled.div`
	display: inline-block;
	button {
		height: 33px;
		border: 1px solid ${props => props.theme.color.primary};
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 30px;
		border-radius: 5px;
		color: ${props => props.theme.color.primary};

		&.btn {
			height: 33px;
			cursor: pointer;
			font-size: ${props => props.theme.font.small};
			padding: 0px 20px;
			border-radius: 5px;
			color: ${props => props.theme.color.primary};
			display: inline-block;
			line-height: 2;

			&.btn-primary {
				background-color: ${props => props.theme.bg.primary};
				border: 1px solid ${props => props.theme.border.primary};
				color: #fff;
			}

			&.btn-default {
				background-color: ${props => props.theme.bg.default};
				border: 1px solid ${props => props.theme.border.primary};
			}

			&.btn-cancel {
				background-color: ${props => props.theme.bg.gray};
				border: 1px solid ${props => props.theme.border.gray};
				color: ${props => props.theme.color.gray};
			}
		}
		img {
			margin-right: 5px;
			margin-top: -3px;
		}
	}
`;

// const Comp = ({ children, disabled = false, onClick, iconLeft, btnType, LinkUrl  }) => {

//     return (

//         <Div>
//             <button className={"btn btn-" + btnType} onClick="activeUrl()" disabled={disabled}>
//                 <Switch condition={iconLeft}>
//                     <Case value='icon-export'>
//                         <img src="/icon-export.png" />
//                     </Case>
//                     <Case value='icon-add'>
//                         <img src="/icon-add.png" />
//                     </Case>
//                 </Switch>
//                 {children}
//             </button>
//         </Div>

//     );
// };

class Comp extends Component {
	static defaultProps = {
		onClick: null
	};

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Div>
				<button
					className={"btn btn-" + this.props.btnType}
					disabled={this.props.disabled}
					onClick={event => {
						if (this.props.onClick) this.props.onClick(event);
					}}
				>
					<Switch condition={this.props.iconLeft}>
						<Case value="icon-export">
							<img src="/assets/images/icon-export.png" />
						</Case>
						<Case value="icon-add">
							<img src="/assets/images/icon-add.png" />
						</Case>
					</Switch>
					{this.props.children}
				</button>
			</Div>
		);
	}
}

export default Comp;
