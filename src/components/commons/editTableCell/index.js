import React, { Component } from "react";
import styled from "styled-components";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

class Comp extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		let { dataCell } = this.props;

		return (
			<div>
				{dataCell.map((i, k) => {
					return <td key={k}>{i.Id}</td>;
				})}
			</div>
		);
	}
}

export default Comp;
