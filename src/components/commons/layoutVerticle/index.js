import React, { Component } from "react";
import styled from "styled-components";
import { uuidUtils } from "../../../utils";

const Div = styled.div`
	width: 230px;
	position: relative;
	display: table;
	margin: auto;
	padding: 30px 0;
	.verticle {
		margin: auto;
		display: table-cell;
		.side {
			width: 100%;
			text-align: center;
		}

		table {
			width: 100%;
			border-spacing: 0;
			border-collapse: collapse;
			border: 0;

			tr {
				td {
					font-size: 15px;
					text-align: center;
					padding: 15px 5px;
					border: 1px solid #ccc;
					cursor: pointer;
					position: relative;
					font-family: ${props => props.theme.text.bold};

					&:hover > .popover {
						display: block;
					}
				}
			}
		}

		.inner {
			tr {
				> td {
					border: 0px solid #ccc;
				}
			}
		}
	}
	.popover {
		display: none;
		position: absolute;
		left: 0;
		top: 50%;
		box-shadow: 0px 3px 17px #bdbdbd70;
		background: #fff;
		padding: 15px;
		color: #000;
		z-index: 999;
		width: 200px;
		border-radius: 7px;
		border: 0;

		&.right {
			left: 100%;
			top: -4px;
		}
		&.left {
			left: 100%;
			top: -4px;
		}
		&.top {
			left: 59%;
			top: -4px;
		}
		&.bottom {
			left: 59%;
			top: -4px;
		}
	}

	.arrow-left {
		width: 0;
		height: 0;
		border-top: 6px solid transparent;
		border-bottom: 6px solid transparent;
		border-right: 10px solid white;
		position: absolute;
		left: -5%;
		top: 38%;
	}
`;

const Comp = ({ row = [], el = [], media = [], devices = [], medias = [] }) => {
	return (
		<Div>
			{row.length > 0 && (
				<div className="verticle">
					<table className="side">
						<tbody>
							<tr>
								<td>
									{el[0].label}
									<div className="popover top">
										<div>
											<strong>Position:</strong> {el[0].label}
										</div>
										<div>
											<strong>Model:</strong> {el[0].value}
										</div>
										<div className="arrow-left" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table className="center">
						<tbody>
							<tr>
								<td>
									<table className="inner">
										<tbody>
											{row[0].map((i, k) => (
												<tr key={uuidUtils.create()}>
													<td>
														{i.label}
														<div className="popover left">
															<div>
																<strong>
																	Position:
																</strong>{" "}
																{i.label}
															</div>
															<div>
																<strong>Model:</strong>{" "}
																{i.value}
															</div>

															<div className="arrow-left" />
														</div>
													</td>
												</tr>
											))}
										</tbody>
									</table>
								</td>
								<td>
									<table className="inner">
										<tbody>
											<tr>
												<td>
													{el[2].label}
													<div className="popover left">
														<div>
															<strong>Position:</strong>{" "}
															{el[2].label}
														</div>
														<div>
															<strong>Model:</strong>{" "}
															{el[2].value}
														</div>
														<div className="arrow-left" />
													</div>
												</td>
												<td>
													{el[3].label}
													<div className="popover left">
														<div>
															<strong>Position:</strong>{" "}
															{el[3].label}
														</div>
														<div>
															<strong>Model:</strong>{" "}
															{el[3].value}
														</div>
														<div className="arrow-left" />
													</div>
												</td>
											</tr>
											<tr>
												{media.map((i, k) => {
													if (k < 2) {
														return (
															<td>
																{i.label}
																<div className="popover left">
																	<div>
																		<strong>
																			Position:
																		</strong>{" "}
																		{i.label}
																	</div>
																	<div>
																		<strong>
																			Model:
																		</strong>{" "}
																		{i.value}
																	</div>
																	<div className="arrow-left" />
																</div>
															</td>
														);
													}
												})}
											</tr>
										</tbody>
									</table>
								</td>
								<td>
									<table className="inner">
										<tbody>
											{row[1].length > 0 &&
												row[1].map((i, k) => (
													<tr key={uuidUtils.create()}>
														<td>
															{i.label}
															<div className="popover right">
																<div>
																	<strong>
																		Position:
																	</strong>{" "}
																	{i.label}
																</div>
																<div>
																	<strong>
																		Model:
																	</strong>{" "}
																	{i.value}
																</div>
																<div className="arrow-left" />
															</div>
														</td>
													</tr>
												))
											}
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<table className="side">
						<tbody>
							<tr>
								<td>
									{el[1].label}
									<div className="popover bottom">
										<div>
											<strong>Position:</strong> {el[1].label}
										</div>
										<div>
											<strong>Model:</strong> {el[1].value}
										</div>
										<div className="arrow-left" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			)}
		</Div>
	);
};

export default Comp;
