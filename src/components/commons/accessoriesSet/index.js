import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../textfield";
import SelectOption from "../selectOption";
import DragSortableList from "react-drag-sortable";
import ListsortMedia from "../listSortMedia";
import Button from "../button";
import ModalConfiemDelete from "../../commons/modalDelete";
import ModalConfiemDeleted from "../../commons/modalMessage";

const Div = styled.div`
	float: left;
	width: 100%;
	height: 535px;
	padding: 60px 15px;
	position: relative;
	box-shadow: 1px 1px 10px #ccc;
	box-sizing: border-box;

	.top {
		top: 0px;
		left: 0px;
		width: 100%;
		padding: 15px;
		position: absolute;
		box-sizing: border-box;
		margin: 0px;

		&>div {
			padding: 0px;
		}

		a {
			color: ${props => props.theme.color.primary} !important;
			cursor: pointer;
		}
		h3 {
			font-size: 25px;
			color: ${props => props.theme.color.primary};
			font-family: ${props => props.theme.text.bold};
		}
	}

	.scroll-table {
		height: 400px;
		overflow: auto;
		position: relative;
	}

	h4 {
		font-size: 16px;
		color: ${props => props.theme.color.primary};
		font-family: ${props => props.theme.text.bold};
		margin: 25px 0;
	}

	.device-btn {
		left: 0px;
		bottom: 0px;
		width: 100%;
		padding: 15px;
		position: absolute;
		box-sizing: border-box;
	}
`;

class Comp extends Component {
	static defaultProps = {
		onAddcapacity: null,
		disabled: false
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			selectedOption: "",
			openDelete: false,
			openDeleted: false,
			mediaTableList: [],
			selectedOptionDevice: ""
		};

		this.scrollTable = null;

		this.closeModal = this.closeModal.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
	}

	closeModal() {
		this.setState({
			openDelete: false,
			openDeleted: false
		});
	}

	deleteItem() {
		this.setState({ openDelete: false, openDeleted: true });
	}

	handleChange = selectedOptionDevice => {
		this.setState({ selectedOptionDevice });
	};

	render() {
		// value select
		const { selectedOptionDevice } = this.state;
		const value = selectedOptionDevice && selectedOptionDevice.value;

		let listGridDevice = [{
			content: (
				<table>
					<tbody>
						<tr width="100%">
							<td width="12%">Position</td>
							<td width="20%">Device</td>
							<td width="20%">Model</td>
							<td width="20%">Price</td>
						</tr>
					</tbody>
				</table>
			),
			classes: ["head no-drag"]
		}];
		this.props.data.devices.forEach((element, index) => {
			listGridDevice.push({
				content: (
					<ListsortMedia
						id={index + 1}
						options_model={element.options.models}
						options_device={element.options.devices}
						options_position={element.options.positions}
						onDeleteBox={() => this.props.onDeleteBox(this.props.data.id, element.id)}
						onSelectChange={event => this.props.onSelectChange(this.props.data.id, element.id, event)}
						data={element}
						edit={this.props.disabled}
					/>
				),
				classes: ["body no-drag"]
			});
		});

		return (
			<Div style={this.props.disabled ? {
				paddingBottom: "15px"
			} : {}}>
				<div className="row top">
					<div className="col-md-6">
						{this.props.title && <h3>{this.props.title}</h3>}
					</div>
				</div>
				<div className="row scroll-table" ref={ref => this.scrollTable = ref} style={this.props.disabled ? {
					height: "445px"
				} : {}}>
					<div className="col-12">
						<DragSortableList
							items={listGridDevice}
							onSort={sortedList => {
								//
							}}
							moveTransitionDuration={0.3}
						/>
					</div>
				</div>
				{!this.props.disabled && (
					<div className="device-btn">
						<div className="col-md-12 text-center">
							<div>
								<Button
									iconLeft="icon-add"
									btnType="default"
									children="Add Capacity"
									onClick={() => this.props.onAddcapacity(this.scrollTable)}
								/>
							</div>
						</div>
					</div>
				)}
			</Div>
		);
	}
}

export default Comp;
