import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../../commons/textfield";
import SelectOption from "../../commons/selectOption";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import Button from "../../commons/button";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

const Comp = ({ data = {}, onChange }) => {
	return (
		<Div>
			<div className=" col-md-6 ">
				<SelectOption
					title="Branch"
					name="branchId"
					options={data.branchs.map(i => {
						return {
							id: i.id,
							title: i.title
						};
					})}
					selectedOption={data.branchId}
					placeholder="Branch"
					onChange={onChange}
				/>
				<Textfield
					type="text"
					title="Username"
					placeholder="Username"
					name="username"
					value={data.username}
					onChange={onChange}
				/>
				<Textfield
					type="text"
					title="Email"
					placeholder="Email"
					name="email"
					value={data.email}
					onChange={onChange}
				/>
				<CheckboxRadioInput
					title="Status"
					type="radio"
					name="active"
					value={data.active}
					onChange={onChange}
					options={[
						{
							id: 1,
							title: "Active"
						},
						{
							id: 2,
							title: "Inactive"
						}
					]}
				/>
			</div>
		</Div>
	);
};

export default Comp;
