import React from "react";
import styled from "styled-components";

import Label from "../label";

const Div = styled.div`
	width: 100%;
	position: reletive;

	.list-group {
		margin-bottom: 15px;
	}

	span {
		color: red;
		width: 100%;
		font-size: 12px;
		padding: 5px 0;
		display: block;
		text-align: center;
	}

	input {
		width: 100%;
		height: 35px;
		line-height: 30px;
		padding: 5px 10px;
		border: 1px solid #b3b3b3;
		border-radius: 5px;
		display: block;
		box-sizing: border-box;
		-webkit-appearance: none;
		-moz-appearance: none;
		appearance: none;
		outline: none;
		line-height: 1;

		&.icon-left {
			padding-left: 50px;
		}
	}

	.input-wrap {
		position: relative;

		i {
			top: 0px;
			left: 0px;
			width: 40px;
			height: 40px;
			line-height: 40px;
			position: absolute;
			text-align: center;
		}
	}
`;

const Comp = ({
	type,
	title,
	value,
	error,
	name,
	zone = "",
	placeholder,
	onChange,
	required
}) => {
	return (
		<Label title={title}>
			<Div>
				<div className="">
					<div className="input-wrap">
						<input
							require={required}
							type={type}
							name={name}
							value={value}
							placeholder={placeholder}
							onChange={event => {
								if (onChange)
									onChange({ ...event, zone: zone });
							}}
						/>
					</div>
					{error && <span>{error}</span>}
				</div>
			</Div>
		</Label>
	);
};

export default Comp;
