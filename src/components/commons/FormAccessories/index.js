import React from "react";
import styled from "styled-components";
import Button from "../button";
import TableSet from "../tableSet";
import Textfield from "../textfield";
import SelectOption from "../../commons/selectOption";
import AccessoriesSet from "../accessoriesSet";
import Boxdrag from "../boxDrag";
import Hr from "../../commons/hr";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import { Tab, Tabs } from "react-tabs";
import "react-tabs/style/react-tabs.css";

const Div = styled.div`
	width: 100%;
	position: relative;
	.set {
		margin: 50px -15px 20px -15px;

		h3 {
			color: ${props => props.theme.color.primary};
			font-family: ${props => props.theme.text.bold};
		}
	}

	.ass-box {
		padding-top: 10px;
		padding-bottom: 10px;
	}
`;

const Comp = ({
	data = {},
	onChange,
	openManage,
	openArrangeTable,
	closeArrangeTable,
	onChangeImage,
	onAccept,
	onAddcapacity,
	onSelectChange,
	onDeleteBox
}) => {
	return (
		<Div>
			<div className="table-box">
				<div className="row">
					<div className="col-md-6">
						<Textfield
							type="file"
							title="Wall Image"
							name="image"
							placeholder="Wall Image"
							onChange={onChangeImage}
						/>
					</div>
					<div className="col-md-6">
						<Textfield
							type="number"
							title="Box QTY"
							name="qty"
							placeholder="Box QTY"
							value={data.qty}
							onChange={onChange}
							zone="qty"
						/>
					</div>
				</div>
				<div className="row">
					<div className="col-md-6">
						<SelectOption
							title="Shop Type"
							name="shopType"
							options={data.branchTypes.map(i => {
								return { id: i.id, title: i.title };
							})}
							selectedOption={data.shopType}
							placeholder="Shop Type"
							onChange={onChange}
						/>
					</div>
					<div className="col-md-6">
						<CheckboxRadioInput
							title="Status"
							type="radio"
							name="active"
							value={data.active}
							onChange={onChange}
							options={[
								{
									id: 1,
									title: "Active"
								}, {
									id: 2,
									title: "Inactive"
								}
							]}
						/>
					</div>
				</div>
				<div className="row mt-20  mb-30 ">
					<div className="col-md-12 text-center">
						<button onClick={openManage} className="btn btn-default" type="button">Manage Accessories Set</button>
					</div>
				</div>
			</div>
			{data.showManagePlanogramSet && (
				<div>
					<div className="row set">
						<div className="col-md-6">
							<h3>Manage Accessories Set</h3>
						</div>
						<div className="col-md-6 text-right">
							{/* <button
								onClick={event => openArrangeTable()}
								className="btn btn-default"
							>
								Arrange Table
							</button> */}
						</div>
					</div>
					<div className="row device">
						{data.accessories.map((i, k) => {
							return (
								<div className="ass-box col-12 col-sm-12 col-md-6" key={k}>
									<AccessoriesSet
										title={`Box ${k + 1}`}
										onAddcapacity={ref => onAddcapacity(i.id, ref)}
										onSelectChange={onSelectChange}
										onDeleteBox={onDeleteBox}
										data={i}
									/>
								</div>
							);
						})}
					</div>
				</div>
			)}
			{data.showManagePlanogramSet && (
				<div className="row">
					<div className="col-md-12 text-center">
						<button onClick={onAccept} className="btn btn-default">Submit</button>
					</div>
				</div>
			)}
		</Div>
	);
};

export default Comp;
