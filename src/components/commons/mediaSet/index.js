import React, { Component } from "react";
import styled from "styled-components";
import Button from "../button";
import Textfield from "../textfield";
import SelectGroup from "react-select-plus";
import "react-select-plus/dist/react-select-plus.css";
import ModalConfiemDelete from "../../commons/modalDelete";
import ModalConfiemDeleted from "../../commons/modalMessage";

const Div = styled.div`
	padding: 30px 15px 30px 15px;
	box-shadow: 1px 1px 10px #ccc;
	margin-bottom: 30px;
	float: left;
	width: 100;
	display: inline-block;
	.top {
		a {
			color: ${props => props.theme.color.primary} !important;
			cursor: pointer;
		}
		h3 {
			font-size: 20px;
			color: ${props => props.theme.color.primary};
			font-family: ${props => props.theme.text.bold};
		}
	}

	h4 {
		font-size: 16px;
		color: ${props => props.theme.color.primary};
		font-family: ${props => props.theme.text.bold};
		margin: 25px 0;
	}

	.device-btn {
		margin-top: 30px;
		float: left;
		width: 100%;
	}

	.media .Select-control,
	.media .Select.is-open .Select-control {
		border-color: transparent;
	}
	.media td,
	.media th {
		padding: 5px 10px;
	}
	.media thead {
		background-color: #f2f2f2;
	}
`;

let options_device = [];
let options_model = [];

class Comp extends Component {
	static defaultProps = {
		title: "",
		data: {
			options: {
				devices: [],
				models: []
			}
		},
		openDelete: false,
		openDeleted: false,
		onChange: false
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			selectedModel: "",
			selectedDevice: "",
			openDelete: false,
			openDeleted: false,
			options: {
				models: []
			}
		};

		this.DeleteBox = this.DeleteBox.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
	}

	componentDidMount() {
		this.setState(prevState => ({
			...prevState,
			selectedDevice: this.props.data.type,
			selectedModel: this.props.data.model,
			options: {
				models: this.filterModel(this.props.data.type)
			}
		}));
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.data !== nextProps.data) {
			this.setState(prevState => ({
				...prevState,
				selectedDevice: this.props.data.type,
				selectedModel: this.props.data.model,
				options: {
					models: this.filterModel(this.props.data.type)
				}
			}));
		}
	}
	
	handleChange(selectedOption) {
		let name = selectedOption.selectName;
		let id = selectedOption.value;

		console.log("handleChange", selectedOption);

		if (name === "device") {
			this.setState(prevState => ({
				...prevState,
				selectedDevice: id,
				selectedModel: "",
				options: {
					models: this.filterModel(selectedOption.code)
				}
			}), () => {
				this.sendOnChange();
			});
		} else {
			this.setState(prevState => ({
				...prevState,
				selectedModel: id
			}), () => {
				this.sendOnChange();
			});
		}
	}

	filterModel(code) {
		return this.props.data.options.models.filter(i => i.typeInfo.indexOf(code) !== -1)
	}

	DeleteBox() {
		this.setState({ openDelete: true });
	}

	closeModal() {
		this.setState({
			openDelete: false,
			openDeleted: false
		});
	}

	deleteItem() {
		this.setState({ openDelete: false, openDeleted: true });
	}

	sendOnChange() {
		this.props.onChange({
			id: this.props.data.id,
			model: this.state.selectedModel,
			type: this.state.selectedDevice
		});
	}

	render() {
		console.log("inner render", this.state);
		return (
			<Div>
				<div className="row top">
					<div className="col-md-6">
						{this.props.title && <h3>{this.props.title}</h3>}
					</div>
					<div className="col-md-6 text-right">
						<img
							onClick={this.DeleteBox}
							src="/assets/images/cabbage.png"
						/>
					</div>
				</div>

				<div className="media">
					<table>
						<thead width="100%">
							<tr>
								<th width="10%">Position</th>
								<th width="30%">Device</th>
								<th width="30%">Model</th>
								<th width="30%">Price</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="10%">1</td>
								<td width="30%">
									<SelectGroup
										placeholder="Add Device"
										onChange={event => this.handleChange({...event, selectName: "device"})}
										value={this.state.selectedDevice}
										options={this.props.data.options.devices}
									/>
								</td>
								<td width="30%">
									<SelectGroup
										placeholder="Add Model"
										onChange={event => this.handleChange({...event, selectName: "model"})}
										value={this.state.selectedModel}
										options={this.props.data.options.models}
										clearable={false}
									/>
								</td>
								<td width="30%">
									<div>
										<span className="middle" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				{this.state.openDelete && (
					<ModalConfiemDelete
						onAccept={this.deleteItem}
						onCancel={this.closeModal}
					/>
				)}

				{this.state.openDeleted && (
					<ModalConfiemDeleted onAccept={this.closeModal} />
				)}
			</Div>
		);
	}
}

export default Comp;
