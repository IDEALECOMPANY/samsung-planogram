import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../textfield";
import SelectOption from "../selectOption";
import Select from "react-select";
import "react-select/dist/react-select.css";

const Div = styled.div``;

class Comp extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			selectedOption: ""
		};
	}

	handleChange = selectedOption => {
		this.setState({ selectedOption });
	};

	render() {
		return (
			<Div>
				<h3>{this.props.title}</h3>
			</Div>
		);
	}
}

export default Comp;
