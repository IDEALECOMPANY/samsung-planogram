import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

const Div = styled.div`
	position: relative;
	width: 100%;

	ul.tabs-nav.nav.navbar-nav.navbar-left {
		margin: 0px;
		padding: 0px;
		list-style: none;
	}
`;

class Comp extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			activeTabIndex: this.props.defaultActiveTabIndex
		};
		this.handleTabClick = this.handleTabClick.bind(this);
	}

	// Toggle currently active tab
	handleTabClick(tabIndex) {
		this.setState({
			activeTabIndex:
				tabIndex === this.state.activeTabIndex
					? this.props.defaultActiveTabIndex
					: tabIndex
		});
	}

	// Encapsulate <Tabs/> component API as props for <Tab/> children
	renderChildrenWithTabsApiAsProps() {
		return React.Children.map(this.props.children, (child, index) => {
			return React.cloneElement(child, {
				onClick: this.handleTabClick,
				tabIndex: index,
				isActive: index === this.state.activeTabIndex
			});
		});
	}

	// Render current active tab content
	renderActiveTabContent() {
		const { children } = this.props;
		const { activeTabIndex } = this.state;
		if (children[activeTabIndex]) {
			return children[activeTabIndex].props.children;
		}
	}

	render() {
		return (
			<div className="tabs">
				<ul className="tabs-nav nav navbar-nav navbar-left">
					{this.renderChildrenWithTabsApiAsProps()}
				</ul>
				<div className="tabs-active-content">
					{this.renderActiveTabContent()}
				</div>
			</div>
		);
	}
}

export default Comp;
