import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../textfield";
import SelectGroup from "react-select-plus";
import "react-select-plus/dist/react-select-plus.css";

const Div = styled.div`
	width: 100%;
	position: relative;

	.id {
		text-align: left;
	}
	.middle {
		padding-top: 7px;
		display: inline-block;
	}
	&.row {
		margin: 0px;
	}
	.Select-value {
		padding-left: 0px !important;
	}
`;

const Comp = ({
	data = {},
	options_model_media = [],
	onChangeSelect,
	edit
}) => {
	return (
		<Div className="row no-drag">
			<table>
				<tbody>
					<tr width="100%">
						<td width="12%">
							<span className="middle">{data.label}</span>
						</td>
						<td width="50%">
							<SelectGroup
								name={data.name}
								placeholder="Add Modia"
								value={data.id}
								onChange={event =>
									onChangeSelect(data.name, event)
								}
								options={options_model_media.filter(i => {
									let find = i.mediaTypes.filter(
										m =>
											`${m.id}` === `${data.mediaType.id}`
									);
									if (find.length > 0) {
										return {
											value: i.id,
											label: i.title
										};
									}
								})}
								clearable={false}
								disabled={edit ? true : false}
							/>
						</td>
					</tr>
				</tbody>
			</table>
		</Div>
	);
};

export default Comp;
