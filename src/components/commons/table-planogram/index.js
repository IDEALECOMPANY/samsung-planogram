import React from "react";
import styled from "styled-components";
import Switch, { Case, Default } from "react-switch-case";
import { error } from "util";

const Div = styled.div`
	.box {
		background-color: #fff;
		box-shadow: 1px 1px 9px #ccc;
		padding: 15px 0px;
		margin-top: 5px;
		padding-left: 0;
		clear: both;
		display: block;
	}

	h3 {
		padding-left: 20px;
		margin: 0 0 15px 0;
		font-size: 16px;
		color: ${props => props.theme.color.black};
		font-family: ${props => props.theme.text.bold};
	}

	table {
		border-spacing: 0px;
		color: ${props => props.theme.color.black};
		text-align: left;
		width: 100%;
	}

	.table1 {
		margin-bottom: 15px;
		padding: 0 20px;
	}

	tbody:before {
		content: "-";
		display: block;
		line-height: 10px;
		color: transparent;
	}

	table tr td {
		padding: 3px 15px;
	}
	table tr td .number {
		display: none;
	}
	table tr th {
		border-top: 1px solid #eaeaea;
		border-bottom: 1px solid #eaeaea;
		padding: 15px;
		color: ${props => props.theme.color.black1};
		font-size: 16px;
	}

	table tr td:first-child {
		padding-left: 15px;
	}

	table tr td:first-child .number {
		display: inline-block;
	}

	table tr th:first-child {
		padding-left: 20px;
	}

	table tr:nth-child(odd) td:first-child {
		border-left: 5px solid #c1c9f5;
	}
	table tr:nth-child(even) td:first-child {
		border-left: 5px solid #7281d6;
	}

	.box.sm table thead {
		display: none;
	}

	.box.lg table tr td {
		padding: 18px 15px;
	}
`;

const Comp = ({ size, title, PanelData }) => {
	const dataColumns = PanelData.columns;
	const dataRows = PanelData.rows;

	return (
		<Div>
			<div className={"box " + size}>
				<div>{title && <h3>{title}</h3>}</div>

				<table className="table2">
					{dataColumns && (
						<thead>
							<tr>
								{dataColumns.map((e, f) => {
									return <th key={f}>{e}</th>;
								})}
							</tr>
						</thead>
					)}

					<tbody>
						{dataRows.map((c, d) => {
							return (
								<tr key={d}>
									{dataColumns.map((a, b) => {
										return <td key={b}> {c[a]}</td>;
									})}
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		</Div>
	);
};

export default Comp;
