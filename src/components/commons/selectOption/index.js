import React from "react";
import styled from "styled-components";
import Label from "../label";
import { uuidUtils } from "../../../utils";

const Div = styled.div`
	width: 100%;
	position: reletive;

	select {
		height: 35px;
		background-color: transparent;
		border: 1px solid #b3b3b3;
		width: 100%;
		color: ${props => props.theme.color.gray};
		-webkit-appearance: none;
		-moz-appearance: none;
		appearance: none;
		padding-left: 10px;

		&.active {
			color: ${props => props.theme.color.black};
		}
	}
`;

const Comp = ({
	title,
	name,
	zone = "",
	options,
	selectedOption,
	onChange,
	placeholder,
	error,
	disabled
}) => {
	return (
		<Label title={title}>
			<Div>
				<select
					name={name}
					value={selectedOption}
					onChange={event => {
						if (onChange) onChange({ ...event, zone: zone });
					}}
					className={selectedOption ? "active" : ""}
					disabled={disabled ? "disabled" : ""}
				>
					<option className="placeholder" value="">
						{placeholder}
					</option>
					{options.map((i, k) => {
						return (
							<option key={uuidUtils.create()} value={i.id}>
								{i.title}
							</option>
						);
					})}
				</select>
				{error && <span>{error}</span>}
			</Div>
		</Label>
	);
};

export default Comp;
