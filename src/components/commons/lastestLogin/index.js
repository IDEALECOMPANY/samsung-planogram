import React from "react";
import styled from "styled-components";

const Div = styled.div`
	position: relative;
	width: 100%;
	font-size: 16px;
	margin: 25px 0;
`;

const Comp = ({ date, time }) => {
	return (
		<Div>
			Lastest login : {date} {time}
		</Div>
	);
};

export default Comp;
