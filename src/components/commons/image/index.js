import React, { Component } from "react";
import styled from "styled-components";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

class Comp extends Component {
	static defaultProps = {
		src: "",
		errorSrc: ""
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			src: ""
		};
	}

	componentDidMount() {
		if (this.props.src !== this.state.src) {
			this.setState(prevState => ({ ...prevState, src: this.props.src }));
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.src !== nextProps.src) {
			if (this.state.src !== nextProps.src) {
				this.setState(prevState => ({
					...prevState,
					src: nextProps.src
				}));
			}
		}
	}

	onError() {
		this.setState(prevState => ({
			...prevState,
			src: this.props.errorSrc
		}));
	}

	render() {
		return (
			<Div>
				<img
					src={this.state.src}
					onError={this.onError}
					onClick={event => {
						if (this.props.onClick) this.props.onClick(event);
					}}
				/>
			</Div>
		);
	}
}

export default Comp;
