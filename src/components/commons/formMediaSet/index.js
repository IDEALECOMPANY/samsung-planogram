import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../../commons/textfield";
import SelectOption from "../../commons/selectOption";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import Button from "../../commons/button";

const Div = styled.div`
	width: 100%;
	position: relative;
`;

const Comp = ({ data = {}, onChange, deleteTableSet }) => {
	return (
		<Div>
			<div className=" col-md-6 ">
				<Textfield
					type="text"
					title="Media Type"
					placeholder="Media Type"
					name="title"
					value={data.title}
					onChange={onChange}
				/>
				<Textfield
					type="text"
					title="Code"
					placeholder="Code"
					name="code"
					value={data.code}
					onChange={onChange}
				/>

				<CheckboxRadioInput
					title="Status"
					type="radio"
					name="active"
					value={data.active}
					onChange={onChange}
					options={[
						{
							id: 1,
							title: "Active"
						},
						{
							id: 2,
							title: "Inactive"
						}
					]}
				/>
			</div>
		</Div>
	);
};

export default Comp;
