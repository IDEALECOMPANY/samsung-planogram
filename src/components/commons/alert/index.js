import React from "react";
import styled from "styled-components";
import Switch, { Case, Default } from "react-switch-case";

import Button from "../button";

const Div = styled.div`
	.model-alert {
		box-shadow: 0px 1px 4px #ccc;
		padding: 50px;
		width: -webkit-max-content;
		width: -moz-max-content;
		width: max-content;
		height: auto;
		text-align: center;
		margin: auto;
		border-radius: 15px;
		margin: 30px auto;
		z-index: 999;
		top: 30%;
		background-color: #fff;
		position: relative;
		-webkit-transform: translate(0, 0);
		-ms-transform: translate(0, 0);
		-o-transform: translate(0, 0);
		-webkit-transform: translate(0, 0);
		-ms-transform: translate(0, 0);
		transform: translate(0, 0);
		-webkit-transition: -webkit-transform 0.3s ease-out;
		-o-transition: -o-transform 0.3s ease-out;
		-webkit-transition: -webkit-transform 0.3s ease-out;
		-webkit-transition: transform 0.3s ease-out;
		transition: transform 0.3s ease-out;
		-webkit-transform: translate(0, -25%);
		-ms-transform: translate(0, -25%);
		-o-transform: translate(0, -25%);
		-webkit-transform: translate(0, -25%);
		-ms-transform: translate(0, -25%);
		transform: translate(0, -25%);

		h2 {
			font-size: 30px;
			color: ${props => props.theme.color.gray};
		}

		p {
			font-size: 16px;
			color: ${props => props.theme.color.gray};
		}
	}

	.btn {
		margin: 0 5px;
		height: 35px;
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 15px;
		border-radius: 8px;
		color: ${props => props.theme.color.primary};
		display: inline-block;
	}

	.btn-primary {
		background-color: ${props => props.theme.bg.primary};
		border: 1px solid ${props => props.theme.border.primary};
		color: #fff;
	}

	.btn-default {
		background-color: ${props => props.theme.bg.default};
		border: 1px solid ${props => props.theme.border.primary};
	}

	.btn-cancel {
		background-color: ${props => props.theme.bg.gray};
		border: 1px solid ${props => props.theme.border.gray};
		color: ${props => props.theme.color.gray};
	}

	.modal_ {
		position: fixed;
		width: 100%;
		height: 100%;
		z-index: 99999;
		background-color: rgba(0, 0, 0, 0.5);
		top: 0;
		left: 0;
	}
`;

const Comp = ({
	iconTop,
	title,
	message,
	confirmLabel,
	confirmLabelType,
	cancelLabel,
	cancelLabelType,
	cancleEven,
	modalIsOpen
}) => {
	return (
		<Div>
			<div className=" modal_ ">
				<div className="model-alert">
					<div>
						<Switch condition={iconTop}>
							<Case value="mark">
								<img src="/assets/exclamation-mark.png" />
							</Case>
							<Case value="check">
								<img src="/assets/check.png" />
							</Case>
						</Switch>
					</div>

					<h2>{title}</h2>
					<p>{message}</p>

					{cancelLabel && (
						<Button
							link="branch"
							btnType="cancel"
							children={cancelLabel}
						/>
					)}

					{confirmLabel && (
						<Button btnType="primary" children={confirmLabel} />
					)}
				</div>
			</div>
		</Div>
	);
};

export default Comp;
