import React from "react";
import styled from "styled-components";

const Div = styled.div`
	width: 100%;
	position: relative;

	.list-group {
		margin-bottom: 15px;

		h3 {
			font-size: 16px;
			margin-top: 0;
			margin-bottom: 5px;
			font-family: ${props => props.theme.text.bold};
		}
	}
`;

const Comp = ({ title, children }) => {
	return (
		<Div>
			<div className="list-group">
				{title && <h3>{title}</h3>}
				<div className="input-wrap">{children}</div>
			</div>
		</Div>
	);
};

export default Comp;
