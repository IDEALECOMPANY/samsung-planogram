import React, { Component } from "react";
import ReactDOM from "react-dom";

class MySearchField extends React.Component {
	// It's necessary to implement getValue
	getValue() {
		return ReactDOM.findDOMNode(this.userEntry).value;
	}

	// It's necessary to implement setValue
	setValue(value) {
		ReactDOM.findDOMNode(this.userEntry).value = value;
	}

	render() {
		return (
			<div className="form-group row">
				<label className="col-sm-2 col-form-label">Search: </label>
				<div className="col-sm-10">
					<input
						className={`form-control`}
						type="text"
						defaultValue={this.props.defaultValue}
						placeholder={this.props.placeholder}
						onKeyUp={this.props.search}
						ref={ref => (this.userEntry = ref)}
					/>
				</div>
			</div>
		);
	}
}

export default MySearchField;
