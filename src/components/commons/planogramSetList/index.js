import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter, Link } from "react-router-dom";
import styled from "styled-components";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

import { planogramActions } from "../../../actions";
import { planogramsFormatter } from "../../../selectors";

import MySearchField from "../../commons/MySearchField";
import ModalConfiemDelete from "../../commons/modalDelete";
import ModalConfiemDeleted from "../../commons/modalMessage";
import ModalAlert from "../../commons/modalAlert";

const Div = styled.div`
	position: relative;
	width: 100%;

	.modal-header {
		padding: 10px 15px;

		h5 {
			font-size: 18px;
			font-family: ${props => props.theme.text.bold};
		}
	}
	.modal-footer {
		border-top: 0;
	}

	.modal-body {
		h6 {
			background-color: #f5f5f5;
			font-size: 16px;
			padding: 10px 15px;
			border-bottom: 1px solid #ccc;
		}

		.body-inner {
			float: left;
			width: 100%;
			padding: 0;
			border-radius: 7px;
			overflow: auto;
		}
	}

	button {
		height: 33px;
		border: 1px solid ${props => props.theme.color.primary};
		cursor: pointer;
		font-size: ${props => props.theme.font.small};
		padding: 0px 30px;
		border-radius: 5px;
		color: ${props => props.theme.color.primary};

		&.btn {
			height: 33px;
			cursor: pointer;
			font-size: ${props => props.theme.font.small};
			padding: 0px 20px;
			border-radius: 5px;
			color: ${props => props.theme.color.primary};
			display: inline-block;
			line-height: 2;

			&.btn-primary {
				background-color: ${props => props.theme.bg.primary};
				border: 1px solid ${props => props.theme.border.primary};
				color: #fff;
			}

			&.btn-default {
				background-color: ${props => props.theme.bg.default};
				border: 1px solid ${props => props.theme.border.primary};
			}

			&.btn-cancel {
				background-color: ${props => props.theme.bg.gray};
				border: 1px solid ${props => props.theme.border.gray};
				color: ${props => props.theme.color.gray};
			}
		}
		img {
			margin-right: 5px;
			margin-top: -3px;
		}
	}
`;

export class Page extends Component {
	static defaultProps = {
		planograms: [],
		onDelete: null
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			searchTxt: "่่่่",
			data: [{ id: 1 }, { id: 2 }],
			openDelete: false,
			openDeleted: false,
			openTableList: false,
			openBranchList: false,
			openDeviceList: false
		};

		this.genEditTable = this.genEditTable.bind(this);
		this.genDeleteTable = this.genDeleteTable.bind(this);
		this.toEditPage = this.toEditPage.bind(this);
		this.openConfirmDelete = this.openConfirmDelete.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
		this.closeModal = this.closeModal.bind(this);

		this.viewTableList = this.viewTableList.bind(this);
		this.modalTableList = this.modalTableList.bind(this);
		this.viewBranchList = this.viewBranchList.bind(this);
		this.modalBranchList = this.modalBranchList.bind(this);
		this.viewDeviceList = this.viewDeviceList.bind(this);
		this.modalBranchList = this.modalBranchList.bind(this);
	}

	filterPosts(posts) {
		return posts.filter(i => i.title.indexOf(this.state.searchTxt) !== -1 || i.msg.indexOf(this.state.searchTxt) !== -1);
	}

	customTableSearch(onClick) {
		return <span>Search:</span>;
	}

	genEditTable(cell, row) {
		let id = row.raw.id;
		return (
			<a onClick={() => this.toEditPage(id)}>
				<img src="/assets/images/edit.png" />
			</a>
		);
	}

	genDeleteTable(cell, row) {
		return (
			<a onClick={() => this.openConfirmDelete(row.raw.id)}>
				<img src="/assets/images/cabbage.png" />
			</a>
		);
	}

	toEditPage(id) {
		this.props.history.push(`/planogram-set/${id}`);
	}

	openConfirmDelete(id) {
		this.setState({ openDelete: true, deleteId: id });
	}

	deleteItem() {
		if (this.props.onDelete) {
			this.props.onDelete(this.state.deleteId).then(() => {
				this.setState({
					openDelete: false,
					openDeleted: true
				});
			}).catch(error => {
				//
			});
		}
	}

	closeModal() {
		this.setState({
			openDelete: false,
			openDeleted: false,
			openTableList: false,
			openBranchList: false,
			openDeviceList: false
		});
	}

	viewTableList(cell, row) {
		return (
			<div cell={cell} row={row}>
				{cell}{" "}
				<a onClick={() => this.modalTableList()}>
					<img src="assets/images/icon-search.png" />
				</a>
			</div>
		);
	}

	viewBranchList(cell, row) {
		return (
			<div cell={cell} row={row}>
				{cell}{" "}
				<a onClick={() => this.modalBranchList()}>
					<img src="assets/images/icon-search.png" />
				</a>
			</div>
		);
	}

	viewDeviceList(cell, row) {
		return (
			<div cell={cell} row={row}>
				{cell}{" "}
				<a onClick={() => this.modalDeviceList()}>
					<img src="assets/images/icon-search.png" />
				</a>
			</div>
		);
	}

	modalTableList() {
		this.setState({ openTableList: true });
	}
	modalBranchList() {
		this.setState({ openBranchList: true });
	}
	modalDeviceList() {
		this.setState({ openDeviceList: true });
	}

	render() {
		const options = {
			sizePerPageList: [
				{ text: "6", value: 6 },
				{ text: "20", value: 20 },
				{ text: "All", value: this.props.planograms.length }
			],
			sizePerPage: 6,
			prePage: "Previous",
			nextPage: "Next",
			firstPage: "First",
			lastPage: "Last",
			searchField: props => <MySearchField text="search" {...props} />
		}; // Previous page button text // Next page button text // First page button text // Last page button text
		return (
			<Div>
				<BootstrapTable striped data={this.props.planograms} pagination options={options} search>
					<TableHeaderColumn width="80" dataSort dataField="No" isKey={true}>No</TableHeaderColumn>
					<TableHeaderColumn width="200" dataSort dataField="Planogram_Code">Planogram Code</TableHeaderColumn>
					<TableHeaderColumn width="200" dataSort dataField="Shop_Type">Shop Type</TableHeaderColumn>
					<TableHeaderColumn width="200" dataSort dataField="Table_QTY">Table QTY</TableHeaderColumn>
					<TableHeaderColumn width="200" dataSort dataField="Table_Type">Table Type</TableHeaderColumn>
					<TableHeaderColumn width="200" dataSort dataField="Size">Size</TableHeaderColumn>
					<TableHeaderColumn width="100" dataSort dataField="Status">Status</TableHeaderColumn>
					<TableHeaderColumn width="200" dataSort dataField="Updated_On">Updated On</TableHeaderColumn>
					<TableHeaderColumn width="200" dataSort dataField="Published_On">Published On</TableHeaderColumn>
					<TableHeaderColumn width="80" dataField="edit" dataFormat={this.genEditTable}>Edit</TableHeaderColumn>
					<TableHeaderColumn width="80" dataField="delete" dataFormat={this.genDeleteTable}>Delete</TableHeaderColumn>
				</BootstrapTable>
				{this.state.openDelete && (
					<ModalConfiemDelete onAccept={this.deleteItem} onCancel={this.closeModal} />
				)}
				{this.state.openDeleted && (
					<ModalConfiemDeleted onAccept={this.closeModal} />
				)}
				{this.state.openTableList && (
					<ModalAlert>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">Table List</h5>
								<button onClick={this.closeModal} className="btn btn-default pull-right">Export PDF</button>
							</div>
							<div className="modal-body">
								<div className="body-inner">
									<div className="table-responsive">
										<table className="table table-striped">
											<thead>
												<th>ID</th>
												<th>Code</th>
												<th>Table Type</th>
												<th>Media Type</th>
											</thead>
											<tbody>
												{this.props.tableLists.map((i, k) => {
													return (
														<tr key={k}>
															{Object.keys(i).map((j, k2) => {
																return (
																	<td key={k2}>{i[j]}</td>
																);
															})}
														</tr>
													);
												})}
											</tbody>
										</table>
									</div>
									<div className=" p-30 bt">
										<button onClick={this.closeModal} className="btn btn-cancel">Close</button>
									</div>
								</div>
							</div>
						</div>
					</ModalAlert>
				)}
				{this.state.openBranchList && (
					<ModalAlert>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">Branch List</h5>
								<button onClick={this.closeModal} className="btn btn-default pull-right">Export PDF</button>
							</div>
							<div className="modal-body">
								<div className="body-inner">
									<div className="table-responsive">
										<table className="table table-striped">
											<thead>
												<th>ID</th>
												<th>Site Code</th>
												<th>Site Name</th>
												<th>State</th>
												<th>Shop Type</th>
												<th>Planogram Set</th>
												<th>Customer</th>
												<th>Updated On</th>
											</thead>
											<tbody>
												{this.props.branchLists.map((i, k) => {
													return (
														<tr key={k}>
															{Object.keys(i).map((j, k2) => {
																return (
																	<td key={k2}>{i[j]}</td>
																);
															})}
														</tr>
													);
												})}
											</tbody>
										</table>
									</div>
									<div className=" p-30 bt">
										<button onClick={this.closeModal} className="btn btn-cancel">Close</button>
									</div>
								</div>
							</div>
						</div>
					</ModalAlert>
				)}
				{this.state.openDeviceList && (
					<ModalAlert>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">Device List</h5>
								<button onClick={this.closeModal} className="btn btn-default pull-right">Export PDF</button>
							</div>
							<div className="modal-body">
								<div className="body-inner">
									<div className="table-responsive">
										<table className="table table-striped">
											<thead>
												<th>ID</th>
												<th>Device Category</th>
												<th>Model Name</th>
												<th>Device Status</th>
												<th>Color</th>
												<th>Price</th>
											</thead>
											<tbody>
												{this.props.deviceLists.map((i, k) => {
													return (
														<tr key={k}>
															{Object.keys(i).map((j, k2) => {
																return (
																	<td key={k2}>{i[j]}</td>
																);
															})}
														</tr>
													);
												})}
											</tbody>
										</table>
									</div>
									<div className=" p-30 bt">
										<button onClick={this.closeModal} className="btn btn-cancel">Close</button>
									</div>
								</div>
							</div>
						</div>
					</ModalAlert>
				)}
			</Div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		planograms: planogramsFormatter(state.planograms),
		tableLists: state.tableLists,
		branchLists: state.branchLists,
		deviceLists: state.deviceLists
	};
};

export default withRouter(connect(mapStateToProps)(Page));
