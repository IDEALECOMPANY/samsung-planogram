import React from "react";
import styled from "styled-components";

import Label from "../label";

const Div = styled.div`
	width: 100%;
	position: reletive;

	.list-group {
		margin-bottom: 15px;
	}

	span {
		color: red;
		width: 100%;
		font-size: 12px;
		padding: 5px 0;
		display: block;
		text-align: center;
	}

	textarea {
		width: 100%;
		line-height: 30px;
		padding: 5px 10px;
		border: 1px solid #b3b3b3;
		border-radius: 5px;
		display: block;
		box-sizing: border-box;
		-webkit-appearance: none;
		-moz-appearance: none;
		appearance: none;
		outline: none;
		font-size: 16px;

		&.icon-left {
			padding-left: 50px;
		}
	}

	.input-wrap {
		position: relative;

		i {
			top: 0px;
			left: 0px;
			width: 40px;
			height: 40px;
			line-height: 40px;
			position: absolute;
			text-align: center;
		}
	}
`;

const Comp = ({
	type,
	rows,
	content,
	title,
	error,
	name,
	resize,
	placeholder,
	onChange
}) => {
	return (
		<Label title={title}>
			<Div>
				<textarea
					className="form-input"
					style={resize ? null : { resize: "none" }}
					name={name}
					rows={rows}
					value={content}
					onChange={onChange}
					placeholder={placeholder}
				/>
				{error && <h3>{error}</h3>}
			</Div>
		</Label>
	);
};

export default Comp;
