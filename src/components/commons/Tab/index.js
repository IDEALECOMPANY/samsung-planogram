import React from "react";
import styled from "styled-components";

const Div = styled.div`
	li.tab {
		display: inline-block;
		padding: 10px;
		margin-right: 5px;
	}

	a.tab-link.custom-link {
		background: #6161a7;
		color: white;
		border-radius: 100px;
		width: 30px;
		height: 30px;
		line-height: 30px;
		display: block;
		text-align: center;
		cursor: pointer;
	}

	a.tab-link.custom-link.active {
		background: #969696;
	}
`;

const Comp = ({
	tabIndex,
	linkClassName,
	iconClassName,
	onClick,
	isActive
}) => {
	return (
		<Div>
			<li className="tab">
				<a
					className={`tab-link ${linkClassName} ${
						isActive ? "active" : ""
					}`}
					onClick={event => {
						event.preventDefault();
						onClick(tabIndex);
					}}
				>
					<i className={`tab-icon ${iconClassName}`} />
				</a>
			</li>
		</Div>
	);
};

export default Comp;
