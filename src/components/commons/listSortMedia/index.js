import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../textfield";
import SelectGroup from "react-select-plus";
import "react-select-plus/dist/react-select-plus.css";

const Div = styled.div`
	width: 100%;
	position: relative;

	.id {
		text-align: left;
	}
	.middle {
		padding-top: 7px;
		display: inline-block;
	}
	&.row {
		margin: 0px;
	}
	.Select-value {
		padding-left: 0px !important;
	}
`;

const Comp = ({
	options_device = [],
	options_model = [],
	options_position = [],
	id,
	onSelectChange,
	onDeleteBox,
	data = {},
	edit=false
}) => {
	return (
		<Div className="no-drag">
			<table>
				<tbody>
					<tr width="100%">
						<td width="10%">
							<SelectGroup
								value={data.label}
								onChange={event => onSelectChange({...event, selectName: "position"})}
								options={options_position}
								disabled={edit ? true : false}
							/>
						</td>
						<td width="30%">
							<SelectGroup
								placeholder="Add Device"
								value={data.typeId}
								onChange={event => onSelectChange({...event, selectName: "device"})}
								options={options_device}
								disabled={edit ? true : false}
							/>
						</td>
						<td width="30%">
							<SelectGroup
								placeholder="Add Model"
								value={data.id}
								onChange={event => onSelectChange({...event, selectName: "model"})}
								options={options_model}
								disabled={edit ? true : false}
							/>
						</td>
						<td width="25%">
							<div>
								<span className="middle">{data.price}</span>
							</div>
						</td>
						<td width="5%">
							{!edit && (
								<div>
									<img
										onClick={onDeleteBox}
										className=""
										src="/assets/images/cabbage.png"
									/>
								</div>
							)}
						</td>
					</tr>
				</tbody>
			</table>
		</Div>
	);
};

export default Comp;
