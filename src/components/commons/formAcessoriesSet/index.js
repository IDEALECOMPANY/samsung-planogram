import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";
import { generateUUID } from "../../../selectors";
import Button from "../button";
import TableSet from "../tableSet";
import PlanogramSet from "../planogramSet";
import Boxdrag from "../boxDrag";
import Hr from "../../commons/hr";
import DragSortableList from "react-drag-sortable";
import CheckboxRadioInput from "../../commons/checkboxRadioInput";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

const Div = styled.div`
	width: 100%;
	position: relative;
	.list {
		h3 {
			margin: 30px 0;
		}
		button {
			margin: 30px 0;
		}
	}
	.react-tabs__tab-list {
		padding-left: 0px !important;
	}
`;

const Comp = ({
	data = {},
	onChange,
	openManage,
	openArrangeTable,
	closeArrangeTable,
	addTableSet,
	deleteTableSet,
	onChangeSelect,
	onChangeSelectDevice,
	onAccept
}) => {
	return (
		<Div>
			<div className="table-box">
				{data.planogramSet.tableSets.map((i, k) => (
					<TableSet
						title={`Table Set ${k + 1}`}
						key={k}
						onChange={onChange}
						deleteTableSet={deleteTableSet}
						data={i}
						tableTypes={data.tableTypes}
						tableSizes={data.tableSizes}
						branchTypes={data.branchTypes}
					/>
				))}
				<div className="row mt-20  mb-30 ">
					<div className="col-md-12 text-center">
						<Button
							onClick={addTableSet}
							iconLeft="icon-add"
							btnType="default"
							children="Add Table Set"
						/>
					</div>
				</div>
				<Hr />
				<CheckboxRadioInput
					title="Status"
					type="radio"
					name="active"
					value={data.planogramSet.active}
					onChange={onChange}
					options={[
						{
							id: 1,
							title: "Active"
						},
						{
							id: 2,
							title: "Inactive"
						}
					]}
				/>

				<div className="row">
					<div className="col-md-12 text-center">
						<button
							onClick={openManage}
							className="btn btn-default"
						>
							Manage Planogram Set
						</button>
					</div>
				</div>
			</div>
			{data.showManagePlanogramSet && (
				<Tabs>
					<TabList>
						{data.planogramSet.tableSets.map((i, k) => {
							return <Tab key={k}>Table Set {k + 1}</Tab>;
						})}
					</TabList>

					{data.planogramSet.tableSets.map((i, k) => {
						return (
							<div key={k}>
								<TabPanel>
									<div className="list row">
										<div className="col-md-6">
											<h3>Manage Planogram Set</h3>
										</div>
										<div className="col-md-6 text-right">
											{/* <button
												onClick={event => openArrangeTable(i)}
												className="btn btn-default"
											>
												Arrange Table
											</button> */}
										</div>
										{i.tables.map((t, k2) => {
											let row = t.devices;
											let el = t.devices;
											row = row.slice(0, row.length - 4);
											el = el.slice(
												row.length,
												el.length
											);
											let text = data.tableSizes.find(
												ts =>
													`${ts.id}` ===
													`${i.tableSizeId}`
											).title;
											return (
												<div
													className="col-12 col-md-6 col-lg-6 col-xl-4 "
													key={k2}
												>
													<PlanogramSet
														title={`${text}${k2 +
															1}`}
														tableQTY={`${text}${k2 +
															1}`}
														options_device={
															data.options_device
														}
														options_model={
															data.options_model
														}
														options_model_media={
															data.options_model_media
														}
														row={row}
														el={el}
														tables={t}
														devices={data.devices}
														medias={data.medias}
														onChangeSelect={
															onChangeSelect
														}
														onChangeSelectDevice={
															onChangeSelectDevice
														}
													/>
												</div>
											);
										})}
									</div>
								</TabPanel>
							</div>
						);
					})}
				</Tabs>
			)}

			{data.showManagePlanogramSet && (
				<div className="row">
					<div className="col-md-12 text-center">
						<button onClick={onAccept} className="btn btn-default">
							Submit
						</button>
					</div>
				</div>
			)}
			{data.showArrangeTableModal && (
				<div className="modal modal_ ">
					<div className="modal-dialog" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">Planogram Set</h5>
							</div>
							<div className="modal-body">
								<div className="body-inner">
									<h6>Arrange Table</h6>
									<DragSortableList
										moveTransitionDuration={0.3}
										items={data.planogramSetArrange.map(
											(i, k) => ({
												content: (
													<div>
														<Boxdrag
															title={`M${k + 1}`}
														/>
													</div>
												),
												classes: ["box_"]
											})
										)}
										type="grid"
									/>
								</div>
							</div>
							<div className="modal-footer text-left">
								<button
									onClick={closeArrangeTable}
									link="add-planogram-set"
									className="btn  btn-cancel"
								>
									Cancel
								</button>
								<button
									onClick={closeArrangeTable}
									link="add-planogram-set"
									btnType="btn  btn-default"
								>
									Save
								</button>
							</div>
						</div>
					</div>
				</div>
			)}
		</Div>
	);
};

export default Comp;
