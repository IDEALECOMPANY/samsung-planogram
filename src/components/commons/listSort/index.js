import React, { Component } from "react";
import styled from "styled-components";
import Textfield from "../textfield";
import SelectGroup from "react-select-plus";
import "react-select-plus/dist/react-select-plus.css";

const Div = styled.div`
	width: 100%;
	position: relative;

	.id {
		text-align: left;
	}
	.middle {
		padding-top: 7px;
		display: inline-block;
	}
	&.row {
		margin: 0px;
	}
	.Select-value {
		padding-left: 0px !important;
	}
`;

const Comp = ({
	data = {},
	options_device = [],
	options_model = [],
	onChangeSelect,
	onChangeSelectDevice,
	edit
}) => {
	return (
		<Div className="no-drag">
			<table>
				<tbody>
					<tr width="100%">
						<td width="12%">
							<div className="id">
								<span className="middle">{data.label}</span>
							</div>
						</td>
						<td width="20%">
							<SelectGroup
								placeholder="Add Device"
								value={`${data.type}-${data.typeId}`}
								onChange={event => onChangeSelectDevice(data.name, event)}
								options={options_device}
								clearable={false}
								disabled={edit ? true : false}
							/>
						</td>
						<td width="20%">
							<SelectGroup
								name={data.name}
								placeholder="Add Model"
								value={data.id}
								onChange={event => onChangeSelect(data.name, event)}
								options={data.type === "device" ? options_model.find(i => i.label === "device").options.filter(x => x.typeInfo && `${x.typeInfo.id}` === `${data.typeId}`) : options_model.find(i => i.label === "media").options.filter(x => x.typeInfo.find(xx => `${xx.id}` === `${data.typeId}`) !== undefined)}
								clearable={false}
								disabled={edit ? true : false}
							/>
						</td>
						<td width="20%">
							<div>
								<span className="middle">{data.price}</span>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</Div>
	);
};

export default Comp;
