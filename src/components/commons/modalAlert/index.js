import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Switch, { Case, Default } from "react-switch-case";

const Div = styled.div`
	position: relative;
	width: 100%;

	.modal_ {
		position: fixed;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background-color: rgba(0, 0, 0, 0.5);
		top: 0;
		left: 0;

		.model-alert {
			box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.3);
			padding: 30px;
			width: -webkit-max-content;
			width: -moz-max-content;
			width: max-content;
			height: auto;
			text-align: center;
			margin: auto;
			border-radius: 15px;
			margin: 0 auto;
			z-index: 999;
			top: 50%;
			background-color: #fff;
			position: relative;
			-webkit-transition: -webkit-transform 0.3s ease-out;
			-o-transition: -o-transform 0.3s ease-out;
			-webkit-transition: -webkit-transform 0.3s ease-out;
			-webkit-transition: transform 0.3s ease-out;
			transition: transform 0.3s ease-out;
			-webkit-transform: translate(0, -50%);
			-ms-transform: translate(0, -50%);
			-o-transform: translate(0, -50%);
			-webkit-transform: translate(0, -50%);
			-ms-transform: translate(0, -50%);
			transform: translate(0, -50%);
		}
	}
`;

const Comp = ({ children }) => {
	return (
		<Div>
			<div className="modal_">
				<div className="model-alert ">{children}</div>
			</div>
		</Div>
	);
};

export default Comp;
