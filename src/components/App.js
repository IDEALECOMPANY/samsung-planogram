import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";

import {
	userActions,
	deviceColorActions,
	deviceCapacityActions,
	addressActions
} from "../actions";

import Layout from "./pages/layout";
import Loading from "./commons/loading";

export class App extends Component {
	static defaultProps = {
		loading: false,
		user: null,
		deviceColors: [],
		deviceCapacities: [],
		address: []
	};

	constructor(props, context) {
		super(props, context);

		this.state = {
			userLoaded: false,
			user: null
		};

		this.Layout = null;

		this.checkAuthentication = this.checkAuthentication.bind(this);
	}

	componentDidMount() {
		this.loadUser();
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.user !== nextProps.user) {
			if (this.state.user !== nextProps.user) {
				let user = nextProps.user !== null ? { ...nextProps.user } : null;
				this.setState(
					prevState => ({
						...prevState,
						user: user
					}),
					() => {
						this.checkAuthentication();
					}
				);
			}
		}
	}

	checkAuthentication() {
		if (this.props.location.pathname === "/login") {
			if (this.state.user !== null) {
				this.props.history.push("/");
			}
		} else {
			if (this.state.user === null) {
				this.props.history.push("/login");
			}
		}
	}

	loadUser() {
		this.props.userActions
			.getUser()
			.then(user => {
				this.setState(
					prevState => ({
						...prevState,
						userLoaded: true,
						user: user
					}),
					() => {
						this.initStateDefault();
						this.checkAuthentication();
					}
				);
			})
			.catch(error => {
				this.setState(
					prevState => ({
						...prevState,
						userLoaded: true,
						user: null
					}),
					() => {
						this.checkAuthentication();
					}
				);
			});
	}

	initStateDefault() {
		if (this.props.deviceColors.length === 0) {
			this.props.deviceColorActions.getDeviceColors();
		}
		if (this.props.deviceCapacities.length === 0) {
			this.props.deviceCapacityActions.getDeviceCapacities();
		}
		if (this.props.address.length === 0) {
			this.props.addressActions.getAddress();
		}
	}

	render() {
		this.Layout = this.props.location.pathname !== "/login" ? Layout : null;
		return (
			<div>
				{
					this.Layout !== null ? (
						<this.Layout>
							{this.props.loading && <Loading />}
							{this.props.children}
						</this.Layout>
					) : (
						<div>
							{this.props.loading && <Loading />}
							{this.props.children}
						</div>
					)
				}
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		loading: state.ajax > 0,
		user: state.user,
		deviceColors: state.deviceColors,
		deviceCapacities: state.deviceCapacities,
		address: state.address
	};
};

const mapDispatchToProps = dispatch => {
	return {
		userActions: bindActionCreators(userActions, dispatch),
		deviceColorActions: bindActionCreators(deviceColorActions, dispatch),
		deviceCapacityActions: bindActionCreators(
			deviceCapacityActions,
			dispatch
		),
		addressActions: bindActionCreators(addressActions, dispatch)
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
