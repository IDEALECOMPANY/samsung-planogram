import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import App from "./App";
import * as Pages from "./pages";

const Routes = () => {
	return (
		<App>
			<Switch>
				<Route
					exact
					path="/device/edit-profile"
					component={Pages.EditProfilePage}
				/>
				<Route
					exact
					path="/device/category/add"
					component={Pages.DeviceCategoryAddPage}
				/>
				<Route
					exact
					path="/device/category/:id"
					component={Pages.DeviceCategoryDetailPage}
				/>
				<Route
					exact
					path="/device/category"
					component={Pages.DeviceCategoryPage}
				/>
				<Route
					exact
					path="/device/status/add"
					component={Pages.DeviceStatusAddPage}
				/>
				<Route
					exact
					path="/device/status/:id"
					component={Pages.DeviceStatusDetailPage}
				/>
				<Route
					exact
					path="/device/status"
					component={Pages.DeviceStatusPage}
				/>
				<Route
					exact
					path="/device/type/add"
					component={Pages.DeviceTypeShopAddPage}
				/>
				<Route
					exact
					path="/device/type/:id"
					component={Pages.DeviceTypeShopDetailPage}
				/>
				<Route
					exact
					path="/device/type"
					component={Pages.DeviceTypeShopPage}
				/>
				<Route
					exact
					path="/device/add"
					component={Pages.DeviceAddPage}
				/>
				<Route
					exact
					path="/device/:id"
					component={Pages.DeviceDetailPage}
				/>
				<Route exact path="/device" component={Pages.DevicePage} />
				<Route
					exact
					name="media"
					path="/media/type/add"
					component={Pages.addMediaTypePage}
				/>
				<Route
					exact
					name="media"
					path="/media/type/:id"
					component={Pages.MediaTypeDetailPage}
				/>
				<Route
					exact
					name="media"
					path="/media/type"
					component={Pages.MediaTypePage}
				/>
				<Route exact path="/media/add" component={Pages.AddMediaPage} />

				<Route
					exact
					path="/media/size/add"
					name="media"
					component={Pages.AddMediaSizePage}
				/>
				<Route
					exact
					path="/media/size/:id"
					name="media"
					component={Pages.MediaSizeDetailPage}
				/>
				<Route
					exact
					path="/media/size"
					name="media"
					component={Pages.MediaSizePage}
				/>

				<Route
					exact
					name="media"
					path="/media/:id"
					component={Pages.MediaDetailPage}
				/>

				<Route
					exact
					path="/media"
					name="media"
					component={Pages.MediaPage}
				/>

				<Route
					exact
					path="/planogram-set/add"
					component={Pages.addPlanogramSetPage}
				/>
				<Route
					exact
					path="/planogram-set/:id"
					component={Pages.PlanogramSetDetailPage}
				/>
				<Route
					exact
					path="/planogram-set"
					component={Pages.PlanogramSetPage}
				/>
				<Route exact path="/table/type" component={Pages.TablePage} />
				<Route
					exact
					path="/table/type/add"
					component={Pages.AddTablePage}
				/>
				<Route
					exact
					path="/table/type/:id"
					component={Pages.TableTypeDetail}
				/>
				<Route exact path="/table/size" component={Pages.SizePage} />
				<Route
					exact
					path="/table/size/add"
					component={Pages.AddSizePage}
				/>
				<Route
					exact
					path="/table/size/:id"
					component={Pages.SizeDetailPage}
				/>
				<Route exact path="/user/add" component={Pages.addUserPage} />
				<Route
					exact
					path="/user/:id"
					component={Pages.UserDetailPage}
				/>
				<Route exact path="/user" component={Pages.UserPage} />
				<Route
					exact
					path="/branch/type/add"
					component={Pages.AddShopTypePage}
				/>
				<Route
					exact
					path="/branch/type/:id"
					component={Pages.ShopTypeDetailPage}
				/>
				<Route
					exact
					path="/branch/type"
					component={Pages.ShopTypePage}
				/>
				<Route
					exact
					path="/branch/add"
					component={Pages.AddBranchPage}
				/>
				<Route
					exact
					path="/branch/edit-branch"
					component={Pages.EditBranchPage}
				/>
				<Route
					exact
					path="/branch/:id"
					component={Pages.BranchDetailPage}
				/>
				<Route
					exact
					path="/branch"
					name="branch"
					component={Pages.BranchPage}
				/>

				<Route
					exact
					path="/accessories-set/add"
					name="accessories"
					component={Pages.AddAccessoriesSetPage}
				/>
				<Route
					exact
					path="/accessories-set/detail/:id"
					name="accessories"
					component={Pages.AccessoriesSetDetailPage}
				/>
				<Route
					exact
					path="/accessories-set"
					name="accessories"
					component={Pages.AccessoriesSetPage}
				/>

				<Route exact path="/" component={Pages.HomePage} />
				<Route exact path="/logout" component={Pages.LogoutPage} />
				<Route exact path="/login" component={Pages.LoginPage} />
				<Redirect to="/" />
			</Switch>
		</App>
	);
};

export default Routes;
