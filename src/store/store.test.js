import expect from "expect";
import { createStore } from "redux";
import rootReducer from "../reducers";
import initialState from "../reducers/initialState";
import * as postActions from "../actions/postActions";

describe("Store", () => {
	it("should handle get posts", () => {
		const store = createStore(rootReducer, initialState);

		const posts = [
			{
				id: 1,
				msg: "test",
				auther: {
					id: 1,
					firstName: "name",
					lastName: "lname"
				}
			}
		];

		const action = postActions.getPostsSuccess(posts);
		store.dispatch(action);

		const actual = store.getState().posts[0];
		const expected = {
			id: 1,
			msg: "test",
			auther: {
				id: 1,
				firstName: "name",
				lastName: "lname"
			}
		};

		expect(actual).toEqual(expected);
	});
});
