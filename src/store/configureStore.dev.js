import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { routerMiddleware, routerReducer } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import rootReducer from "../reducers";

export const history = createHistory();
const enhancers = [];
const middleware = [
	thunk,
	reduxImmutableStateInvariant(),
	routerMiddleware(history)
];

const devToolsExtension = window.devToolsExtension;

if (typeof devToolsExtension === "function") {
	enhancers.push(devToolsExtension());
}

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

export default initialState => {
	return createStore(rootReducer, initialState, composedEnhancers);
};
