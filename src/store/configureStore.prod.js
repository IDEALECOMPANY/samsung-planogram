import { createStore, applyMiddleware, compose } from "redux";
import { routerMiddleware, routerReducer } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import thunk from "redux-thunk";
import rootReducer from "../reducers";

export const history = createHistory();
const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

export default initialState => {
	return createStore(rootReducer, initialState, composedEnhancers);
};
