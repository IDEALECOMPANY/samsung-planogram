export const randomString = (length = 10) => {
	let text = "";
	let possible =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
};

export const timestamp = () => {
	return Math.floor(new Date() / 1000);
};

export const create = (separate = "_", length = 10) => {
	return `${timestamp()}${separate}${randomString(length)}`;
};