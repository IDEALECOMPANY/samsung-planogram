export default {
	color: {
		primary: "#1227A0",
		darken: "#ff5500",
		black: "#4D4D4D",
		black1: "#7B7B7B",
		blue: "#7281D6",
		blue2: "#C1C9F5",
		gray: "#95989A",
		gray1: "#CCCCCC"
	},
	font: {
		small: `14px`,
		normal: `16px`,
		large: `25px`
	},

	bg: {
		primary: "#1227A0",
		default: "#FFFFFF",
		gray: "FFFFFF"
	},

	border: {
		primary: "#1227A0",
		gray: "#999999"
	},
	text: {
		normal: "Roboto Regular",
		bold: "Roboto Bold"
	}
};
