import "babel-polyfill";
import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import configueStore, { history } from "./store/configureStore";
import registerServiceWorker from "./registerServiceWorker";
import initialState from "./reducers/initialState";
import Main from "./Main";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import "./style.css";
import { userActions } from "./actions";

const rootEl = document.getElementById("root");
const store = configueStore(initialState);
store.dispatch(userActions.getUser());

render(
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<Main />
		</ConnectedRouter>
	</Provider>,
	rootEl
);

if (module.hot) {
	module.hot.accept("./Main", () => {
		const NextApp = require("./Main").default;
		render(
			<Provider store={store}>
				<ConnectedRouter history={history}>
					<NextApp />
				</ConnectedRouter>
			</Provider>,
			rootEl
		);
	});
}

registerServiceWorker();
