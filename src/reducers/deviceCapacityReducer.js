import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.deviceCapacities, action) => {
	switch (action.type) {
		case types.GET_DEVICE_CAPACITIES_SUCCESS:
			return [...action.deviceCapacities];
		default:
			return state;
	}
};
