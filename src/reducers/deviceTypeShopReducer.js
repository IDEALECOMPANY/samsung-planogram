import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.deviceTypeShops, action) => {
	switch (action.type) {
		case types.GET_DEVICE_TYPE_SHOPS_SUCCESS:
			return [...action.deviceTypeShops];

		case types.ADD_DEVICE_TYPE_SHOP_SUCCESS:
			return [...state, action.deviceTypeShop];

		case types.EDIT_DEVICE_TYPE_SHOP_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.deviceTypeShop.id}`) {
					return { ...action.deviceTypeShop };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_DEVICE_TYPE_SHOP_SUCCESS:
			return state.filter(
				i => `${i.id}` !== `${action.deviceTypeShop.id}`
			);

		default:
			return state;
	}
};
