import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.branchTypes, action) => {
	switch (action.type) {
		case types.GET_BRANCH_TYPES_SUCCESS:
			return [...action.branchTypes];

		case types.ADD_BRANCH_TYPE_SUCCESS:
			return [...state, action.branchType];

		case types.EDIT_BRANCH_TYPE_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.branchType.id}`) {
					return { ...action.branchType };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_BRANCH_TYPE_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.branchType.id}`);

		default:
			return state;
	}
};
