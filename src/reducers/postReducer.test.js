import expect from "expect";
import postReducer from "./postReducer";
import * as postActions from "../actions/postActions";

describe("Post Reducer", () => {
	it("should initial posts when pass GET_POSTS_SUCCESS", () => {
		const initialState = [];

		const posts = [
			{
				id: 1,
				msg: "test",
				auther: {
					id: 1,
					firstName: "name",
					lastName: "lname"
				}
			}
		];

		const action = postActions.getPostsSuccess(posts);

		const newState = postReducer(initialState, action);

		expect(newState.length).toBe(1);
		expect(newState[newState.length - 1].id).toBe(1);
		expect(newState[newState.length - 1].msg).toEqual("test");
		expect(newState[newState.length - 1].auther.firstName).toEqual("name");
	});
});
