import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.mediaSizes, action) => {
	switch (action.type) {
		case types.GET_MEDIA_SIZES_SUCCESS:
			return [...action.mediaSizes];

		case types.ADD_MEDIA_SIZE_SUCCESS:
			return [...state, action.mediaSize];

		case types.EDIT_MEDIA_SIZE_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.mediaSize.id}`) {
					return { ...action.mediaSize };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_MEDIA_SIZE_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.mediaSize.id}`);

		default:
			return state;
	}
};
