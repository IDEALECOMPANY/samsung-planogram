import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.deviceColors, action) => {
	switch (action.type) {
		case types.GET_DEVICE_COLORS_SUCCESS:
			return [...action.deviceColors];
		default:
			return state;
	}
};
