import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.tableTypes, action) => {
	switch (action.type) {
		case types.GET_TABLE_TYPES_SUCCESS:
			return [...action.tableTypes];

		case types.ADD_TABLE_TYPE_SUCCESS:
			return [...state, action.tableType];

		case types.EDIT_TABLE_TYPE_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.tableType.id}`) {
					return { ...action.tableType };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_TABLE_TYPE_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.tableType.id}`);

		default:
			return state;
	}
};
