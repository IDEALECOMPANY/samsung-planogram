import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.deviceModels, action) => {
	switch (action.type) {
		case types.GET_DEVICE_MODELS_SUCCESS:
			return [...action.deviceModels];
		default:
			return state;
	}
};
