import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.accessories, action) => {
	switch (action.type) {

		case types.GET_ACCESSORIES_SUCCESS:
			return [...action.accessories];

		case types.ADD_ACCESSORY_SUCCESS: 
			return [...state, action.accessory];

		case types.EDIT_ACCESSORY_SUCCESS:
			return state.map(i => `${i.id}` !== `${action.accessory.id}` ? i : { ...action.accessory });

		case types.DELETE_ACCESSORY_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.accessory.id}`);

		default:
			return state;
	}
};
