import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.medias, action) => {
	switch (action.type) {
		case types.GET_MEDIAS_SUCCESS:
			return [...action.medias];

		case types.ADD_MEDIA_SUCCESS:
			return [...state, action.media];

		case types.EDIT_MEDIA_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.media.id}`) {
					return { ...action.media };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_MEDIA_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.media.id}`);

		default:
			return state;
	}
};
