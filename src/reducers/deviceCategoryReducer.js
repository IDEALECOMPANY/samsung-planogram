import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.deviceCategories, action) => {
	switch (action.type) {
		case types.GET_DEVICE_CATEGORIES_SUCCESS:
			return [...action.deviceCategories];

		case types.ADD_DEVICE_CATEGORY_SUCCESS:
			return [...state, action.deviceCategory];

		case types.EDIT_DEVICE_CATEGORY_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.deviceCategory.id}`) {
					return { ...action.deviceCategory };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_DEVICE_CATEGORY_SUCCESS:
			return state.filter(
				i => `${i.id}` !== `${action.deviceCategory.id}`
			);

		default:
			return state;
	}
};
