import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.users, action) => {
	switch (action.type) {
		case types.GET_USERS_SUCCESS:
			return [...action.users];

		case types.ADD_USER_SUCCESS:
			return [...state, action.user];

		case types.EDIT_USER_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.user.id}`) {
					return { ...action.user };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_USER_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.user.id}`);

		default:
			return state;
	}
};
