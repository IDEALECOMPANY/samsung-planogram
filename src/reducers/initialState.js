export default {
	ajax: 0,
	user: null,
	openMenu: "",
	posts: [],
	tableSizes: [],
	branchs: [],
	devices: [],
	tableTypes: [],
	address: [],
	planograms: [],
	medias: [],
	mediaTypes: [],
	deviceCategories: [],
	deviceStatuses: [],
	deviceTypeShops: [],
	deviceColors: [],
	deviceModels: [],
	deviceCapacities: [],
	users: [],
	deviceLists: [
		{
			id: "1",
			Device_Category: "Device_Category",
			Model_Name: "Model_Name",
			Device_Status: "Device_Status",
			Color: "Color",
			Price: "Price"
		},
		{
			id: "1",
			Device_Category: "Device_Category",
			Model_Name: "Model_Name",
			Device_Status: "Device_Status",
			Color: "Color",
			Price: "Price"
		},
		{
			id: "1",
			Device_Category: "Device_Category",
			Model_Name: "Model_Name",
			Device_Status: "Device_Status",
			Color: "Color",
			Price: "Price"
		},
		{
			id: "1",
			Device_Category: "Device_Category",
			Model_Name: "Model_Name",
			Device_Status: "Device_Status",
			Color: "Color",
			Price: "Price"
		}
	],
	shopTypes: [
		{
			id: "1",
			Shop_type: "Shop_type",
			Status: "Status",
			Updated_On: "2017-10-25 14: 16: 16",
			Published_On: "2017-10-25 14: 16: 16"
		},
		{
			id: "1",
			Shop_type: "Device_Category",
			Status: "Model_Name",
			Updated_On: "2017-10-25 14: 16: 16",
			Published_On: "2017-10-25 14: 16: 16"
		},
		{
			id: "1",
			Shop_type: "Device_Category",
			Status: "Model_Name",
			Updated_On: "2017-10-25 14: 16: 16",
			Published_On: "2017-10-25 14: 16: 16"
		},
		{
			id: "1",
			Shop_type: "Device_Category",
			Status: "Model_Name",
			Updated_On: "2017-10-25 14: 16: 16",
			Published_On: "2017-10-25 14: 16: 16"
		}
	],
	branchTypes: [],
	accessories: [],
	mediaSizes: []
};

// "deviceListData", "shopTypeList", "accessoriesList"