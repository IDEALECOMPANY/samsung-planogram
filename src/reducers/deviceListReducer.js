import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.deviceLists, action) => {
	switch (action.type) {
		default:
			return state;
	}
};
