import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.devices, action) => {
	switch (action.type) {
		case types.GET_DEVICES_SUCCESS:
			return [...action.devices];

		case types.ADD_DEVICE_SUCCESS:
			return [...state, action.device];

		case types.EDIT_DEVICE_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.device.id}`) {
					return { ...action.device };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_DEVICE_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.device.id}`);

		default:
			return state;
	}
};
