import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.mediaTypes, action) => {
	switch (action.type) {
		case types.GET_MEDIA_TYPES_SUCCESS:
			return [...action.mediaTypes];

		case types.ADD_MEDIA_TYPE_SUCCESS:
			return [...state, action.mediaType];

		case types.EDIT_MEDIA_TYPE_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.mediaType.id}`) {
					return { ...action.mediaType };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_MEDIA_TYPE_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.mediaType.id}`);

		default:
			return state;
	}
};
