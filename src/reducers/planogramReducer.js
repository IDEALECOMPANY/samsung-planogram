import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.planograms, action) => {
	switch (action.type) {
		case types.GET_PLANOGRAMS_SUCCESS:
			return [...action.planograms];

		case types.ADD_PLANOGRAM_SUCCESS:
			return [...state, action.planogram];

		case types.EDIT_PLANOGRAM_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.planogram.id}`) {
					return { ...action.planogram };
				} else {
					return { ...i };
				}
			});
		case types.DELETE_PLANOGRAM_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.planogram.id}`);

		default:
			return state;
	}
};
