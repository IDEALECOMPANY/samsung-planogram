import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.deviceStatuses, action) => {
	switch (action.type) {
		case types.GET_DEVICE_STATUSES_SUCCESS:
			return [...action.deviceStatuses];

		case types.ADD_DEVICE_STATUS_SUCCESS:
			return [...state, action.deviceStatus];

		case types.EDIT_DEVICE_STATUS_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.deviceStatus.id}`) {
					return { ...action.deviceStatus };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_DEVICE_STATUS_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.deviceStatus.id}`);

		default:
			return state;
	}
};
