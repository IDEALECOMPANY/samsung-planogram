import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.openMenu, action) => {
	switch (action.type) {
		case types.CHANGE_OPENMENU:
			return action.openMenu;
		default:
			return state;
	}
};
