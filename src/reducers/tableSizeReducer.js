import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.tableSizes, action) => {
	switch (action.type) {
		case types.GET_TABLE_SIZES_SUCCESS:
			return [...action.tableSizes];

		case types.ADD_TABLE_SIZE_SUCCESS:
			return [...state, action.tableSize];

		case types.EDIT_TABLE_SIZE_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.tableSize.id}`) {
					return { ...action.tableSize };
				} else {
					return { ...i };
				}
			});

		case types.DELETE_TABLE_SIZE_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.tableSize.id}`);

		default:
			return state;
	}
};
