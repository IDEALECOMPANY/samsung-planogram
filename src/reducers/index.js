import { combineReducers } from "redux";
import { routerReducer as router } from "react-router-redux";

import ajax from "./ajaxReducer";
import user from "./userReducer";
import posts from "./postReducer";
import tableSizes from "./tableSizeReducer";
import branchs from "./branchReducer";
import devices from "./deviceReducer";
import deviceCategories from "./deviceCategoryReducer";
import tableTypes from "./tableTypeReducer";
import planograms from "./planogramReducer";
import medias from "./mediaReducer";
import mediaTypes from "./mediaTypeReducer";
import deviceStatuses from "./deviceStatusReducer";
import deviceTypeShops from "./deviceTypeShopReducer";
import users from "./usersReducer";
import deviceLists from "./deviceListReducer";
import shopTypes from "./shopTypeReducer";
import deviceColors from "./deviceColorReducer";
import deviceCapacities from "./deviceCapacityReducer";
import branchTypes from "./branchTypeReducer";
import openMenu from "./openMenuReducer";
import deviceModels from "./deviceModelReducer";
import address from "./addressReducer";
import accessories from "./accessoriesReducer";
import mediaSizes from "./mediaSizeReducer";

const rootReducer = combineReducers({
	router,
	ajax,
	user,
	posts,
	tableSizes,
	branchs,
	devices,
	tableTypes,
	planograms,
	medias,
	mediaTypes,
	deviceCategories,
	deviceStatuses,
	deviceTypeShops,
	users,
	deviceLists,
	shopTypes,
	deviceColors,
	deviceCapacities,
	branchTypes,
	openMenu,
	deviceModels,
	address,
	accessories,
	mediaSizes
});

export default rootReducer;