import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.address, action) => {
	switch (action.type) {
		case types.GET_ADDRESS_SUCCESS:
			return [...action.address];
		default:
			return state;
	}
};
