import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.branchs, action) => {
	switch (action.type) {
		case types.GET_BRANCHS_SUCCESS:
			return [...action.branchs];

		case types.ADD_BRANCH_SUCCESS:
			return [...state, action.branch];

		case types.EDIT_BRANCH_SUCCESS:
			return state.map(i => {
				if (`${i.id}` === `${action.branch.id}`) {
					return { ...action.branch };
				} else {
					return { ...i };
				}
			});

		case types.EDIT_FLOORPLAN_SUCCESS:
			return state;

		case types.DELETE_BRANCH_SUCCESS:
			return state.filter(i => `${i.id}` !== `${action.branch.id}`);

		case types.EDIT_BRANCH_PLANOGRAM_SUCCESS:
			return state;

		default:
			return state;
	}
};
