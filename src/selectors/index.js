import config from "../../config";
import {
	getStatus,
	getDate,
	getTitle,
	getMediaTypeArray,
	getAddress,
	getMediaSizeArray
} from "./common";
import { mediaTypeActions } from "../actions/index";

export const getToken = () => {
	let token = localStorage.getItem(config.auth.token);
	return `${token}` !== "null" ? token : null;
};

export const setToken = token => {
	localStorage.setItem(config.auth.token, token);
};

export const clearToken = () => {
	localStorage.removeItem(config.auth.token);
};

export const randomString = (length = 10) => {
	let text = "";
	let possible =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
};

export const timestamp = () => {
	return Math.floor(new Date() / 1000);
};

export const getUniqueString = (separate = "_", length = 10) => {
	return `uuid_${timestamp()}${separate}${randomString(length)}`;
};

export const generateUUID = (length = 10) => {
	let text = "";
	const possible =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
};

export const postsFormatter = posts => {
	return posts.map(i => {
		return {
			id: i.id,
			title: `${i.auther.firstName} ${i.auther.lastName}`,
			msg: i.msg,
			raw: { ...i }
		};
	});
};

export const deviceCatagoriesFormatter = deviceCategories => {
	return deviceCategories.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Device_category: i.title,
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const deviceStatusesFormatter = deviceStatuses => {
	return deviceStatuses.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Device_status: i.title,
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const deviceTypeShopsFormatter = deviceTypeShops => {
	return deviceTypeShops.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Device_type_shop: i.title,
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const mediaTypesFormatter = mediaTypes => {
	return mediaTypes.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Code: i.code,
			Media_Type: i.title,
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const tableTypesFormatter = tableTypes => {
	return tableTypes.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Code: i.code,
			Table_Type: i.title,
			Media_Type: getMediaTypeArray(i.mediaTypes),
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const mediasFormatter = medias => {
	return medias.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Image: i.image,
			Name: i.title,
			Detail: i.desc,
			Material: i.material,
			Media_Location: i.tableTypes.length > 0 ? i.tableTypes.map(i => i.title) : "",
			MediaSize: i.mediaSizes.length > 0 ? getMediaSizeArray(i.mediaSizes) : "",
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const tableSizesFormatter = tableSizes => {
	return tableSizes.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Size_Code: i.code,
			Size: i.title,
			Capacity: i.capacity,
			Table_Type: getTitle(i.tableType),
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const devicesFormatter = devices => {
	return devices.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Device: getTitle(i.deviceCategory),
			Model_Name: getTitle(i.deviceModel),
			Device_Status: getTitle(i.deviceStatus),
			Device_Type_In_Shop: getTitle(i.deviceTypeInShop),
			Color: getTitle(i.deviceColor),
			Price: i.price,
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const branchTypesFormatter = branchTypes => {
	return branchTypes.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Shop_type: i.title,
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const branchsFormatter = branchs => {
	return branchs.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Site_Code: i.code,
			Site_Name: i.title,
			State: getAddress(i.district),
			Shop_Type: getTitle(i.shopType),
			Planogram_Set: i.planogram ? i.planogram.code : "",
			Customer: i.customer,
			Floorplan: i.floorPlans.length > 0 ? i.floorPlans[0].image : "",
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const planogramsFormatter = planograms => {
	return planograms.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Planogram_Code: i.code,
			Shop_Type: getTitle(i.planogramTableSets[0].shopType),
			Table_Type: getTitle(i.planogramTableSets[0].tableType),
			Branch_List: "-",
			Table_QTY: i.planogramTableSets[0].planogramTableSetTables.length,
			Size: i.planogramTableSets[0].tableSize.code,
			Table_List: "6",
			Device_List: "8",
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const mediaSizesFormatter = mediaSizes => {
	return mediaSizes.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Media_Size: i.title,
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const usersFormatter = users => {
	return users.map((i, k) => {
		return {
			id: i.id,
			No: k + 1,
			Username: i.username,
			Email: i.email,
			Branch: i.userBranches.length > 0 ? i.userBranches[0].branch.title : "-",
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt),
			raw: { ...i }
		};
	});
};

export const accessoriesFormatter = accessories => {
	return accessories.map((i, k) => {
		return {
			id: i.id,
			Accessories_Code: i.code,
			Shop_Type: i.shopType.title,
			Box_QTY: i.accessoryTableSets.length,
			Position: "",
			Wall_Image: "img",
			Branch_List: "0",
			Device_List: "0",
			Status: getStatus(i.active),
			Updated_On: getDate(i.updatedAt),
			Published_On: getDate(i.createdAt)
		};
	});
};