const padDigits = (number, digits) => {
	return (
		Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number
	);
};

export const getStatus = status => (status ? "Active" : "Inactive");

export const getDate = dateStr => {
	if (dateStr === null || dateStr.trim() === "") {
		return "-";
	} else {
		const date = new Date(dateStr);
		return `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(
			-2
		)}-${("0" + date.getDate()).slice(-2)} ${("0" + date.getHours()).slice(
			-2
		)}:${("0" + date.getMinutes()).slice(-2)}:${(
			"0" + date.getSeconds()
		).slice(-2)}`;
	}
};

export const getTitle = data => (data ? data.title : "");

export const getMediaTypeArray = data =>
	data.map(i => `${i.code}${i.qty}`).join(", ");

export const getAddress = address => {
	let arr = [];
	if (address !== null) {
		arr.push(address.titleTh);
		if (address.amphur !== null) {
			arr.push(address.amphur.titleTh);
			if (address.amphur.province !== null) {
				arr.push(address.amphur.province.titleTh);
			}
		}
	}
	return arr.join(" ");
};

export const getMediaSizeArray = data =>
	data.map(i => `${i.title}${i.qty}`).join(", ");
