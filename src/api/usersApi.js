import { get, post } from "./call";

export const getUsers = (params = {}) => {
	return get("/api/users", params);
};

export const addUser = (params = {}) => {
	return post("/api/users", params);
};

export const editUser = (params = {}) => {
	return post(`/api/users/${params.id}`, params);
};

export const deleteUser = (params = {}) => {
	return post(`/api/users/${params.id}/delete`, params);
};
