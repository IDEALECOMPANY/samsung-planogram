import { get, post } from "./call";

export const getMediaSizes = (params = {}) => {
	return get("/api/media/size", params);
};

export const addMediaSize = (params = {}) => {
	return post("/api/media/size", params);
};

export const editMediaSize = (params = {}) => {
	return post(`/api/media/size/${params.id}`, params);
};

export const deleteMediaSize = (params = {}) => {
	return post(`/api/media/size/${params.id}/delete`, params);
};
