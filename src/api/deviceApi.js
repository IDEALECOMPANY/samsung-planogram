import { get, post } from "./call";

export const getDevices = (params = {}) => {
	return get("/api/device", params);
};

export const addDevice = (params = {}) => {
	return post("/api/device", params);
};

export const editDevice = (params = {}) => {
	return post(`/api/device/${params.id}`, params);
};

export const deleteDevice = (params = {}) => {
	return post(`/api/device/${params.id}/delete`, params);
};
