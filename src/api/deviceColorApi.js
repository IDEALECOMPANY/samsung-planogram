import { get, post } from "./call";

export const getDeviceColors = (params = {}) => {
	return get("/api/device/color", params);
};
