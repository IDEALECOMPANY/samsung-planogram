import { get, post } from "./call";

export const getAccessories = (params = {}) => {
	return get("/api/accessory", params);
};

export const addAccessory = (params = {}) => {
	return post("/api/accessory", params);
};

export const editAccessory = (params = {}) => {
	return post(`/api/accessory/${params.id}`, params);
};

export const deleteAccessory = (params = {}) => {
	return post(`/api/accessory/${params.id}/delete`, params);
};