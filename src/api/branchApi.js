import { get, post } from "./call";

export const getBranchs = (params = {}) => {
	return get("/api/branch", params);
};

export const addBranch = (params = {}) => {
	return post("/api/branch", params);
};

export const editBranch = (params = {}) => {
	return post(`/api/branch/${params.id}`, params);
};

export const deleteBranch = (params = {}) => {
	return post(`/api/branch/${params.id}/delete`, params);
};

export const editFloorPlan = (params = {}) => {
	return post(`/api/branch/${params.id}/floorPlan`, params);
};

export const editPlanogram = (params = {}) => {
	return post(`/api/branch/${params.id}/planogram`, params);
};
