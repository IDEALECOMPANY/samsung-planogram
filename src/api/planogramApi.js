import { get, post } from "./call";

export const getPlanograms = (params = {}) => {
	return get("/api/planogram", params);
};

export const addPlanogram = (params = {}) => {
	return post("/api/planogram", params);
};

export const editPlanogram = (params = {}) => {
	return post(`/api/planogram/${params.id}`, params);
};

export const deletePlanogram = (params = {}) => {
	return post(`/api/planogram/${params.id}/delete`, params);
};
