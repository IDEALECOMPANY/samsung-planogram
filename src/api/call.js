import axios from "axios";
import config from "../../config";
import { getToken } from "../selectors";

export const get = (url, params, options = {}) => {
	return axios.get(`${config.origin}${url}`, {
		headers: {
			Authorization: `Bearer ${getToken()}`,
			...options
		},
		params: params
	});
};

export const post = (url, params, options = {}) => {
	return axios.post(`${config.origin}${url}`, params, {
		headers: {
			Authorization: `Bearer ${getToken()}`,
			...options
		}
	});
};
