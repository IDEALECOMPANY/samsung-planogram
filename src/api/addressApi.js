import { get, post } from "./call";

export const getAddress = (params = {}) => {
	return get("/api/address", params);
};
