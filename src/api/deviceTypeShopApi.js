import { get, post } from "./call";

export const getDeviceTypeShops = (params = {}) => {
	return get("/api/device/type", params);
};

export const addDeviceTypeShop = (params = {}) => {
	return post("/api/device/type", params);
};

export const editDeviceTypeShop = (params = {}) => {
	return post(`/api/device/type/${params.id}`, params);
};

export const deleteDeviceTypeShop = (params = {}) => {
	return post(`/api/device/type/${params.id}/delete`, params);
};
