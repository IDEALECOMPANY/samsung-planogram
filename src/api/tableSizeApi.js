import { get, post } from "./call";

export const getTableSizes = (params = {}) => {
	return get("/api/table/size", params);
};

export const addTableSize = (params = {}) => {
	return post("/api/table/size", params);
};

export const editTableSize = (params = {}) => {
	return post(`/api/table/size/${params.id}`, params);
};

export const deleteTableSize = (params = {}) => {
	return post(`/api/table/size/${params.id}/delete`, params);
};
