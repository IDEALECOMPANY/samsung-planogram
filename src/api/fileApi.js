import { get, post } from "./call";

export const uploadFile = (params = {}, zone = "") => {
	return post(`/api/${zone}/upload`, params, {
		"content-type": "multipart/form-data"
	});
};
