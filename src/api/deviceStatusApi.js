import { get, post } from "./call";

export const getDeviceStatuses = (params = {}) => {
	return get("/api/device/status", params);
};

export const addDeviceStatus = (params = {}) => {
	return post("/api/device/status", params);
};

export const editDeviceStatus = (params = {}) => {
	return post(`/api/device/status/${params.id}`, params);
};

export const deleteDeviceStatus = (params = {}) => {
	return post(`/api/device/status/${params.id}/delete`, params);
};
