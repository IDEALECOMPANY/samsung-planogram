import { get, post } from "./call";

export const getPosts = (params = {}) => {
	return get("/api/posts", params);
};
