import { get, post } from "./call";

export const getTableTypes = (params = {}) => {
	return get("/api/table/type", params);
};

export const addTableType = (params = {}) => {
	return post("/api/table/type", params);
};

export const editTableType = (params = {}) => {
	return post(`/api/table/type/${params.id}`, params);
};

export const deleteTableType = (params = {}) => {
	return post(`/api/table/type/${params.id}/delete`, params);
};
