import { get, post } from "./call";

export const getDeviceCapacities = (params = {}) => {
	return get("/api/device/capacity", params);
};
