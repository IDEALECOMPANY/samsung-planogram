import { get, post } from "./call";

export const getDeviceCategories = (params = {}) => {
	return get("/api/device/category", params);
};

export const addDeviceCategory = (params = {}) => {
	return post("/api/device/category", params);
};

export const editDeviceCategory = (params = {}) => {
	return post(`/api/device/category/${params.id}`, params);
};

export const deleteDeviceCategory = (params = {}) => {
	return post(`/api/device/category/${params.id}/delete`, params);
};
