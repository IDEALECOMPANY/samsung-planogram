import { get, post } from "./call";

export const getMedias = (params = {}) => {
	return get("/api/media", params);
};

export const addMedia = (params = {}) => {
	return post("/api/media", params);
};

export const editMedia = (params = {}) => {
	return post(`/api/media/${params.id}`, params);
};

export const deleteMedia = (params = {}) => {
	return post(`/api/media/${params.id}/delete`, params);
};
