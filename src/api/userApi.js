import { get, post } from "./call";

export const login = ({ username, password }) => {
	return post("/api/login", {
		username,
		password
	});
};

export const getUser = () => {
	return get("/api/user", {});
};
