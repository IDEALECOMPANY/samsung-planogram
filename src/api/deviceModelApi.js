import { get, post } from "./call";

export const getDeviceModels = (params = {}) => {
	return get("/api/device/model", params);
};
