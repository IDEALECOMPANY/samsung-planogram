import { get, post } from "./call";

export const getBranchTypes = (params = {}) => {
	return get("/api/branch/type", params);
};

export const addBranchType = (params = {}) => {
	return post("/api/branch/type", params);
};

export const editBranchType = (params = {}) => {
	return post(`/api/branch/type/${params.id}`, params);
};

export const deleteBranchType = (params = {}) => {
	return post(`/api/branch/type/${params.id}/delete`, params);
};
