import { get, post } from "./call";

export const getMediaTypes = (params = {}) => {
	return get("/api/media/type", params);
};

export const addMediaType = (params = {}) => {
	return post("/api/media/type", params);
};

export const editMediaType = (params = {}) => {
	return post(`/api/media/type/${params.id}`, params);
};

export const deleteMediaType = (params = {}) => {
	return post(`/api/media/type/${params.id}/delete`, params);
};
