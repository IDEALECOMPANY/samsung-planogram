import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as mediaSizeApi from "../api/mediaSizeApi";

export const getMediaSizesSuccess = mediaSizes => {
	return {
		type: types.GET_MEDIA_SIZES_SUCCESS,
		mediaSizes
	};
};

export const getMediaSizesError = () => {
	return {
		type: types.GET_MEDIA_SIZES_ERROR
	};
};

export const getMediaSizes = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaSizeApi
			.getMediaSizes()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getMediaSizesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getMediaSizesError());
			});
	};
};

export const addMediaSizeSuccess = mediaSize => {
	return {
		type: types.ADD_MEDIA_SIZE_SUCCESS,
		mediaSize
	};
};

export const addMediaSizeError = () => {
	return {
		type: types.ADD_MEDIA_SIZE_ERROR
	};
};

export const addMediaSize = mediaSize => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaSizeApi
			.addMediaSize(mediaSize)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addMediaSizeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addMediaSizeError());
			});
	};
};

export const editMediaSizeSuccess = mediaSize => {
	return {
		type: types.EDIT_MEDIA_SIZE_SUCCESS,
		mediaSize
	};
};

export const editMediaSizeError = () => {
	return {
		type: types.EDIT_MEDIA_SIZE_ERROR
	};
};

export const editMediaSize = mediaSize => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaSizeApi
			.editMediaSize(mediaSize)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editMediaSizeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editMediaSizeError());
			});
	};
};

export const deleteMediaSizeSuccess = mediaSize => {
	return {
		type: types.DELETE_MEDIA_SIZE_SUCCESS,
		mediaSize
	};
};

export const deleteMediaSizeError = () => {
	return {
		type: types.DELETE_MEDIA_SIZE_ERROR
	};
};

export const deleteMediaSize = mediaSize => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaSizeApi
			.deleteMediaSize(mediaSize)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteMediaSizeSuccess(mediaSize));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteMediaSizeError());
			});
	};
};
