import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as deviceApi from "../api/deviceApi";

export const getDevicesSuccess = devices => {
	return {
		type: types.GET_DEVICES_SUCCESS,
		devices
	};
};

export const getDevicesError = () => {
	return {
		type: types.GET_DEVICES_ERROR
	};
};

export const getDevices = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceApi
			.getDevices()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getDevicesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getDevicesError());
			});
	};
};

export const addDeviceSuccess = device => {
	return {
		type: types.ADD_DEVICE_SUCCESS,
		device
	};
};

export const addDeviceError = () => {
	return {
		type: types.ADD_DEVICE_ERROR
	};
};

export const addDevice = device => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceApi
			.addDevice(device)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addDeviceSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addDeviceError());
			});
	};
};

export const editDeviceSuccess = device => {
	return {
		type: types.EDIT_DEVICE_SUCCESS,
		device
	};
};

export const editDeviceError = () => {
	return {
		type: types.EDIT_DEVICE_ERROR
	};
};

export const editDevice = device => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceApi
			.editDevice(device)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editDeviceSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editDeviceError());
			});
	};
};

export const deleteDeviceSuccess = device => {
	return {
		type: types.DELETE_DEVICE_SUCCESS,
		device
	};
};

export const deleteDeviceError = () => {
	return {
		type: types.DELETE_DEVICE_ERROR
	};
};

export const deleteDevice = device => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceApi
			.deleteDevice(device)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteDeviceSuccess(device));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteDeviceError());
			});
	};
};
