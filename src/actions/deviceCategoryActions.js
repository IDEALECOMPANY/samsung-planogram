import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as deviceCategoryApi from "../api/deviceCategoryApi";

export const getDeviceCategoriesSuccess = deviceCategories => {
	return {
		type: types.GET_DEVICE_CATEGORIES_SUCCESS,
		deviceCategories
	};
};

export const getDeviceCategoriesError = () => {
	return {
		type: types.GET_DEVICE_CATEGORIES_ERROR
	};
};

export const getDeviceCategories = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceCategoryApi
			.getDeviceCategories()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getDeviceCategoriesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getDeviceCategoriesError());
			});
	};
};

export const addDeviceCategorySuccess = deviceCategory => {
	return {
		type: types.ADD_DEVICE_CATEGORY_SUCCESS,
		deviceCategory
	};
};

export const addDeviceCategoryError = () => {
	return {
		type: types.ADD_DEVICE_CATEGORY_ERROR
	};
};

export const addDeviceCategory = deviceCategory => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceCategoryApi
			.addDeviceCategory(deviceCategory)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addDeviceCategorySuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addDeviceCategoryError());
			});
	};
};

export const editDeviceCategorySuccess = deviceCategory => {
	return {
		type: types.EDIT_DEVICE_CATEGORY_SUCCESS,
		deviceCategory
	};
};

export const editDeviceCategoryError = () => {
	return {
		type: types.EDIT_DEVICE_CATEGORY_ERROR
	};
};

export const editDeviceCategory = deviceCategory => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceCategoryApi
			.editDeviceCategory(deviceCategory)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editDeviceCategorySuccess(deviceCategory));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editDeviceCategoryError());
			});
	};
};

export const deleteDeviceCategorySuccess = deviceCategory => {
	return {
		type: types.DELETE_DEVICE_CATEGORY_SUCCESS,
		deviceCategory
	};
};

export const deleteDeviceCategoryError = () => {
	return {
		type: types.DELETE_DEVICE_CATEGORY_ERROR
	};
};

export const deleteDeviceCategory = deviceCategory => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceCategoryApi
			.deleteDeviceCategory(deviceCategory)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteDeviceCategorySuccess(deviceCategory));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteDeviceCategoryError());
			});
	};
};
