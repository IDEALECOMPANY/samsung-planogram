import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as mediaApi from "../api/mediaApi";

export const getMediasSuccess = medias => {
	return {
		type: types.GET_MEDIAS_SUCCESS,
		medias
	};
};

export const getMediasError = () => {
	return {
		type: types.GET_MEDIAS_ERROR
	};
};

export const getMedias = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaApi
			.getMedias()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getMediasSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getMediasError());
			});
	};
};

export const addMediaSuccess = media => {
	return {
		type: types.ADD_MEDIA_SUCCESS,
		media
	};
};

export const addMediaError = () => {
	return {
		type: types.ADD_MEDIA_ERROR
	};
};

export const addMedia = media => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaApi
			.addMedia(media)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addMediaSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addMediaError());
			});
	};
};

export const editMediaSuccess = media => {
	return {
		type: types.EDIT_MEDIA_SUCCESS,
		media
	};
};

export const editMediaError = () => {
	return {
		type: types.EDIT_MEDIA_ERROR
	};
};

export const editMedia = media => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaApi
			.editMedia(media)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editMediaSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editMediaError());
			});
	};
};

export const deleteMediaSuccess = media => {
	return {
		type: types.DELETE_MEDIA_SUCCESS,
		media
	};
};

export const deleteMediaError = () => {
	return {
		type: types.DELETE_MEDIA_ERROR
	};
};

export const deleteMedia = media => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaApi
			.deleteMedia(media)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteMediaSuccess(media));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteMediaError());
			});
	};
};
