import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as deviceModelApi from "../api/deviceModelApi";

export const getDeviceModelsSuccess = deviceModels => {
	return {
		type: types.GET_DEVICE_MODELS_SUCCESS,
		deviceModels
	};
};

export const getDeviceModelsError = () => {
	return {
		type: types.GET_DEVICE_MODELS_ERROR
	};
};

export const getDeviceModels = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceModelApi
			.getDeviceModels()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getDeviceModelsSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getDeviceModelsError());
			});
	};
};
