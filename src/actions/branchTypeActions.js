import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as branchTypeApi from "../api/branchTypeApi";

export const getBranchTypesSuccess = branchTypes => {
	return {
		type: types.GET_BRANCH_TYPES_SUCCESS,
		branchTypes
	};
};

export const getBranchTypesError = () => {
	return {
		type: types.GET_BRANCH_TYPES_ERROR
	};
};

export const getBranchTypes = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchTypeApi
			.getBranchTypes()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getBranchTypesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getBranchTypesError());
			});
	};
};

export const addBranchTypeSuccess = branchType => {
	return {
		type: types.ADD_BRANCH_TYPE_SUCCESS,
		branchType
	};
};

export const addBranchTypeError = () => {
	return {
		type: types.ADD_BRANCH_TYPE_ERROR
	};
};

export const addBranchType = branchType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchTypeApi
			.addBranchType(branchType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addBranchTypeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addBranchTypeError());
			});
	};
};

export const editBranchTypeSuccess = branchType => {
	return {
		type: types.EDIT_BRANCH_TYPE_SUCCESS,
		branchType
	};
};

export const editBranchTypeError = () => {
	return {
		type: types.EDIT_BRANCH_TYPE_ERROR
	};
};

export const editBranchType = branchType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchTypeApi
			.editBranchType(branchType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editBranchTypeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editBranchTypeError());
			});
	};
};

export const deleteBranchTypeSuccess = branchType => {
	return {
		type: types.DELETE_BRANCH_TYPE_SUCCESS,
		branchType
	};
};

export const deleteBranchTypeError = () => {
	return {
		type: types.DELETE_BRANCH_TYPE_ERROR
	};
};

export const deleteBranchType = branchType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchTypeApi
			.deleteBranchType(branchType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteBranchTypeSuccess(branchType));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteBranchTypeError());
			});
	};
};
