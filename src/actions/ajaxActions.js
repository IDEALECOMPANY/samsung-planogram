import * as types from "./actionTypes";

export const onAjaxBegin = () => {
	return {
		type: types.ON_AJAX_BEGIN
	};
};

export const onAjaxSuccess = () => {
	return {
		type: types.ON_AJAX_SUCCESS
	};
};

export const onAjaxError = () => {
	return {
		type: types.ON_AJAX_ERROR
	};
};
