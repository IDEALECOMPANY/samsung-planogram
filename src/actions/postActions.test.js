// import expect from "expect";
// import * as types from "./actionTypes";
// import * as postActions from "./postActions";

// import thunk from "redux-thunk";
// import nock from "nock";
// import configureMockStore from "redux-mock-store";

// describe("Post Actions", () => {
//     describe("getPostsSuccess", () => {
//         it("should create a GET_POSTS_SUCCESS action", () => {
//             let posts = [{
//                 id: 1,
//                 msg: "test",
//                 auther: {
//                     id: 1,
//                     firstName: "name",
//                     lastName: "lname"
//                 }
//             }];

//             let expected = {
//                 type: types.GET_POSTS_SUCCESS,
//                 posts
//             };

//             let action = postActions.getPostsSuccess(posts);
//             expect(action).toEqual(expected);
//             expect(action.posts.length).toBe(1);
//         });
//     });
// });

// const middleware = [thunk];
// const mockStore = configureMockStore(middleware);

// describe("Async Actions", () => {
//     afterEach(() => {
//         nock.cleanAll();
//     });

//     it("should create ON_AJAX_BEGIN and GET_POSTS_SUCCESS when loading post ", done => {
//         const expectedActions = [
//             {
//                 type: types.ON_AJAX_BEGIN
//             }, {
//                 type: types.ON_AJAX_SUCCESS
//             }, {
//                 type: types.GET_POSTS_SUCCESS
//             }
//         ];

//         const store = mockStore({ posts: [] }, expectedActions);
//         store.dispatch(postActions.getPosts()).then(() => {
//             const actions = store.getActions();
//             expect(actions[0].type).toEqual(types.ON_AJAX_BEGIN);
//             expect(actions[1].type).toEqual(types.ON_AJAX_SUCCESS);
//             done();
//         });
//     });
// });
