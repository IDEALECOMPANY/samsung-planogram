import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as deviceColorApi from "../api/deviceColorApi";

export const getDeviceColorsSuccess = deviceColors => {
	return {
		type: types.GET_DEVICE_COLORS_SUCCESS,
		deviceColors
	};
};

export const getDeviceColorsError = () => {
	return {
		type: types.GET_DEVICE_COLORS_ERROR
	};
};

export const getDeviceColors = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceColorApi
			.getDeviceColors()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getDeviceColorsSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getDeviceColorsError());
			});
	};
};
