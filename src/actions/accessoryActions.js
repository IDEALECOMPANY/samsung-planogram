import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as accessoryApi from "../api/accessoryApi";

export const getAccessoriesSuccess = accessories => {
	return {
		type: types.GET_ACCESSORIES_SUCCESS,
		accessories
	};
};

export const getAccessoriesError = () => {
	return {
		type: types.GET_ACCESSORIES_ERROR
	};
};

export const getAccessories = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return accessoryApi.getAccessories().then(res => {
			dispatch(onAjaxSuccess());
			dispatch(getAccessoriesSuccess(res.data));
		}).catch(error => {
			dispatch(onAjaxError());
			dispatch(getAccessoriesError());
		});
	};
};

export const addAccessorySuccess = accessory => {
	return {
		type: types.ADD_ACCESSORY_SUCCESS,
		accessory
	};
};

export const addAccessoryError = () => {
	return {
		type: types.ADD_ACCESSORY_ERROR
	};
};

export const addAccessory = accessory => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return accessoryApi.addAccessory(accessory).then(res => {
			dispatch(onAjaxSuccess());
			dispatch(addAccessorySuccess(res.data));
		}).catch(error => {
			dispatch(onAjaxError());
			dispatch(addAccessoryError());
		});
	};
};

export const editAccessorySuccess = accessory => {
	return {
		type: types.EDIT_ACCESSORY_SUCCESS,
		accessory
	};
};

export const editAccessoryError = () => {
	return {
		type: types.EDIT_ACCESSORY_ERROR
	};
};

export const editAccessory = accessory => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return accessoryApi.editAccessory(accessory).then(res => {
			dispatch(onAjaxSuccess());
			dispatch(editAccessorySuccess(res.data));
		}).catch(error => {
			dispatch(onAjaxError());
			dispatch(editAccessoryError());
		});
	};
};

export const deleteAccessorySuccess = accessory => {
	return {
		type: types.DELETE_ACCESSORY_SUCCESS,
		accessory
	};
};

export const deleteAccessoryError = () => {
	return {
		type: types.DELETE_ACCESSORY_ERROR
	};
};

export const deleteAccessory = accessory => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return accessoryApi
			.deleteAccessory(accessory)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteAccessorySuccess(accessory));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteAccessoryError());
			});
	};
};