// Ajax
export const ON_AJAX_BEGIN = "ON_AJAX_BEGIN";
export const ON_AJAX_SUCCESS = "ON_AJAX_SUCCESS";
export const ON_AJAX_ERROR = "ON_AJAX_ERROR";

export const CHANGE_OPENMENU = "CHANGE_OPENMENU";

export const GET_POSTS_SUCCESS = "GET_POSTS_SUCCESS";
export const GET_POSTS_ERROR = "GET_POSTS_ERROR";

// Authen
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";

// Address
export const GET_ADDRESS_SUCCESS = "GET_ADDRESS_SUCCESS";
export const GET_ADDRESS_ERROR = "GET_ADDRESS_ERROR";

// Media Type
export const GET_MEDIA_TYPES_SUCCESS = "GET_MEDIA_TYPES_SUCCESS";
export const GET_MEDIA_TYPES_ERROR = "GET_MEDIA_TYPES_ERROR";
export const ADD_MEDIA_TYPE_SUCCESS = "ADD_MEDIA_TYPE_SUCCESS";
export const ADD_MEDIA_TYPE_ERROR = "ADD_MEDIA_TYPE_ERROR";
export const EDIT_MEDIA_TYPE_SUCCESS = "EDIT_MEDIA_TYPE_SUCCESS";
export const EDIT_MEDIA_TYPE_ERROR = "EDIT_MEDIA_TYPE_ERROR";
export const DELETE_MEDIA_TYPE_SUCCESS = "DELETE_MEDIA_TYPE_SUCCESS";
export const DELETE_MEDIA_TYPE_ERROR = "DELETE_MEDIA_TYPE_ERROR";

// Table Type
export const GET_TABLE_TYPES_SUCCESS = "GET_TABLE_TYPES_SUCCESS";
export const GET_TABLE_TYPES_ERROR = "GET_TABLE_TYPES_ERROR";
export const ADD_TABLE_TYPE_SUCCESS = "ADD_TABLE_TYPE_SUCCESS";
export const ADD_TABLE_TYPE_ERROR = "ADD_TABLE_TYPE_ERROR";
export const EDIT_TABLE_TYPE_SUCCESS = "EDIT_TABLE_TYPE_SUCCESS";
export const EDIT_TABLE_TYPE_ERROR = "EDIT_TABLE_TYPE_ERROR";
export const DELETE_TABLE_TYPE_SUCCESS = "DELETE_TABLE_TYPE_SUCCESS";
export const DELETE_TABLE_TYPE_ERROR = "DELETE_TABLE_TYPE_ERROR";

// Media
export const GET_MEDIAS_SUCCESS = "GET_MEDIAS_SUCCESS";
export const GET_MEDIAS_ERROR = "GET_MEDIAS_ERROR";
export const ADD_MEDIA_SUCCESS = "ADD_MEDIA_SUCCESS";
export const ADD_MEDIA_ERROR = "ADD_MEDIA_ERROR";
export const EDIT_MEDIA_SUCCESS = "EDIT_MEDIA_SUCCESS";
export const EDIT_MEDIA_ERROR = "EDIT_MEDIA_ERROR";
export const DELETE_MEDIA_SUCCESS = "DELETE_MEDIA_SUCCESS";
export const DELETE_MEDIA_ERROR = "DELETE_MEDIA_ERROR";

// Table Size
export const GET_TABLE_SIZES_SUCCESS = "GET_TABLE_SIZES_SUCCESS";
export const GET_TABLE_SIZES_ERROR = "GET_TABLE_SIZES_ERROR";
export const ADD_TABLE_SIZE_SUCCESS = "ADD_TABLE_SIZE_SUCCESS";
export const ADD_TABLE_SIZE_ERROR = "ADD_TABLE_SIZE_ERROR";
export const EDIT_TABLE_SIZE_SUCCESS = "EDIT_TABLE_SIZE_SUCCESS";
export const EDIT_TABLE_SIZE_ERROR = "EDIT_TABLE_SIZE_ERROR";
export const DELETE_TABLE_SIZE_SUCCESS = "DELETE_TABLE_SIZE_SUCCESS";
export const DELETE_TABLE_SIZE_ERROR = "DELETE_TABLE_SIZE_ERROR";

// Device
export const GET_DEVICES_SUCCESS = "GET_DEVICES_SUCCESS";
export const GET_DEVICES_ERROR = "GET_DEVICES_ERROR";
export const ADD_DEVICE_SUCCESS = "ADD_DEVICE_SUCCESS";
export const ADD_DEVICE_ERROR = "ADD_DEVICE_ERROR";
export const EDIT_DEVICE_SUCCESS = "EDIT_DEVICE_SUCCESS";
export const EDIT_DEVICE_ERROR = "EDIT_DEVICE_ERROR";
export const DELETE_DEVICE_SUCCESS = "DELETE_DEVICE_SUCCESS";
export const DELETE_DEVICE_ERROR = "DELETE_DEVICE_ERROR";

// Device Color
export const GET_DEVICE_COLORS_SUCCESS = "GET_DEVICE_COLORS_SUCCESS";
export const GET_DEVICE_COLORS_ERROR = "GET_DEVICE_COLORS_ERROR";

// Device Capacity
export const GET_DEVICE_CAPACITIES_SUCCESS = "GET_DEVICE_CAPACITIES_SUCCESS";
export const GET_DEVICE_CAPACITIES_ERROR = "GET_DEVICE_CAPACITIES_ERROR";

// Device Model
export const GET_DEVICE_MODELS_SUCCESS = "GET_DEVICE_MODELS_SUCCESS";
export const GET_DEVICE_MODELS_ERROR = "GET_DEVICE_MODELS_ERROR";

// Device Category
export const GET_DEVICE_CATEGORIES_SUCCESS = "GET_DEVICE_CATEGORIES_SUCCESS";
export const GET_DEVICE_CATEGORIES_ERROR = "GET_DEVICE_CATEGORIES_ERROR";
export const ADD_DEVICE_CATEGORY_SUCCESS = "ADD_DEVICE_CATEGORY_SUCCESS";
export const ADD_DEVICE_CATEGORY_ERROR = "ADD_DEVICE_CATEGORY_ERROR";
export const EDIT_DEVICE_CATEGORY_SUCCESS = "EDIT_DEVICE_CATEGORY_SUCCESS";
export const EDIT_DEVICE_CATEGORY_ERROR = "EDIT_DEVICE_CATEGORY_ERROR";
export const DELETE_DEVICE_CATEGORY_SUCCESS = "DELETE_DEVICE_CATEGORY_SUCCESS";
export const DELETE_DEVICE_CATEGORY_ERROR = "DELETE_DEVICE_CATEGORY_ERROR";

// Device Status
export const GET_DEVICE_STATUSES_SUCCESS = "GET_DEVICE_STATUSES_SUCCESS";
export const GET_DEVICE_STATUSES_ERROR = "GET_DEVICE_STATUSES_ERROR";
export const ADD_DEVICE_STATUS_SUCCESS = "ADD_DEVICE_STATUS_SUCCESS";
export const ADD_DEVICE_STATUS_ERROR = "ADD_DEVICE_STATUS_ERROR";
export const EDIT_DEVICE_STATUS_SUCCESS = "EDIT_DEVICE_STATUS_SUCCESS";
export const EDIT_DEVICE_STATUS_ERROR = "EDIT_DEVICE_STATUS_ERROR";
export const DELETE_DEVICE_STATUS_SUCCESS = "DELETE_DEVICE_STATUS_SUCCESS";
export const DELETE_DEVICE_STATUS_ERROR = "DELETE_DEVICE_STATUS_ERROR";

// Device Type
export const GET_DEVICE_TYPE_SHOPS_SUCCESS = "GET_DEVICE_TYPE_SHOPS_SUCCESS";
export const GET_DEVICE_TYPE_SHOPS_ERROR = "GET_DEVICE_TYPE_SHOPS_ERROR";
export const ADD_DEVICE_TYPE_SHOP_SUCCESS = "ADD_DEVICE_TYPE_SHOP_SUCCESS";
export const ADD_DEVICE_TYPE_SHOP_ERROR = "ADD_DEVICE_TYPE_SHOP_ERROR";
export const EDIT_DEVICE_TYPE_SHOP_SUCCESS = "EDIT_DEVICE_TYPE_SHOP_SUCCESS";
export const EDIT_DEVICE_TYPE_SHOP_ERROR = "EDIT_DEVICE_TYPE_SHOP_ERROR";
export const DELETE_DEVICE_TYPE_SHOP_SUCCESS = "DELETE_DEVICE_TYPE_SHOP_SUCCESS";
export const DELETE_DEVICE_TYPE_SHOP_ERROR = "DELETE_DEVICE_TYPE_SHOP_ERROR";

// Branch Type
export const GET_BRANCH_TYPES_SUCCESS = "GET_BRANCH_TYPES_SUCCESS";
export const GET_BRANCH_TYPES_ERROR = "GET_BRANCH_TYPES_ERROR";
export const ADD_BRANCH_TYPE_SUCCESS = "ADD_BRANCH_TYPE_SUCCESS";
export const ADD_BRANCH_TYPE_ERROR = "ADD_BRANCH_TYPE_ERROR";
export const EDIT_BRANCH_TYPE_SUCCESS = "EDIT_BRANCH_TYPE_SUCCESS";
export const EDIT_BRANCH_TYPE_ERROR = "EDIT_BRANCH_TYPE_ERROR";
export const DELETE_BRANCH_TYPE_SUCCESS = "DELETE_BRANCH_TYPE_SUCCESS";
export const DELETE_BRANCH_TYPE_ERROR = "DELETE_BRANCH_TYPE_ERROR";

// Branch
export const GET_BRANCHS_SUCCESS = "GET_BRANCHS_SUCCESS";
export const GET_BRANCHS_ERROR = "GET_BRANCHS_ERROR";
export const ADD_BRANCH_SUCCESS = "ADD_BRANCH_SUCCESS";
export const ADD_BRANCH_ERROR = "ADD_BRANCH_ERROR";
export const EDIT_BRANCH_SUCCESS = "EDIT_BRANCH_SUCCESS";
export const EDIT_BRANCH_ERROR = "EDIT_BRANCH_ERROR";
export const DELETE_BRANCH_SUCCESS = "DELETE_BRANCH_SUCCESS";
export const DELETE_BRANCH_ERROR = "DELETE_BRANCH_ERROR";
export const EDIT_BRANCH_PLANOGRAM_SUCCESS = "EDIT_BRANCH_PLANOGRAM_SUCCESS";
export const EDIT_BRANCH_PLANOGRAM_ERROR = "EDIT_BRANCH_PLANOGRAM_ERROR";

// Floorplan
export const EDIT_FLOORPLAN_SUCCESS = "EDIT_FLOORPLAN_SUCCESS";
export const EDIT_FLOORPLAN_ERROR = "EDIT_FLOORPLAN_ERROR";

// Planogram
export const GET_PLANOGRAMS_SUCCESS = "GET_PLANOGRAMS_SUCCESS";
export const GET_PLANOGRAMS_ERROR = "GET_PLANOGRAMS_ERROR";
export const ADD_PLANOGRAM_SUCCESS = "ADD_PLANOGRAM_SUCCESS";
export const ADD_PLANOGRAM_ERROR = "ADD_PLANOGRAM_ERROR";
export const EDIT_PLANOGRAM_SUCCESS = "EDIT_PLANOGRAM_SUCCESS";
export const EDIT_PLANOGRAM_ERROR = "EDIT_PLANOGRAM_ERROR";
export const DELETE_PLANOGRAM_SUCCESS = "DELETE_PLANOGRAM_SUCCESS";
export const DELETE_PLANOGRAM_ERROR = "DELETE_PLANOGRAM_ERROR";


// Upload File
export const UPLOAD_FILE_SUCCESS = "UPLOAD_FILE_SUCCESS";
export const UPLOAD_FILE_ERROR = "UPLOAD_FILE_ERROR";

// Media Size
export const GET_MEDIA_SIZES_SUCCESS = "GET_MEDIA_SIZES_SUCCESS";
export const GET_MEDIA_SIZES_ERROR = "GET_MEDIA_SIZES_ERROR";
export const ADD_MEDIA_SIZE_SUCCESS = "ADD_MEDIA_SIZE_SUCCESS";
export const ADD_MEDIA_SIZE_ERROR = "ADD_MEDIA_SIZE_ERROR";
export const EDIT_MEDIA_SIZE_SUCCESS = "EDIT_MEDIA_SIZE_SUCCESS";
export const EDIT_MEDIA_SIZE_ERROR = "EDIT_MEDIA_SIZE_ERROR";
export const DELETE_MEDIA_SIZE_SUCCESS = "DELETE_MEDIA_SIZE_SUCCESS";
export const DELETE_MEDIA_SIZE_ERROR = "DELETE_MEDIA_SIZE_ERROR";

// User
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const GET_USER_ERROR = "GET_USER_ERROR";
export const GET_USERS_SUCCESS = "GET_USERS_SUCCESS";
export const GET_USERS_ERROR = "GET_USERS_ERROR";
export const ADD_USER_SUCCESS = "ADD_USER_SUCCESS";
export const ADD_USER_ERROR = "ADD_USER_ERROR";
export const EDIT_USER_SUCCESS = "EDIT_USER_SUCCESS";
export const EDIT_USER_ERROR = "EDIT_USER_ERROR";
export const DELETE_USER_SUCCESS = "DELETE_USER_SUCCESS";
export const DELETE_USER_ERROR = "DELETE_USER_ERROR";

// Accessory
export const GET_ACCESSORIES_SUCCESS = "GET_ACCESSORIES_SUCCESS";
export const GET_ACCESSORIES_ERROR = "GET_ACCESSORIES_ERROR";
export const ADD_ACCESSORY_SUCCESS = "ADD_ACCESSORY_SUCCESS";
export const ADD_ACCESSORY_ERROR = "ADD_ACCESSORY_ERROR";
export const EDIT_ACCESSORY_SUCCESS = "EDIT_ACCESSORY_SUCCESS";
export const EDIT_ACCESSORY_ERROR = "EDIT_ACCESSORY_ERROR";
export const DELETE_ACCESSORY_SUCCESS = "DELETE_ACCESSORY_SUCCESS";
export const DELETE_ACCESSORY_ERROR = "DELETE_ACCESSORY_ERROR";