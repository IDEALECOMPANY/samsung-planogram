import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as deviceStatusApi from "../api/deviceStatusApi";

export const getDeviceStatusesSuccess = deviceStatuses => {
	return {
		type: types.GET_DEVICE_STATUSES_SUCCESS,
		deviceStatuses
	};
};

export const getDeviceStatusesError = () => {
	return {
		type: types.GET_DEVICE_STATUSES_ERROR
	};
};

export const getDeviceStatuses = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceStatusApi
			.getDeviceStatuses()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getDeviceStatusesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getDeviceStatusesError());
			});
	};
};

export const addDeviceStatusSuccess = deviceStatus => {
	return {
		type: types.ADD_DEVICE_STATUS_SUCCESS,
		deviceStatus
	};
};

export const addDeviceStatusError = () => {
	return {
		type: types.ADD_DEVICE_STATUS_ERROR
	};
};

export const addDeviceStatus = deviceStatus => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceStatusApi
			.addDeviceStatus(deviceStatus)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addDeviceStatusSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addDeviceStatusError());
			});
	};
};

export const editDeviceStatusSuccess = deviceStatus => {
	return {
		type: types.EDIT_DEVICE_STATUS_SUCCESS,
		deviceStatus
	};
};

export const editDeviceStatusError = () => {
	return {
		type: types.EDIT_DEVICE_STATUS_ERROR
	};
};

export const editDeviceStatus = deviceStatus => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceStatusApi
			.editDeviceStatus(deviceStatus)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editDeviceStatusSuccess(deviceStatus));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editDeviceStatusError());
			});
	};
};

export const deleteDeviceStatusSuccess = deviceStatus => {
	return {
		type: types.DELETE_DEVICE_STATUS_SUCCESS,
		deviceStatus
	};
};

export const deleteDeviceStatusError = () => {
	return {
		type: types.DELETE_DEVICE_STATUS_ERROR
	};
};

export const deleteDeviceStatus = deviceStatus => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceStatusApi
			.deleteDeviceStatus(deviceStatus)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteDeviceStatusSuccess(deviceStatus));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteDeviceStatusError());
			});
	};
};
