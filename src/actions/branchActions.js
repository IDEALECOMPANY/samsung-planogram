import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as branchApi from "../api/branchApi";

export const getBranchsSuccess = branchs => {
	return {
		type: types.GET_BRANCHS_SUCCESS,
		branchs
	};
};

export const getBranchsError = () => {
	return {
		type: types.GET_BRANCHS_ERROR
	};
};

export const getBranchs = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchApi
			.getBranchs()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getBranchsSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getBranchsError());
			});
	};
};

export const addBranchSuccess = branch => {
	return {
		type: types.ADD_BRANCH_SUCCESS,
		branch
	};
};

export const addBranchError = () => {
	return {
		type: types.ADD_BRANCH_ERROR
	};
};

export const addBranch = branch => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchApi
			.addBranch(branch)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addBranchSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addBranchError());
			});
	};
};

export const editBranchSuccess = branch => {
	return {
		type: types.EDIT_BRANCH_SUCCESS,
		branch
	};
};

export const editBranchError = () => {
	return {
		type: types.EDIT_BRANCH_ERROR
	};
};

export const editBranch = branch => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchApi
			.editBranch(branch)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editBranchSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editBranchError());
			});
	};
};

export const deleteBranchSuccess = branch => {
	return {
		type: types.DELETE_BRANCH_SUCCESS,
		branch
	};
};

export const deleteBranchError = () => {
	return {
		type: types.DELETE_BRANCH_ERROR
	};
};

export const deleteBranch = branch => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchApi
			.deleteBranch(branch)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteBranchSuccess(branch));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteBranchError());
			});
	};
};

export const editFloorPlanSuccess = branch => {
	return {
		type: types.EDIT_FLOORPLAN_SUCCESS,
		branch
	};
};

export const editFloorPlanError = () => {
	return {
		type: types.EDIT_FLOORPLAN_ERROR
	};
};

export const editFloorPlan = floorplan => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchApi
			.editFloorPlan(floorplan)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editFloorPlanSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editFloorPlanError());
			});
	};
};

export const editPlanogramSuccess = branch => {
	return {
		type: types.EDIT_BRANCH_PLANOGRAM_SUCCESS,
		branch
	};
};

export const editPlanogramError = () => {
	return {
		type: types.EDIT_BRANCH_PLANOGRAM_ERROR
	};
};

export const editPlanogram = params => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return branchApi
			.editPlanogram(params)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editPlanogramSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editPlanogramError());
			});
	};
};
