import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as planogramApi from "../api/planogramApi";

export const getPlanogramsSuccess = planograms => {
	return {
		type: types.GET_PLANOGRAMS_SUCCESS,
		planograms
	};
};

export const getPlanogramsError = () => {
	return {
		type: types.GET_PLANOGRAMS_ERROR
	};
};

export const getPlanograms = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return planogramApi
			.getPlanograms()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getPlanogramsSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getPlanogramsError());
			});
	};
};

export const addPlanogramSuccess = planogram => {
	return {
		type: types.ADD_PLANOGRAM_SUCCESS,
		planogram
	};
};

export const addPlanogramError = () => {
	return {
		type: types.ADD_PLANOGRAM_ERROR
	};
};

export const addPlanogram = planogram => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return planogramApi
			.addPlanogram(planogram)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addPlanogramSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addPlanogramError());
			});
	};
};

export const editPlanogramSuccess = planogram => {
	return {
		type: types.EDIT_PLANOGRAM_SUCCESS,
		planogram
	};
};

export const editPlanogramError = () => {
	return {
		type: types.EDIT_PLANOGRAM_ERROR
	};
};

export const editPlanogram = planogram => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return planogramApi
			.editPlanogram(planogram)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editPlanogramSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editPlanogramError());
			});
	};
};

export const deletePlanogramSuccess = planogram => {
	return {
		type: types.DELETE_PLANOGRAM_SUCCESS,
		planogram
	};
};

export const deletePlanogramError = () => {
	return {
		type: types.DELETE_PLANOGRAM_ERROR
	};
};

export const deletePlanogram = planogram => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return planogramApi
			.deletePlanogram(planogram)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deletePlanogramSuccess(planogram));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deletePlanogramError());
			});
	};
};
