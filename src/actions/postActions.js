import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as postApi from "../api/postApi";

export const getPostsSuccess = posts => {
	return {
		type: types.GET_POSTS_SUCCESS,
		posts
	};
};

export const getPostsError = () => {
	return {
		type: types.GET_POSTS_ERROR
	};
};

export const getPosts = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return postApi
			.getPosts()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getPostsSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getPostsError());
				throw error;
			});
	};
};
