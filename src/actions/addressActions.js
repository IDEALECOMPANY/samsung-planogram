import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as addressApi from "../api/addressApi";

export const getAddressSuccess = address => {
	return {
		type: types.GET_ADDRESS_SUCCESS,
		address
	};
};

export const getAddressError = () => {
	return {
		type: types.GET_ADDRESS_ERROR
	};
};

export const getAddress = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return addressApi
			.getAddress()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getAddressSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getAddressError());
			});
	};
};
