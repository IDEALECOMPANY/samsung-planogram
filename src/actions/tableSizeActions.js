import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as tableSizeApi from "../api/tableSizeApi";

export const getTableSizesSuccess = tableSizes => {
	return {
		type: types.GET_TABLE_SIZES_SUCCESS,
		tableSizes
	};
};

export const getTableSizesError = () => {
	return {
		type: types.GET_TABLE_SIZES_ERROR
	};
};

export const getTableSizes = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return tableSizeApi
			.getTableSizes()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getTableSizesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getTableSizesError());
			});
	};
};

export const addTableSizeSuccess = tableSize => {
	return {
		type: types.ADD_TABLE_SIZE_SUCCESS,
		tableSize
	};
};

export const addTableSizeError = () => {
	return {
		type: types.ADD_TABLE_SIZE_ERROR
	};
};

export const addTableSize = tableSize => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return tableSizeApi
			.addTableSize(tableSize)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addTableSizeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addTableSizeError());
			});
	};
};

export const editTableSizeSuccess = tableSize => {
	return {
		type: types.EDIT_TABLE_SIZE_SUCCESS,
		tableSize
	};
};

export const editTableSizeError = () => {
	return {
		type: types.EDIT_TABLE_SIZE_ERROR
	};
};

export const editTableSize = tableSize => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return tableSizeApi
			.editTableSize(tableSize)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editTableSizeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editTableSizeError());
			});
	};
};

export const deleteTableSizeSuccess = tableSize => {
	return {
		type: types.DELETE_TABLE_SIZE_SUCCESS,
		tableSize
	};
};

export const deleteTableSizeError = () => {
	return {
		type: types.DELETE_TABLE_SIZE_ERROR
	};
};

export const deleteTableSize = tableSize => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return tableSizeApi
			.deleteTableSize(tableSize)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteTableSizeSuccess(tableSize));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteTableSizeError());
			});
	};
};
