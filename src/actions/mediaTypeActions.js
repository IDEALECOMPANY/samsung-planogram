import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as mediaTypeApi from "../api/mediaTypeApi";

export const getMediaTypesSuccess = mediaTypes => {
	return {
		type: types.GET_MEDIA_TYPES_SUCCESS,
		mediaTypes
	};
};

export const getMediaTypesError = () => {
	return {
		type: types.GET_MEDIA_TYPES_ERROR
	};
};

export const getMediaTypes = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaTypeApi
			.getMediaTypes()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getMediaTypesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getMediaTypesError());
			});
	};
};

export const addMediaTypeSuccess = mediaType => {
	return {
		type: types.ADD_MEDIA_TYPE_SUCCESS,
		mediaType
	};
};

export const addMediaTypeError = () => {
	return {
		type: types.ADD_MEDIA_TYPE_ERROR
	};
};

export const addMediaType = mediaType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaTypeApi
			.addMediaType(mediaType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addMediaTypeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addMediaTypeError());
			});
	};
};

export const editMediaTypeSuccess = mediaType => {
	return {
		type: types.EDIT_MEDIA_TYPE_SUCCESS,
		mediaType
	};
};

export const editMediaTypeError = () => {
	return {
		type: types.EDIT_MEDIA_TYPE_ERROR
	};
};

export const editMediaType = mediaType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaTypeApi
			.editMediaType(mediaType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editMediaTypeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editMediaTypeError());
			});
	};
};

export const deleteMediaTypeSuccess = mediaType => {
	return {
		type: types.DELETE_MEDIA_TYPE_SUCCESS,
		mediaType
	};
};

export const deleteMediaTypeError = () => {
	return {
		type: types.DELETE_MEDIA_TYPE_ERROR
	};
};

export const deleteMediaType = mediaType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return mediaTypeApi
			.deleteMediaType(mediaType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteMediaTypeSuccess(mediaType));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteMediaTypeError());
			});
	};
};
