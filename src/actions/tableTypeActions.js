import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as tableTypeApi from "../api/tableTypeApi";

export const getTableTypesSuccess = tableTypes => {
	return {
		type: types.GET_TABLE_TYPES_SUCCESS,
		tableTypes
	};
};

export const getTableTypesError = () => {
	return {
		type: types.GET_TABLE_TYPES_ERROR
	};
};

export const getTableTypes = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return tableTypeApi
			.getTableTypes()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getTableTypesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getTableTypesError());
			});
	};
};

export const addTableTypeSuccess = tableType => {
	return {
		type: types.ADD_TABLE_TYPE_SUCCESS,
		tableType
	};
};

export const addTableTypeError = () => {
	return {
		type: types.ADD_TABLE_TYPE_ERROR
	};
};

export const addTableType = tableType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return tableTypeApi
			.addTableType(tableType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addTableTypeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addTableTypeError());
			});
	};
};

export const editTableTypeSuccess = tableType => {
	return {
		type: types.EDIT_TABLE_TYPE_SUCCESS,
		tableType
	};
};

export const editTableTypeError = () => {
	return {
		type: types.EDIT_TABLE_TYPE_ERROR
	};
};

export const editTableType = tableType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return tableTypeApi
			.editTableType(tableType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editTableTypeSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editTableTypeError());
			});
	};
};

export const deleteTableTypeSuccess = tableType => {
	return {
		type: types.DELETE_TABLE_TYPE_SUCCESS,
		tableType
	};
};

export const deleteTableTypeError = () => {
	return {
		type: types.DELETE_TABLE_TYPE_ERROR
	};
};

export const deleteTableType = tableType => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return tableTypeApi
			.deleteTableType(tableType)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteTableTypeSuccess(tableType));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteTableTypeError());
			});
	};
};
