import * as types from "./actionTypes";

export const onChangeOpenMenu = openMenu => {
	return {
		type: types.CHANGE_OPENMENU,
		openMenu
	};
};
export const changeOpenMenu = openMenu => {
	return dispatch => {
		dispatch(onChangeOpenMenu(openMenu));
	};
};
