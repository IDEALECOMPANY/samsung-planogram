import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";
import { setToken, getToken, clearToken } from "../selectors";

import * as userApi from "../api/userApi";

export const loginSuccess = user => {
	return {
		type: types.LOGIN_SUCCESS,
		user
	};
};

export const loginError = () => {
	return {
		type: types.LOGIN_ERROR
	};
};

export const login = (username, password) => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return userApi
			.login({ username, password })
			.then(res => {
				setToken(res.data.token);
				dispatch(onAjaxSuccess());
				dispatch(loginSuccess(res.data.user));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(loginError());
			});
	};
};

export const getUserSuccess = user => {
	return {
		type: types.GET_USER_SUCCESS,
		user
	};
};

export const getUserError = () => {
	return {
		type: types.GET_USER_ERROR
	};
};

export const getUser = () => {
	return dispatch => {
		if (getToken()) {
			dispatch(onAjaxBegin());
			return userApi
				.getUser()
				.then(user => {
					dispatch(onAjaxSuccess());
					dispatch(getUserSuccess(user.data));
				})
				.catch(error => {
					dispatch(onAjaxError());
					dispatch(getUserError());
				});
		} else {
			return new Promise((resolve, reject) => {
				reject(false);
				dispatch(getUserError());
			});
		}
	};
};

export const logoutSuccess = () => {
	return {
		type: types.LOGOUT_SUCCESS,
		user: null
	};
};

export const logout = () => {
	return dispatch => {
		clearToken();
		dispatch(logoutSuccess());
	};
};
