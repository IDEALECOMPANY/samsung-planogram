import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";
import { setToken, getToken, clearToken } from "../selectors";

import * as usersApi from "../api/usersApi";

export const getUsersSuccess = users => {
	return {
		type: types.GET_USERS_SUCCESS,
		users
	};
};

export const getUsersError = () => {
	return {
		type: types.GET_USERS_ERROR
	};
};

export const getUsers = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return usersApi
			.getUsers()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getUsersSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getUsersError());
			});
	};
};

export const addUserSuccess = user => {
	return {
		type: types.ADD_USER_SUCCESS,
		user
	};
};

export const addUserError = () => {
	return {
		type: types.ADD_USER_ERROR
	};
};

export const addUser = user => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return usersApi
			.addUser(user)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addUserSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addUserError());
			});
	};
};

export const editUserSuccess = user => {
	return {
		type: types.EDIT_USER_SUCCESS,
		user
	};
};

export const editUserError = () => {
	return {
		type: types.EDIT_USER_ERROR
	};
};

export const editUser = user => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return usersApi
			.editUser(user)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editUserSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editUserError());
			});
	};
};

export const deleteUserSuccess = user => {
	return {
		type: types.DELETE_USER_SUCCESS,
		user
	};
};

export const deleteUserError = () => {
	return {
		type: types.DELETE_USER_ERROR
	};
};

export const deleteUser = user => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return usersApi
			.deleteUser(user)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteUserSuccess(user));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteUserError());
			});
	};
};
