import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as deviceCapacityApi from "../api/deviceCapacityApi";

export const getDeviceCapacitiesSuccess = deviceCapacities => {
	return {
		type: types.GET_DEVICE_CAPACITIES_SUCCESS,
		deviceCapacities
	};
};

export const getDeviceCapacitiesError = () => {
	return {
		type: types.GET_DEVICE_CAPACITIES_ERROR
	};
};

export const getDeviceCapacities = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceCapacityApi
			.getDeviceCapacities()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getDeviceCapacitiesSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getDeviceCapacitiesError());
			});
	};
};
