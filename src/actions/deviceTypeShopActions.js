import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as deviceTypeShopApi from "../api/deviceTypeShopApi";

export const getDeviceTypeShopsSuccess = deviceTypeShops => {
	return {
		type: types.GET_DEVICE_TYPE_SHOPS_SUCCESS,
		deviceTypeShops
	};
};

export const getDeviceTypeShopsError = () => {
	return {
		type: types.GET_DEVICE_TYPE_SHOPS_ERROR
	};
};

export const getDeviceTypeShops = () => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceTypeShopApi
			.getDeviceTypeShops()
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(getDeviceTypeShopsSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(getDeviceTypeShopsError());
			});
	};
};

export const addDeviceTypeShopSuccess = deviceTypeShop => {
	return {
		type: types.ADD_DEVICE_TYPE_SHOP_SUCCESS,
		deviceTypeShop
	};
};

export const addDeviceTypeShopError = () => {
	return {
		type: types.ADD_DEVICE_TYPE_SHOP_ERROR
	};
};

export const addDeviceTypeShop = deviceTypeShop => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceTypeShopApi
			.addDeviceTypeShop(deviceTypeShop)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(addDeviceTypeShopSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(addDeviceTypeShopError());
			});
	};
};

export const editDeviceTypeShopSuccess = deviceTypeShop => {
	return {
		type: types.EDIT_DEVICE_TYPE_SHOP_SUCCESS,
		deviceTypeShop
	};
};

export const editDeviceTypeShopError = () => {
	return {
		type: types.EDIT_DEVICE_TYPE_SHOP_ERROR
	};
};

export const editDeviceTypeShop = deviceTypeShop => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceTypeShopApi
			.editDeviceTypeShop(deviceTypeShop)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(editDeviceTypeShopSuccess(deviceTypeShop));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(editDeviceTypeShopError());
			});
	};
};

export const deleteDeviceTypeShopSuccess = deviceTypeShop => {
	return {
		type: types.DELETE_DEVICE_TYPE_SHOP_SUCCESS,
		deviceTypeShop
	};
};

export const deleteDeviceTypeShopError = () => {
	return {
		type: types.DELETE_DEVICE_TYPE_SHOP_ERROR
	};
};

export const deleteDeviceTypeShop = deviceTypeShop => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return deviceTypeShopApi
			.deleteDeviceTypeShop(deviceTypeShop)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(deleteDeviceTypeShopSuccess(deviceTypeShop));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(deleteDeviceTypeShopError());
			});
	};
};
