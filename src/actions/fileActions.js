import * as types from "./actionTypes";
import { onAjaxBegin, onAjaxSuccess, onAjaxError } from "./ajaxActions";

import * as fileApi from "../api/fileApi";

export const uploadFileSuccess = () => {
	return {
		type: types.UPLOAD_FILE_SUCCESS
	};
};

export const uploadFileError = () => {
	return {
		type: types.UPLOAD_FILE_ERROR
	};
};

export const uploadFile = (file, zone) => {
	return dispatch => {
		dispatch(onAjaxBegin());
		return fileApi
			.uploadFile(file, zone)
			.then(res => {
				dispatch(onAjaxSuccess());
				dispatch(uploadFileSuccess(res.data));
			})
			.catch(error => {
				dispatch(onAjaxError());
				dispatch(uploadFileError());
			});
	};
};
