import React, { Component } from "react";
import Routes from "./components/Routes";
import { ThemeProvider } from "styled-components";

import Theme from "./themes";

class Main extends Component {
	render() {
		return (
			<div>
				<ThemeProvider theme={Theme}>
					<Routes />
				</ThemeProvider>
			</div>
		);
	}
}

export default Main;
